# GIBZ App - Mobile Client

This repository contains the codebase for the mobile application intended to run on Android and iOS.

To run the application in development mode, it is recommended to follow the corresponding chapters of Flutters [Get started guid](https://docs.flutter.dev/get-started/install):

- [Install](https://docs.flutter.dev/get-started/install)
- [Setup an editor](https://docs.flutter.dev/get-started/editor): It's recommended to use _VS Code_ as editor for development.
- [Run the app](https://docs.flutter.dev/get-started/test-drive): Skip the paragraph _Create a sample flutter app_ and use this project instead.

## Run the app on Android or iOS

For the app to be run you need to consider some aspects, depending on the platform you'd like the app to run.

### iOS

You can only run the **iOS** version of the app (either on a simulator on a physical iOS device) if you have a mac. Otherwise you have to run the android version.

### Android

To run the **android** version of the app, you need to download and install _Android Studio_. Within Android Studio, use the _SDK Manager_ to download and install at least
  - the _SDK Platforms_ for SDK Levels `30`, `33` and `35` along with
  - the _Android SDK Command-line Tools_.

After that, you may either use a physical device or an android emulator running on your computer.

## Development FAQ 

Q: **On android emulator, how can I access a service/application running on my development machine (localhost)?**  
A: Since 127.0.0.1 refers to the localhost of the emulator itself, you can use the dedicated ip address `10.0.2.2.` from the app running on the emulator to access the emulators host machine.

Q: **On a physical android device, how can I access a service/application running on my development machine (localhost)?**  
A: Referring to [this post on Stack Overflow](https://stackoverflow.com/a/4779992), probably the easiest way is to use a hotspot to unite your development machine and the physical android device in the same network.

Q: **On android (emulator or physical device), when I try to login in the GIBZ App, after successful authentication, I won't get properly redirected back to the app. Why?**  
A: This seems to be an issue with the _development build_ of the app. You may fix this in the phones/emulators _Settings_. Go to the settings of _GIBZ App_ and make sure in section _Open by default_ the url `gibz.ch` is added and checked.

Q: **Why is Flutter that hard to get into?**  
A: It's actually not 😉 Stay curious and motivated to broaden your experience with new languages, frameworks and tools 👍