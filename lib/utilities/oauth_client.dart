import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/authentication/authenticated_user.dart';
import 'package:oauth2/oauth2.dart';
import 'package:url_launcher/url_launcher.dart';

class OAuthClient {
  static final OAuthClient _oAuthClient = OAuthClient._internal();

  factory OAuthClient() {
    return _oAuthClient;
  }

  OAuthClient._internal();

  static const clientIdentifier = 'gibz-mobile-app';
  static const secureStorageCredentialsKey = 'oauthCredentials';

  static const baseUrl = 'https://auth.gibz-app.ch/connect';
  final authorizationEndpoint = Uri.parse('$baseUrl/authorize?prompt=login');
  final tokenEndpoint = Uri.parse('$baseUrl/token');
  final redirectUrl = Uri.parse('https://gibz-app.ch/client-auth');

  AuthorizationCodeGrant? _grant;
  Client? _client;

  Future<void> startAuthentication() async {
    final authorizationUri = _getAuthorizationUri();
    if (await canLaunchUrl(authorizationUri)) {
      await launchUrl(authorizationUri, mode: LaunchMode.inAppBrowserView);
    }
  }

  Future<void> logout() async {
    final idToken = await getIdentityToken();

    if (idToken != null) {
      final logoutUri = Uri.parse('$baseUrl/endsession?id_token_hint=$idToken');
      if (_client == null) {
        final credentials = await getCredentials();
        if (credentials != null) {
          _client = Client(credentials, identifier: clientIdentifier);
        }
      }
      final response = await _client?.get(logoutUri);

      if (response?.statusCode == 200) {
        _client?.close();
        _grant?.close();
        _clearCredentials();
      }
    }
  }

  Future<AuthenticatedUser?> handleAuthorizationResponse(
      Map<String, String> queryParameters) async {
    try {
      _client = await _grant?.handleAuthorizationResponse(queryParameters);
      if (_client != null) {
        await _persistCredentialsToSecureStorage(_client!.credentials);

        // DioClient is implemented as singleton.
        // Therefore updateToken might be called on an arbitrary instance.
        DioClient().updateToken(_client!.credentials.accessToken);

        return await getAuthenticatedUserFromCredentials(
          credentials: _client!.credentials,
        );
      }
    } catch (e) {
      print(e);
    } finally {
      closeInAppWebView();
    }

    return null;
  }

  Future<AuthenticatedUser?> getAuthenticatedUserFromCredentials(
      {Credentials? credentials}) async {
    credentials ??= await _loadCredentialsFromLocalStorage();

    if (credentials == null) {
      return null;
    }

    credentials = await refreshCredentials(credentials);

    if (credentials == null) {
      return null;
    }

    if (credentials.idToken != null && credentials.idToken!.isNotEmpty) {
      return AuthenticatedUser.fromIdToken(credentials.idToken!);
    }

    return null;
  }

  Future<Credentials?> getCredentials() async {
    if (_client != null) {
      return _client!.credentials;
    }

    return await _loadCredentialsFromLocalStorage();
  }

  Future<String?> getAccessToken() async {
    return (await getCredentials())?.accessToken;
  }

  Future<String?> getIdentityToken() async {
    return (await getCredentials())?.idToken;
  }

  Future<Credentials?> refreshCredentials(Credentials credentials) async {
    if (credentials.canRefresh) {
      try {
        final refreshedCredentials =
            await credentials.refresh(identifier: clientIdentifier);
        await _persistCredentialsToSecureStorage(refreshedCredentials);
        return refreshedCredentials;
      } catch (e) {
        // Refreshing failed...
        // TODO: Log failure...
        print(e);
      }
    }

    return null;
  }

  Uri _getAuthorizationUri() {
    _grant = AuthorizationCodeGrant(
      clientIdentifier,
      authorizationEndpoint,
      tokenEndpoint,
    );

    // A URL on the authorization server (authorizationEndpoint with some additional
    // query parameters). Scopes and state can optionally be passed into this method.
    return _grant!.getAuthorizationUrl(
      redirectUrl,
      scopes: ['openid', 'profile', 'offline_access'],
    );
  }

  Future<void> _persistCredentialsToSecureStorage(
      Credentials credentials) async {
    const secureStorage = FlutterSecureStorage();

    await secureStorage.write(
      key: secureStorageCredentialsKey,
      value: credentials.toJson(),
    );
  }

  Future<Credentials?> _loadCredentialsFromLocalStorage() async {
    const secureStorage = FlutterSecureStorage();

    final credentialsString =
        await secureStorage.read(key: secureStorageCredentialsKey);

    if (credentialsString != null) {
      return Credentials.fromJson(credentialsString);
    }

    return null;
  }

  Future<void> _clearCredentials() async {
    const secureStorage = FlutterSecureStorage();
    await secureStorage.delete(key: secureStorageCredentialsKey);
  }
}
