// As seen on: https://stackoverflow.com/a/59803518

extension RoundDurationExtension on Duration {
  /// Rounds the time of this duration up to the nearest multiple of [to].
  Duration ceil(Duration to) {
    int us = inMicroseconds;
    int toUs = to.inMicroseconds.abs(); // Ignore if [to] is negative.
    int mod = us % toUs;
    if (mod != 0) {
      return Duration(microseconds: us - mod + toUs);
    }
    return this;
  }
}
