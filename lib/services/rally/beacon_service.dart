import 'package:dchs_flutter_beacon/dchs_flutter_beacon.dart';
import 'package:gibz_mobileapp/models/rally/location_marker.dart';
import 'package:gibz_mobileapp/models/rally/stage.dart';

class BeaconService {
  late Stream<RangingResult> rangingStream;

  Future<bool> startRanging(Stage stage) async {
    List<Region> regions = _getIBeaconRegions(stage);

    if (regions.isEmpty) {
      return false;
    }

    final isInitialized = await flutterBeacon.initializeScanning;
    if (!isInitialized) {
      return false;
    }

    rangingStream = flutterBeacon.ranging(regions).where((rangingResult) =>
        rangingResult.beacons.isNotEmpty &&
        rangingResult.beacons.any((beacon) => beacon.accuracy >= 0));

    return true;
  }

  List<Region> _getIBeaconRegions(Stage stage) {
    List<IBeaconLocationMarker> iBeaconLocationMarkers = [];

    for (var location in stage.locations) {
      iBeaconLocationMarkers.addAll(
          location.locationMarkers.whereType<IBeaconLocationMarker>().toList());
    }

    if (iBeaconLocationMarkers.isEmpty) {
      return <Region>[];
    }

    return iBeaconLocationMarkers
        .map((marker) => Region(
              identifier: marker.id,
              proximityUUID: marker.uuid,
              major: marker.major,
              minor: marker.minor,
            ))
        .toList();
  }
}
