import 'package:dio/dio.dart';
import 'package:gibz_mobileapp/utilities/oauth_client.dart';

class DioClient {
  static final DioClient _dioClient = DioClient._internal();

  final Dio _dio;

  factory DioClient() {
    return _dioClient;
  }

  DioClient._internal()
      : _dio = Dio(BaseOptions(
          baseUrl: 'https://gibz-app.ch',
          // baseUrl: 'http://localhost:8008',
          connectTimeout: const Duration(seconds: 10),
          receiveTimeout: const Duration(seconds: 30),
          responseType: ResponseType.plain,
        )) {
    // Retrieve and apply access token
    OAuthClient oAuthClient = OAuthClient();
    oAuthClient.getAccessToken().then((accessToken) {
      if (accessToken != null && accessToken.isNotEmpty) {
        updateToken(accessToken);
      }
    });

    // Add 401 interceptor
    _dio.interceptors.add(InterceptorsWrapper(
      onError: (DioException e, handler) async {
        if (e.response?.statusCode == 401) {
          final accessToken = await _getFreshAccessToken();
          if (accessToken != null) {
            e.requestOptions.headers['Authorization'] = 'Bearer $accessToken';
            return handler.resolve(await _dio.fetch(e.requestOptions));
          }
        }
        return handler.next(e);
      },
    ));
  }

  Future<String?> _getFreshAccessToken() async {
    OAuthClient oAuthClient = OAuthClient();
    final credentials = await oAuthClient.getCredentials();

    if (credentials != null) {
      final refreshedCredentials =
          await oAuthClient.refreshCredentials(credentials);
      if (refreshedCredentials != null) {
        return refreshedCredentials.accessToken;
      }
    }

    return null;
  }

  void updateToken(String token) {
    _dio.options.headers['Authorization'] = 'Bearer $token';
  }

  Future<Response<T>> get<T>(
    String path, {
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    void Function(int, int)? onReceiveProgress,
  }) {
    return _dio.get(
      path,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onReceiveProgress: onReceiveProgress,
    );
  }

  Future<Response<T>> post<T>(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    void Function(int, int)? onSendProgress,
    void Function(int, int)? onReceiveProgress,
  }) {
    return _dio.post(
      path,
      data: data,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );
  }

  Future<Response<T>> put<T>(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
    void Function(int, int)? onSendProgress,
    void Function(int, int)? onReceiveProgress,
  }) {
    return _dio.put(
      path,
      data: data,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );
  }

  Future<Response<T>> delete<T>(
    String path, {
    dynamic data,
    Map<String, dynamic>? queryParameters,
    Options? options,
    CancelToken? cancelToken,
  }) {
    return _dio.delete(
      path,
      data: data,
      queryParameters: queryParameters,
      options: options,
      cancelToken: cancelToken,
    );
  }
}
