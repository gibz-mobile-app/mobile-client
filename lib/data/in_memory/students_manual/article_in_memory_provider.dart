import 'package:collection/collection.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';

class ArticleInMemoryProvider {
  static const cachingTime = Duration(days: 1);
  DateTime? lastUpdate;
  List<Article> articles = [];
  Map<String, DateTime> updatedArticleIds = {};

  void addData(List<Article> articles) {
    this.articles = articles;
    lastUpdate = DateTime.now();
  }

  void replaceSingleArticle(Article newArticle) {
    var index = articles.indexWhere((article) => article.id == newArticle.id);
    if (index >= 0) {
      articles[index] = newArticle;
      if (!updatedArticleIds.keys.contains(newArticle.id)) {
        updatedArticleIds[newArticle.id] = DateTime.now();
      }
    }
  }

  List<Article>? getArticles() {
    if (lastUpdate == null || !_isValid(lastUpdate!)) {
      return null;
    }
    return articles;
  }

  Article? getArticle(String articleId) {
    if (!updatedArticleIds.keys.contains(articleId) ||
        !_isValid(updatedArticleIds[articleId]!)) {
      return null;
    }
    return articles.firstWhereOrNull((article) => article.id == articleId);
  }

  bool _isValid(DateTime moment) {
    return DateTime.now().difference(moment) <= cachingTime;
  }
}
