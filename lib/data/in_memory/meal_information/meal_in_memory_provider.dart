import 'package:gibz_mobileapp/models/meal_information.dart';
import 'package:collection/collection.dart';
import 'package:gibz_mobileapp/utilities/datetime_extension_issamedate.dart';

class MealInMemoryProvider {
  DateTime? lastUpdate;
  List<Day> menuDays = [];

  void addData(List<Day> menuDays) {
    this.menuDays = menuDays;
  }

  List<Day>? getMealsForWholeWeek() {
    if (lastUpdate == null || !lastUpdate!.isSameDate(DateTime.now())) {
      return null;
    }
    return menuDays;
  }

  Day? getMenuDayForWeekDay(int weekDay) {
    return menuDays
        .firstWhereOrNull((menuDay) => menuDay.date.weekday == weekDay);
  }

  Meal? getMeal(String mealId) {
    return menuDays
        .expand((days) => days.menus)
        .firstWhere((meal) => meal.id == mealId);
  }
}
