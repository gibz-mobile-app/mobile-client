import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';

import '../../../models/fitness_check/attempts_response.dart';

class FitnessCheckInMemoryProvider {
  static const cachingTime = Duration(days: 1);
  DateTime? lastUpdate;
  List<AttemptsResponse<Attempt>> attemptResponses = [];

  void setAttemptsResponse<T extends Attempt>(
      AttemptsResponse attemptsResponse) {
    final existing =
        attemptResponses.whereType<AttemptsResponse<T>>().firstOrNull;
    attemptResponses.remove(existing);
    attemptResponses.add(attemptsResponse);
  }

  AttemptsResponse<T>? getAttemptsResponse<T extends Attempt>() {
    return attemptResponses.whereType<AttemptsResponse<T>>().firstOrNull;
  }
}
