import 'package:gibz_mobileapp/data/dio_client.dart';

class ImprintApiDataProvider {
  final DioClient _dioClient;

  ImprintApiDataProvider(this._dioClient);

  Future<String?> getImprint() async {
    try {
      final response = await _dioClient.get(
          'https://gitlab.com/gibz-mobile-app/mobile-client/-/raw/main/Imprint.md');
      return response.data.toString();
    } catch (e) {
      // TODO: Handle exception
      return null;
    }
  }
}
