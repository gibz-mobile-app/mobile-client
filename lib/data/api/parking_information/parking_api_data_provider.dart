import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/parking_information/parking_information.dart';

class ParkingApiDataProvider {
  final DioClient _dioClient;

  ParkingApiDataProvider(this._dioClient);

  Future<CarPark?> getCurrentAvailability({int plsId = 61}) async {
    try {
      final response = await _dioClient.get('/api/parking/v1/availability/61');
      return CarParkMapper.fromJson(response.data);
    } catch (e) {
      // TODO: Handle exceptions
      // ignore: avoid_print
      return null;
    }
  }
}
