import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';

class ArticleApiDataProvider {
  final DioClient _dioClient;

  ArticleApiDataProvider(this._dioClient);

  Future<List<Article>?> getArticles() async {
    try {
      final response = await _dioClient.get('/api/studentsmanual/v1/article');
      return MapperContainer.globals.fromJson<List<Article>>(response.data);
    } catch (e) {
      // TODO: Handle exception
      return null;
    }
  }

  Future<Article?> getArticle(String id) async {
    try {
      final response =
          await _dioClient.get('/api/studentsmanual/v1/article/$id');
      return Article.fromJson(response.data);
    } catch (e) {
      // TODO: Handle exception
      return null;
    }
  }
}
