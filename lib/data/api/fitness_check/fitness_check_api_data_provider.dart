import 'dart:async';

import 'package:dart_mappable/dart_mappable.dart';
import 'package:dio/dio.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempts_response.dart';

import '../../../models/fitness_check/attempt_creation/attempt_creation_request.dart';
import '../../../models/fitness_check/fitness_check.dart';

class FitnessCheckApiDataProvider {
  final DioClient _dioClient;

  final String basePath = '/bffm/fitness-check/v1/attempt';
  // final String basePath = '/v1/attempt';

  FitnessCheckApiDataProvider(this._dioClient);

  String? _getPath<T extends Attempt>() {
    if (T == CoreStrengthAttempt) {
      return 'coreStrength';
    } else if (T == MedicineBallPushAttempt) {
      return 'medicineBallPush';
    } else if (T == OneLegStandAttempt) {
      return 'oneLegStand';
    } else if (T == ShuttleRunAttempt) {
      return 'shuttleRun';
    } else if (T == StandingLongJumpAttempt) {
      return 'standingLongJump';
    } else if (T == TwelveMinutesRunAttempt) {
      return 'twelveMinutesRun';
    }

    return null;
  }

  Future<AttemptsResponse<T>?> getAttempts<T extends Attempt>() async {
    String? disciplinePathSegment = _getPath<T>();

    if (disciplinePathSegment != null) {
      try {
        final path = '$basePath/$disciplinePathSegment';
        final response = await _dioClient.get(path);
        return MapperContainer.globals
            .fromJson<AttemptsResponse<T>>(response.data);
      } on DioException catch (dioException) {
        // TODO: Handle exception
        print(dioException);
      } on Exception catch (exception) {
        print(exception);
      }
    }
    return null;
  }

  Future<T?> submitAttempt<R extends AttemptCreationRequest, T extends Attempt>(
      R attemptCreationRequest) async {
    final disciplinePathSegment = _getPath<T>();

    try {
      final response = await _dioClient.post(
        '$basePath/$disciplinePathSegment',
        data: attemptCreationRequest.toJson(),
        options: Options(contentType: Headers.jsonContentType),
      );
      return MapperContainer.globals.fromJson<T>(response.data);
    } on DioException catch (dioException) {
      if (dioException.response?.statusCode == 400) {
        throw AttemptCreationFailedException(dioException.response!.data);
      }
    }

    return null;
  }

  Future<void> deleteAttempt<T extends Attempt>(String attemptId) async {
    final disciplinePathSegment = _getPath<T>();

    try {
      await _dioClient.delete(
        '$basePath/$disciplinePathSegment/$attemptId',
        options: Options(contentType: Headers.jsonContentType),
      );
    } on DioException catch (dioException) {
      print(dioException);
    }
  }
}

class AttemptCreationFailedException implements Exception {
  final String message;

  AttemptCreationFailedException(this.message);
}

class AttemptDeletionFailedException implements Exception {
  final String message;

  AttemptDeletionFailedException(this.message);
}
