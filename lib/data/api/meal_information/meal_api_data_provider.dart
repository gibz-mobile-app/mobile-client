import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';

class MealApiDataProvider {
  final DioClient _dioClient;

  MealApiDataProvider(this._dioClient);

  Future<List<Day>?> getMealsForWholeWeek() async {
    try {
      final response = await _dioClient.get('/bffm/meal-information/v2');
      final menus = MapperContainer.globals.fromJson<List<Day>>(response.data);
      menus.sort((a, b) => a.date.compareTo(b.date));
      return menus;
    } catch (e) {
      // TODO: Handle exception
      return null;
    }
  }
}
