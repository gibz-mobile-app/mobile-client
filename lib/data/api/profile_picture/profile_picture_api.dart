import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/profile_picture/profile_picture_submission_error.dart';
import 'package:gibz_mobileapp/models/profile_picture/successful_token_verification_result.dart';
import 'package:http_parser/http_parser.dart';

import '../../../models/profile_picture/failed_token_verification_result.dart';
import '../../../models/profile_picture/token_verification_result.dart';

class ProfilePictureApi {
  final DioClient _dioClient;

  ProfilePictureApi(this._dioClient);

  Future<TokenVerificationResult?> verifyToken(String token) async {
    try {
      final response = await _dioClient.get(
        // 'http://10.32.25.31:8002/v1/token',
        // 'http://172.20.10.3:8002/v1/token',
        // 'http://10.10.10.10:8002/v1/token',
        '/bffm/profile-picture/v1/token',
        queryParameters: {'verify': token},
      );

      return SuccessfulTokenVerificationResult.fromJson(response.data);
    } on DioException catch (dioException) {
      if (dioException.response?.statusCode == HttpStatus.notFound) {
        return null;
      }
      if (dioException.response?.data != null) {
        return FailedTokenVerificationResultMapper.fromJson(
            dioException.response!.data);
      }
    } catch (e) {
      return null;
    }
    return null;
  }

  Future<ProfilePictureSubmissionError?> submitPicture(
      String token, Uint8List image) async {
    try {
      final picture = MultipartFile.fromBytes(
        image,
        filename: 'profilePicture.jpeg',
        contentType: MediaType('image', 'jpeg'),
      );

      final formData = FormData.fromMap({
        'profilePicture': picture,
        'token': token,
      });

      await _dioClient.post(
        // 'http://10.10.10.10:8001/v1/profilePicture',
        '/bffm/profile-picture/v1/profilePicture',
        data: formData,
      );

      return null;

      // return response.statusCode != null &&
      //     response.statusCode! >= 200 &&
      //     response.statusCode! <= 299;
    } on DioException catch (dioException) {
      if (dioException.response?.statusCode == HttpStatus.badRequest) {
        final data = dioException.response!.data;
        return ProfilePictureSubmissionError.fromJson(data);
      }
    } catch (e) {
      return const ProfilePictureSubmissionError(
        type: "UNKNOWN",
        errorCode: 99,
        message: "...",
        details: null,
      );
    }

    return null;
  }
}
