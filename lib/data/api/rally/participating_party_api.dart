import 'dart:convert';

import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/rally/dto/participating_party_update_request_dto.dart';
import 'package:gibz_mobileapp/models/rally/stage.dart';
import 'package:gibz_mobileapp/models/rally/successful_rally_join_response.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';

class ParticipatingPartyApi {
  final DioClient _dioClient;
  final ProgressIndicatorCubit _progressIndicatorCubit;

  ParticipatingPartyApi(this._dioClient, this._progressIndicatorCubit);

  Future<SuccessfulRallyJoinResponse?> createParticipatingParty(
      String joiningCode) async {
    final payload = jsonEncode({"joiningCode": joiningCode});
    try {
      final response = await _dioClient.post(
        // 'http://192.168.1.11:8084/v1/party',
        // 'http://172.20.10.3:8084/v1/party',
        // 'http://10.10.10.10:8084/v1/party',
        '/api/rally/v1/party',
        data: payload,
      );

      if ((response.statusCode ?? 999) > 299) {
        _progressIndicatorCubit.finish(LoadingAspect.joiningRally);
        return null;
      }

      _progressIndicatorCubit.finish(LoadingAspect.joiningRally);
      return SuccessfulRallyJoinResponseMapper.fromJson(response.data);
    } catch (error) {
      _progressIndicatorCubit.finish(LoadingAspect.joiningRally);
      return null;
    } finally {
      _progressIndicatorCubit.finish(LoadingAspect.joiningRally);
    }
  }

  Future<Stage?> updateParticipatingParty({
    required String id,
    required String title,
    required List<String> names,
  }) async {
    final updateRequest = ParticipatingPartyUpdateRequestDto(
      id: id,
      title: title,
      names: names,
    );
    final payload = updateRequest.toJson();
    final response = await _dioClient.put(
      // 'http://192.168.1.11:8084/v1/party/$id',
      // 'http://172.20.10.3:8084/v1/party/$id',
      // 'http://10.10.10.10:8084/v1/party/$id',
      '/api/rally/v1/party/$id',
      data: payload,
    );

    if (response.statusCode == 204) {
      return null;
    }

    return StageMapper.fromJson(response.data);
  }
}
