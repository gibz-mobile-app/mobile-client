import 'dart:convert';

import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/rally/participating_party_appearance.dart';
import 'package:gibz_mobileapp/models/rally/stage.dart';

class RallyStageApi {
  final DioClient _dioClient;

  RallyStageApi(this._dioClient);

  Future<Stage?> getNext(
    String rallyId,
    String stageId,
    String participatingPartyId,
  ) async {
    final response = await _dioClient.get(
      // 'http://192.168.1.11:8084/v1/rally/$rallyId/stage/$stageId/next/$participatingPartyId',
      // 'http://172.20.10.3:8084/v1/rally/$rallyId/stage/$stageId/next/$participatingPartyId',
      // 'http://10.10.10.10:8084/v1/rally/$rallyId/stage/$stageId/next/$participatingPartyId',
      '/api/rally/v1/rally/$rallyId/stage/$stageId/next/$participatingPartyId',
    );
    if (response.statusCode == 204) {
      return null;
    }

    return StageMapper.fromJson(response.data);
  }

  Future<ParticipatingPartyAppearance> addParticipatingPartyAppearance({
    required String rallyId,
    required String stageId,
    required String participatingPartyId,
    required String locationMarkerId,
  }) async {
    final response = await _dioClient.post(
      // 'http://192.168.1.11:8084/v1/rally/$rallyId/stage/$stageId/appearance',
      // 'http://172.20.10.3:8084/v1/rally/$rallyId/stage/$stageId/appearance',
      // 'http://10.10.10.10:8084/v1/rally/$rallyId/stage/$stageId/appearance',
      '/api/rally/v1/rally/$rallyId/stage/$stageId/appearance',
      data: jsonEncode({
        "participatingPartyId": participatingPartyId,
        "locationMarkerId": locationMarkerId,
      }),
    );

    return ParticipatingPartyAppearanceMapper.fromJson(response.data);
  }
}
