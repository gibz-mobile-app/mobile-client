import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/models/rally/dto/stage_activity_result_creation_request.dart';
import 'package:gibz_mobileapp/models/rally/stage_activity_result.dart';

class StageActivityResultApi {
  final DioClient _dioClient;

  StageActivityResultApi(this._dioClient);

  Future<StageActivityResult?> submitStageActivityResult(
    String rallyId,
    String stageId,
    StageActivityResultCreationRequestDto payload,
  ) async {
    try {
      final response = await _dioClient.post(
        // 'http://192.168.1.11:8084/v1/rally/$rallyId/stage/$stageId/stageActivityResult',
        // 'http://172.20.10.3:8084/v1/rally/$rallyId/stage/$stageId/stageActivityResult',
        // 'http://10.10.10.10:8084/v1/rally/$rallyId/stage/$stageId/stageActivityResult',
        '/api/rally/v1/rally/$rallyId/stage/$stageId/stageActivityResult',
        data: payload.toJson(),
      );

      return StageActivityResultMapper.fromJson(response.data);
    } catch (error) {
      return null;
    }
  }
}
