import 'package:gibz_mobileapp/data/api/imprint/imprint_api_data_provider.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';

class ImprintRepository {
  final ImprintApiDataProvider _apiDataProvider;

  ImprintRepository({
    ImprintApiDataProvider? apiDataProvider,
  }) : _apiDataProvider =
            apiDataProvider ?? ImprintApiDataProvider(DioClient());

  Future<String?> getImprint() async {
    return await _apiDataProvider.getImprint();
  }
}
