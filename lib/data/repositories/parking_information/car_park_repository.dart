import 'package:gibz_mobileapp/data/api/parking_information/parking_api_data_provider.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/data/in_memory/parking_information.dart/parking_in_memory_provider.dart';
import 'package:gibz_mobileapp/models/parking_information/parking_information.dart';

class CarParkRepository {
  final ParkingApiDataProvider _apiDataProvider;
  final ParkingInMemoryProvider _inMemoryDataProvider;

  CarParkRepository(
      {ParkingApiDataProvider? apiDataProvider,
      ParkingInMemoryProvider? inMemoryDataProvider})
      : _apiDataProvider =
            apiDataProvider ?? ParkingApiDataProvider(DioClient()),
        _inMemoryDataProvider =
            inMemoryDataProvider ?? ParkingInMemoryProvider();

  Future<CarPark?> getCurrentAvailability() async {
    final inMemoryData = _inMemoryDataProvider.getCurrentAvailability();
    if (inMemoryData != null) {
      return inMemoryData;
    }

    final carPark = await _apiDataProvider.getCurrentAvailability();
    if (carPark != null) {
      _inMemoryDataProvider.addData(carPark);
    }

    return carPark;
  }
}
