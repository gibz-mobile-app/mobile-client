import 'package:gibz_mobileapp/data/api/fitness_check/fitness_check_api_data_provider.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/data/in_memory/fitness_check/fitness_check_in_memory_provider.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt_creation/attempt_creation_request.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempts_response.dart';

import '../../../models/fitness_check/fitness_check.dart';

class FitnessCheckRepository {
  final FitnessCheckApiDataProvider _apiDataProvider;
  final FitnessCheckInMemoryProvider _inMemoryDataProvider;

  FitnessCheckRepository({
    FitnessCheckApiDataProvider? apiDataProvider,
    FitnessCheckInMemoryProvider? inMemoryDataProvider,
  })  : _apiDataProvider =
            apiDataProvider ?? FitnessCheckApiDataProvider(DioClient()),
        _inMemoryDataProvider =
            inMemoryDataProvider ?? FitnessCheckInMemoryProvider();

  Future<AttemptsResponse<T>?> getAttempts<T extends Attempt>(
      {bool forceApiRequest = false}) async {
    if (!forceApiRequest) {
      final attemptsResponse = _inMemoryDataProvider.getAttemptsResponse<T>();
      if (attemptsResponse != null) {
        return attemptsResponse;
      }
    }

    final apiResponse = await _apiDataProvider.getAttempts<T>();
    if (apiResponse != null) {
      _inMemoryDataProvider.setAttemptsResponse(apiResponse);
    }

    return apiResponse;
  }

  Future<void>
      submitAttempt<R extends AttemptCreationRequest, T extends Attempt>(
          R attemptCreationRequest) async {
    await _apiDataProvider.submitAttempt<R, T>(attemptCreationRequest);
  }

  Future<void> deleteAttempt<T extends Attempt>(String attemptId) async {
    await _apiDataProvider.deleteAttempt<T>(attemptId);
  }
}
