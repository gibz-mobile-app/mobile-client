import 'package:gibz_mobileapp/data/api/students_manual/article_api_data_provider.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/data/in_memory/students_manual/article_in_memory_provider.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';

class ArticleRepository {
  final ArticleApiDataProvider _apiDataProvider;
  final ArticleInMemoryProvider _inMemoryDataProvider;

  ArticleRepository({
    ArticleApiDataProvider? apiDataProvider,
    ArticleInMemoryProvider? inMemoryDataProvider,
  })  : _apiDataProvider =
            apiDataProvider ?? ArticleApiDataProvider(DioClient()),
        _inMemoryDataProvider =
            inMemoryDataProvider ?? ArticleInMemoryProvider();

  Future<List<Article>?> getArticles() async {
    final inMemoryData = _inMemoryDataProvider.getArticles();
    if (inMemoryData != null && inMemoryData.isNotEmpty) {
      return inMemoryData;
    }

    final articles = await _apiDataProvider.getArticles();
    if (articles != null) {
      _inMemoryDataProvider.addData(articles);
    }

    return articles;
  }

  Future<Article?> getArticle(String articleId) async {
    var inMemoryArticle = _inMemoryDataProvider.getArticle(articleId);

    if (inMemoryArticle is Article) {
      return inMemoryArticle;
    }

    var articleFromApi = await _apiDataProvider.getArticle(articleId);
    if (articleFromApi is Article) {
      _inMemoryDataProvider.replaceSingleArticle(articleFromApi);
    }
    return articleFromApi;
  }
}
