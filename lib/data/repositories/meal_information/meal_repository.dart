import 'package:gibz_mobileapp/data/api/meal_information/meal_api_data_provider.dart';
import 'package:gibz_mobileapp/data/dio_client.dart';
import 'package:gibz_mobileapp/data/in_memory/meal_information/meal_in_memory_provider.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';

class MealRepository {
  final MealApiDataProvider _apiDataProvider;
  final MealInMemoryProvider _inMemoryDataProvider;

  MealRepository({
    MealApiDataProvider? apiDataProvider,
    MealInMemoryProvider? inMemoryDataProvider,
  })  : _apiDataProvider = apiDataProvider ?? MealApiDataProvider(DioClient()),
        _inMemoryDataProvider = inMemoryDataProvider ?? MealInMemoryProvider();

  Future<List<Day>?> getMealsForWholeWeek() async {
    final inMemoryData = _inMemoryDataProvider.getMealsForWholeWeek();
    if (inMemoryData != null && inMemoryData.isNotEmpty) {
      return inMemoryData;
    }

    final menuDays = await _apiDataProvider.getMealsForWholeWeek();
    if (menuDays != null) {
      _inMemoryDataProvider.addData(menuDays);
    }

    return menuDays;
  }

  Future<Day?> getMenuDayForWeekDay(int weekDay) {
    return Future.value(_inMemoryDataProvider.getMenuDayForWeekDay(weekDay));
  }

  Future<Meal?> getMeal(String mealId) {
    return Future.value(_inMemoryDataProvider.getMeal(mealId));
  }
}
