import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';

part 'attempt.mapper.dart';

@MappableClass(discriminatorKey: "discipline")
abstract class Attempt extends Equatable with AttemptMappable {
  final String id;
  final int points;
  final int attemptNumber;
  @MappableField(hook: DateTimeUtcHook())
  final DateTime momentUtc;
  final String userId;
  final String gender;
  final String cohortId;

  const Attempt({
    required this.id,
    required this.points,
    required this.attemptNumber,
    required this.momentUtc,
    required this.userId,
    required this.gender,
    required this.cohortId,
  });

  static const fromMap = AttemptMapper.fromMap;
  static const fromJson = AttemptMapper.fromJson;

  String get displayValue;

  @override
  List<Object?> get props => [id];
}

class DateTimeUtcHook extends MappingHook {
  const DateTimeUtcHook();

  @override
  Object? beforeDecode(Object? value) {
    // Parse the time string as UTC
    if (value is String) {
      final utc = DateFormat("yyyy-MM-dd'T'HH:mm:ss").parseUtc(value);
      return utc.toLocal();
    }

    return value;
  }
}
