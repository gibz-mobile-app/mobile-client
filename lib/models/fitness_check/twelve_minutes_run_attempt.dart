import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';

part 'twelve_minutes_run_attempt.mapper.dart';

@MappableClass(discriminatorValue: "twelveMinutesRun")
class TwelveMinutesRunAttempt extends Attempt
    with TwelveMinutesRunAttemptMappable {
  final double resultInRounds;

  const TwelveMinutesRunAttempt({
    required super.id,
    required super.points,
    required super.attemptNumber,
    required super.momentUtc,
    required super.userId,
    required super.gender,
    required super.cohortId,
    required this.resultInRounds,
  });

  static const fromMap = TwelveMinutesRunAttemptMapper.fromMap;
  static const fromJson = TwelveMinutesRunAttemptMapper.fromJson;

  @override
  String get displayValue => '$resultInRounds Runden';
}
