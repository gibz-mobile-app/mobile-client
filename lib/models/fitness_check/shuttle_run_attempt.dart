import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';

part 'shuttle_run_attempt.mapper.dart';

@MappableClass(discriminatorValue: "shuttleRun")
class ShuttleRunAttempt extends Attempt with ShuttleRunAttemptMappable {
  final int resultInMilliseconds;

  const ShuttleRunAttempt({
    required super.id,
    required super.points,
    required super.attemptNumber,
    required super.momentUtc,
    required super.userId,
    required super.gender,
    required super.cohortId,
    required this.resultInMilliseconds,
  });

  static const fromMap = ShuttleRunAttemptMapper.fromMap;
  static const fromJson = ShuttleRunAttemptMapper.fromJson;

  double get resultInSeconds => resultInMilliseconds / 1000;

  @override
  String get displayValue => '${resultInSeconds.toStringAsFixed(2)} Sekunden';
}
