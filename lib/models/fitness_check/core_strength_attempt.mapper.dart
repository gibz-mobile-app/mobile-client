// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'core_strength_attempt.dart';

class CoreStrengthAttemptMapper
    extends SubClassMapperBase<CoreStrengthAttempt> {
  CoreStrengthAttemptMapper._();

  static CoreStrengthAttemptMapper? _instance;
  static CoreStrengthAttemptMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = CoreStrengthAttemptMapper._());
      AttemptMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'CoreStrengthAttempt';

  static String _$id(CoreStrengthAttempt v) => v.id;
  static const Field<CoreStrengthAttempt, String> _f$id = Field('id', _$id);
  static int _$points(CoreStrengthAttempt v) => v.points;
  static const Field<CoreStrengthAttempt, int> _f$points =
      Field('points', _$points);
  static int _$attemptNumber(CoreStrengthAttempt v) => v.attemptNumber;
  static const Field<CoreStrengthAttempt, int> _f$attemptNumber =
      Field('attemptNumber', _$attemptNumber);
  static DateTime _$momentUtc(CoreStrengthAttempt v) => v.momentUtc;
  static const Field<CoreStrengthAttempt, DateTime> _f$momentUtc =
      Field('momentUtc', _$momentUtc, hook: DateTimeUtcHook());
  static String _$userId(CoreStrengthAttempt v) => v.userId;
  static const Field<CoreStrengthAttempt, String> _f$userId =
      Field('userId', _$userId);
  static String _$gender(CoreStrengthAttempt v) => v.gender;
  static const Field<CoreStrengthAttempt, String> _f$gender =
      Field('gender', _$gender);
  static String _$cohortId(CoreStrengthAttempt v) => v.cohortId;
  static const Field<CoreStrengthAttempt, String> _f$cohortId =
      Field('cohortId', _$cohortId);
  static int _$resultInSeconds(CoreStrengthAttempt v) => v.resultInSeconds;
  static const Field<CoreStrengthAttempt, int> _f$resultInSeconds =
      Field('resultInSeconds', _$resultInSeconds);

  @override
  final MappableFields<CoreStrengthAttempt> fields = const {
    #id: _f$id,
    #points: _f$points,
    #attemptNumber: _f$attemptNumber,
    #momentUtc: _f$momentUtc,
    #userId: _f$userId,
    #gender: _f$gender,
    #cohortId: _f$cohortId,
    #resultInSeconds: _f$resultInSeconds,
  };

  @override
  final String discriminatorKey = 'discipline';
  @override
  final dynamic discriminatorValue = "coreStrength";
  @override
  late final ClassMapperBase superMapper = AttemptMapper.ensureInitialized();

  static CoreStrengthAttempt _instantiate(DecodingData data) {
    return CoreStrengthAttempt(
        id: data.dec(_f$id),
        points: data.dec(_f$points),
        attemptNumber: data.dec(_f$attemptNumber),
        momentUtc: data.dec(_f$momentUtc),
        userId: data.dec(_f$userId),
        gender: data.dec(_f$gender),
        cohortId: data.dec(_f$cohortId),
        resultInSeconds: data.dec(_f$resultInSeconds));
  }

  @override
  final Function instantiate = _instantiate;

  static CoreStrengthAttempt fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<CoreStrengthAttempt>(map);
  }

  static CoreStrengthAttempt fromJson(String json) {
    return ensureInitialized().decodeJson<CoreStrengthAttempt>(json);
  }
}

mixin CoreStrengthAttemptMappable {
  String toJson() {
    return CoreStrengthAttemptMapper.ensureInitialized()
        .encodeJson<CoreStrengthAttempt>(this as CoreStrengthAttempt);
  }

  Map<String, dynamic> toMap() {
    return CoreStrengthAttemptMapper.ensureInitialized()
        .encodeMap<CoreStrengthAttempt>(this as CoreStrengthAttempt);
  }

  CoreStrengthAttemptCopyWith<CoreStrengthAttempt, CoreStrengthAttempt,
          CoreStrengthAttempt>
      get copyWith => _CoreStrengthAttemptCopyWithImpl(
          this as CoreStrengthAttempt, $identity, $identity);
  @override
  String toString() {
    return CoreStrengthAttemptMapper.ensureInitialized()
        .stringifyValue(this as CoreStrengthAttempt);
  }

  @override
  bool operator ==(Object other) {
    return CoreStrengthAttemptMapper.ensureInitialized()
        .equalsValue(this as CoreStrengthAttempt, other);
  }

  @override
  int get hashCode {
    return CoreStrengthAttemptMapper.ensureInitialized()
        .hashValue(this as CoreStrengthAttempt);
  }
}

extension CoreStrengthAttemptValueCopy<$R, $Out>
    on ObjectCopyWith<$R, CoreStrengthAttempt, $Out> {
  CoreStrengthAttemptCopyWith<$R, CoreStrengthAttempt, $Out>
      get $asCoreStrengthAttempt =>
          $base.as((v, t, t2) => _CoreStrengthAttemptCopyWithImpl(v, t, t2));
}

abstract class CoreStrengthAttemptCopyWith<$R, $In extends CoreStrengthAttempt,
    $Out> implements AttemptCopyWith<$R, $In, $Out> {
  @override
  $R call(
      {String? id,
      int? points,
      int? attemptNumber,
      DateTime? momentUtc,
      String? userId,
      String? gender,
      String? cohortId,
      int? resultInSeconds});
  CoreStrengthAttemptCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _CoreStrengthAttemptCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, CoreStrengthAttempt, $Out>
    implements CoreStrengthAttemptCopyWith<$R, CoreStrengthAttempt, $Out> {
  _CoreStrengthAttemptCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<CoreStrengthAttempt> $mapper =
      CoreStrengthAttemptMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          int? points,
          int? attemptNumber,
          DateTime? momentUtc,
          String? userId,
          String? gender,
          String? cohortId,
          int? resultInSeconds}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (points != null) #points: points,
        if (attemptNumber != null) #attemptNumber: attemptNumber,
        if (momentUtc != null) #momentUtc: momentUtc,
        if (userId != null) #userId: userId,
        if (gender != null) #gender: gender,
        if (cohortId != null) #cohortId: cohortId,
        if (resultInSeconds != null) #resultInSeconds: resultInSeconds
      }));
  @override
  CoreStrengthAttempt $make(CopyWithData data) => CoreStrengthAttempt(
      id: data.get(#id, or: $value.id),
      points: data.get(#points, or: $value.points),
      attemptNumber: data.get(#attemptNumber, or: $value.attemptNumber),
      momentUtc: data.get(#momentUtc, or: $value.momentUtc),
      userId: data.get(#userId, or: $value.userId),
      gender: data.get(#gender, or: $value.gender),
      cohortId: data.get(#cohortId, or: $value.cohortId),
      resultInSeconds: data.get(#resultInSeconds, or: $value.resultInSeconds));

  @override
  CoreStrengthAttemptCopyWith<$R2, CoreStrengthAttempt, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _CoreStrengthAttemptCopyWithImpl($value, $cast, t);
}
