// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'attempts_response.dart';

class AttemptsResponseMapper extends ClassMapperBase<AttemptsResponse> {
  AttemptsResponseMapper._();

  static AttemptsResponseMapper? _instance;
  static AttemptsResponseMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AttemptsResponseMapper._());
      AttemptMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'AttemptsResponse';
  @override
  Function get typeFactory =>
      <T extends Attempt>(f) => f<AttemptsResponse<T>>();

  static int _$points(AttemptsResponse v) => v.points;
  static const Field<AttemptsResponse, int> _f$points =
      Field('points', _$points);
  static String _$bestAttemptIds(AttemptsResponse v) => v.bestAttemptIds;
  static const Field<AttemptsResponse, String> _f$bestAttemptIds =
      Field('bestAttemptIds', _$bestAttemptIds);
  static int _$remainingAttempts(AttemptsResponse v) => v.remainingAttempts;
  static const Field<AttemptsResponse, int> _f$remainingAttempts =
      Field('remainingAttempts', _$remainingAttempts);
  static List<Attempt> _$attempts(AttemptsResponse v) => v.attempts;
  static dynamic _arg$attempts<T extends Attempt>(f) => f<List<T>>();
  static const Field<AttemptsResponse, List<Attempt>> _f$attempts =
      Field('attempts', _$attempts, arg: _arg$attempts);

  @override
  final MappableFields<AttemptsResponse> fields = const {
    #points: _f$points,
    #bestAttemptIds: _f$bestAttemptIds,
    #remainingAttempts: _f$remainingAttempts,
    #attempts: _f$attempts,
  };

  static AttemptsResponse<T> _instantiate<T extends Attempt>(
      DecodingData data) {
    return AttemptsResponse(
        points: data.dec(_f$points),
        bestAttemptIds: data.dec(_f$bestAttemptIds),
        remainingAttempts: data.dec(_f$remainingAttempts),
        attempts: data.dec(_f$attempts));
  }

  @override
  final Function instantiate = _instantiate;

  static AttemptsResponse<T> fromMap<T extends Attempt>(
      Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<AttemptsResponse<T>>(map);
  }

  static AttemptsResponse<T> fromJson<T extends Attempt>(String json) {
    return ensureInitialized().decodeJson<AttemptsResponse<T>>(json);
  }
}

mixin AttemptsResponseMappable<T extends Attempt> {
  String toJson() {
    return AttemptsResponseMapper.ensureInitialized()
        .encodeJson<AttemptsResponse<T>>(this as AttemptsResponse<T>);
  }

  Map<String, dynamic> toMap() {
    return AttemptsResponseMapper.ensureInitialized()
        .encodeMap<AttemptsResponse<T>>(this as AttemptsResponse<T>);
  }

  AttemptsResponseCopyWith<AttemptsResponse<T>, AttemptsResponse<T>,
          AttemptsResponse<T>, T>
      get copyWith => _AttemptsResponseCopyWithImpl(
          this as AttemptsResponse<T>, $identity, $identity);
  @override
  String toString() {
    return AttemptsResponseMapper.ensureInitialized()
        .stringifyValue(this as AttemptsResponse<T>);
  }

  @override
  bool operator ==(Object other) {
    return AttemptsResponseMapper.ensureInitialized()
        .equalsValue(this as AttemptsResponse<T>, other);
  }

  @override
  int get hashCode {
    return AttemptsResponseMapper.ensureInitialized()
        .hashValue(this as AttemptsResponse<T>);
  }
}

extension AttemptsResponseValueCopy<$R, $Out, T extends Attempt>
    on ObjectCopyWith<$R, AttemptsResponse<T>, $Out> {
  AttemptsResponseCopyWith<$R, AttemptsResponse<T>, $Out, T>
      get $asAttemptsResponse =>
          $base.as((v, t, t2) => _AttemptsResponseCopyWithImpl(v, t, t2));
}

abstract class AttemptsResponseCopyWith<$R, $In extends AttemptsResponse<T>,
    $Out, T extends Attempt> implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, T, ObjectCopyWith<$R, T, T>> get attempts;
  $R call(
      {int? points,
      String? bestAttemptIds,
      int? remainingAttempts,
      List<T>? attempts});
  AttemptsResponseCopyWith<$R2, $In, $Out2, T> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _AttemptsResponseCopyWithImpl<$R, $Out, T extends Attempt>
    extends ClassCopyWithBase<$R, AttemptsResponse<T>, $Out>
    implements AttemptsResponseCopyWith<$R, AttemptsResponse<T>, $Out, T> {
  _AttemptsResponseCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<AttemptsResponse> $mapper =
      AttemptsResponseMapper.ensureInitialized();
  @override
  ListCopyWith<$R, T, ObjectCopyWith<$R, T, T>> get attempts => ListCopyWith(
      $value.attempts,
      (v, t) => ObjectCopyWith(v, $identity, t),
      (v) => call(attempts: v));
  @override
  $R call(
          {int? points,
          String? bestAttemptIds,
          int? remainingAttempts,
          List<T>? attempts}) =>
      $apply(FieldCopyWithData({
        if (points != null) #points: points,
        if (bestAttemptIds != null) #bestAttemptIds: bestAttemptIds,
        if (remainingAttempts != null) #remainingAttempts: remainingAttempts,
        if (attempts != null) #attempts: attempts
      }));
  @override
  AttemptsResponse<T> $make(CopyWithData data) => AttemptsResponse(
      points: data.get(#points, or: $value.points),
      bestAttemptIds: data.get(#bestAttemptIds, or: $value.bestAttemptIds),
      remainingAttempts:
          data.get(#remainingAttempts, or: $value.remainingAttempts),
      attempts: data.get(#attempts, or: $value.attempts));

  @override
  AttemptsResponseCopyWith<$R2, AttemptsResponse<T>, $Out2, T>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _AttemptsResponseCopyWithImpl($value, $cast, t);
}
