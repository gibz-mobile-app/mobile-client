import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';

part 'standing_long_jump_attempt.mapper.dart';

@MappableClass(discriminatorValue: "standingLongJump")
class StandingLongJumpAttempt extends Attempt
    with StandingLongJumpAttemptMappable {
  final int resultInCentimeters;

  const StandingLongJumpAttempt({
    required super.id,
    required super.points,
    required super.attemptNumber,
    required super.momentUtc,
    required super.userId,
    required super.gender,
    required super.cohortId,
    required this.resultInCentimeters,
  });

  static const fromMap = StandingLongJumpAttemptMapper.fromMap;
  static const fromJson = StandingLongJumpAttemptMapper.fromJson;

  double get resultInMeters => resultInCentimeters / 100;

  @override
  String get displayValue => '$resultInMeters Meter';
}
