import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';

part 'attempts_response.mapper.dart';

@MappableClass()
class AttemptsResponse<T extends Attempt> with AttemptsResponseMappable<T> {
  final int points;
  final String bestAttemptIds;
  final int remainingAttempts;
  final List<T> attempts;

  AttemptsResponse({
    required this.points,
    required this.bestAttemptIds,
    required this.remainingAttempts,
    required this.attempts,
  });

  static const fromMap = AttemptsResponseMapper.fromMap;
  static const fromJson = AttemptsResponseMapper.fromJson;
}
