// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'shuttle_run_attempt.dart';

class ShuttleRunAttemptMapper extends SubClassMapperBase<ShuttleRunAttempt> {
  ShuttleRunAttemptMapper._();

  static ShuttleRunAttemptMapper? _instance;
  static ShuttleRunAttemptMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ShuttleRunAttemptMapper._());
      AttemptMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'ShuttleRunAttempt';

  static String _$id(ShuttleRunAttempt v) => v.id;
  static const Field<ShuttleRunAttempt, String> _f$id = Field('id', _$id);
  static int _$points(ShuttleRunAttempt v) => v.points;
  static const Field<ShuttleRunAttempt, int> _f$points =
      Field('points', _$points);
  static int _$attemptNumber(ShuttleRunAttempt v) => v.attemptNumber;
  static const Field<ShuttleRunAttempt, int> _f$attemptNumber =
      Field('attemptNumber', _$attemptNumber);
  static DateTime _$momentUtc(ShuttleRunAttempt v) => v.momentUtc;
  static const Field<ShuttleRunAttempt, DateTime> _f$momentUtc =
      Field('momentUtc', _$momentUtc, hook: DateTimeUtcHook());
  static String _$userId(ShuttleRunAttempt v) => v.userId;
  static const Field<ShuttleRunAttempt, String> _f$userId =
      Field('userId', _$userId);
  static String _$gender(ShuttleRunAttempt v) => v.gender;
  static const Field<ShuttleRunAttempt, String> _f$gender =
      Field('gender', _$gender);
  static String _$cohortId(ShuttleRunAttempt v) => v.cohortId;
  static const Field<ShuttleRunAttempt, String> _f$cohortId =
      Field('cohortId', _$cohortId);
  static int _$resultInMilliseconds(ShuttleRunAttempt v) =>
      v.resultInMilliseconds;
  static const Field<ShuttleRunAttempt, int> _f$resultInMilliseconds =
      Field('resultInMilliseconds', _$resultInMilliseconds);

  @override
  final MappableFields<ShuttleRunAttempt> fields = const {
    #id: _f$id,
    #points: _f$points,
    #attemptNumber: _f$attemptNumber,
    #momentUtc: _f$momentUtc,
    #userId: _f$userId,
    #gender: _f$gender,
    #cohortId: _f$cohortId,
    #resultInMilliseconds: _f$resultInMilliseconds,
  };

  @override
  final String discriminatorKey = 'discipline';
  @override
  final dynamic discriminatorValue = "shuttleRun";
  @override
  late final ClassMapperBase superMapper = AttemptMapper.ensureInitialized();

  static ShuttleRunAttempt _instantiate(DecodingData data) {
    return ShuttleRunAttempt(
        id: data.dec(_f$id),
        points: data.dec(_f$points),
        attemptNumber: data.dec(_f$attemptNumber),
        momentUtc: data.dec(_f$momentUtc),
        userId: data.dec(_f$userId),
        gender: data.dec(_f$gender),
        cohortId: data.dec(_f$cohortId),
        resultInMilliseconds: data.dec(_f$resultInMilliseconds));
  }

  @override
  final Function instantiate = _instantiate;

  static ShuttleRunAttempt fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ShuttleRunAttempt>(map);
  }

  static ShuttleRunAttempt fromJson(String json) {
    return ensureInitialized().decodeJson<ShuttleRunAttempt>(json);
  }
}

mixin ShuttleRunAttemptMappable {
  String toJson() {
    return ShuttleRunAttemptMapper.ensureInitialized()
        .encodeJson<ShuttleRunAttempt>(this as ShuttleRunAttempt);
  }

  Map<String, dynamic> toMap() {
    return ShuttleRunAttemptMapper.ensureInitialized()
        .encodeMap<ShuttleRunAttempt>(this as ShuttleRunAttempt);
  }

  ShuttleRunAttemptCopyWith<ShuttleRunAttempt, ShuttleRunAttempt,
          ShuttleRunAttempt>
      get copyWith => _ShuttleRunAttemptCopyWithImpl(
          this as ShuttleRunAttempt, $identity, $identity);
  @override
  String toString() {
    return ShuttleRunAttemptMapper.ensureInitialized()
        .stringifyValue(this as ShuttleRunAttempt);
  }

  @override
  bool operator ==(Object other) {
    return ShuttleRunAttemptMapper.ensureInitialized()
        .equalsValue(this as ShuttleRunAttempt, other);
  }

  @override
  int get hashCode {
    return ShuttleRunAttemptMapper.ensureInitialized()
        .hashValue(this as ShuttleRunAttempt);
  }
}

extension ShuttleRunAttemptValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ShuttleRunAttempt, $Out> {
  ShuttleRunAttemptCopyWith<$R, ShuttleRunAttempt, $Out>
      get $asShuttleRunAttempt =>
          $base.as((v, t, t2) => _ShuttleRunAttemptCopyWithImpl(v, t, t2));
}

abstract class ShuttleRunAttemptCopyWith<$R, $In extends ShuttleRunAttempt,
    $Out> implements AttemptCopyWith<$R, $In, $Out> {
  @override
  $R call(
      {String? id,
      int? points,
      int? attemptNumber,
      DateTime? momentUtc,
      String? userId,
      String? gender,
      String? cohortId,
      int? resultInMilliseconds});
  ShuttleRunAttemptCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _ShuttleRunAttemptCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ShuttleRunAttempt, $Out>
    implements ShuttleRunAttemptCopyWith<$R, ShuttleRunAttempt, $Out> {
  _ShuttleRunAttemptCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ShuttleRunAttempt> $mapper =
      ShuttleRunAttemptMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          int? points,
          int? attemptNumber,
          DateTime? momentUtc,
          String? userId,
          String? gender,
          String? cohortId,
          int? resultInMilliseconds}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (points != null) #points: points,
        if (attemptNumber != null) #attemptNumber: attemptNumber,
        if (momentUtc != null) #momentUtc: momentUtc,
        if (userId != null) #userId: userId,
        if (gender != null) #gender: gender,
        if (cohortId != null) #cohortId: cohortId,
        if (resultInMilliseconds != null)
          #resultInMilliseconds: resultInMilliseconds
      }));
  @override
  ShuttleRunAttempt $make(CopyWithData data) => ShuttleRunAttempt(
      id: data.get(#id, or: $value.id),
      points: data.get(#points, or: $value.points),
      attemptNumber: data.get(#attemptNumber, or: $value.attemptNumber),
      momentUtc: data.get(#momentUtc, or: $value.momentUtc),
      userId: data.get(#userId, or: $value.userId),
      gender: data.get(#gender, or: $value.gender),
      cohortId: data.get(#cohortId, or: $value.cohortId),
      resultInMilliseconds:
          data.get(#resultInMilliseconds, or: $value.resultInMilliseconds));

  @override
  ShuttleRunAttemptCopyWith<$R2, ShuttleRunAttempt, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _ShuttleRunAttemptCopyWithImpl($value, $cast, t);
}
