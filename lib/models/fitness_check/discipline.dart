enum Discipline {
  coreStrength,
  medicineBallPush,
  oneLegStand,
  shuttleRun,
  standingLongJump,
  twelveMinutesRun,
}
