import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'attempt_creation_request.mapper.dart';

@MappableClass()
abstract class AttemptCreationRequest extends Equatable
    with AttemptCreationRequestMappable {}
