// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'one_leg_stand_attempt_creation_request.dart';

class OneLegStandAttemptCreationRequestMapper
    extends ClassMapperBase<OneLegStandAttemptCreationRequest> {
  OneLegStandAttemptCreationRequestMapper._();

  static OneLegStandAttemptCreationRequestMapper? _instance;
  static OneLegStandAttemptCreationRequestMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = OneLegStandAttemptCreationRequestMapper._());
      AttemptCreationRequestMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'OneLegStandAttemptCreationRequest';

  static int _$resultInSeconds(OneLegStandAttemptCreationRequest v) =>
      v.resultInSeconds;
  static const Field<OneLegStandAttemptCreationRequest, int>
      _f$resultInSeconds = Field('resultInSeconds', _$resultInSeconds);
  static int _$foot(OneLegStandAttemptCreationRequest v) => v.foot;
  static const Field<OneLegStandAttemptCreationRequest, int> _f$foot =
      Field('foot', _$foot);

  @override
  final MappableFields<OneLegStandAttemptCreationRequest> fields = const {
    #resultInSeconds: _f$resultInSeconds,
    #foot: _f$foot,
  };

  static OneLegStandAttemptCreationRequest _instantiate(DecodingData data) {
    return OneLegStandAttemptCreationRequest(
        resultInSeconds: data.dec(_f$resultInSeconds), foot: data.dec(_f$foot));
  }

  @override
  final Function instantiate = _instantiate;

  static OneLegStandAttemptCreationRequest fromMap(Map<String, dynamic> map) {
    return ensureInitialized()
        .decodeMap<OneLegStandAttemptCreationRequest>(map);
  }

  static OneLegStandAttemptCreationRequest fromJson(String json) {
    return ensureInitialized()
        .decodeJson<OneLegStandAttemptCreationRequest>(json);
  }
}

mixin OneLegStandAttemptCreationRequestMappable {
  String toJson() {
    return OneLegStandAttemptCreationRequestMapper.ensureInitialized()
        .encodeJson<OneLegStandAttemptCreationRequest>(
            this as OneLegStandAttemptCreationRequest);
  }

  Map<String, dynamic> toMap() {
    return OneLegStandAttemptCreationRequestMapper.ensureInitialized()
        .encodeMap<OneLegStandAttemptCreationRequest>(
            this as OneLegStandAttemptCreationRequest);
  }

  OneLegStandAttemptCreationRequestCopyWith<OneLegStandAttemptCreationRequest,
          OneLegStandAttemptCreationRequest, OneLegStandAttemptCreationRequest>
      get copyWith => _OneLegStandAttemptCreationRequestCopyWithImpl(
          this as OneLegStandAttemptCreationRequest, $identity, $identity);
  @override
  String toString() {
    return OneLegStandAttemptCreationRequestMapper.ensureInitialized()
        .stringifyValue(this as OneLegStandAttemptCreationRequest);
  }

  @override
  bool operator ==(Object other) {
    return OneLegStandAttemptCreationRequestMapper.ensureInitialized()
        .equalsValue(this as OneLegStandAttemptCreationRequest, other);
  }

  @override
  int get hashCode {
    return OneLegStandAttemptCreationRequestMapper.ensureInitialized()
        .hashValue(this as OneLegStandAttemptCreationRequest);
  }
}

extension OneLegStandAttemptCreationRequestValueCopy<$R, $Out>
    on ObjectCopyWith<$R, OneLegStandAttemptCreationRequest, $Out> {
  OneLegStandAttemptCreationRequestCopyWith<$R,
          OneLegStandAttemptCreationRequest, $Out>
      get $asOneLegStandAttemptCreationRequest => $base.as((v, t, t2) =>
          _OneLegStandAttemptCreationRequestCopyWithImpl(v, t, t2));
}

abstract class OneLegStandAttemptCreationRequestCopyWith<
    $R,
    $In extends OneLegStandAttemptCreationRequest,
    $Out> implements AttemptCreationRequestCopyWith<$R, $In, $Out> {
  @override
  $R call({int? resultInSeconds, int? foot});
  OneLegStandAttemptCreationRequestCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _OneLegStandAttemptCreationRequestCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, OneLegStandAttemptCreationRequest, $Out>
    implements
        OneLegStandAttemptCreationRequestCopyWith<$R,
            OneLegStandAttemptCreationRequest, $Out> {
  _OneLegStandAttemptCreationRequestCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<OneLegStandAttemptCreationRequest> $mapper =
      OneLegStandAttemptCreationRequestMapper.ensureInitialized();
  @override
  $R call({int? resultInSeconds, int? foot}) => $apply(FieldCopyWithData({
        if (resultInSeconds != null) #resultInSeconds: resultInSeconds,
        if (foot != null) #foot: foot
      }));
  @override
  OneLegStandAttemptCreationRequest $make(CopyWithData data) =>
      OneLegStandAttemptCreationRequest(
          resultInSeconds:
              data.get(#resultInSeconds, or: $value.resultInSeconds),
          foot: data.get(#foot, or: $value.foot));

  @override
  OneLegStandAttemptCreationRequestCopyWith<$R2,
      OneLegStandAttemptCreationRequest, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _OneLegStandAttemptCreationRequestCopyWithImpl($value, $cast, t);
}
