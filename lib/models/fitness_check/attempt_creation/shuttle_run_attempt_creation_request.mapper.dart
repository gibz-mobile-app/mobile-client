// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'shuttle_run_attempt_creation_request.dart';

class ShuttleRunAttemptCreationRequestMapper
    extends ClassMapperBase<ShuttleRunAttemptCreationRequest> {
  ShuttleRunAttemptCreationRequestMapper._();

  static ShuttleRunAttemptCreationRequestMapper? _instance;
  static ShuttleRunAttemptCreationRequestMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = ShuttleRunAttemptCreationRequestMapper._());
      AttemptCreationRequestMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'ShuttleRunAttemptCreationRequest';

  static int _$resultInMilliseconds(ShuttleRunAttemptCreationRequest v) =>
      v.resultInMilliseconds;
  static const Field<ShuttleRunAttemptCreationRequest, int>
      _f$resultInMilliseconds =
      Field('resultInMilliseconds', _$resultInMilliseconds);

  @override
  final MappableFields<ShuttleRunAttemptCreationRequest> fields = const {
    #resultInMilliseconds: _f$resultInMilliseconds,
  };

  static ShuttleRunAttemptCreationRequest _instantiate(DecodingData data) {
    return ShuttleRunAttemptCreationRequest(
        resultInMilliseconds: data.dec(_f$resultInMilliseconds));
  }

  @override
  final Function instantiate = _instantiate;

  static ShuttleRunAttemptCreationRequest fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ShuttleRunAttemptCreationRequest>(map);
  }

  static ShuttleRunAttemptCreationRequest fromJson(String json) {
    return ensureInitialized()
        .decodeJson<ShuttleRunAttemptCreationRequest>(json);
  }
}

mixin ShuttleRunAttemptCreationRequestMappable {
  String toJson() {
    return ShuttleRunAttemptCreationRequestMapper.ensureInitialized()
        .encodeJson<ShuttleRunAttemptCreationRequest>(
            this as ShuttleRunAttemptCreationRequest);
  }

  Map<String, dynamic> toMap() {
    return ShuttleRunAttemptCreationRequestMapper.ensureInitialized()
        .encodeMap<ShuttleRunAttemptCreationRequest>(
            this as ShuttleRunAttemptCreationRequest);
  }

  ShuttleRunAttemptCreationRequestCopyWith<ShuttleRunAttemptCreationRequest,
          ShuttleRunAttemptCreationRequest, ShuttleRunAttemptCreationRequest>
      get copyWith => _ShuttleRunAttemptCreationRequestCopyWithImpl(
          this as ShuttleRunAttemptCreationRequest, $identity, $identity);
  @override
  String toString() {
    return ShuttleRunAttemptCreationRequestMapper.ensureInitialized()
        .stringifyValue(this as ShuttleRunAttemptCreationRequest);
  }

  @override
  bool operator ==(Object other) {
    return ShuttleRunAttemptCreationRequestMapper.ensureInitialized()
        .equalsValue(this as ShuttleRunAttemptCreationRequest, other);
  }

  @override
  int get hashCode {
    return ShuttleRunAttemptCreationRequestMapper.ensureInitialized()
        .hashValue(this as ShuttleRunAttemptCreationRequest);
  }
}

extension ShuttleRunAttemptCreationRequestValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ShuttleRunAttemptCreationRequest, $Out> {
  ShuttleRunAttemptCreationRequestCopyWith<$R, ShuttleRunAttemptCreationRequest,
          $Out>
      get $asShuttleRunAttemptCreationRequest => $base.as((v, t, t2) =>
          _ShuttleRunAttemptCreationRequestCopyWithImpl(v, t, t2));
}

abstract class ShuttleRunAttemptCreationRequestCopyWith<
    $R,
    $In extends ShuttleRunAttemptCreationRequest,
    $Out> implements AttemptCreationRequestCopyWith<$R, $In, $Out> {
  @override
  $R call({int? resultInMilliseconds});
  ShuttleRunAttemptCreationRequestCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _ShuttleRunAttemptCreationRequestCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ShuttleRunAttemptCreationRequest, $Out>
    implements
        ShuttleRunAttemptCreationRequestCopyWith<$R,
            ShuttleRunAttemptCreationRequest, $Out> {
  _ShuttleRunAttemptCreationRequestCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ShuttleRunAttemptCreationRequest> $mapper =
      ShuttleRunAttemptCreationRequestMapper.ensureInitialized();
  @override
  $R call({int? resultInMilliseconds}) => $apply(FieldCopyWithData({
        if (resultInMilliseconds != null)
          #resultInMilliseconds: resultInMilliseconds
      }));
  @override
  ShuttleRunAttemptCreationRequest $make(CopyWithData data) =>
      ShuttleRunAttemptCreationRequest(
          resultInMilliseconds:
              data.get(#resultInMilliseconds, or: $value.resultInMilliseconds));

  @override
  ShuttleRunAttemptCreationRequestCopyWith<$R2,
      ShuttleRunAttemptCreationRequest, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _ShuttleRunAttemptCreationRequestCopyWithImpl($value, $cast, t);
}
