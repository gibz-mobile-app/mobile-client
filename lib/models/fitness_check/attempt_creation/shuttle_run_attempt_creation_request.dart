import 'package:dart_mappable/dart_mappable.dart';

import 'attempt_creation_request.dart';

part 'shuttle_run_attempt_creation_request.mapper.dart';

@MappableClass()
class ShuttleRunAttemptCreationRequest extends AttemptCreationRequest
    with ShuttleRunAttemptCreationRequestMappable {
  final int resultInMilliseconds;

  ShuttleRunAttemptCreationRequest({required this.resultInMilliseconds});

  @override
  List<Object?> get props => [resultInMilliseconds];
}
