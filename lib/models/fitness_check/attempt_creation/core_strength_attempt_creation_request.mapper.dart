// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'core_strength_attempt_creation_request.dart';

class CoreStrengthAttemptCreationRequestMapper
    extends ClassMapperBase<CoreStrengthAttemptCreationRequest> {
  CoreStrengthAttemptCreationRequestMapper._();

  static CoreStrengthAttemptCreationRequestMapper? _instance;
  static CoreStrengthAttemptCreationRequestMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = CoreStrengthAttemptCreationRequestMapper._());
      AttemptCreationRequestMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'CoreStrengthAttemptCreationRequest';

  static int _$resultInSeconds(CoreStrengthAttemptCreationRequest v) =>
      v.resultInSeconds;
  static const Field<CoreStrengthAttemptCreationRequest, int>
      _f$resultInSeconds = Field('resultInSeconds', _$resultInSeconds);

  @override
  final MappableFields<CoreStrengthAttemptCreationRequest> fields = const {
    #resultInSeconds: _f$resultInSeconds,
  };

  static CoreStrengthAttemptCreationRequest _instantiate(DecodingData data) {
    return CoreStrengthAttemptCreationRequest(
        resultInSeconds: data.dec(_f$resultInSeconds));
  }

  @override
  final Function instantiate = _instantiate;

  static CoreStrengthAttemptCreationRequest fromMap(Map<String, dynamic> map) {
    return ensureInitialized()
        .decodeMap<CoreStrengthAttemptCreationRequest>(map);
  }

  static CoreStrengthAttemptCreationRequest fromJson(String json) {
    return ensureInitialized()
        .decodeJson<CoreStrengthAttemptCreationRequest>(json);
  }
}

mixin CoreStrengthAttemptCreationRequestMappable {
  String toJson() {
    return CoreStrengthAttemptCreationRequestMapper.ensureInitialized()
        .encodeJson<CoreStrengthAttemptCreationRequest>(
            this as CoreStrengthAttemptCreationRequest);
  }

  Map<String, dynamic> toMap() {
    return CoreStrengthAttemptCreationRequestMapper.ensureInitialized()
        .encodeMap<CoreStrengthAttemptCreationRequest>(
            this as CoreStrengthAttemptCreationRequest);
  }

  CoreStrengthAttemptCreationRequestCopyWith<
          CoreStrengthAttemptCreationRequest,
          CoreStrengthAttemptCreationRequest,
          CoreStrengthAttemptCreationRequest>
      get copyWith => _CoreStrengthAttemptCreationRequestCopyWithImpl(
          this as CoreStrengthAttemptCreationRequest, $identity, $identity);
  @override
  String toString() {
    return CoreStrengthAttemptCreationRequestMapper.ensureInitialized()
        .stringifyValue(this as CoreStrengthAttemptCreationRequest);
  }

  @override
  bool operator ==(Object other) {
    return CoreStrengthAttemptCreationRequestMapper.ensureInitialized()
        .equalsValue(this as CoreStrengthAttemptCreationRequest, other);
  }

  @override
  int get hashCode {
    return CoreStrengthAttemptCreationRequestMapper.ensureInitialized()
        .hashValue(this as CoreStrengthAttemptCreationRequest);
  }
}

extension CoreStrengthAttemptCreationRequestValueCopy<$R, $Out>
    on ObjectCopyWith<$R, CoreStrengthAttemptCreationRequest, $Out> {
  CoreStrengthAttemptCreationRequestCopyWith<$R,
          CoreStrengthAttemptCreationRequest, $Out>
      get $asCoreStrengthAttemptCreationRequest => $base.as((v, t, t2) =>
          _CoreStrengthAttemptCreationRequestCopyWithImpl(v, t, t2));
}

abstract class CoreStrengthAttemptCreationRequestCopyWith<
    $R,
    $In extends CoreStrengthAttemptCreationRequest,
    $Out> implements AttemptCreationRequestCopyWith<$R, $In, $Out> {
  @override
  $R call({int? resultInSeconds});
  CoreStrengthAttemptCreationRequestCopyWith<$R2, $In, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _CoreStrengthAttemptCreationRequestCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, CoreStrengthAttemptCreationRequest, $Out>
    implements
        CoreStrengthAttemptCreationRequestCopyWith<$R,
            CoreStrengthAttemptCreationRequest, $Out> {
  _CoreStrengthAttemptCreationRequestCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<CoreStrengthAttemptCreationRequest> $mapper =
      CoreStrengthAttemptCreationRequestMapper.ensureInitialized();
  @override
  $R call({int? resultInSeconds}) => $apply(FieldCopyWithData(
      {if (resultInSeconds != null) #resultInSeconds: resultInSeconds}));
  @override
  CoreStrengthAttemptCreationRequest $make(CopyWithData data) =>
      CoreStrengthAttemptCreationRequest(
          resultInSeconds:
              data.get(#resultInSeconds, or: $value.resultInSeconds));

  @override
  CoreStrengthAttemptCreationRequestCopyWith<$R2,
      CoreStrengthAttemptCreationRequest, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _CoreStrengthAttemptCreationRequestCopyWithImpl($value, $cast, t);
}
