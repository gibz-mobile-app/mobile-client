// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'attempt_creation_request.dart';

class AttemptCreationRequestMapper
    extends ClassMapperBase<AttemptCreationRequest> {
  AttemptCreationRequestMapper._();

  static AttemptCreationRequestMapper? _instance;
  static AttemptCreationRequestMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AttemptCreationRequestMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'AttemptCreationRequest';

  @override
  final MappableFields<AttemptCreationRequest> fields = const {};

  static AttemptCreationRequest _instantiate(DecodingData data) {
    throw MapperException.missingConstructor('AttemptCreationRequest');
  }

  @override
  final Function instantiate = _instantiate;

  static AttemptCreationRequest fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<AttemptCreationRequest>(map);
  }

  static AttemptCreationRequest fromJson(String json) {
    return ensureInitialized().decodeJson<AttemptCreationRequest>(json);
  }
}

mixin AttemptCreationRequestMappable {
  String toJson();
  Map<String, dynamic> toMap();
  AttemptCreationRequestCopyWith<AttemptCreationRequest, AttemptCreationRequest,
      AttemptCreationRequest> get copyWith;
}

abstract class AttemptCreationRequestCopyWith<
    $R,
    $In extends AttemptCreationRequest,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  $R call();
  AttemptCreationRequestCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}
