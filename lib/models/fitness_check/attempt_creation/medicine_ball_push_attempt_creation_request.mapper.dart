// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'medicine_ball_push_attempt_creation_request.dart';

class MedicineBallPushAttemptCreationRequestMapper
    extends ClassMapperBase<MedicineBallPushAttemptCreationRequest> {
  MedicineBallPushAttemptCreationRequestMapper._();

  static MedicineBallPushAttemptCreationRequestMapper? _instance;
  static MedicineBallPushAttemptCreationRequestMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = MedicineBallPushAttemptCreationRequestMapper._());
      AttemptCreationRequestMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'MedicineBallPushAttemptCreationRequest';

  static int _$resultInCentimeters(MedicineBallPushAttemptCreationRequest v) =>
      v.resultInCentimeters;
  static const Field<MedicineBallPushAttemptCreationRequest, int>
      _f$resultInCentimeters =
      Field('resultInCentimeters', _$resultInCentimeters);

  @override
  final MappableFields<MedicineBallPushAttemptCreationRequest> fields = const {
    #resultInCentimeters: _f$resultInCentimeters,
  };

  static MedicineBallPushAttemptCreationRequest _instantiate(
      DecodingData data) {
    return MedicineBallPushAttemptCreationRequest(
        resultInCentimeters: data.dec(_f$resultInCentimeters));
  }

  @override
  final Function instantiate = _instantiate;

  static MedicineBallPushAttemptCreationRequest fromMap(
      Map<String, dynamic> map) {
    return ensureInitialized()
        .decodeMap<MedicineBallPushAttemptCreationRequest>(map);
  }

  static MedicineBallPushAttemptCreationRequest fromJson(String json) {
    return ensureInitialized()
        .decodeJson<MedicineBallPushAttemptCreationRequest>(json);
  }
}

mixin MedicineBallPushAttemptCreationRequestMappable {
  String toJson() {
    return MedicineBallPushAttemptCreationRequestMapper.ensureInitialized()
        .encodeJson<MedicineBallPushAttemptCreationRequest>(
            this as MedicineBallPushAttemptCreationRequest);
  }

  Map<String, dynamic> toMap() {
    return MedicineBallPushAttemptCreationRequestMapper.ensureInitialized()
        .encodeMap<MedicineBallPushAttemptCreationRequest>(
            this as MedicineBallPushAttemptCreationRequest);
  }

  MedicineBallPushAttemptCreationRequestCopyWith<
          MedicineBallPushAttemptCreationRequest,
          MedicineBallPushAttemptCreationRequest,
          MedicineBallPushAttemptCreationRequest>
      get copyWith => _MedicineBallPushAttemptCreationRequestCopyWithImpl(
          this as MedicineBallPushAttemptCreationRequest, $identity, $identity);
  @override
  String toString() {
    return MedicineBallPushAttemptCreationRequestMapper.ensureInitialized()
        .stringifyValue(this as MedicineBallPushAttemptCreationRequest);
  }

  @override
  bool operator ==(Object other) {
    return MedicineBallPushAttemptCreationRequestMapper.ensureInitialized()
        .equalsValue(this as MedicineBallPushAttemptCreationRequest, other);
  }

  @override
  int get hashCode {
    return MedicineBallPushAttemptCreationRequestMapper.ensureInitialized()
        .hashValue(this as MedicineBallPushAttemptCreationRequest);
  }
}

extension MedicineBallPushAttemptCreationRequestValueCopy<$R, $Out>
    on ObjectCopyWith<$R, MedicineBallPushAttemptCreationRequest, $Out> {
  MedicineBallPushAttemptCreationRequestCopyWith<$R,
          MedicineBallPushAttemptCreationRequest, $Out>
      get $asMedicineBallPushAttemptCreationRequest => $base.as((v, t, t2) =>
          _MedicineBallPushAttemptCreationRequestCopyWithImpl(v, t, t2));
}

abstract class MedicineBallPushAttemptCreationRequestCopyWith<
    $R,
    $In extends MedicineBallPushAttemptCreationRequest,
    $Out> implements AttemptCreationRequestCopyWith<$R, $In, $Out> {
  @override
  $R call({int? resultInCentimeters});
  MedicineBallPushAttemptCreationRequestCopyWith<$R2, $In, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _MedicineBallPushAttemptCreationRequestCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, MedicineBallPushAttemptCreationRequest, $Out>
    implements
        MedicineBallPushAttemptCreationRequestCopyWith<$R,
            MedicineBallPushAttemptCreationRequest, $Out> {
  _MedicineBallPushAttemptCreationRequestCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<MedicineBallPushAttemptCreationRequest> $mapper =
      MedicineBallPushAttemptCreationRequestMapper.ensureInitialized();
  @override
  $R call({int? resultInCentimeters}) => $apply(FieldCopyWithData({
        if (resultInCentimeters != null)
          #resultInCentimeters: resultInCentimeters
      }));
  @override
  MedicineBallPushAttemptCreationRequest $make(CopyWithData data) =>
      MedicineBallPushAttemptCreationRequest(
          resultInCentimeters:
              data.get(#resultInCentimeters, or: $value.resultInCentimeters));

  @override
  MedicineBallPushAttemptCreationRequestCopyWith<$R2,
      MedicineBallPushAttemptCreationRequest, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _MedicineBallPushAttemptCreationRequestCopyWithImpl($value, $cast, t);
}
