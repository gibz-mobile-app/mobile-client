// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'standing_long_jump_attempt_creation_request.dart';

class StandingLongJumpAttemptCreationRequestMapper
    extends ClassMapperBase<StandingLongJumpAttemptCreationRequest> {
  StandingLongJumpAttemptCreationRequestMapper._();

  static StandingLongJumpAttemptCreationRequestMapper? _instance;
  static StandingLongJumpAttemptCreationRequestMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = StandingLongJumpAttemptCreationRequestMapper._());
      AttemptCreationRequestMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'StandingLongJumpAttemptCreationRequest';

  static int _$resultInCentimeters(StandingLongJumpAttemptCreationRequest v) =>
      v.resultInCentimeters;
  static const Field<StandingLongJumpAttemptCreationRequest, int>
      _f$resultInCentimeters =
      Field('resultInCentimeters', _$resultInCentimeters);

  @override
  final MappableFields<StandingLongJumpAttemptCreationRequest> fields = const {
    #resultInCentimeters: _f$resultInCentimeters,
  };

  static StandingLongJumpAttemptCreationRequest _instantiate(
      DecodingData data) {
    return StandingLongJumpAttemptCreationRequest(
        resultInCentimeters: data.dec(_f$resultInCentimeters));
  }

  @override
  final Function instantiate = _instantiate;

  static StandingLongJumpAttemptCreationRequest fromMap(
      Map<String, dynamic> map) {
    return ensureInitialized()
        .decodeMap<StandingLongJumpAttemptCreationRequest>(map);
  }

  static StandingLongJumpAttemptCreationRequest fromJson(String json) {
    return ensureInitialized()
        .decodeJson<StandingLongJumpAttemptCreationRequest>(json);
  }
}

mixin StandingLongJumpAttemptCreationRequestMappable {
  String toJson() {
    return StandingLongJumpAttemptCreationRequestMapper.ensureInitialized()
        .encodeJson<StandingLongJumpAttemptCreationRequest>(
            this as StandingLongJumpAttemptCreationRequest);
  }

  Map<String, dynamic> toMap() {
    return StandingLongJumpAttemptCreationRequestMapper.ensureInitialized()
        .encodeMap<StandingLongJumpAttemptCreationRequest>(
            this as StandingLongJumpAttemptCreationRequest);
  }

  StandingLongJumpAttemptCreationRequestCopyWith<
          StandingLongJumpAttemptCreationRequest,
          StandingLongJumpAttemptCreationRequest,
          StandingLongJumpAttemptCreationRequest>
      get copyWith => _StandingLongJumpAttemptCreationRequestCopyWithImpl(
          this as StandingLongJumpAttemptCreationRequest, $identity, $identity);
  @override
  String toString() {
    return StandingLongJumpAttemptCreationRequestMapper.ensureInitialized()
        .stringifyValue(this as StandingLongJumpAttemptCreationRequest);
  }

  @override
  bool operator ==(Object other) {
    return StandingLongJumpAttemptCreationRequestMapper.ensureInitialized()
        .equalsValue(this as StandingLongJumpAttemptCreationRequest, other);
  }

  @override
  int get hashCode {
    return StandingLongJumpAttemptCreationRequestMapper.ensureInitialized()
        .hashValue(this as StandingLongJumpAttemptCreationRequest);
  }
}

extension StandingLongJumpAttemptCreationRequestValueCopy<$R, $Out>
    on ObjectCopyWith<$R, StandingLongJumpAttemptCreationRequest, $Out> {
  StandingLongJumpAttemptCreationRequestCopyWith<$R,
          StandingLongJumpAttemptCreationRequest, $Out>
      get $asStandingLongJumpAttemptCreationRequest => $base.as((v, t, t2) =>
          _StandingLongJumpAttemptCreationRequestCopyWithImpl(v, t, t2));
}

abstract class StandingLongJumpAttemptCreationRequestCopyWith<
    $R,
    $In extends StandingLongJumpAttemptCreationRequest,
    $Out> implements AttemptCreationRequestCopyWith<$R, $In, $Out> {
  @override
  $R call({int? resultInCentimeters});
  StandingLongJumpAttemptCreationRequestCopyWith<$R2, $In, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _StandingLongJumpAttemptCreationRequestCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, StandingLongJumpAttemptCreationRequest, $Out>
    implements
        StandingLongJumpAttemptCreationRequestCopyWith<$R,
            StandingLongJumpAttemptCreationRequest, $Out> {
  _StandingLongJumpAttemptCreationRequestCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<StandingLongJumpAttemptCreationRequest> $mapper =
      StandingLongJumpAttemptCreationRequestMapper.ensureInitialized();
  @override
  $R call({int? resultInCentimeters}) => $apply(FieldCopyWithData({
        if (resultInCentimeters != null)
          #resultInCentimeters: resultInCentimeters
      }));
  @override
  StandingLongJumpAttemptCreationRequest $make(CopyWithData data) =>
      StandingLongJumpAttemptCreationRequest(
          resultInCentimeters:
              data.get(#resultInCentimeters, or: $value.resultInCentimeters));

  @override
  StandingLongJumpAttemptCreationRequestCopyWith<$R2,
      StandingLongJumpAttemptCreationRequest, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _StandingLongJumpAttemptCreationRequestCopyWithImpl($value, $cast, t);
}
