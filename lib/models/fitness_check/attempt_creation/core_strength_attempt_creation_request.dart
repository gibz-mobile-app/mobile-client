import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt_creation/attempt_creation_request.dart';

part 'core_strength_attempt_creation_request.mapper.dart';

@MappableClass()
class CoreStrengthAttemptCreationRequest extends AttemptCreationRequest
    with CoreStrengthAttemptCreationRequestMappable {
  final int resultInSeconds;

  CoreStrengthAttemptCreationRequest({required this.resultInSeconds});

  @override
  List<Object?> get props => [resultInSeconds];
}
