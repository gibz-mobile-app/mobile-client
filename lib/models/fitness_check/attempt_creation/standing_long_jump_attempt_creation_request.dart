import 'package:dart_mappable/dart_mappable.dart';

import 'attempt_creation_request.dart';

part 'standing_long_jump_attempt_creation_request.mapper.dart';

@MappableClass()
class StandingLongJumpAttemptCreationRequest extends AttemptCreationRequest
    with StandingLongJumpAttemptCreationRequestMappable {
  final int resultInCentimeters;

  StandingLongJumpAttemptCreationRequest({required this.resultInCentimeters});

  @override
  List<Object?> get props => [resultInCentimeters];
}
