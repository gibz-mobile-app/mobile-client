import 'package:dart_mappable/dart_mappable.dart';

import 'attempt_creation_request.dart';

part 'twelve_minutes_run_attempt_creation_request.mapper.dart';

@MappableClass()
class TwelveMinutesRunAttemptCreationRequest extends AttemptCreationRequest
    with TwelveMinutesRunAttemptCreationRequestMappable {
  final int resultInRounds;

  TwelveMinutesRunAttemptCreationRequest({required this.resultInRounds});

  @override
  List<Object?> get props => [resultInRounds];
}
