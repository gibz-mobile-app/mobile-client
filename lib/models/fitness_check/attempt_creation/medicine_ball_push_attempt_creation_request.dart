import 'package:dart_mappable/dart_mappable.dart';

import 'attempt_creation_request.dart';

part 'medicine_ball_push_attempt_creation_request.mapper.dart';

@MappableClass()
class MedicineBallPushAttemptCreationRequest extends AttemptCreationRequest
    with MedicineBallPushAttemptCreationRequestMappable {
  final int resultInCentimeters;

  MedicineBallPushAttemptCreationRequest({required this.resultInCentimeters});

  @override
  List<Object?> get props => [resultInCentimeters];
}
