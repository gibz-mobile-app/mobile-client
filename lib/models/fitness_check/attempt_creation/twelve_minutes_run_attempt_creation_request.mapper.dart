// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'twelve_minutes_run_attempt_creation_request.dart';

class TwelveMinutesRunAttemptCreationRequestMapper
    extends ClassMapperBase<TwelveMinutesRunAttemptCreationRequest> {
  TwelveMinutesRunAttemptCreationRequestMapper._();

  static TwelveMinutesRunAttemptCreationRequestMapper? _instance;
  static TwelveMinutesRunAttemptCreationRequestMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = TwelveMinutesRunAttemptCreationRequestMapper._());
      AttemptCreationRequestMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'TwelveMinutesRunAttemptCreationRequest';

  static int _$resultInRounds(TwelveMinutesRunAttemptCreationRequest v) =>
      v.resultInRounds;
  static const Field<TwelveMinutesRunAttemptCreationRequest, int>
      _f$resultInRounds = Field('resultInRounds', _$resultInRounds);

  @override
  final MappableFields<TwelveMinutesRunAttemptCreationRequest> fields = const {
    #resultInRounds: _f$resultInRounds,
  };

  static TwelveMinutesRunAttemptCreationRequest _instantiate(
      DecodingData data) {
    return TwelveMinutesRunAttemptCreationRequest(
        resultInRounds: data.dec(_f$resultInRounds));
  }

  @override
  final Function instantiate = _instantiate;

  static TwelveMinutesRunAttemptCreationRequest fromMap(
      Map<String, dynamic> map) {
    return ensureInitialized()
        .decodeMap<TwelveMinutesRunAttemptCreationRequest>(map);
  }

  static TwelveMinutesRunAttemptCreationRequest fromJson(String json) {
    return ensureInitialized()
        .decodeJson<TwelveMinutesRunAttemptCreationRequest>(json);
  }
}

mixin TwelveMinutesRunAttemptCreationRequestMappable {
  String toJson() {
    return TwelveMinutesRunAttemptCreationRequestMapper.ensureInitialized()
        .encodeJson<TwelveMinutesRunAttemptCreationRequest>(
            this as TwelveMinutesRunAttemptCreationRequest);
  }

  Map<String, dynamic> toMap() {
    return TwelveMinutesRunAttemptCreationRequestMapper.ensureInitialized()
        .encodeMap<TwelveMinutesRunAttemptCreationRequest>(
            this as TwelveMinutesRunAttemptCreationRequest);
  }

  TwelveMinutesRunAttemptCreationRequestCopyWith<
          TwelveMinutesRunAttemptCreationRequest,
          TwelveMinutesRunAttemptCreationRequest,
          TwelveMinutesRunAttemptCreationRequest>
      get copyWith => _TwelveMinutesRunAttemptCreationRequestCopyWithImpl(
          this as TwelveMinutesRunAttemptCreationRequest, $identity, $identity);
  @override
  String toString() {
    return TwelveMinutesRunAttemptCreationRequestMapper.ensureInitialized()
        .stringifyValue(this as TwelveMinutesRunAttemptCreationRequest);
  }

  @override
  bool operator ==(Object other) {
    return TwelveMinutesRunAttemptCreationRequestMapper.ensureInitialized()
        .equalsValue(this as TwelveMinutesRunAttemptCreationRequest, other);
  }

  @override
  int get hashCode {
    return TwelveMinutesRunAttemptCreationRequestMapper.ensureInitialized()
        .hashValue(this as TwelveMinutesRunAttemptCreationRequest);
  }
}

extension TwelveMinutesRunAttemptCreationRequestValueCopy<$R, $Out>
    on ObjectCopyWith<$R, TwelveMinutesRunAttemptCreationRequest, $Out> {
  TwelveMinutesRunAttemptCreationRequestCopyWith<$R,
          TwelveMinutesRunAttemptCreationRequest, $Out>
      get $asTwelveMinutesRunAttemptCreationRequest => $base.as((v, t, t2) =>
          _TwelveMinutesRunAttemptCreationRequestCopyWithImpl(v, t, t2));
}

abstract class TwelveMinutesRunAttemptCreationRequestCopyWith<
    $R,
    $In extends TwelveMinutesRunAttemptCreationRequest,
    $Out> implements AttemptCreationRequestCopyWith<$R, $In, $Out> {
  @override
  $R call({int? resultInRounds});
  TwelveMinutesRunAttemptCreationRequestCopyWith<$R2, $In, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _TwelveMinutesRunAttemptCreationRequestCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, TwelveMinutesRunAttemptCreationRequest, $Out>
    implements
        TwelveMinutesRunAttemptCreationRequestCopyWith<$R,
            TwelveMinutesRunAttemptCreationRequest, $Out> {
  _TwelveMinutesRunAttemptCreationRequestCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<TwelveMinutesRunAttemptCreationRequest> $mapper =
      TwelveMinutesRunAttemptCreationRequestMapper.ensureInitialized();
  @override
  $R call({int? resultInRounds}) => $apply(FieldCopyWithData(
      {if (resultInRounds != null) #resultInRounds: resultInRounds}));
  @override
  TwelveMinutesRunAttemptCreationRequest $make(CopyWithData data) =>
      TwelveMinutesRunAttemptCreationRequest(
          resultInRounds: data.get(#resultInRounds, or: $value.resultInRounds));

  @override
  TwelveMinutesRunAttemptCreationRequestCopyWith<$R2,
      TwelveMinutesRunAttemptCreationRequest, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _TwelveMinutesRunAttemptCreationRequestCopyWithImpl($value, $cast, t);
}
