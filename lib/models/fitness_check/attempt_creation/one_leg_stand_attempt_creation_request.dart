import 'package:dart_mappable/dart_mappable.dart';

import 'attempt_creation_request.dart';

part 'one_leg_stand_attempt_creation_request.mapper.dart';

@MappableClass()
class OneLegStandAttemptCreationRequest extends AttemptCreationRequest
    with OneLegStandAttemptCreationRequestMappable {
  final int resultInSeconds;
  final int foot;

  OneLegStandAttemptCreationRequest({
    required this.resultInSeconds,
    required this.foot,
  });

  @override
  List<Object?> get props => [resultInSeconds, foot];
}
