import 'package:gibz_mobileapp/models/fitness_check/attempt_creation/attempt_creation_request.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt_measurement.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';

import 'core_strength_attempt_creation_request.dart';
import 'medicine_ball_push_attempt_creation_request.dart';
import 'one_leg_stand_attempt_creation_request.dart';
import 'shuttle_run_attempt_creation_request.dart';
import 'standing_long_jump_attempt_creation_request.dart';
import 'twelve_minutes_run_attempt_creation_request.dart';

class AttemptCreationRequestFactory {
  static AttemptCreationRequest createPayload<T extends Attempt>(
    Discipline discipline,
    AttemptMeasurement measurement,
  ) {
    if (T == CoreStrengthAttempt) {
      return CoreStrengthAttemptCreationRequest(
        resultInSeconds: measurement.value,
      );
    } else if (T == MedicineBallPushAttempt) {
      return MedicineBallPushAttemptCreationRequest(
        resultInCentimeters: measurement.value,
      );
    } else if (T == OneLegStandAttempt) {
      return OneLegStandAttemptCreationRequest(
        resultInSeconds: measurement.value,
        foot: measurement.side!.index,
      );
    } else if (T == ShuttleRunAttempt) {
      return ShuttleRunAttemptCreationRequest(
        resultInMilliseconds: measurement.value,
      );
    } else if (T == StandingLongJumpAttempt) {
      return StandingLongJumpAttemptCreationRequest(
        resultInCentimeters: measurement.value,
      );
    } else if (T == TwelveMinutesRunAttempt) {
      return TwelveMinutesRunAttemptCreationRequest(
        resultInRounds: measurement.value,
      );
    }

    throw Exception(
        "No AttemptCreationRequest could be initialized based on type '$T'.");
  }
}
