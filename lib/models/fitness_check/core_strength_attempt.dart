import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';

part 'core_strength_attempt.mapper.dart';

@MappableClass(discriminatorValue: "coreStrength")
class CoreStrengthAttempt extends Attempt with CoreStrengthAttemptMappable {
  final int resultInSeconds;

  const CoreStrengthAttempt({
    required super.id,
    required super.points,
    required super.attemptNumber,
    required super.momentUtc,
    required super.userId,
    required super.gender,
    required super.cohortId,
    required this.resultInSeconds,
  });

  static const fromMap = CoreStrengthAttemptMapper.fromMap;
  static const fromJson = CoreStrengthAttemptMapper.fromJson;

  @override
  String get displayValue => '$resultInSeconds Sekunden';
}
