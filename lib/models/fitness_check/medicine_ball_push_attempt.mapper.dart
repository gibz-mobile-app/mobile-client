// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'medicine_ball_push_attempt.dart';

class MedicineBallPushAttemptMapper
    extends SubClassMapperBase<MedicineBallPushAttempt> {
  MedicineBallPushAttemptMapper._();

  static MedicineBallPushAttemptMapper? _instance;
  static MedicineBallPushAttemptMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = MedicineBallPushAttemptMapper._());
      AttemptMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'MedicineBallPushAttempt';

  static String _$id(MedicineBallPushAttempt v) => v.id;
  static const Field<MedicineBallPushAttempt, String> _f$id = Field('id', _$id);
  static int _$points(MedicineBallPushAttempt v) => v.points;
  static const Field<MedicineBallPushAttempt, int> _f$points =
      Field('points', _$points);
  static int _$attemptNumber(MedicineBallPushAttempt v) => v.attemptNumber;
  static const Field<MedicineBallPushAttempt, int> _f$attemptNumber =
      Field('attemptNumber', _$attemptNumber);
  static DateTime _$momentUtc(MedicineBallPushAttempt v) => v.momentUtc;
  static const Field<MedicineBallPushAttempt, DateTime> _f$momentUtc =
      Field('momentUtc', _$momentUtc, hook: DateTimeUtcHook());
  static String _$userId(MedicineBallPushAttempt v) => v.userId;
  static const Field<MedicineBallPushAttempt, String> _f$userId =
      Field('userId', _$userId);
  static String _$gender(MedicineBallPushAttempt v) => v.gender;
  static const Field<MedicineBallPushAttempt, String> _f$gender =
      Field('gender', _$gender);
  static String _$cohortId(MedicineBallPushAttempt v) => v.cohortId;
  static const Field<MedicineBallPushAttempt, String> _f$cohortId =
      Field('cohortId', _$cohortId);
  static int _$resultInCentimeters(MedicineBallPushAttempt v) =>
      v.resultInCentimeters;
  static const Field<MedicineBallPushAttempt, int> _f$resultInCentimeters =
      Field('resultInCentimeters', _$resultInCentimeters);

  @override
  final MappableFields<MedicineBallPushAttempt> fields = const {
    #id: _f$id,
    #points: _f$points,
    #attemptNumber: _f$attemptNumber,
    #momentUtc: _f$momentUtc,
    #userId: _f$userId,
    #gender: _f$gender,
    #cohortId: _f$cohortId,
    #resultInCentimeters: _f$resultInCentimeters,
  };

  @override
  final String discriminatorKey = 'discipline';
  @override
  final dynamic discriminatorValue = "medicineBallPush";
  @override
  late final ClassMapperBase superMapper = AttemptMapper.ensureInitialized();

  static MedicineBallPushAttempt _instantiate(DecodingData data) {
    return MedicineBallPushAttempt(
        id: data.dec(_f$id),
        points: data.dec(_f$points),
        attemptNumber: data.dec(_f$attemptNumber),
        momentUtc: data.dec(_f$momentUtc),
        userId: data.dec(_f$userId),
        gender: data.dec(_f$gender),
        cohortId: data.dec(_f$cohortId),
        resultInCentimeters: data.dec(_f$resultInCentimeters));
  }

  @override
  final Function instantiate = _instantiate;

  static MedicineBallPushAttempt fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<MedicineBallPushAttempt>(map);
  }

  static MedicineBallPushAttempt fromJson(String json) {
    return ensureInitialized().decodeJson<MedicineBallPushAttempt>(json);
  }
}

mixin MedicineBallPushAttemptMappable {
  String toJson() {
    return MedicineBallPushAttemptMapper.ensureInitialized()
        .encodeJson<MedicineBallPushAttempt>(this as MedicineBallPushAttempt);
  }

  Map<String, dynamic> toMap() {
    return MedicineBallPushAttemptMapper.ensureInitialized()
        .encodeMap<MedicineBallPushAttempt>(this as MedicineBallPushAttempt);
  }

  MedicineBallPushAttemptCopyWith<MedicineBallPushAttempt,
          MedicineBallPushAttempt, MedicineBallPushAttempt>
      get copyWith => _MedicineBallPushAttemptCopyWithImpl(
          this as MedicineBallPushAttempt, $identity, $identity);
  @override
  String toString() {
    return MedicineBallPushAttemptMapper.ensureInitialized()
        .stringifyValue(this as MedicineBallPushAttempt);
  }

  @override
  bool operator ==(Object other) {
    return MedicineBallPushAttemptMapper.ensureInitialized()
        .equalsValue(this as MedicineBallPushAttempt, other);
  }

  @override
  int get hashCode {
    return MedicineBallPushAttemptMapper.ensureInitialized()
        .hashValue(this as MedicineBallPushAttempt);
  }
}

extension MedicineBallPushAttemptValueCopy<$R, $Out>
    on ObjectCopyWith<$R, MedicineBallPushAttempt, $Out> {
  MedicineBallPushAttemptCopyWith<$R, MedicineBallPushAttempt, $Out>
      get $asMedicineBallPushAttempt => $base
          .as((v, t, t2) => _MedicineBallPushAttemptCopyWithImpl(v, t, t2));
}

abstract class MedicineBallPushAttemptCopyWith<
    $R,
    $In extends MedicineBallPushAttempt,
    $Out> implements AttemptCopyWith<$R, $In, $Out> {
  @override
  $R call(
      {String? id,
      int? points,
      int? attemptNumber,
      DateTime? momentUtc,
      String? userId,
      String? gender,
      String? cohortId,
      int? resultInCentimeters});
  MedicineBallPushAttemptCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _MedicineBallPushAttemptCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, MedicineBallPushAttempt, $Out>
    implements
        MedicineBallPushAttemptCopyWith<$R, MedicineBallPushAttempt, $Out> {
  _MedicineBallPushAttemptCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<MedicineBallPushAttempt> $mapper =
      MedicineBallPushAttemptMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          int? points,
          int? attemptNumber,
          DateTime? momentUtc,
          String? userId,
          String? gender,
          String? cohortId,
          int? resultInCentimeters}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (points != null) #points: points,
        if (attemptNumber != null) #attemptNumber: attemptNumber,
        if (momentUtc != null) #momentUtc: momentUtc,
        if (userId != null) #userId: userId,
        if (gender != null) #gender: gender,
        if (cohortId != null) #cohortId: cohortId,
        if (resultInCentimeters != null)
          #resultInCentimeters: resultInCentimeters
      }));
  @override
  MedicineBallPushAttempt $make(CopyWithData data) => MedicineBallPushAttempt(
      id: data.get(#id, or: $value.id),
      points: data.get(#points, or: $value.points),
      attemptNumber: data.get(#attemptNumber, or: $value.attemptNumber),
      momentUtc: data.get(#momentUtc, or: $value.momentUtc),
      userId: data.get(#userId, or: $value.userId),
      gender: data.get(#gender, or: $value.gender),
      cohortId: data.get(#cohortId, or: $value.cohortId),
      resultInCentimeters:
          data.get(#resultInCentimeters, or: $value.resultInCentimeters));

  @override
  MedicineBallPushAttemptCopyWith<$R2, MedicineBallPushAttempt, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _MedicineBallPushAttemptCopyWithImpl($value, $cast, t);
}
