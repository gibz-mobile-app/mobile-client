// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'one_leg_stand_attempt.dart';

class OneLegStandAttemptMapper extends SubClassMapperBase<OneLegStandAttempt> {
  OneLegStandAttemptMapper._();

  static OneLegStandAttemptMapper? _instance;
  static OneLegStandAttemptMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = OneLegStandAttemptMapper._());
      AttemptMapper.ensureInitialized().addSubMapper(_instance!);
      FootMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'OneLegStandAttempt';

  static String _$id(OneLegStandAttempt v) => v.id;
  static const Field<OneLegStandAttempt, String> _f$id = Field('id', _$id);
  static int _$points(OneLegStandAttempt v) => v.points;
  static const Field<OneLegStandAttempt, int> _f$points =
      Field('points', _$points);
  static int _$attemptNumber(OneLegStandAttempt v) => v.attemptNumber;
  static const Field<OneLegStandAttempt, int> _f$attemptNumber =
      Field('attemptNumber', _$attemptNumber);
  static DateTime _$momentUtc(OneLegStandAttempt v) => v.momentUtc;
  static const Field<OneLegStandAttempt, DateTime> _f$momentUtc =
      Field('momentUtc', _$momentUtc, hook: DateTimeUtcHook());
  static String _$userId(OneLegStandAttempt v) => v.userId;
  static const Field<OneLegStandAttempt, String> _f$userId =
      Field('userId', _$userId);
  static String _$gender(OneLegStandAttempt v) => v.gender;
  static const Field<OneLegStandAttempt, String> _f$gender =
      Field('gender', _$gender);
  static String _$cohortId(OneLegStandAttempt v) => v.cohortId;
  static const Field<OneLegStandAttempt, String> _f$cohortId =
      Field('cohortId', _$cohortId);
  static int _$resultInSeconds(OneLegStandAttempt v) => v.resultInSeconds;
  static const Field<OneLegStandAttempt, int> _f$resultInSeconds =
      Field('resultInSeconds', _$resultInSeconds);
  static Foot _$foot(OneLegStandAttempt v) => v.foot;
  static const Field<OneLegStandAttempt, Foot> _f$foot = Field('foot', _$foot);

  @override
  final MappableFields<OneLegStandAttempt> fields = const {
    #id: _f$id,
    #points: _f$points,
    #attemptNumber: _f$attemptNumber,
    #momentUtc: _f$momentUtc,
    #userId: _f$userId,
    #gender: _f$gender,
    #cohortId: _f$cohortId,
    #resultInSeconds: _f$resultInSeconds,
    #foot: _f$foot,
  };

  @override
  final String discriminatorKey = 'discipline';
  @override
  final dynamic discriminatorValue = "oneLegStand";
  @override
  late final ClassMapperBase superMapper = AttemptMapper.ensureInitialized();

  static OneLegStandAttempt _instantiate(DecodingData data) {
    return OneLegStandAttempt(
        id: data.dec(_f$id),
        points: data.dec(_f$points),
        attemptNumber: data.dec(_f$attemptNumber),
        momentUtc: data.dec(_f$momentUtc),
        userId: data.dec(_f$userId),
        gender: data.dec(_f$gender),
        cohortId: data.dec(_f$cohortId),
        resultInSeconds: data.dec(_f$resultInSeconds),
        foot: data.dec(_f$foot));
  }

  @override
  final Function instantiate = _instantiate;

  static OneLegStandAttempt fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<OneLegStandAttempt>(map);
  }

  static OneLegStandAttempt fromJson(String json) {
    return ensureInitialized().decodeJson<OneLegStandAttempt>(json);
  }
}

mixin OneLegStandAttemptMappable {
  String toJson() {
    return OneLegStandAttemptMapper.ensureInitialized()
        .encodeJson<OneLegStandAttempt>(this as OneLegStandAttempt);
  }

  Map<String, dynamic> toMap() {
    return OneLegStandAttemptMapper.ensureInitialized()
        .encodeMap<OneLegStandAttempt>(this as OneLegStandAttempt);
  }

  OneLegStandAttemptCopyWith<OneLegStandAttempt, OneLegStandAttempt,
          OneLegStandAttempt>
      get copyWith => _OneLegStandAttemptCopyWithImpl(
          this as OneLegStandAttempt, $identity, $identity);
  @override
  String toString() {
    return OneLegStandAttemptMapper.ensureInitialized()
        .stringifyValue(this as OneLegStandAttempt);
  }

  @override
  bool operator ==(Object other) {
    return OneLegStandAttemptMapper.ensureInitialized()
        .equalsValue(this as OneLegStandAttempt, other);
  }

  @override
  int get hashCode {
    return OneLegStandAttemptMapper.ensureInitialized()
        .hashValue(this as OneLegStandAttempt);
  }
}

extension OneLegStandAttemptValueCopy<$R, $Out>
    on ObjectCopyWith<$R, OneLegStandAttempt, $Out> {
  OneLegStandAttemptCopyWith<$R, OneLegStandAttempt, $Out>
      get $asOneLegStandAttempt =>
          $base.as((v, t, t2) => _OneLegStandAttemptCopyWithImpl(v, t, t2));
}

abstract class OneLegStandAttemptCopyWith<$R, $In extends OneLegStandAttempt,
    $Out> implements AttemptCopyWith<$R, $In, $Out> {
  @override
  $R call(
      {String? id,
      int? points,
      int? attemptNumber,
      DateTime? momentUtc,
      String? userId,
      String? gender,
      String? cohortId,
      int? resultInSeconds,
      Foot? foot});
  OneLegStandAttemptCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _OneLegStandAttemptCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, OneLegStandAttempt, $Out>
    implements OneLegStandAttemptCopyWith<$R, OneLegStandAttempt, $Out> {
  _OneLegStandAttemptCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<OneLegStandAttempt> $mapper =
      OneLegStandAttemptMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          int? points,
          int? attemptNumber,
          DateTime? momentUtc,
          String? userId,
          String? gender,
          String? cohortId,
          int? resultInSeconds,
          Foot? foot}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (points != null) #points: points,
        if (attemptNumber != null) #attemptNumber: attemptNumber,
        if (momentUtc != null) #momentUtc: momentUtc,
        if (userId != null) #userId: userId,
        if (gender != null) #gender: gender,
        if (cohortId != null) #cohortId: cohortId,
        if (resultInSeconds != null) #resultInSeconds: resultInSeconds,
        if (foot != null) #foot: foot
      }));
  @override
  OneLegStandAttempt $make(CopyWithData data) => OneLegStandAttempt(
      id: data.get(#id, or: $value.id),
      points: data.get(#points, or: $value.points),
      attemptNumber: data.get(#attemptNumber, or: $value.attemptNumber),
      momentUtc: data.get(#momentUtc, or: $value.momentUtc),
      userId: data.get(#userId, or: $value.userId),
      gender: data.get(#gender, or: $value.gender),
      cohortId: data.get(#cohortId, or: $value.cohortId),
      resultInSeconds: data.get(#resultInSeconds, or: $value.resultInSeconds),
      foot: data.get(#foot, or: $value.foot));

  @override
  OneLegStandAttemptCopyWith<$R2, OneLegStandAttempt, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _OneLegStandAttemptCopyWithImpl($value, $cast, t);
}
