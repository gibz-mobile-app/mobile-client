import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/fitness_check/foot_enum.dart';

class AttemptMeasurement extends Equatable {
  final int value;
  Foot? side;

  AttemptMeasurement({
    required this.value,
    this.side,
  });

  @override
  List<Object?> get props => [value];

  setSide(Foot foot) {
    side = foot;
  }
}
