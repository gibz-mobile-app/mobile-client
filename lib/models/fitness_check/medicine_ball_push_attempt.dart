import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';

part 'medicine_ball_push_attempt.mapper.dart';

@MappableClass(discriminatorValue: "medicineBallPush")
class MedicineBallPushAttempt extends Attempt
    with MedicineBallPushAttemptMappable {
  final int resultInCentimeters;

  const MedicineBallPushAttempt({
    required super.id,
    required super.points,
    required super.attemptNumber,
    required super.momentUtc,
    required super.userId,
    required super.gender,
    required super.cohortId,
    required this.resultInCentimeters,
  });

  static const fromMap = MedicineBallPushAttemptMapper.fromMap;
  static const fromJson = MedicineBallPushAttemptMapper.fromJson;

  double get resultInMeters => resultInCentimeters / 100.0;

  @override
  String get displayValue => '${resultInMeters.toStringAsFixed(2)} Meter';
}
