// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'twelve_minutes_run_attempt.dart';

class TwelveMinutesRunAttemptMapper
    extends SubClassMapperBase<TwelveMinutesRunAttempt> {
  TwelveMinutesRunAttemptMapper._();

  static TwelveMinutesRunAttemptMapper? _instance;
  static TwelveMinutesRunAttemptMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = TwelveMinutesRunAttemptMapper._());
      AttemptMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'TwelveMinutesRunAttempt';

  static String _$id(TwelveMinutesRunAttempt v) => v.id;
  static const Field<TwelveMinutesRunAttempt, String> _f$id = Field('id', _$id);
  static int _$points(TwelveMinutesRunAttempt v) => v.points;
  static const Field<TwelveMinutesRunAttempt, int> _f$points =
      Field('points', _$points);
  static int _$attemptNumber(TwelveMinutesRunAttempt v) => v.attemptNumber;
  static const Field<TwelveMinutesRunAttempt, int> _f$attemptNumber =
      Field('attemptNumber', _$attemptNumber);
  static DateTime _$momentUtc(TwelveMinutesRunAttempt v) => v.momentUtc;
  static const Field<TwelveMinutesRunAttempt, DateTime> _f$momentUtc =
      Field('momentUtc', _$momentUtc, hook: DateTimeUtcHook());
  static String _$userId(TwelveMinutesRunAttempt v) => v.userId;
  static const Field<TwelveMinutesRunAttempt, String> _f$userId =
      Field('userId', _$userId);
  static String _$gender(TwelveMinutesRunAttempt v) => v.gender;
  static const Field<TwelveMinutesRunAttempt, String> _f$gender =
      Field('gender', _$gender);
  static String _$cohortId(TwelveMinutesRunAttempt v) => v.cohortId;
  static const Field<TwelveMinutesRunAttempt, String> _f$cohortId =
      Field('cohortId', _$cohortId);
  static double _$resultInRounds(TwelveMinutesRunAttempt v) => v.resultInRounds;
  static const Field<TwelveMinutesRunAttempt, double> _f$resultInRounds =
      Field('resultInRounds', _$resultInRounds);

  @override
  final MappableFields<TwelveMinutesRunAttempt> fields = const {
    #id: _f$id,
    #points: _f$points,
    #attemptNumber: _f$attemptNumber,
    #momentUtc: _f$momentUtc,
    #userId: _f$userId,
    #gender: _f$gender,
    #cohortId: _f$cohortId,
    #resultInRounds: _f$resultInRounds,
  };

  @override
  final String discriminatorKey = 'discipline';
  @override
  final dynamic discriminatorValue = "twelveMinutesRun";
  @override
  late final ClassMapperBase superMapper = AttemptMapper.ensureInitialized();

  static TwelveMinutesRunAttempt _instantiate(DecodingData data) {
    return TwelveMinutesRunAttempt(
        id: data.dec(_f$id),
        points: data.dec(_f$points),
        attemptNumber: data.dec(_f$attemptNumber),
        momentUtc: data.dec(_f$momentUtc),
        userId: data.dec(_f$userId),
        gender: data.dec(_f$gender),
        cohortId: data.dec(_f$cohortId),
        resultInRounds: data.dec(_f$resultInRounds));
  }

  @override
  final Function instantiate = _instantiate;

  static TwelveMinutesRunAttempt fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<TwelveMinutesRunAttempt>(map);
  }

  static TwelveMinutesRunAttempt fromJson(String json) {
    return ensureInitialized().decodeJson<TwelveMinutesRunAttempt>(json);
  }
}

mixin TwelveMinutesRunAttemptMappable {
  String toJson() {
    return TwelveMinutesRunAttemptMapper.ensureInitialized()
        .encodeJson<TwelveMinutesRunAttempt>(this as TwelveMinutesRunAttempt);
  }

  Map<String, dynamic> toMap() {
    return TwelveMinutesRunAttemptMapper.ensureInitialized()
        .encodeMap<TwelveMinutesRunAttempt>(this as TwelveMinutesRunAttempt);
  }

  TwelveMinutesRunAttemptCopyWith<TwelveMinutesRunAttempt,
          TwelveMinutesRunAttempt, TwelveMinutesRunAttempt>
      get copyWith => _TwelveMinutesRunAttemptCopyWithImpl(
          this as TwelveMinutesRunAttempt, $identity, $identity);
  @override
  String toString() {
    return TwelveMinutesRunAttemptMapper.ensureInitialized()
        .stringifyValue(this as TwelveMinutesRunAttempt);
  }

  @override
  bool operator ==(Object other) {
    return TwelveMinutesRunAttemptMapper.ensureInitialized()
        .equalsValue(this as TwelveMinutesRunAttempt, other);
  }

  @override
  int get hashCode {
    return TwelveMinutesRunAttemptMapper.ensureInitialized()
        .hashValue(this as TwelveMinutesRunAttempt);
  }
}

extension TwelveMinutesRunAttemptValueCopy<$R, $Out>
    on ObjectCopyWith<$R, TwelveMinutesRunAttempt, $Out> {
  TwelveMinutesRunAttemptCopyWith<$R, TwelveMinutesRunAttempt, $Out>
      get $asTwelveMinutesRunAttempt => $base
          .as((v, t, t2) => _TwelveMinutesRunAttemptCopyWithImpl(v, t, t2));
}

abstract class TwelveMinutesRunAttemptCopyWith<
    $R,
    $In extends TwelveMinutesRunAttempt,
    $Out> implements AttemptCopyWith<$R, $In, $Out> {
  @override
  $R call(
      {String? id,
      int? points,
      int? attemptNumber,
      DateTime? momentUtc,
      String? userId,
      String? gender,
      String? cohortId,
      double? resultInRounds});
  TwelveMinutesRunAttemptCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _TwelveMinutesRunAttemptCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, TwelveMinutesRunAttempt, $Out>
    implements
        TwelveMinutesRunAttemptCopyWith<$R, TwelveMinutesRunAttempt, $Out> {
  _TwelveMinutesRunAttemptCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<TwelveMinutesRunAttempt> $mapper =
      TwelveMinutesRunAttemptMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          int? points,
          int? attemptNumber,
          DateTime? momentUtc,
          String? userId,
          String? gender,
          String? cohortId,
          double? resultInRounds}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (points != null) #points: points,
        if (attemptNumber != null) #attemptNumber: attemptNumber,
        if (momentUtc != null) #momentUtc: momentUtc,
        if (userId != null) #userId: userId,
        if (gender != null) #gender: gender,
        if (cohortId != null) #cohortId: cohortId,
        if (resultInRounds != null) #resultInRounds: resultInRounds
      }));
  @override
  TwelveMinutesRunAttempt $make(CopyWithData data) => TwelveMinutesRunAttempt(
      id: data.get(#id, or: $value.id),
      points: data.get(#points, or: $value.points),
      attemptNumber: data.get(#attemptNumber, or: $value.attemptNumber),
      momentUtc: data.get(#momentUtc, or: $value.momentUtc),
      userId: data.get(#userId, or: $value.userId),
      gender: data.get(#gender, or: $value.gender),
      cohortId: data.get(#cohortId, or: $value.cohortId),
      resultInRounds: data.get(#resultInRounds, or: $value.resultInRounds));

  @override
  TwelveMinutesRunAttemptCopyWith<$R2, TwelveMinutesRunAttempt, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _TwelveMinutesRunAttemptCopyWithImpl($value, $cast, t);
}
