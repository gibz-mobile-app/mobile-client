import 'package:dart_mappable/dart_mappable.dart';

part 'foot_enum.mapper.dart';

@MappableEnum(mode: ValuesMode.indexed)
enum Foot { left, right }
