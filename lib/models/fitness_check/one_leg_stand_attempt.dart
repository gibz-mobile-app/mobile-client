import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';
import 'package:gibz_mobileapp/models/fitness_check/foot_enum.dart';

part 'one_leg_stand_attempt.mapper.dart';

@MappableClass(discriminatorValue: "oneLegStand")
class OneLegStandAttempt extends Attempt with OneLegStandAttemptMappable {
  final int resultInSeconds;
  final Foot foot;

  const OneLegStandAttempt({
    required super.id,
    required super.points,
    required super.attemptNumber,
    required super.momentUtc,
    required super.userId,
    required super.gender,
    required super.cohortId,
    required this.resultInSeconds,
    required this.foot,
  });

  static const fromMap = OneLegStandAttemptMapper.fromMap;
  static const fromJson = OneLegStandAttemptMapper.fromJson;

  @override
  String get displayValue => '$resultInSeconds Sekunden';
}
