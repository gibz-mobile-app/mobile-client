// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'standing_long_jump_attempt.dart';

class StandingLongJumpAttemptMapper
    extends SubClassMapperBase<StandingLongJumpAttempt> {
  StandingLongJumpAttemptMapper._();

  static StandingLongJumpAttemptMapper? _instance;
  static StandingLongJumpAttemptMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = StandingLongJumpAttemptMapper._());
      AttemptMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'StandingLongJumpAttempt';

  static String _$id(StandingLongJumpAttempt v) => v.id;
  static const Field<StandingLongJumpAttempt, String> _f$id = Field('id', _$id);
  static int _$points(StandingLongJumpAttempt v) => v.points;
  static const Field<StandingLongJumpAttempt, int> _f$points =
      Field('points', _$points);
  static int _$attemptNumber(StandingLongJumpAttempt v) => v.attemptNumber;
  static const Field<StandingLongJumpAttempt, int> _f$attemptNumber =
      Field('attemptNumber', _$attemptNumber);
  static DateTime _$momentUtc(StandingLongJumpAttempt v) => v.momentUtc;
  static const Field<StandingLongJumpAttempt, DateTime> _f$momentUtc =
      Field('momentUtc', _$momentUtc, hook: DateTimeUtcHook());
  static String _$userId(StandingLongJumpAttempt v) => v.userId;
  static const Field<StandingLongJumpAttempt, String> _f$userId =
      Field('userId', _$userId);
  static String _$gender(StandingLongJumpAttempt v) => v.gender;
  static const Field<StandingLongJumpAttempt, String> _f$gender =
      Field('gender', _$gender);
  static String _$cohortId(StandingLongJumpAttempt v) => v.cohortId;
  static const Field<StandingLongJumpAttempt, String> _f$cohortId =
      Field('cohortId', _$cohortId);
  static int _$resultInCentimeters(StandingLongJumpAttempt v) =>
      v.resultInCentimeters;
  static const Field<StandingLongJumpAttempt, int> _f$resultInCentimeters =
      Field('resultInCentimeters', _$resultInCentimeters);

  @override
  final MappableFields<StandingLongJumpAttempt> fields = const {
    #id: _f$id,
    #points: _f$points,
    #attemptNumber: _f$attemptNumber,
    #momentUtc: _f$momentUtc,
    #userId: _f$userId,
    #gender: _f$gender,
    #cohortId: _f$cohortId,
    #resultInCentimeters: _f$resultInCentimeters,
  };

  @override
  final String discriminatorKey = 'discipline';
  @override
  final dynamic discriminatorValue = "standingLongJump";
  @override
  late final ClassMapperBase superMapper = AttemptMapper.ensureInitialized();

  static StandingLongJumpAttempt _instantiate(DecodingData data) {
    return StandingLongJumpAttempt(
        id: data.dec(_f$id),
        points: data.dec(_f$points),
        attemptNumber: data.dec(_f$attemptNumber),
        momentUtc: data.dec(_f$momentUtc),
        userId: data.dec(_f$userId),
        gender: data.dec(_f$gender),
        cohortId: data.dec(_f$cohortId),
        resultInCentimeters: data.dec(_f$resultInCentimeters));
  }

  @override
  final Function instantiate = _instantiate;

  static StandingLongJumpAttempt fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<StandingLongJumpAttempt>(map);
  }

  static StandingLongJumpAttempt fromJson(String json) {
    return ensureInitialized().decodeJson<StandingLongJumpAttempt>(json);
  }
}

mixin StandingLongJumpAttemptMappable {
  String toJson() {
    return StandingLongJumpAttemptMapper.ensureInitialized()
        .encodeJson<StandingLongJumpAttempt>(this as StandingLongJumpAttempt);
  }

  Map<String, dynamic> toMap() {
    return StandingLongJumpAttemptMapper.ensureInitialized()
        .encodeMap<StandingLongJumpAttempt>(this as StandingLongJumpAttempt);
  }

  StandingLongJumpAttemptCopyWith<StandingLongJumpAttempt,
          StandingLongJumpAttempt, StandingLongJumpAttempt>
      get copyWith => _StandingLongJumpAttemptCopyWithImpl(
          this as StandingLongJumpAttempt, $identity, $identity);
  @override
  String toString() {
    return StandingLongJumpAttemptMapper.ensureInitialized()
        .stringifyValue(this as StandingLongJumpAttempt);
  }

  @override
  bool operator ==(Object other) {
    return StandingLongJumpAttemptMapper.ensureInitialized()
        .equalsValue(this as StandingLongJumpAttempt, other);
  }

  @override
  int get hashCode {
    return StandingLongJumpAttemptMapper.ensureInitialized()
        .hashValue(this as StandingLongJumpAttempt);
  }
}

extension StandingLongJumpAttemptValueCopy<$R, $Out>
    on ObjectCopyWith<$R, StandingLongJumpAttempt, $Out> {
  StandingLongJumpAttemptCopyWith<$R, StandingLongJumpAttempt, $Out>
      get $asStandingLongJumpAttempt => $base
          .as((v, t, t2) => _StandingLongJumpAttemptCopyWithImpl(v, t, t2));
}

abstract class StandingLongJumpAttemptCopyWith<
    $R,
    $In extends StandingLongJumpAttempt,
    $Out> implements AttemptCopyWith<$R, $In, $Out> {
  @override
  $R call(
      {String? id,
      int? points,
      int? attemptNumber,
      DateTime? momentUtc,
      String? userId,
      String? gender,
      String? cohortId,
      int? resultInCentimeters});
  StandingLongJumpAttemptCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _StandingLongJumpAttemptCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, StandingLongJumpAttempt, $Out>
    implements
        StandingLongJumpAttemptCopyWith<$R, StandingLongJumpAttempt, $Out> {
  _StandingLongJumpAttemptCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<StandingLongJumpAttempt> $mapper =
      StandingLongJumpAttemptMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          int? points,
          int? attemptNumber,
          DateTime? momentUtc,
          String? userId,
          String? gender,
          String? cohortId,
          int? resultInCentimeters}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (points != null) #points: points,
        if (attemptNumber != null) #attemptNumber: attemptNumber,
        if (momentUtc != null) #momentUtc: momentUtc,
        if (userId != null) #userId: userId,
        if (gender != null) #gender: gender,
        if (cohortId != null) #cohortId: cohortId,
        if (resultInCentimeters != null)
          #resultInCentimeters: resultInCentimeters
      }));
  @override
  StandingLongJumpAttempt $make(CopyWithData data) => StandingLongJumpAttempt(
      id: data.get(#id, or: $value.id),
      points: data.get(#points, or: $value.points),
      attemptNumber: data.get(#attemptNumber, or: $value.attemptNumber),
      momentUtc: data.get(#momentUtc, or: $value.momentUtc),
      userId: data.get(#userId, or: $value.userId),
      gender: data.get(#gender, or: $value.gender),
      cohortId: data.get(#cohortId, or: $value.cohortId),
      resultInCentimeters:
          data.get(#resultInCentimeters, or: $value.resultInCentimeters));

  @override
  StandingLongJumpAttemptCopyWith<$R2, StandingLongJumpAttempt, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _StandingLongJumpAttemptCopyWithImpl($value, $cast, t);
}
