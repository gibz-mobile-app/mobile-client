// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'attempt.dart';

class AttemptMapper extends ClassMapperBase<Attempt> {
  AttemptMapper._();

  static AttemptMapper? _instance;
  static AttemptMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AttemptMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'Attempt';

  static String _$id(Attempt v) => v.id;
  static const Field<Attempt, String> _f$id = Field('id', _$id);
  static int _$points(Attempt v) => v.points;
  static const Field<Attempt, int> _f$points = Field('points', _$points);
  static int _$attemptNumber(Attempt v) => v.attemptNumber;
  static const Field<Attempt, int> _f$attemptNumber =
      Field('attemptNumber', _$attemptNumber);
  static DateTime _$momentUtc(Attempt v) => v.momentUtc;
  static const Field<Attempt, DateTime> _f$momentUtc =
      Field('momentUtc', _$momentUtc, hook: DateTimeUtcHook());
  static String _$userId(Attempt v) => v.userId;
  static const Field<Attempt, String> _f$userId = Field('userId', _$userId);
  static String _$gender(Attempt v) => v.gender;
  static const Field<Attempt, String> _f$gender = Field('gender', _$gender);
  static String _$cohortId(Attempt v) => v.cohortId;
  static const Field<Attempt, String> _f$cohortId =
      Field('cohortId', _$cohortId);

  @override
  final MappableFields<Attempt> fields = const {
    #id: _f$id,
    #points: _f$points,
    #attemptNumber: _f$attemptNumber,
    #momentUtc: _f$momentUtc,
    #userId: _f$userId,
    #gender: _f$gender,
    #cohortId: _f$cohortId,
  };

  static Attempt _instantiate(DecodingData data) {
    throw MapperException.missingConstructor('Attempt');
  }

  @override
  final Function instantiate = _instantiate;

  static Attempt fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Attempt>(map);
  }

  static Attempt fromJson(String json) {
    return ensureInitialized().decodeJson<Attempt>(json);
  }
}

mixin AttemptMappable {
  String toJson();
  Map<String, dynamic> toMap();
  AttemptCopyWith<Attempt, Attempt, Attempt> get copyWith;
}

abstract class AttemptCopyWith<$R, $In extends Attempt, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call(
      {String? id,
      int? points,
      int? attemptNumber,
      DateTime? momentUtc,
      String? userId,
      String? gender,
      String? cohortId});
  AttemptCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}
