import 'package:equatable/equatable.dart';
import 'package:jwt_decoder/jwt_decoder.dart';

class AuthenticatedUser extends Equatable {
  final String id;
  final String username;
  final String firstName;
  final String lastName;

  const AuthenticatedUser({
    required this.id,
    required this.username,
    required this.firstName,
    required this.lastName,
  });

  factory AuthenticatedUser.fromIdToken(String idToken) {
    final claims = JwtDecoder.decode(idToken);
    if (!claims.containsKey('preferred_username')) {
      throw ArgumentError(
          'The given token does not contain a claim named "preferred_username".');
    }

    return AuthenticatedUser(
      id: claims['sub'],
      username: claims['preferred_username'],
      firstName: claims['given_name'],
      lastName: claims['family_name'],
    );
  }

  @override
  List<Object?> get props => [username];
}
