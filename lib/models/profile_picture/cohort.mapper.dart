// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'cohort.dart';

class CohortMapper extends ClassMapperBase<Cohort> {
  CohortMapper._();

  static CohortMapper? _instance;
  static CohortMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = CohortMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'Cohort';

  static String _$id(Cohort v) => v.id;
  static const Field<Cohort, String> _f$id = Field('id', _$id);
  static String _$uniqueName(Cohort v) => v.uniqueName;
  static const Field<Cohort, String> _f$uniqueName =
      Field('uniqueName', _$uniqueName, key: 'unique_name');
  static String _$friendlyName(Cohort v) => v.friendlyName;
  static const Field<Cohort, String> _f$friendlyName =
      Field('friendlyName', _$friendlyName, key: 'friendly_name');

  @override
  final MappableFields<Cohort> fields = const {
    #id: _f$id,
    #uniqueName: _f$uniqueName,
    #friendlyName: _f$friendlyName,
  };

  static Cohort _instantiate(DecodingData data) {
    return Cohort(
        id: data.dec(_f$id),
        uniqueName: data.dec(_f$uniqueName),
        friendlyName: data.dec(_f$friendlyName));
  }

  @override
  final Function instantiate = _instantiate;

  static Cohort fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Cohort>(map);
  }

  static Cohort fromJson(String json) {
    return ensureInitialized().decodeJson<Cohort>(json);
  }
}

mixin CohortMappable {
  String toJson() {
    return CohortMapper.ensureInitialized().encodeJson<Cohort>(this as Cohort);
  }

  Map<String, dynamic> toMap() {
    return CohortMapper.ensureInitialized().encodeMap<Cohort>(this as Cohort);
  }

  CohortCopyWith<Cohort, Cohort, Cohort> get copyWith =>
      _CohortCopyWithImpl(this as Cohort, $identity, $identity);
  @override
  String toString() {
    return CohortMapper.ensureInitialized().stringifyValue(this as Cohort);
  }

  @override
  bool operator ==(Object other) {
    return CohortMapper.ensureInitialized().equalsValue(this as Cohort, other);
  }

  @override
  int get hashCode {
    return CohortMapper.ensureInitialized().hashValue(this as Cohort);
  }
}

extension CohortValueCopy<$R, $Out> on ObjectCopyWith<$R, Cohort, $Out> {
  CohortCopyWith<$R, Cohort, $Out> get $asCohort =>
      $base.as((v, t, t2) => _CohortCopyWithImpl(v, t, t2));
}

abstract class CohortCopyWith<$R, $In extends Cohort, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? uniqueName, String? friendlyName});
  CohortCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _CohortCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Cohort, $Out>
    implements CohortCopyWith<$R, Cohort, $Out> {
  _CohortCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Cohort> $mapper = CohortMapper.ensureInitialized();
  @override
  $R call({String? id, String? uniqueName, String? friendlyName}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (uniqueName != null) #uniqueName: uniqueName,
        if (friendlyName != null) #friendlyName: friendlyName
      }));
  @override
  Cohort $make(CopyWithData data) => Cohort(
      id: data.get(#id, or: $value.id),
      uniqueName: data.get(#uniqueName, or: $value.uniqueName),
      friendlyName: data.get(#friendlyName, or: $value.friendlyName));

  @override
  CohortCopyWith<$R2, Cohort, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _CohortCopyWithImpl($value, $cast, t);
}
