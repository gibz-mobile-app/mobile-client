import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'token_verification_result.mapper.dart';

@MappableClass()
abstract class TokenVerificationResult extends Equatable
    with TokenVerificationResultMappable {
  final String token;
  final String requestId;
  final int requestState;
  final DateTime requestExpiry;

  TokenVerificationResult({
    required this.token,
    required this.requestId,
    required this.requestState,
    required this.requestExpiry,
  });

  @override
  List<Object?> get props => throw UnimplementedError();

  static const fromMap = TokenVerificationResultMapper.fromMap;
  static const fromJson = TokenVerificationResultMapper.fromJson;
}
