// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'failed_token_verification_result.dart';

class FailedTokenVerificationResultMapper
    extends SubClassMapperBase<FailedTokenVerificationResult> {
  FailedTokenVerificationResultMapper._();

  static FailedTokenVerificationResultMapper? _instance;
  static FailedTokenVerificationResultMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = FailedTokenVerificationResultMapper._());
      TokenVerificationResultMapper.ensureInitialized()
          .addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'FailedTokenVerificationResult';

  static String _$token(FailedTokenVerificationResult v) => v.token;
  static const Field<FailedTokenVerificationResult, String> _f$token =
      Field('token', _$token);
  static String _$requestId(FailedTokenVerificationResult v) => v.requestId;
  static const Field<FailedTokenVerificationResult, String> _f$requestId =
      Field('requestId', _$requestId);
  static int _$requestState(FailedTokenVerificationResult v) => v.requestState;
  static const Field<FailedTokenVerificationResult, int> _f$requestState =
      Field('requestState', _$requestState);
  static DateTime _$requestExpiry(FailedTokenVerificationResult v) =>
      v.requestExpiry;
  static const Field<FailedTokenVerificationResult, DateTime> _f$requestExpiry =
      Field('requestExpiry', _$requestExpiry);
  static int _$statusCode(FailedTokenVerificationResult v) => v.statusCode;
  static const Field<FailedTokenVerificationResult, int> _f$statusCode =
      Field('statusCode', _$statusCode);
  static String _$statusMessage(FailedTokenVerificationResult v) =>
      v.statusMessage;
  static const Field<FailedTokenVerificationResult, String> _f$statusMessage =
      Field('statusMessage', _$statusMessage);

  @override
  final MappableFields<FailedTokenVerificationResult> fields = const {
    #token: _f$token,
    #requestId: _f$requestId,
    #requestState: _f$requestState,
    #requestExpiry: _f$requestExpiry,
    #statusCode: _f$statusCode,
    #statusMessage: _f$statusMessage,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = FailedTokenVerificationResult.checkType;
  @override
  late final ClassMapperBase superMapper =
      TokenVerificationResultMapper.ensureInitialized();

  static FailedTokenVerificationResult _instantiate(DecodingData data) {
    return FailedTokenVerificationResult(
        token: data.dec(_f$token),
        requestId: data.dec(_f$requestId),
        requestState: data.dec(_f$requestState),
        requestExpiry: data.dec(_f$requestExpiry),
        statusCode: data.dec(_f$statusCode),
        statusMessage: data.dec(_f$statusMessage));
  }

  @override
  final Function instantiate = _instantiate;

  static FailedTokenVerificationResult fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<FailedTokenVerificationResult>(map);
  }

  static FailedTokenVerificationResult fromJson(String json) {
    return ensureInitialized().decodeJson<FailedTokenVerificationResult>(json);
  }
}

mixin FailedTokenVerificationResultMappable {
  String toJson() {
    return FailedTokenVerificationResultMapper.ensureInitialized()
        .encodeJson<FailedTokenVerificationResult>(
            this as FailedTokenVerificationResult);
  }

  Map<String, dynamic> toMap() {
    return FailedTokenVerificationResultMapper.ensureInitialized()
        .encodeMap<FailedTokenVerificationResult>(
            this as FailedTokenVerificationResult);
  }

  FailedTokenVerificationResultCopyWith<FailedTokenVerificationResult,
          FailedTokenVerificationResult, FailedTokenVerificationResult>
      get copyWith => _FailedTokenVerificationResultCopyWithImpl(
          this as FailedTokenVerificationResult, $identity, $identity);
  @override
  String toString() {
    return FailedTokenVerificationResultMapper.ensureInitialized()
        .stringifyValue(this as FailedTokenVerificationResult);
  }

  @override
  bool operator ==(Object other) {
    return FailedTokenVerificationResultMapper.ensureInitialized()
        .equalsValue(this as FailedTokenVerificationResult, other);
  }

  @override
  int get hashCode {
    return FailedTokenVerificationResultMapper.ensureInitialized()
        .hashValue(this as FailedTokenVerificationResult);
  }
}

extension FailedTokenVerificationResultValueCopy<$R, $Out>
    on ObjectCopyWith<$R, FailedTokenVerificationResult, $Out> {
  FailedTokenVerificationResultCopyWith<$R, FailedTokenVerificationResult, $Out>
      get $asFailedTokenVerificationResult => $base.as(
          (v, t, t2) => _FailedTokenVerificationResultCopyWithImpl(v, t, t2));
}

abstract class FailedTokenVerificationResultCopyWith<
    $R,
    $In extends FailedTokenVerificationResult,
    $Out> implements TokenVerificationResultCopyWith<$R, $In, $Out> {
  @override
  $R call(
      {String? token,
      String? requestId,
      int? requestState,
      DateTime? requestExpiry,
      int? statusCode,
      String? statusMessage});
  FailedTokenVerificationResultCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _FailedTokenVerificationResultCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, FailedTokenVerificationResult, $Out>
    implements
        FailedTokenVerificationResultCopyWith<$R, FailedTokenVerificationResult,
            $Out> {
  _FailedTokenVerificationResultCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<FailedTokenVerificationResult> $mapper =
      FailedTokenVerificationResultMapper.ensureInitialized();
  @override
  $R call(
          {String? token,
          String? requestId,
          int? requestState,
          DateTime? requestExpiry,
          int? statusCode,
          String? statusMessage}) =>
      $apply(FieldCopyWithData({
        if (token != null) #token: token,
        if (requestId != null) #requestId: requestId,
        if (requestState != null) #requestState: requestState,
        if (requestExpiry != null) #requestExpiry: requestExpiry,
        if (statusCode != null) #statusCode: statusCode,
        if (statusMessage != null) #statusMessage: statusMessage
      }));
  @override
  FailedTokenVerificationResult $make(CopyWithData data) =>
      FailedTokenVerificationResult(
          token: data.get(#token, or: $value.token),
          requestId: data.get(#requestId, or: $value.requestId),
          requestState: data.get(#requestState, or: $value.requestState),
          requestExpiry: data.get(#requestExpiry, or: $value.requestExpiry),
          statusCode: data.get(#statusCode, or: $value.statusCode),
          statusMessage: data.get(#statusMessage, or: $value.statusMessage));

  @override
  FailedTokenVerificationResultCopyWith<$R2, FailedTokenVerificationResult,
      $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _FailedTokenVerificationResultCopyWithImpl($value, $cast, t);
}
