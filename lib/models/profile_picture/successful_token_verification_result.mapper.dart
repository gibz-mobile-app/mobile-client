// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'successful_token_verification_result.dart';

class SuccessfulTokenVerificationResultMapper
    extends SubClassMapperBase<SuccessfulTokenVerificationResult> {
  SuccessfulTokenVerificationResultMapper._();

  static SuccessfulTokenVerificationResultMapper? _instance;
  static SuccessfulTokenVerificationResultMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = SuccessfulTokenVerificationResultMapper._());
      TokenVerificationResultMapper.ensureInitialized()
          .addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'SuccessfulTokenVerificationResult';

  static String _$token(SuccessfulTokenVerificationResult v) => v.token;
  static const Field<SuccessfulTokenVerificationResult, String> _f$token =
      Field('token', _$token);
  static String _$requestId(SuccessfulTokenVerificationResult v) => v.requestId;
  static const Field<SuccessfulTokenVerificationResult, String> _f$requestId =
      Field('requestId', _$requestId);
  static int _$requestState(SuccessfulTokenVerificationResult v) =>
      v.requestState;
  static const Field<SuccessfulTokenVerificationResult, int> _f$requestState =
      Field('requestState', _$requestState);
  static DateTime _$requestExpiry(SuccessfulTokenVerificationResult v) =>
      v.requestExpiry;
  static const Field<SuccessfulTokenVerificationResult, DateTime>
      _f$requestExpiry = Field('requestExpiry', _$requestExpiry);
  static String _$firstName(SuccessfulTokenVerificationResult v) => v.firstName;
  static const Field<SuccessfulTokenVerificationResult, String> _f$firstName =
      Field('firstName', _$firstName);
  static String _$lastName(SuccessfulTokenVerificationResult v) => v.lastName;
  static const Field<SuccessfulTokenVerificationResult, String> _f$lastName =
      Field('lastName', _$lastName);
  static String _$cohortUniqueName(SuccessfulTokenVerificationResult v) =>
      v.cohortUniqueName;
  static const Field<SuccessfulTokenVerificationResult, String>
      _f$cohortUniqueName = Field('cohortUniqueName', _$cohortUniqueName);
  static String _$cohortDisplayName(SuccessfulTokenVerificationResult v) =>
      v.cohortDisplayName;
  static const Field<SuccessfulTokenVerificationResult, String>
      _f$cohortDisplayName = Field('cohortDisplayName', _$cohortDisplayName);

  @override
  final MappableFields<SuccessfulTokenVerificationResult> fields = const {
    #token: _f$token,
    #requestId: _f$requestId,
    #requestState: _f$requestState,
    #requestExpiry: _f$requestExpiry,
    #firstName: _f$firstName,
    #lastName: _f$lastName,
    #cohortUniqueName: _f$cohortUniqueName,
    #cohortDisplayName: _f$cohortDisplayName,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue =
      SuccessfulTokenVerificationResult.checkType;
  @override
  late final ClassMapperBase superMapper =
      TokenVerificationResultMapper.ensureInitialized();

  static SuccessfulTokenVerificationResult _instantiate(DecodingData data) {
    return SuccessfulTokenVerificationResult(
        token: data.dec(_f$token),
        requestId: data.dec(_f$requestId),
        requestState: data.dec(_f$requestState),
        requestExpiry: data.dec(_f$requestExpiry),
        firstName: data.dec(_f$firstName),
        lastName: data.dec(_f$lastName),
        cohortUniqueName: data.dec(_f$cohortUniqueName),
        cohortDisplayName: data.dec(_f$cohortDisplayName));
  }

  @override
  final Function instantiate = _instantiate;

  static SuccessfulTokenVerificationResult fromMap(Map<String, dynamic> map) {
    return ensureInitialized()
        .decodeMap<SuccessfulTokenVerificationResult>(map);
  }

  static SuccessfulTokenVerificationResult fromJson(String json) {
    return ensureInitialized()
        .decodeJson<SuccessfulTokenVerificationResult>(json);
  }
}

mixin SuccessfulTokenVerificationResultMappable {
  String toJson() {
    return SuccessfulTokenVerificationResultMapper.ensureInitialized()
        .encodeJson<SuccessfulTokenVerificationResult>(
            this as SuccessfulTokenVerificationResult);
  }

  Map<String, dynamic> toMap() {
    return SuccessfulTokenVerificationResultMapper.ensureInitialized()
        .encodeMap<SuccessfulTokenVerificationResult>(
            this as SuccessfulTokenVerificationResult);
  }

  SuccessfulTokenVerificationResultCopyWith<SuccessfulTokenVerificationResult,
          SuccessfulTokenVerificationResult, SuccessfulTokenVerificationResult>
      get copyWith => _SuccessfulTokenVerificationResultCopyWithImpl(
          this as SuccessfulTokenVerificationResult, $identity, $identity);
  @override
  String toString() {
    return SuccessfulTokenVerificationResultMapper.ensureInitialized()
        .stringifyValue(this as SuccessfulTokenVerificationResult);
  }

  @override
  bool operator ==(Object other) {
    return SuccessfulTokenVerificationResultMapper.ensureInitialized()
        .equalsValue(this as SuccessfulTokenVerificationResult, other);
  }

  @override
  int get hashCode {
    return SuccessfulTokenVerificationResultMapper.ensureInitialized()
        .hashValue(this as SuccessfulTokenVerificationResult);
  }
}

extension SuccessfulTokenVerificationResultValueCopy<$R, $Out>
    on ObjectCopyWith<$R, SuccessfulTokenVerificationResult, $Out> {
  SuccessfulTokenVerificationResultCopyWith<$R,
          SuccessfulTokenVerificationResult, $Out>
      get $asSuccessfulTokenVerificationResult => $base.as((v, t, t2) =>
          _SuccessfulTokenVerificationResultCopyWithImpl(v, t, t2));
}

abstract class SuccessfulTokenVerificationResultCopyWith<
    $R,
    $In extends SuccessfulTokenVerificationResult,
    $Out> implements TokenVerificationResultCopyWith<$R, $In, $Out> {
  @override
  $R call(
      {String? token,
      String? requestId,
      int? requestState,
      DateTime? requestExpiry,
      String? firstName,
      String? lastName,
      String? cohortUniqueName,
      String? cohortDisplayName});
  SuccessfulTokenVerificationResultCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _SuccessfulTokenVerificationResultCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, SuccessfulTokenVerificationResult, $Out>
    implements
        SuccessfulTokenVerificationResultCopyWith<$R,
            SuccessfulTokenVerificationResult, $Out> {
  _SuccessfulTokenVerificationResultCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<SuccessfulTokenVerificationResult> $mapper =
      SuccessfulTokenVerificationResultMapper.ensureInitialized();
  @override
  $R call(
          {String? token,
          String? requestId,
          int? requestState,
          DateTime? requestExpiry,
          String? firstName,
          String? lastName,
          String? cohortUniqueName,
          String? cohortDisplayName}) =>
      $apply(FieldCopyWithData({
        if (token != null) #token: token,
        if (requestId != null) #requestId: requestId,
        if (requestState != null) #requestState: requestState,
        if (requestExpiry != null) #requestExpiry: requestExpiry,
        if (firstName != null) #firstName: firstName,
        if (lastName != null) #lastName: lastName,
        if (cohortUniqueName != null) #cohortUniqueName: cohortUniqueName,
        if (cohortDisplayName != null) #cohortDisplayName: cohortDisplayName
      }));
  @override
  SuccessfulTokenVerificationResult $make(CopyWithData data) =>
      SuccessfulTokenVerificationResult(
          token: data.get(#token, or: $value.token),
          requestId: data.get(#requestId, or: $value.requestId),
          requestState: data.get(#requestState, or: $value.requestState),
          requestExpiry: data.get(#requestExpiry, or: $value.requestExpiry),
          firstName: data.get(#firstName, or: $value.firstName),
          lastName: data.get(#lastName, or: $value.lastName),
          cohortUniqueName:
              data.get(#cohortUniqueName, or: $value.cohortUniqueName),
          cohortDisplayName:
              data.get(#cohortDisplayName, or: $value.cohortDisplayName));

  @override
  SuccessfulTokenVerificationResultCopyWith<$R2,
      SuccessfulTokenVerificationResult, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _SuccessfulTokenVerificationResultCopyWithImpl($value, $cast, t);
}
