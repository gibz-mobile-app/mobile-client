import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'cohort.mapper.dart';

@MappableClass(caseStyle: CaseStyle.snakeCase)
class Cohort extends Equatable with CohortMappable {
  final String id;
  final String uniqueName;
  final String friendlyName;

  const Cohort({
    required this.id,
    required this.uniqueName,
    required this.friendlyName,
  });

  @override
  List<Object?> get props => [id];
}
