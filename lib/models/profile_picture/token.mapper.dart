// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'token.dart';

class TokenMapper extends ClassMapperBase<Token> {
  TokenMapper._();

  static TokenMapper? _instance;
  static TokenMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = TokenMapper._());
      StudentMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'Token';

  static String _$id(Token v) => v.id;
  static const Field<Token, String> _f$id = Field('id', _$id, key: 'token');
  static bool _$isUsed(Token v) => v.isUsed;
  static const Field<Token, bool> _f$isUsed =
      Field('isUsed', _$isUsed, key: 'is_used');
  static Student _$student(Token v) => v.student;
  static const Field<Token, Student> _f$student = Field('student', _$student);

  @override
  final MappableFields<Token> fields = const {
    #id: _f$id,
    #isUsed: _f$isUsed,
    #student: _f$student,
  };

  static Token _instantiate(DecodingData data) {
    return Token(
        id: data.dec(_f$id),
        isUsed: data.dec(_f$isUsed),
        student: data.dec(_f$student));
  }

  @override
  final Function instantiate = _instantiate;

  static Token fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Token>(map);
  }

  static Token fromJson(String json) {
    return ensureInitialized().decodeJson<Token>(json);
  }
}

mixin TokenMappable {
  String toJson() {
    return TokenMapper.ensureInitialized().encodeJson<Token>(this as Token);
  }

  Map<String, dynamic> toMap() {
    return TokenMapper.ensureInitialized().encodeMap<Token>(this as Token);
  }

  TokenCopyWith<Token, Token, Token> get copyWith =>
      _TokenCopyWithImpl(this as Token, $identity, $identity);
  @override
  String toString() {
    return TokenMapper.ensureInitialized().stringifyValue(this as Token);
  }

  @override
  bool operator ==(Object other) {
    return TokenMapper.ensureInitialized().equalsValue(this as Token, other);
  }

  @override
  int get hashCode {
    return TokenMapper.ensureInitialized().hashValue(this as Token);
  }
}

extension TokenValueCopy<$R, $Out> on ObjectCopyWith<$R, Token, $Out> {
  TokenCopyWith<$R, Token, $Out> get $asToken =>
      $base.as((v, t, t2) => _TokenCopyWithImpl(v, t, t2));
}

abstract class TokenCopyWith<$R, $In extends Token, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  StudentCopyWith<$R, Student, Student> get student;
  $R call({String? id, bool? isUsed, Student? student});
  TokenCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _TokenCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Token, $Out>
    implements TokenCopyWith<$R, Token, $Out> {
  _TokenCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Token> $mapper = TokenMapper.ensureInitialized();
  @override
  StudentCopyWith<$R, Student, Student> get student =>
      $value.student.copyWith.$chain((v) => call(student: v));
  @override
  $R call({String? id, bool? isUsed, Student? student}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (isUsed != null) #isUsed: isUsed,
        if (student != null) #student: student
      }));
  @override
  Token $make(CopyWithData data) => Token(
      id: data.get(#id, or: $value.id),
      isUsed: data.get(#isUsed, or: $value.isUsed),
      student: data.get(#student, or: $value.student));

  @override
  TokenCopyWith<$R2, Token, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _TokenCopyWithImpl($value, $cast, t);
}
