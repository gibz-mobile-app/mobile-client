// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'student.dart';

class StudentMapper extends ClassMapperBase<Student> {
  StudentMapper._();

  static StudentMapper? _instance;
  static StudentMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = StudentMapper._());
      CohortMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'Student';

  static String _$id(Student v) => v.id;
  static const Field<Student, String> _f$id = Field('id', _$id);
  static String _$gib(Student v) => v.gib;
  static const Field<Student, String> _f$gib = Field('gib', _$gib);
  static String _$username(Student v) => v.username;
  static const Field<Student, String> _f$username =
      Field('username', _$username);
  static String _$firstName(Student v) => v.firstName;
  static const Field<Student, String> _f$firstName =
      Field('firstName', _$firstName, key: 'first_name');
  static String _$lastName(Student v) => v.lastName;
  static const Field<Student, String> _f$lastName =
      Field('lastName', _$lastName, key: 'last_name');
  static Cohort _$cohort(Student v) => v.cohort;
  static const Field<Student, Cohort> _f$cohort =
      Field('cohort', _$cohort, key: 'class');

  @override
  final MappableFields<Student> fields = const {
    #id: _f$id,
    #gib: _f$gib,
    #username: _f$username,
    #firstName: _f$firstName,
    #lastName: _f$lastName,
    #cohort: _f$cohort,
  };

  static Student _instantiate(DecodingData data) {
    return Student(
        id: data.dec(_f$id),
        gib: data.dec(_f$gib),
        username: data.dec(_f$username),
        firstName: data.dec(_f$firstName),
        lastName: data.dec(_f$lastName),
        cohort: data.dec(_f$cohort));
  }

  @override
  final Function instantiate = _instantiate;

  static Student fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Student>(map);
  }

  static Student fromJson(String json) {
    return ensureInitialized().decodeJson<Student>(json);
  }
}

mixin StudentMappable {
  String toJson() {
    return StudentMapper.ensureInitialized()
        .encodeJson<Student>(this as Student);
  }

  Map<String, dynamic> toMap() {
    return StudentMapper.ensureInitialized()
        .encodeMap<Student>(this as Student);
  }

  StudentCopyWith<Student, Student, Student> get copyWith =>
      _StudentCopyWithImpl(this as Student, $identity, $identity);
  @override
  String toString() {
    return StudentMapper.ensureInitialized().stringifyValue(this as Student);
  }

  @override
  bool operator ==(Object other) {
    return StudentMapper.ensureInitialized()
        .equalsValue(this as Student, other);
  }

  @override
  int get hashCode {
    return StudentMapper.ensureInitialized().hashValue(this as Student);
  }
}

extension StudentValueCopy<$R, $Out> on ObjectCopyWith<$R, Student, $Out> {
  StudentCopyWith<$R, Student, $Out> get $asStudent =>
      $base.as((v, t, t2) => _StudentCopyWithImpl(v, t, t2));
}

abstract class StudentCopyWith<$R, $In extends Student, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  CohortCopyWith<$R, Cohort, Cohort> get cohort;
  $R call(
      {String? id,
      String? gib,
      String? username,
      String? firstName,
      String? lastName,
      Cohort? cohort});
  StudentCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _StudentCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, Student, $Out>
    implements StudentCopyWith<$R, Student, $Out> {
  _StudentCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Student> $mapper =
      StudentMapper.ensureInitialized();
  @override
  CohortCopyWith<$R, Cohort, Cohort> get cohort =>
      $value.cohort.copyWith.$chain((v) => call(cohort: v));
  @override
  $R call(
          {String? id,
          String? gib,
          String? username,
          String? firstName,
          String? lastName,
          Cohort? cohort}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (gib != null) #gib: gib,
        if (username != null) #username: username,
        if (firstName != null) #firstName: firstName,
        if (lastName != null) #lastName: lastName,
        if (cohort != null) #cohort: cohort
      }));
  @override
  Student $make(CopyWithData data) => Student(
      id: data.get(#id, or: $value.id),
      gib: data.get(#gib, or: $value.gib),
      username: data.get(#username, or: $value.username),
      firstName: data.get(#firstName, or: $value.firstName),
      lastName: data.get(#lastName, or: $value.lastName),
      cohort: data.get(#cohort, or: $value.cohort));

  @override
  StudentCopyWith<$R2, Student, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _StudentCopyWithImpl($value, $cast, t);
}
