import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/profile_picture/token_verification_result.dart';

part 'successful_token_verification_result.mapper.dart';

@MappableClass(discriminatorValue: SuccessfulTokenVerificationResult.checkType)
class SuccessfulTokenVerificationResult extends TokenVerificationResult
    with SuccessfulTokenVerificationResultMappable {
  final String firstName;
  final String lastName;
  final String cohortUniqueName;
  final String cohortDisplayName;

  SuccessfulTokenVerificationResult({
    required super.token,
    required super.requestId,
    required super.requestState,
    required super.requestExpiry,
    required this.firstName,
    required this.lastName,
    required this.cohortUniqueName,
    required this.cohortDisplayName,
  });

  static bool checkType(value) {
    return value is Map && value.keys.contains('firstName');
  }

  static const fromMap = SuccessfulTokenVerificationResultMapper.fromMap;
  static const fromJson = SuccessfulTokenVerificationResultMapper.fromJson;
}
