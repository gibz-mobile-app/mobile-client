import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'profile_picture_submission_error.mapper.dart';

@MappableClass()
class ProfilePictureSubmissionError extends Equatable
    with ProfilePictureSubmissionErrorMappable {
  final String type;
  final int errorCode;
  final String message;
  final String? details;

  const ProfilePictureSubmissionError({
    required this.type,
    required this.errorCode,
    required this.message,
    required this.details,
  });

  @override
  List<Object?> get props => [type, errorCode, message];

  static const fromMap = ProfilePictureSubmissionErrorMapper.fromMap;
  static const fromJson = ProfilePictureSubmissionErrorMapper.fromJson;
}
