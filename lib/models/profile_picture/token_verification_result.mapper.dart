// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'token_verification_result.dart';

class TokenVerificationResultMapper
    extends ClassMapperBase<TokenVerificationResult> {
  TokenVerificationResultMapper._();

  static TokenVerificationResultMapper? _instance;
  static TokenVerificationResultMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = TokenVerificationResultMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'TokenVerificationResult';

  static String _$token(TokenVerificationResult v) => v.token;
  static const Field<TokenVerificationResult, String> _f$token =
      Field('token', _$token);
  static String _$requestId(TokenVerificationResult v) => v.requestId;
  static const Field<TokenVerificationResult, String> _f$requestId =
      Field('requestId', _$requestId);
  static int _$requestState(TokenVerificationResult v) => v.requestState;
  static const Field<TokenVerificationResult, int> _f$requestState =
      Field('requestState', _$requestState);
  static DateTime _$requestExpiry(TokenVerificationResult v) => v.requestExpiry;
  static const Field<TokenVerificationResult, DateTime> _f$requestExpiry =
      Field('requestExpiry', _$requestExpiry);

  @override
  final MappableFields<TokenVerificationResult> fields = const {
    #token: _f$token,
    #requestId: _f$requestId,
    #requestState: _f$requestState,
    #requestExpiry: _f$requestExpiry,
  };

  static TokenVerificationResult _instantiate(DecodingData data) {
    throw MapperException.missingConstructor('TokenVerificationResult');
  }

  @override
  final Function instantiate = _instantiate;

  static TokenVerificationResult fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<TokenVerificationResult>(map);
  }

  static TokenVerificationResult fromJson(String json) {
    return ensureInitialized().decodeJson<TokenVerificationResult>(json);
  }
}

mixin TokenVerificationResultMappable {
  String toJson();
  Map<String, dynamic> toMap();
  TokenVerificationResultCopyWith<TokenVerificationResult,
      TokenVerificationResult, TokenVerificationResult> get copyWith;
}

abstract class TokenVerificationResultCopyWith<
    $R,
    $In extends TokenVerificationResult,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  $R call(
      {String? token,
      String? requestId,
      int? requestState,
      DateTime? requestExpiry});
  TokenVerificationResultCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}
