// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'profile_picture_submission_error.dart';

class ProfilePictureSubmissionErrorMapper
    extends ClassMapperBase<ProfilePictureSubmissionError> {
  ProfilePictureSubmissionErrorMapper._();

  static ProfilePictureSubmissionErrorMapper? _instance;
  static ProfilePictureSubmissionErrorMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = ProfilePictureSubmissionErrorMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'ProfilePictureSubmissionError';

  static String _$type(ProfilePictureSubmissionError v) => v.type;
  static const Field<ProfilePictureSubmissionError, String> _f$type =
      Field('type', _$type);
  static int _$errorCode(ProfilePictureSubmissionError v) => v.errorCode;
  static const Field<ProfilePictureSubmissionError, int> _f$errorCode =
      Field('errorCode', _$errorCode);
  static String _$message(ProfilePictureSubmissionError v) => v.message;
  static const Field<ProfilePictureSubmissionError, String> _f$message =
      Field('message', _$message);
  static String? _$details(ProfilePictureSubmissionError v) => v.details;
  static const Field<ProfilePictureSubmissionError, String> _f$details =
      Field('details', _$details);

  @override
  final MappableFields<ProfilePictureSubmissionError> fields = const {
    #type: _f$type,
    #errorCode: _f$errorCode,
    #message: _f$message,
    #details: _f$details,
  };

  static ProfilePictureSubmissionError _instantiate(DecodingData data) {
    return ProfilePictureSubmissionError(
        type: data.dec(_f$type),
        errorCode: data.dec(_f$errorCode),
        message: data.dec(_f$message),
        details: data.dec(_f$details));
  }

  @override
  final Function instantiate = _instantiate;

  static ProfilePictureSubmissionError fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ProfilePictureSubmissionError>(map);
  }

  static ProfilePictureSubmissionError fromJson(String json) {
    return ensureInitialized().decodeJson<ProfilePictureSubmissionError>(json);
  }
}

mixin ProfilePictureSubmissionErrorMappable {
  String toJson() {
    return ProfilePictureSubmissionErrorMapper.ensureInitialized()
        .encodeJson<ProfilePictureSubmissionError>(
            this as ProfilePictureSubmissionError);
  }

  Map<String, dynamic> toMap() {
    return ProfilePictureSubmissionErrorMapper.ensureInitialized()
        .encodeMap<ProfilePictureSubmissionError>(
            this as ProfilePictureSubmissionError);
  }

  ProfilePictureSubmissionErrorCopyWith<ProfilePictureSubmissionError,
          ProfilePictureSubmissionError, ProfilePictureSubmissionError>
      get copyWith => _ProfilePictureSubmissionErrorCopyWithImpl(
          this as ProfilePictureSubmissionError, $identity, $identity);
  @override
  String toString() {
    return ProfilePictureSubmissionErrorMapper.ensureInitialized()
        .stringifyValue(this as ProfilePictureSubmissionError);
  }

  @override
  bool operator ==(Object other) {
    return ProfilePictureSubmissionErrorMapper.ensureInitialized()
        .equalsValue(this as ProfilePictureSubmissionError, other);
  }

  @override
  int get hashCode {
    return ProfilePictureSubmissionErrorMapper.ensureInitialized()
        .hashValue(this as ProfilePictureSubmissionError);
  }
}

extension ProfilePictureSubmissionErrorValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ProfilePictureSubmissionError, $Out> {
  ProfilePictureSubmissionErrorCopyWith<$R, ProfilePictureSubmissionError, $Out>
      get $asProfilePictureSubmissionError => $base.as(
          (v, t, t2) => _ProfilePictureSubmissionErrorCopyWithImpl(v, t, t2));
}

abstract class ProfilePictureSubmissionErrorCopyWith<
    $R,
    $In extends ProfilePictureSubmissionError,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? type, int? errorCode, String? message, String? details});
  ProfilePictureSubmissionErrorCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _ProfilePictureSubmissionErrorCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ProfilePictureSubmissionError, $Out>
    implements
        ProfilePictureSubmissionErrorCopyWith<$R, ProfilePictureSubmissionError,
            $Out> {
  _ProfilePictureSubmissionErrorCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ProfilePictureSubmissionError> $mapper =
      ProfilePictureSubmissionErrorMapper.ensureInitialized();
  @override
  $R call(
          {String? type,
          int? errorCode,
          String? message,
          Object? details = $none}) =>
      $apply(FieldCopyWithData({
        if (type != null) #type: type,
        if (errorCode != null) #errorCode: errorCode,
        if (message != null) #message: message,
        if (details != $none) #details: details
      }));
  @override
  ProfilePictureSubmissionError $make(CopyWithData data) =>
      ProfilePictureSubmissionError(
          type: data.get(#type, or: $value.type),
          errorCode: data.get(#errorCode, or: $value.errorCode),
          message: data.get(#message, or: $value.message),
          details: data.get(#details, or: $value.details));

  @override
  ProfilePictureSubmissionErrorCopyWith<$R2, ProfilePictureSubmissionError,
      $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _ProfilePictureSubmissionErrorCopyWithImpl($value, $cast, t);
}
