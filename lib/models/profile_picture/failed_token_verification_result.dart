import 'package:dart_mappable/dart_mappable.dart';
import 'package:gibz_mobileapp/models/profile_picture/token_verification_result.dart';

part 'failed_token_verification_result.mapper.dart';

@MappableClass(
    discriminatorValue: FailedTokenVerificationResult.checkType,
    caseStyle: CaseStyle.camelCase)
class FailedTokenVerificationResult extends TokenVerificationResult
    with FailedTokenVerificationResultMappable {
  final int statusCode;
  final String statusMessage;

  FailedTokenVerificationResult({
    required super.token,
    required super.requestId,
    required super.requestState,
    required super.requestExpiry,
    required this.statusCode,
    required this.statusMessage,
  });

  static bool checkType(value) {
    return value is Map && !value.keys.contains('firstName');
  }

  static const fromMap = FailedTokenVerificationResultMapper.fromMap;
  static const fromJson = FailedTokenVerificationResultMapper.fromJson;
}
