// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'price.dart';

class PriceMapper extends ClassMapperBase<Price> {
  PriceMapper._();

  static PriceMapper? _instance;
  static PriceMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = PriceMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'Price';

  static String _$parkingDuration(Price v) => v.parkingDuration;
  static const Field<Price, String> _f$parkingDuration =
      Field('parkingDuration', _$parkingDuration);
  static double _$amount(Price v) => v.amount;
  static const Field<Price, double> _f$amount = Field('amount', _$amount);

  @override
  final MappableFields<Price> fields = const {
    #parkingDuration: _f$parkingDuration,
    #amount: _f$amount,
  };

  static Price _instantiate(DecodingData data) {
    return Price(
        parkingDuration: data.dec(_f$parkingDuration),
        amount: data.dec(_f$amount));
  }

  @override
  final Function instantiate = _instantiate;

  static Price fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Price>(map);
  }

  static Price fromJson(String json) {
    return ensureInitialized().decodeJson<Price>(json);
  }
}

mixin PriceMappable {
  String toJson() {
    return PriceMapper.ensureInitialized().encodeJson<Price>(this as Price);
  }

  Map<String, dynamic> toMap() {
    return PriceMapper.ensureInitialized().encodeMap<Price>(this as Price);
  }

  PriceCopyWith<Price, Price, Price> get copyWith =>
      _PriceCopyWithImpl(this as Price, $identity, $identity);
  @override
  String toString() {
    return PriceMapper.ensureInitialized().stringifyValue(this as Price);
  }

  @override
  bool operator ==(Object other) {
    return PriceMapper.ensureInitialized().equalsValue(this as Price, other);
  }

  @override
  int get hashCode {
    return PriceMapper.ensureInitialized().hashValue(this as Price);
  }
}

extension PriceValueCopy<$R, $Out> on ObjectCopyWith<$R, Price, $Out> {
  PriceCopyWith<$R, Price, $Out> get $asPrice =>
      $base.as((v, t, t2) => _PriceCopyWithImpl(v, t, t2));
}

abstract class PriceCopyWith<$R, $In extends Price, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? parkingDuration, double? amount});
  PriceCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _PriceCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Price, $Out>
    implements PriceCopyWith<$R, Price, $Out> {
  _PriceCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Price> $mapper = PriceMapper.ensureInitialized();
  @override
  $R call({String? parkingDuration, double? amount}) =>
      $apply(FieldCopyWithData({
        if (parkingDuration != null) #parkingDuration: parkingDuration,
        if (amount != null) #amount: amount
      }));
  @override
  Price $make(CopyWithData data) => Price(
      parkingDuration: data.get(#parkingDuration, or: $value.parkingDuration),
      amount: data.get(#amount, or: $value.amount));

  @override
  PriceCopyWith<$R2, Price, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _PriceCopyWithImpl($value, $cast, t);
}
