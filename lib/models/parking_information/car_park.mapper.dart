// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'car_park.dart';

class CarParkMapper extends ClassMapperBase<CarPark> {
  CarParkMapper._();

  static CarParkMapper? _instance;
  static CarParkMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = CarParkMapper._());
      AvailabilityMapper.ensureInitialized();
      PriceMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'CarPark';

  static String _$id(CarPark v) => v.id;
  static const Field<CarPark, String> _f$id = Field('id', _$id);
  static int _$plsId(CarPark v) => v.plsId;
  static const Field<CarPark, int> _f$plsId = Field('plsId', _$plsId);
  static String _$name(CarPark v) => v.name;
  static const Field<CarPark, String> _f$name = Field('name', _$name);
  static Availability _$currentAvailability(CarPark v) => v.currentAvailability;
  static const Field<CarPark, Availability> _f$currentAvailability =
      Field('currentAvailability', _$currentAvailability);
  static List<String> _$paymentOptions(CarPark v) => v.paymentOptions;
  static const Field<CarPark, List<String>> _f$paymentOptions =
      Field('paymentOptions', _$paymentOptions);
  static List<String> _$openingHours(CarPark v) => v.openingHours;
  static const Field<CarPark, List<String>> _f$openingHours =
      Field('openingHours', _$openingHours);
  static List<Price> _$prices(CarPark v) => v.prices;
  static const Field<CarPark, List<Price>> _f$prices =
      Field('prices', _$prices);

  @override
  final MappableFields<CarPark> fields = const {
    #id: _f$id,
    #plsId: _f$plsId,
    #name: _f$name,
    #currentAvailability: _f$currentAvailability,
    #paymentOptions: _f$paymentOptions,
    #openingHours: _f$openingHours,
    #prices: _f$prices,
  };

  static CarPark _instantiate(DecodingData data) {
    return CarPark(
        id: data.dec(_f$id),
        plsId: data.dec(_f$plsId),
        name: data.dec(_f$name),
        currentAvailability: data.dec(_f$currentAvailability),
        paymentOptions: data.dec(_f$paymentOptions),
        openingHours: data.dec(_f$openingHours),
        prices: data.dec(_f$prices));
  }

  @override
  final Function instantiate = _instantiate;

  static CarPark fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<CarPark>(map);
  }

  static CarPark fromJson(String json) {
    return ensureInitialized().decodeJson<CarPark>(json);
  }
}

mixin CarParkMappable {
  String toJson() {
    return CarParkMapper.ensureInitialized()
        .encodeJson<CarPark>(this as CarPark);
  }

  Map<String, dynamic> toMap() {
    return CarParkMapper.ensureInitialized()
        .encodeMap<CarPark>(this as CarPark);
  }

  CarParkCopyWith<CarPark, CarPark, CarPark> get copyWith =>
      _CarParkCopyWithImpl(this as CarPark, $identity, $identity);
  @override
  String toString() {
    return CarParkMapper.ensureInitialized().stringifyValue(this as CarPark);
  }

  @override
  bool operator ==(Object other) {
    return CarParkMapper.ensureInitialized()
        .equalsValue(this as CarPark, other);
  }

  @override
  int get hashCode {
    return CarParkMapper.ensureInitialized().hashValue(this as CarPark);
  }
}

extension CarParkValueCopy<$R, $Out> on ObjectCopyWith<$R, CarPark, $Out> {
  CarParkCopyWith<$R, CarPark, $Out> get $asCarPark =>
      $base.as((v, t, t2) => _CarParkCopyWithImpl(v, t, t2));
}

abstract class CarParkCopyWith<$R, $In extends CarPark, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  AvailabilityCopyWith<$R, Availability, Availability> get currentAvailability;
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get paymentOptions;
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>> get openingHours;
  ListCopyWith<$R, Price, PriceCopyWith<$R, Price, Price>> get prices;
  $R call(
      {String? id,
      int? plsId,
      String? name,
      Availability? currentAvailability,
      List<String>? paymentOptions,
      List<String>? openingHours,
      List<Price>? prices});
  CarParkCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _CarParkCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, CarPark, $Out>
    implements CarParkCopyWith<$R, CarPark, $Out> {
  _CarParkCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<CarPark> $mapper =
      CarParkMapper.ensureInitialized();
  @override
  AvailabilityCopyWith<$R, Availability, Availability>
      get currentAvailability => $value.currentAvailability.copyWith
          .$chain((v) => call(currentAvailability: v));
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get paymentOptions => ListCopyWith(
          $value.paymentOptions,
          (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(paymentOptions: v));
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get openingHours => ListCopyWith(
          $value.openingHours,
          (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(openingHours: v));
  @override
  ListCopyWith<$R, Price, PriceCopyWith<$R, Price, Price>> get prices =>
      ListCopyWith($value.prices, (v, t) => v.copyWith.$chain(t),
          (v) => call(prices: v));
  @override
  $R call(
          {String? id,
          int? plsId,
          String? name,
          Availability? currentAvailability,
          List<String>? paymentOptions,
          List<String>? openingHours,
          List<Price>? prices}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (plsId != null) #plsId: plsId,
        if (name != null) #name: name,
        if (currentAvailability != null)
          #currentAvailability: currentAvailability,
        if (paymentOptions != null) #paymentOptions: paymentOptions,
        if (openingHours != null) #openingHours: openingHours,
        if (prices != null) #prices: prices
      }));
  @override
  CarPark $make(CopyWithData data) => CarPark(
      id: data.get(#id, or: $value.id),
      plsId: data.get(#plsId, or: $value.plsId),
      name: data.get(#name, or: $value.name),
      currentAvailability:
          data.get(#currentAvailability, or: $value.currentAvailability),
      paymentOptions: data.get(#paymentOptions, or: $value.paymentOptions),
      openingHours: data.get(#openingHours, or: $value.openingHours),
      prices: data.get(#prices, or: $value.prices));

  @override
  CarParkCopyWith<$R2, CarPark, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _CarParkCopyWithImpl($value, $cast, t);
}
