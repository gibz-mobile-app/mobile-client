// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'availability.dart';

class AvailabilityMapper extends ClassMapperBase<Availability> {
  AvailabilityMapper._();

  static AvailabilityMapper? _instance;
  static AvailabilityMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AvailabilityMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'Availability';

  static String _$id(Availability v) => v.id;
  static const Field<Availability, String> _f$id = Field('id', _$id);
  static String _$status(Availability v) => v.status;
  static const Field<Availability, String> _f$status =
      Field('status', _$status);
  static int _$free(Availability v) => v.free;
  static const Field<Availability, int> _f$free = Field('free', _$free);
  static DateTime _$moment(Availability v) => v.moment;
  static const Field<Availability, DateTime> _f$moment =
      Field('moment', _$moment);

  @override
  final MappableFields<Availability> fields = const {
    #id: _f$id,
    #status: _f$status,
    #free: _f$free,
    #moment: _f$moment,
  };

  static Availability _instantiate(DecodingData data) {
    return Availability(
        id: data.dec(_f$id),
        status: data.dec(_f$status),
        free: data.dec(_f$free),
        moment: data.dec(_f$moment));
  }

  @override
  final Function instantiate = _instantiate;

  static Availability fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Availability>(map);
  }

  static Availability fromJson(String json) {
    return ensureInitialized().decodeJson<Availability>(json);
  }
}

mixin AvailabilityMappable {
  String toJson() {
    return AvailabilityMapper.ensureInitialized()
        .encodeJson<Availability>(this as Availability);
  }

  Map<String, dynamic> toMap() {
    return AvailabilityMapper.ensureInitialized()
        .encodeMap<Availability>(this as Availability);
  }

  AvailabilityCopyWith<Availability, Availability, Availability> get copyWith =>
      _AvailabilityCopyWithImpl(this as Availability, $identity, $identity);
  @override
  String toString() {
    return AvailabilityMapper.ensureInitialized()
        .stringifyValue(this as Availability);
  }

  @override
  bool operator ==(Object other) {
    return AvailabilityMapper.ensureInitialized()
        .equalsValue(this as Availability, other);
  }

  @override
  int get hashCode {
    return AvailabilityMapper.ensureInitialized()
        .hashValue(this as Availability);
  }
}

extension AvailabilityValueCopy<$R, $Out>
    on ObjectCopyWith<$R, Availability, $Out> {
  AvailabilityCopyWith<$R, Availability, $Out> get $asAvailability =>
      $base.as((v, t, t2) => _AvailabilityCopyWithImpl(v, t, t2));
}

abstract class AvailabilityCopyWith<$R, $In extends Availability, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? status, int? free, DateTime? moment});
  AvailabilityCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _AvailabilityCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, Availability, $Out>
    implements AvailabilityCopyWith<$R, Availability, $Out> {
  _AvailabilityCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Availability> $mapper =
      AvailabilityMapper.ensureInitialized();
  @override
  $R call({String? id, String? status, int? free, DateTime? moment}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (status != null) #status: status,
        if (free != null) #free: free,
        if (moment != null) #moment: moment
      }));
  @override
  Availability $make(CopyWithData data) => Availability(
      id: data.get(#id, or: $value.id),
      status: data.get(#status, or: $value.status),
      free: data.get(#free, or: $value.free),
      moment: data.get(#moment, or: $value.moment));

  @override
  AvailabilityCopyWith<$R2, Availability, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _AvailabilityCopyWithImpl($value, $cast, t);
}
