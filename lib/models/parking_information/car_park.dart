import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'availability.dart';
import 'price.dart';

part 'car_park.mapper.dart';

@MappableClass()
class CarPark extends Equatable with CarParkMappable {
  final String id;
  final int plsId;
  final String name;
  final Availability currentAvailability;
  final List<String> paymentOptions;
  final List<String> openingHours;
  final List<Price> prices;

  static const dayIndices = {
    'Mo.-Fr.': 'Montag - Freitag',
    'Sa.': 'Samstag',
    'Sonn- und Feiertage': 'Sonn- und Feiertage',
  };

  Map<String, String> get formattedOpeningHours {
    Map<String, String> entries = {};
    for (final entry in openingHours) {
      String day = '';
      String open = '';
      int i = 0;
      while (open.isEmpty && i < dayIndices.length) {
        String pattern = dayIndices.keys.toList()[i];
        if (entry.contains(pattern)) {
          day = entry.substring(0, pattern.length);
          open = entry.substring(pattern.length + 1);
        }
        i++;
      }

      if (day.isNotEmpty && open.isNotEmpty) {
        day = dayIndices[day] ?? day;
        open = open.replaceAll('.', ':');
        entries[day] = open;
      }
    }
    return entries;
  }

  @override
  List<Object?> get props => [id];

  const CarPark({
    required this.id,
    required this.plsId,
    required this.name,
    required this.currentAvailability,
    required this.paymentOptions,
    required this.openingHours,
    required this.prices,
  });
}
