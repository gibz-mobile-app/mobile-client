// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'meal.dart';

class MealMapper extends ClassMapperBase<Meal> {
  MealMapper._();

  static MealMapper? _instance;
  static MealMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = MealMapper._());
      NutritionalValueMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'Meal';

  static String _$id(Meal v) => v.id;
  static const Field<Meal, String> _f$id = Field('id', _$id);
  static String _$title(Meal v) => v.title;
  static const Field<Meal, String> _f$title = Field('title', _$title);
  static int _$mealRelevance(Meal v) => v.mealRelevance;
  static const Field<Meal, int> _f$mealRelevance =
      Field('mealRelevance', _$mealRelevance, opt: true, def: 0);
  static double _$priceInternal(Meal v) => v.priceInternal;
  static const Field<Meal, double> _f$priceInternal =
      Field('priceInternal', _$priceInternal, opt: true, def: 0.0);
  static double _$priceExternal(Meal v) => v.priceExternal;
  static const Field<Meal, double> _f$priceExternal =
      Field('priceExternal', _$priceExternal, opt: true, def: 0.0);
  static List<String> _$menuComponents(Meal v) => v.menuComponents;
  static const Field<Meal, List<String>> _f$menuComponents =
      Field('menuComponents', _$menuComponents);
  static List<String> _$allergens(Meal v) => v.allergens;
  static const Field<Meal, List<String>> _f$allergens =
      Field('allergens', _$allergens);
  static List<String>? _$balance(Meal v) => v.balance;
  static const Field<Meal, List<String>> _f$balance =
      Field('balance', _$balance, opt: true);
  static String? _$environmentalImpact(Meal v) => v.environmentalImpact;
  static const Field<Meal, String> _f$environmentalImpact =
      Field('environmentalImpact', _$environmentalImpact, opt: true);
  static List<NutritionalValue> _$nutritionalValues(Meal v) =>
      v.nutritionalValues;
  static const Field<Meal, List<NutritionalValue>> _f$nutritionalValues =
      Field('nutritionalValues', _$nutritionalValues);

  @override
  final MappableFields<Meal> fields = const {
    #id: _f$id,
    #title: _f$title,
    #mealRelevance: _f$mealRelevance,
    #priceInternal: _f$priceInternal,
    #priceExternal: _f$priceExternal,
    #menuComponents: _f$menuComponents,
    #allergens: _f$allergens,
    #balance: _f$balance,
    #environmentalImpact: _f$environmentalImpact,
    #nutritionalValues: _f$nutritionalValues,
  };

  static Meal _instantiate(DecodingData data) {
    return Meal(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        mealRelevance: data.dec(_f$mealRelevance),
        priceInternal: data.dec(_f$priceInternal),
        priceExternal: data.dec(_f$priceExternal),
        menuComponents: data.dec(_f$menuComponents),
        allergens: data.dec(_f$allergens),
        balance: data.dec(_f$balance),
        environmentalImpact: data.dec(_f$environmentalImpact),
        nutritionalValues: data.dec(_f$nutritionalValues));
  }

  @override
  final Function instantiate = _instantiate;

  static Meal fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Meal>(map);
  }

  static Meal fromJson(String json) {
    return ensureInitialized().decodeJson<Meal>(json);
  }
}

mixin MealMappable {
  String toJson() {
    return MealMapper.ensureInitialized().encodeJson<Meal>(this as Meal);
  }

  Map<String, dynamic> toMap() {
    return MealMapper.ensureInitialized().encodeMap<Meal>(this as Meal);
  }

  MealCopyWith<Meal, Meal, Meal> get copyWith =>
      _MealCopyWithImpl(this as Meal, $identity, $identity);
  @override
  String toString() {
    return MealMapper.ensureInitialized().stringifyValue(this as Meal);
  }

  @override
  bool operator ==(Object other) {
    return MealMapper.ensureInitialized().equalsValue(this as Meal, other);
  }

  @override
  int get hashCode {
    return MealMapper.ensureInitialized().hashValue(this as Meal);
  }
}

extension MealValueCopy<$R, $Out> on ObjectCopyWith<$R, Meal, $Out> {
  MealCopyWith<$R, Meal, $Out> get $asMeal =>
      $base.as((v, t, t2) => _MealCopyWithImpl(v, t, t2));
}

abstract class MealCopyWith<$R, $In extends Meal, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get menuComponents;
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>> get allergens;
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>? get balance;
  ListCopyWith<$R, NutritionalValue,
          NutritionalValueCopyWith<$R, NutritionalValue, NutritionalValue>>
      get nutritionalValues;
  $R call(
      {String? id,
      String? title,
      int? mealRelevance,
      double? priceInternal,
      double? priceExternal,
      List<String>? menuComponents,
      List<String>? allergens,
      List<String>? balance,
      String? environmentalImpact,
      List<NutritionalValue>? nutritionalValues});
  MealCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _MealCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Meal, $Out>
    implements MealCopyWith<$R, Meal, $Out> {
  _MealCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Meal> $mapper = MealMapper.ensureInitialized();
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>
      get menuComponents => ListCopyWith(
          $value.menuComponents,
          (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(menuComponents: v));
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>> get allergens =>
      ListCopyWith($value.allergens, (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(allergens: v));
  @override
  ListCopyWith<$R, String, ObjectCopyWith<$R, String, String>>? get balance =>
      $value.balance != null
          ? ListCopyWith(
              $value.balance!,
              (v, t) => ObjectCopyWith(v, $identity, t),
              (v) => call(balance: v))
          : null;
  @override
  ListCopyWith<$R, NutritionalValue,
          NutritionalValueCopyWith<$R, NutritionalValue, NutritionalValue>>
      get nutritionalValues => ListCopyWith($value.nutritionalValues,
          (v, t) => v.copyWith.$chain(t), (v) => call(nutritionalValues: v));
  @override
  $R call(
          {String? id,
          String? title,
          int? mealRelevance,
          double? priceInternal,
          double? priceExternal,
          List<String>? menuComponents,
          List<String>? allergens,
          Object? balance = $none,
          Object? environmentalImpact = $none,
          List<NutritionalValue>? nutritionalValues}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (mealRelevance != null) #mealRelevance: mealRelevance,
        if (priceInternal != null) #priceInternal: priceInternal,
        if (priceExternal != null) #priceExternal: priceExternal,
        if (menuComponents != null) #menuComponents: menuComponents,
        if (allergens != null) #allergens: allergens,
        if (balance != $none) #balance: balance,
        if (environmentalImpact != $none)
          #environmentalImpact: environmentalImpact,
        if (nutritionalValues != null) #nutritionalValues: nutritionalValues
      }));
  @override
  Meal $make(CopyWithData data) => Meal(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      mealRelevance: data.get(#mealRelevance, or: $value.mealRelevance),
      priceInternal: data.get(#priceInternal, or: $value.priceInternal),
      priceExternal: data.get(#priceExternal, or: $value.priceExternal),
      menuComponents: data.get(#menuComponents, or: $value.menuComponents),
      allergens: data.get(#allergens, or: $value.allergens),
      balance: data.get(#balance, or: $value.balance),
      environmentalImpact:
          data.get(#environmentalImpact, or: $value.environmentalImpact),
      nutritionalValues:
          data.get(#nutritionalValues, or: $value.nutritionalValues));

  @override
  MealCopyWith<$R2, Meal, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _MealCopyWithImpl($value, $cast, t);
}
