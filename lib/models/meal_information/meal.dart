import 'dart:math';

import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';

part 'meal.mapper.dart';

@MappableClass()
class Meal extends Equatable with MealMappable {
  final String id;
  final String title;
  final int mealRelevance;
  final double priceInternal;
  final double priceExternal;
  final List<String> menuComponents;
  final List<String> allergens;
  final List<String>? balance;
  final String? environmentalImpact;
  final List<NutritionalValue> nutritionalValues;

  double get bestPrice => min(priceInternal, priceExternal);

  const Meal({
    required this.id,
    required this.title,
    this.mealRelevance = 0,
    this.priceInternal = 0.0,
    this.priceExternal = 0.0,
    required this.menuComponents,
    required this.allergens,
    this.balance,
    this.environmentalImpact,
    required this.nutritionalValues,
  });

  @override
  List<Object?> get props => [id];
}
