import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

import '../meal_information.dart';

part 'day.mapper.dart';

@MappableClass()
class Day extends Equatable with DayMappable {
  final DateTime date;
  final List<Meal> menus;

  List<Meal> get orderedMenus =>
      menus..sort((a, b) => -1 * a.mealRelevance.compareTo(b.mealRelevance));

  const Day({
    required this.date,
    required this.menus,
  });

  @override
  List<Object?> get props => [date];
}
