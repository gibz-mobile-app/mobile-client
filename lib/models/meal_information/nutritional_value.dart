import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'nutritional_value.mapper.dart';

@MappableClass()
class NutritionalValue extends Equatable with NutritionalValueMappable {
  final String title;
  final String value;
  final int rowIndex;

  const NutritionalValue({
    required this.title,
    required this.value,
    required this.rowIndex,
  });

  @override
  List<Object?> get props => [title, value, rowIndex];
}
