import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'stage_activity_answer.dart';

part 'stage_activity.mapper.dart';

@MappableClass(discriminatorKey: "type")
abstract class StageActivity extends Equatable with StageActivityMappable {
  final String id;
  final String title;
  final String task;
  final int maxPoints;

  const StageActivity({
    required this.id,
    required this.title,
    required this.task,
    required this.maxPoints,
  });

  @override
  List<Object?> get props => [id];
}

@MappableClass(discriminatorKey: "type")
abstract class ClosedQuestionActivity extends StageActivity
    with ClosedQuestionActivityMappable {
  final List<StageActivityAnswer> answers;

  const ClosedQuestionActivity({
    required super.id,
    required super.title,
    required super.task,
    required super.maxPoints,
    required this.answers,
  });
}

@MappableClass(discriminatorValue: "singleChoice")
class SingleChoiceActivity extends ClosedQuestionActivity
    with SingleChoiceActivityMappable {
  const SingleChoiceActivity({
    required super.id,
    required super.title,
    required super.task,
    required super.maxPoints,
    required super.answers,
  });
}

@MappableClass(discriminatorValue: "multipleChoice")
class MultipleChoiceActivity extends ClosedQuestionActivity
    with MultipleChoiceActivityMappable {
  const MultipleChoiceActivity({
    required super.id,
    required super.title,
    required super.task,
    required super.maxPoints,
    required super.answers,
  });
}

@MappableClass(discriminatorValue: "textInput")
class TextInputActivity extends StageActivity with TextInputActivityMappable {
  const TextInputActivity(
      {required super.id,
      required super.title,
      required super.task,
      required super.maxPoints});
}

@MappableClass(discriminatorValue: "qrCode")
class QrCodeActivity extends StageActivity with QrCodeActivityMappable {
  const QrCodeActivity(
      {required super.id,
      required super.title,
      required super.task,
      required super.maxPoints});
}

@MappableClass()
class QrCodeAnswerValue extends Equatable with QrCodeAnswerValueMappable {
  final String? label;
  final int percentage;

  const QrCodeAnswerValue(this.percentage, {this.label});

  @override
  List<Object?> get props => [label, percentage];

  static const fromMap = QrCodeAnswerValueMapper.fromMap;
  static const fromJson = QrCodeAnswerValueMapper.fromJson;
}
