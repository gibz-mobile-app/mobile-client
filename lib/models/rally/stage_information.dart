import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'attachment.dart';

part 'stage_information.mapper.dart';

@MappableClass()
class StageInformation extends Equatable with StageInformationMappable {
  final String id;
  final String title;
  final String content;
  final List<Attachment> attachments;

  const StageInformation({
    required this.id,
    required this.title,
    required this.content,
    required this.attachments,
  });

  @override
  List<Object?> get props => [id];
}
