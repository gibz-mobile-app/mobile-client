// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'rally.dart';

class RallyMapper extends ClassMapperBase<Rally> {
  RallyMapper._();

  static RallyMapper? _instance;
  static RallyMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = RallyMapper._());
      RallyStageMapper.ensureInitialized();
      AssignmentMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'Rally';

  static String _$id(Rally v) => v.id;
  static const Field<Rally, String> _f$id = Field('id', _$id);
  static String _$title(Rally v) => v.title;
  static const Field<Rally, String> _f$title = Field('title', _$title);
  static List<RallyStage> _$rallyStages(Rally v) => v.rallyStages;
  static const Field<Rally, List<RallyStage>> _f$rallyStages =
      Field('rallyStages', _$rallyStages);
  static List<Assignment> _$assignments(Rally v) => v.assignments;
  static const Field<Rally, List<Assignment>> _f$assignments =
      Field('assignments', _$assignments);

  @override
  final MappableFields<Rally> fields = const {
    #id: _f$id,
    #title: _f$title,
    #rallyStages: _f$rallyStages,
    #assignments: _f$assignments,
  };

  static Rally _instantiate(DecodingData data) {
    return Rally(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        rallyStages: data.dec(_f$rallyStages),
        assignments: data.dec(_f$assignments));
  }

  @override
  final Function instantiate = _instantiate;

  static Rally fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Rally>(map);
  }

  static Rally fromJson(String json) {
    return ensureInitialized().decodeJson<Rally>(json);
  }
}

mixin RallyMappable {
  String toJson() {
    return RallyMapper.ensureInitialized().encodeJson<Rally>(this as Rally);
  }

  Map<String, dynamic> toMap() {
    return RallyMapper.ensureInitialized().encodeMap<Rally>(this as Rally);
  }

  RallyCopyWith<Rally, Rally, Rally> get copyWith =>
      _RallyCopyWithImpl(this as Rally, $identity, $identity);
  @override
  String toString() {
    return RallyMapper.ensureInitialized().stringifyValue(this as Rally);
  }

  @override
  bool operator ==(Object other) {
    return RallyMapper.ensureInitialized().equalsValue(this as Rally, other);
  }

  @override
  int get hashCode {
    return RallyMapper.ensureInitialized().hashValue(this as Rally);
  }
}

extension RallyValueCopy<$R, $Out> on ObjectCopyWith<$R, Rally, $Out> {
  RallyCopyWith<$R, Rally, $Out> get $asRally =>
      $base.as((v, t, t2) => _RallyCopyWithImpl(v, t, t2));
}

abstract class RallyCopyWith<$R, $In extends Rally, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, RallyStage, RallyStageCopyWith<$R, RallyStage, RallyStage>>
      get rallyStages;
  ListCopyWith<$R, Assignment, AssignmentCopyWith<$R, Assignment, Assignment>>
      get assignments;
  $R call(
      {String? id,
      String? title,
      List<RallyStage>? rallyStages,
      List<Assignment>? assignments});
  RallyCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _RallyCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Rally, $Out>
    implements RallyCopyWith<$R, Rally, $Out> {
  _RallyCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Rally> $mapper = RallyMapper.ensureInitialized();
  @override
  ListCopyWith<$R, RallyStage, RallyStageCopyWith<$R, RallyStage, RallyStage>>
      get rallyStages => ListCopyWith($value.rallyStages,
          (v, t) => v.copyWith.$chain(t), (v) => call(rallyStages: v));
  @override
  ListCopyWith<$R, Assignment, AssignmentCopyWith<$R, Assignment, Assignment>>
      get assignments => ListCopyWith($value.assignments,
          (v, t) => v.copyWith.$chain(t), (v) => call(assignments: v));
  @override
  $R call(
          {String? id,
          String? title,
          List<RallyStage>? rallyStages,
          List<Assignment>? assignments}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (rallyStages != null) #rallyStages: rallyStages,
        if (assignments != null) #assignments: assignments
      }));
  @override
  Rally $make(CopyWithData data) => Rally(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      rallyStages: data.get(#rallyStages, or: $value.rallyStages),
      assignments: data.get(#assignments, or: $value.assignments));

  @override
  RallyCopyWith<$R2, Rally, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _RallyCopyWithImpl($value, $cast, t);
}
