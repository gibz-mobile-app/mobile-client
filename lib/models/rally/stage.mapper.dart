// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'stage.dart';

class StageMapper extends ClassMapperBase<Stage> {
  StageMapper._();

  static StageMapper? _instance;
  static StageMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = StageMapper._());
      StageInformationMapper.ensureInitialized();
      LocationMapper.ensureInitialized();
      StageActivityMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'Stage';

  static String _$id(Stage v) => v.id;
  static const Field<Stage, String> _f$id = Field('id', _$id);
  static String _$title(Stage v) => v.title;
  static const Field<Stage, String> _f$title = Field('title', _$title);
  static StageInformation _$preArrivalInformation(Stage v) =>
      v.preArrivalInformation;
  static const Field<Stage, StageInformation> _f$preArrivalInformation =
      Field('preArrivalInformation', _$preArrivalInformation);
  static StageInformation _$information(Stage v) => v.information;
  static const Field<Stage, StageInformation> _f$information =
      Field('information', _$information);
  static List<Location> _$locations(Stage v) => v.locations;
  static const Field<Stage, List<Location>> _f$locations =
      Field('locations', _$locations);
  static StageActivity _$stageActivity(Stage v) => v.stageActivity;
  static const Field<Stage, StageActivity> _f$stageActivity =
      Field('stageActivity', _$stageActivity);

  @override
  final MappableFields<Stage> fields = const {
    #id: _f$id,
    #title: _f$title,
    #preArrivalInformation: _f$preArrivalInformation,
    #information: _f$information,
    #locations: _f$locations,
    #stageActivity: _f$stageActivity,
  };

  static Stage _instantiate(DecodingData data) {
    return Stage(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        preArrivalInformation: data.dec(_f$preArrivalInformation),
        information: data.dec(_f$information),
        locations: data.dec(_f$locations),
        stageActivity: data.dec(_f$stageActivity));
  }

  @override
  final Function instantiate = _instantiate;

  static Stage fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Stage>(map);
  }

  static Stage fromJson(String json) {
    return ensureInitialized().decodeJson<Stage>(json);
  }
}

mixin StageMappable {
  String toJson() {
    return StageMapper.ensureInitialized().encodeJson<Stage>(this as Stage);
  }

  Map<String, dynamic> toMap() {
    return StageMapper.ensureInitialized().encodeMap<Stage>(this as Stage);
  }

  StageCopyWith<Stage, Stage, Stage> get copyWith =>
      _StageCopyWithImpl(this as Stage, $identity, $identity);
  @override
  String toString() {
    return StageMapper.ensureInitialized().stringifyValue(this as Stage);
  }

  @override
  bool operator ==(Object other) {
    return StageMapper.ensureInitialized().equalsValue(this as Stage, other);
  }

  @override
  int get hashCode {
    return StageMapper.ensureInitialized().hashValue(this as Stage);
  }
}

extension StageValueCopy<$R, $Out> on ObjectCopyWith<$R, Stage, $Out> {
  StageCopyWith<$R, Stage, $Out> get $asStage =>
      $base.as((v, t, t2) => _StageCopyWithImpl(v, t, t2));
}

abstract class StageCopyWith<$R, $In extends Stage, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  StageInformationCopyWith<$R, StageInformation, StageInformation>
      get preArrivalInformation;
  StageInformationCopyWith<$R, StageInformation, StageInformation>
      get information;
  ListCopyWith<$R, Location, LocationCopyWith<$R, Location, Location>>
      get locations;
  $R call(
      {String? id,
      String? title,
      StageInformation? preArrivalInformation,
      StageInformation? information,
      List<Location>? locations,
      StageActivity? stageActivity});
  StageCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _StageCopyWithImpl<$R, $Out> extends ClassCopyWithBase<$R, Stage, $Out>
    implements StageCopyWith<$R, Stage, $Out> {
  _StageCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Stage> $mapper = StageMapper.ensureInitialized();
  @override
  StageInformationCopyWith<$R, StageInformation, StageInformation>
      get preArrivalInformation => $value.preArrivalInformation.copyWith
          .$chain((v) => call(preArrivalInformation: v));
  @override
  StageInformationCopyWith<$R, StageInformation, StageInformation>
      get information =>
          $value.information.copyWith.$chain((v) => call(information: v));
  @override
  ListCopyWith<$R, Location, LocationCopyWith<$R, Location, Location>>
      get locations => ListCopyWith($value.locations,
          (v, t) => v.copyWith.$chain(t), (v) => call(locations: v));
  @override
  $R call(
          {String? id,
          String? title,
          StageInformation? preArrivalInformation,
          StageInformation? information,
          List<Location>? locations,
          StageActivity? stageActivity}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (preArrivalInformation != null)
          #preArrivalInformation: preArrivalInformation,
        if (information != null) #information: information,
        if (locations != null) #locations: locations,
        if (stageActivity != null) #stageActivity: stageActivity
      }));
  @override
  Stage $make(CopyWithData data) => Stage(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      preArrivalInformation:
          data.get(#preArrivalInformation, or: $value.preArrivalInformation),
      information: data.get(#information, or: $value.information),
      locations: data.get(#locations, or: $value.locations),
      stageActivity: data.get(#stageActivity, or: $value.stageActivity));

  @override
  StageCopyWith<$R2, Stage, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _StageCopyWithImpl($value, $cast, t);
}
