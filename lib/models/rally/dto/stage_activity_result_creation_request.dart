import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'stage_activity_result_creation_request.mapper.dart';

@MappableClass()
class StageActivityResultCreationRequestDto extends Equatable
    with StageActivityResultCreationRequestDtoMappable {
  final List<String?> answers;
  final String participatingPartyId;
  final String? stageActivityId;

  const StageActivityResultCreationRequestDto({
    required this.answers,
    required this.participatingPartyId,
    required this.stageActivityId,
  });

  @override
  List<Object?> get props => [participatingPartyId, stageActivityId];
}
