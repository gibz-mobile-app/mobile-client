// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'stage_activity_result_creation_request.dart';

class StageActivityResultCreationRequestDtoMapper
    extends ClassMapperBase<StageActivityResultCreationRequestDto> {
  StageActivityResultCreationRequestDtoMapper._();

  static StageActivityResultCreationRequestDtoMapper? _instance;
  static StageActivityResultCreationRequestDtoMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = StageActivityResultCreationRequestDtoMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'StageActivityResultCreationRequestDto';

  static List<String?> _$answers(StageActivityResultCreationRequestDto v) =>
      v.answers;
  static const Field<StageActivityResultCreationRequestDto, List<String?>>
      _f$answers = Field('answers', _$answers);
  static String _$participatingPartyId(
          StageActivityResultCreationRequestDto v) =>
      v.participatingPartyId;
  static const Field<StageActivityResultCreationRequestDto, String>
      _f$participatingPartyId =
      Field('participatingPartyId', _$participatingPartyId);
  static String? _$stageActivityId(StageActivityResultCreationRequestDto v) =>
      v.stageActivityId;
  static const Field<StageActivityResultCreationRequestDto, String>
      _f$stageActivityId = Field('stageActivityId', _$stageActivityId);

  @override
  final MappableFields<StageActivityResultCreationRequestDto> fields = const {
    #answers: _f$answers,
    #participatingPartyId: _f$participatingPartyId,
    #stageActivityId: _f$stageActivityId,
  };

  static StageActivityResultCreationRequestDto _instantiate(DecodingData data) {
    return StageActivityResultCreationRequestDto(
        answers: data.dec(_f$answers),
        participatingPartyId: data.dec(_f$participatingPartyId),
        stageActivityId: data.dec(_f$stageActivityId));
  }

  @override
  final Function instantiate = _instantiate;

  static StageActivityResultCreationRequestDto fromMap(
      Map<String, dynamic> map) {
    return ensureInitialized()
        .decodeMap<StageActivityResultCreationRequestDto>(map);
  }

  static StageActivityResultCreationRequestDto fromJson(String json) {
    return ensureInitialized()
        .decodeJson<StageActivityResultCreationRequestDto>(json);
  }
}

mixin StageActivityResultCreationRequestDtoMappable {
  String toJson() {
    return StageActivityResultCreationRequestDtoMapper.ensureInitialized()
        .encodeJson<StageActivityResultCreationRequestDto>(
            this as StageActivityResultCreationRequestDto);
  }

  Map<String, dynamic> toMap() {
    return StageActivityResultCreationRequestDtoMapper.ensureInitialized()
        .encodeMap<StageActivityResultCreationRequestDto>(
            this as StageActivityResultCreationRequestDto);
  }

  StageActivityResultCreationRequestDtoCopyWith<
          StageActivityResultCreationRequestDto,
          StageActivityResultCreationRequestDto,
          StageActivityResultCreationRequestDto>
      get copyWith => _StageActivityResultCreationRequestDtoCopyWithImpl(
          this as StageActivityResultCreationRequestDto, $identity, $identity);
  @override
  String toString() {
    return StageActivityResultCreationRequestDtoMapper.ensureInitialized()
        .stringifyValue(this as StageActivityResultCreationRequestDto);
  }

  @override
  bool operator ==(Object other) {
    return StageActivityResultCreationRequestDtoMapper.ensureInitialized()
        .equalsValue(this as StageActivityResultCreationRequestDto, other);
  }

  @override
  int get hashCode {
    return StageActivityResultCreationRequestDtoMapper.ensureInitialized()
        .hashValue(this as StageActivityResultCreationRequestDto);
  }
}

extension StageActivityResultCreationRequestDtoValueCopy<$R, $Out>
    on ObjectCopyWith<$R, StageActivityResultCreationRequestDto, $Out> {
  StageActivityResultCreationRequestDtoCopyWith<$R,
          StageActivityResultCreationRequestDto, $Out>
      get $asStageActivityResultCreationRequestDto => $base.as((v, t, t2) =>
          _StageActivityResultCreationRequestDtoCopyWithImpl(v, t, t2));
}

abstract class StageActivityResultCreationRequestDtoCopyWith<
    $R,
    $In extends StageActivityResultCreationRequestDto,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, String?, ObjectCopyWith<$R, String?, String?>?> get answers;
  $R call(
      {List<String?>? answers,
      String? participatingPartyId,
      String? stageActivityId});
  StageActivityResultCreationRequestDtoCopyWith<$R2, $In, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _StageActivityResultCreationRequestDtoCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, StageActivityResultCreationRequestDto, $Out>
    implements
        StageActivityResultCreationRequestDtoCopyWith<$R,
            StageActivityResultCreationRequestDto, $Out> {
  _StageActivityResultCreationRequestDtoCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<StageActivityResultCreationRequestDto> $mapper =
      StageActivityResultCreationRequestDtoMapper.ensureInitialized();
  @override
  ListCopyWith<$R, String?, ObjectCopyWith<$R, String?, String?>?>
      get answers => ListCopyWith($value.answers,
          (v, t) => ObjectCopyWith(v, $identity, t), (v) => call(answers: v));
  @override
  $R call(
          {List<String?>? answers,
          String? participatingPartyId,
          Object? stageActivityId = $none}) =>
      $apply(FieldCopyWithData({
        if (answers != null) #answers: answers,
        if (participatingPartyId != null)
          #participatingPartyId: participatingPartyId,
        if (stageActivityId != $none) #stageActivityId: stageActivityId
      }));
  @override
  StageActivityResultCreationRequestDto $make(CopyWithData data) =>
      StageActivityResultCreationRequestDto(
          answers: data.get(#answers, or: $value.answers),
          participatingPartyId:
              data.get(#participatingPartyId, or: $value.participatingPartyId),
          stageActivityId:
              data.get(#stageActivityId, or: $value.stageActivityId));

  @override
  StageActivityResultCreationRequestDtoCopyWith<$R2,
      StageActivityResultCreationRequestDto, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _StageActivityResultCreationRequestDtoCopyWithImpl($value, $cast, t);
}
