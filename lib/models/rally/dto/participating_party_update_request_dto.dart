import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'participating_party_update_request_dto.mapper.dart';

@MappableClass()
class ParticipatingPartyUpdateRequestDto extends Equatable
    with ParticipatingPartyUpdateRequestDtoMappable {
  final String id;
  final String title;
  final List<String> names;

  const ParticipatingPartyUpdateRequestDto({
    required this.id,
    required this.title,
    required this.names,
  });

  @override
  List<Object?> get props => [id];
}
