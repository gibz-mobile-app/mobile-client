// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'stage_activity_result.dart';

class StageActivityResultMapper extends ClassMapperBase<StageActivityResult> {
  StageActivityResultMapper._();

  static StageActivityResultMapper? _instance;
  static StageActivityResultMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = StageActivityResultMapper._());
      ParticipatingPartyAnswerMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'StageActivityResult';

  static String _$id(StageActivityResult v) => v.id;
  static const Field<StageActivityResult, String> _f$id = Field('id', _$id);
  static List<ParticipatingPartyAnswer> _$answers(StageActivityResult v) =>
      v.answers;
  static const Field<StageActivityResult, List<ParticipatingPartyAnswer>>
      _f$answers = Field('answers', _$answers);
  static int _$awardedPoints(StageActivityResult v) => v.awardedPoints;
  static const Field<StageActivityResult, int> _f$awardedPoints =
      Field('awardedPoints', _$awardedPoints);
  static int _$totalPoints(StageActivityResult v) => v.totalPoints;
  static const Field<StageActivityResult, int> _f$totalPoints =
      Field('totalPoints', _$totalPoints);
  static int _$potentialHighscore(StageActivityResult v) =>
      v.potentialHighscore;
  static const Field<StageActivityResult, int> _f$potentialHighscore =
      Field('potentialHighscore', _$potentialHighscore);
  static DateTime _$momentOfCompletion(StageActivityResult v) =>
      v.momentOfCompletion;
  static const Field<StageActivityResult, DateTime> _f$momentOfCompletion =
      Field('momentOfCompletion', _$momentOfCompletion);

  @override
  final MappableFields<StageActivityResult> fields = const {
    #id: _f$id,
    #answers: _f$answers,
    #awardedPoints: _f$awardedPoints,
    #totalPoints: _f$totalPoints,
    #potentialHighscore: _f$potentialHighscore,
    #momentOfCompletion: _f$momentOfCompletion,
  };

  static StageActivityResult _instantiate(DecodingData data) {
    return StageActivityResult(
        id: data.dec(_f$id),
        answers: data.dec(_f$answers),
        awardedPoints: data.dec(_f$awardedPoints),
        totalPoints: data.dec(_f$totalPoints),
        potentialHighscore: data.dec(_f$potentialHighscore),
        momentOfCompletion: data.dec(_f$momentOfCompletion));
  }

  @override
  final Function instantiate = _instantiate;

  static StageActivityResult fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<StageActivityResult>(map);
  }

  static StageActivityResult fromJson(String json) {
    return ensureInitialized().decodeJson<StageActivityResult>(json);
  }
}

mixin StageActivityResultMappable {
  String toJson() {
    return StageActivityResultMapper.ensureInitialized()
        .encodeJson<StageActivityResult>(this as StageActivityResult);
  }

  Map<String, dynamic> toMap() {
    return StageActivityResultMapper.ensureInitialized()
        .encodeMap<StageActivityResult>(this as StageActivityResult);
  }

  StageActivityResultCopyWith<StageActivityResult, StageActivityResult,
          StageActivityResult>
      get copyWith => _StageActivityResultCopyWithImpl(
          this as StageActivityResult, $identity, $identity);
  @override
  String toString() {
    return StageActivityResultMapper.ensureInitialized()
        .stringifyValue(this as StageActivityResult);
  }

  @override
  bool operator ==(Object other) {
    return StageActivityResultMapper.ensureInitialized()
        .equalsValue(this as StageActivityResult, other);
  }

  @override
  int get hashCode {
    return StageActivityResultMapper.ensureInitialized()
        .hashValue(this as StageActivityResult);
  }
}

extension StageActivityResultValueCopy<$R, $Out>
    on ObjectCopyWith<$R, StageActivityResult, $Out> {
  StageActivityResultCopyWith<$R, StageActivityResult, $Out>
      get $asStageActivityResult =>
          $base.as((v, t, t2) => _StageActivityResultCopyWithImpl(v, t, t2));
}

abstract class StageActivityResultCopyWith<$R, $In extends StageActivityResult,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<
      $R,
      ParticipatingPartyAnswer,
      ObjectCopyWith<$R, ParticipatingPartyAnswer,
          ParticipatingPartyAnswer>> get answers;
  $R call(
      {String? id,
      List<ParticipatingPartyAnswer>? answers,
      int? awardedPoints,
      int? totalPoints,
      int? potentialHighscore,
      DateTime? momentOfCompletion});
  StageActivityResultCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _StageActivityResultCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, StageActivityResult, $Out>
    implements StageActivityResultCopyWith<$R, StageActivityResult, $Out> {
  _StageActivityResultCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<StageActivityResult> $mapper =
      StageActivityResultMapper.ensureInitialized();
  @override
  ListCopyWith<
      $R,
      ParticipatingPartyAnswer,
      ObjectCopyWith<$R, ParticipatingPartyAnswer,
          ParticipatingPartyAnswer>> get answers => ListCopyWith($value.answers,
      (v, t) => ObjectCopyWith(v, $identity, t), (v) => call(answers: v));
  @override
  $R call(
          {String? id,
          List<ParticipatingPartyAnswer>? answers,
          int? awardedPoints,
          int? totalPoints,
          int? potentialHighscore,
          DateTime? momentOfCompletion}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (answers != null) #answers: answers,
        if (awardedPoints != null) #awardedPoints: awardedPoints,
        if (totalPoints != null) #totalPoints: totalPoints,
        if (potentialHighscore != null) #potentialHighscore: potentialHighscore,
        if (momentOfCompletion != null) #momentOfCompletion: momentOfCompletion
      }));
  @override
  StageActivityResult $make(CopyWithData data) => StageActivityResult(
      id: data.get(#id, or: $value.id),
      answers: data.get(#answers, or: $value.answers),
      awardedPoints: data.get(#awardedPoints, or: $value.awardedPoints),
      totalPoints: data.get(#totalPoints, or: $value.totalPoints),
      potentialHighscore:
          data.get(#potentialHighscore, or: $value.potentialHighscore),
      momentOfCompletion:
          data.get(#momentOfCompletion, or: $value.momentOfCompletion));

  @override
  StageActivityResultCopyWith<$R2, StageActivityResult, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _StageActivityResultCopyWithImpl($value, $cast, t);
}
