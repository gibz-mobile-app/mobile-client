// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'participating_party_appearance.dart';

class ParticipatingPartyAppearanceMapper
    extends ClassMapperBase<ParticipatingPartyAppearance> {
  ParticipatingPartyAppearanceMapper._();

  static ParticipatingPartyAppearanceMapper? _instance;
  static ParticipatingPartyAppearanceMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = ParticipatingPartyAppearanceMapper._());
      ParticipatingPartyMapper.ensureInitialized();
      LocationMarkerMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'ParticipatingPartyAppearance';

  static String _$id(ParticipatingPartyAppearance v) => v.id;
  static const Field<ParticipatingPartyAppearance, String> _f$id =
      Field('id', _$id);
  static ParticipatingParty _$participatingParty(
          ParticipatingPartyAppearance v) =>
      v.participatingParty;
  static const Field<ParticipatingPartyAppearance, ParticipatingParty>
      _f$participatingParty = Field('participatingParty', _$participatingParty);
  static LocationMarker _$locationMarker(ParticipatingPartyAppearance v) =>
      v.locationMarker;
  static const Field<ParticipatingPartyAppearance, LocationMarker>
      _f$locationMarker = Field('locationMarker', _$locationMarker);

  @override
  final MappableFields<ParticipatingPartyAppearance> fields = const {
    #id: _f$id,
    #participatingParty: _f$participatingParty,
    #locationMarker: _f$locationMarker,
  };

  static ParticipatingPartyAppearance _instantiate(DecodingData data) {
    return ParticipatingPartyAppearance(
        id: data.dec(_f$id),
        participatingParty: data.dec(_f$participatingParty),
        locationMarker: data.dec(_f$locationMarker));
  }

  @override
  final Function instantiate = _instantiate;

  static ParticipatingPartyAppearance fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ParticipatingPartyAppearance>(map);
  }

  static ParticipatingPartyAppearance fromJson(String json) {
    return ensureInitialized().decodeJson<ParticipatingPartyAppearance>(json);
  }
}

mixin ParticipatingPartyAppearanceMappable {
  String toJson() {
    return ParticipatingPartyAppearanceMapper.ensureInitialized()
        .encodeJson<ParticipatingPartyAppearance>(
            this as ParticipatingPartyAppearance);
  }

  Map<String, dynamic> toMap() {
    return ParticipatingPartyAppearanceMapper.ensureInitialized()
        .encodeMap<ParticipatingPartyAppearance>(
            this as ParticipatingPartyAppearance);
  }

  ParticipatingPartyAppearanceCopyWith<ParticipatingPartyAppearance,
          ParticipatingPartyAppearance, ParticipatingPartyAppearance>
      get copyWith => _ParticipatingPartyAppearanceCopyWithImpl(
          this as ParticipatingPartyAppearance, $identity, $identity);
  @override
  String toString() {
    return ParticipatingPartyAppearanceMapper.ensureInitialized()
        .stringifyValue(this as ParticipatingPartyAppearance);
  }

  @override
  bool operator ==(Object other) {
    return ParticipatingPartyAppearanceMapper.ensureInitialized()
        .equalsValue(this as ParticipatingPartyAppearance, other);
  }

  @override
  int get hashCode {
    return ParticipatingPartyAppearanceMapper.ensureInitialized()
        .hashValue(this as ParticipatingPartyAppearance);
  }
}

extension ParticipatingPartyAppearanceValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ParticipatingPartyAppearance, $Out> {
  ParticipatingPartyAppearanceCopyWith<$R, ParticipatingPartyAppearance, $Out>
      get $asParticipatingPartyAppearance => $base.as(
          (v, t, t2) => _ParticipatingPartyAppearanceCopyWithImpl(v, t, t2));
}

abstract class ParticipatingPartyAppearanceCopyWith<
    $R,
    $In extends ParticipatingPartyAppearance,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  ParticipatingPartyCopyWith<$R, ParticipatingParty, ParticipatingParty>
      get participatingParty;
  $R call(
      {String? id,
      ParticipatingParty? participatingParty,
      LocationMarker? locationMarker});
  ParticipatingPartyAppearanceCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _ParticipatingPartyAppearanceCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ParticipatingPartyAppearance, $Out>
    implements
        ParticipatingPartyAppearanceCopyWith<$R, ParticipatingPartyAppearance,
            $Out> {
  _ParticipatingPartyAppearanceCopyWithImpl(
      super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ParticipatingPartyAppearance> $mapper =
      ParticipatingPartyAppearanceMapper.ensureInitialized();
  @override
  ParticipatingPartyCopyWith<$R, ParticipatingParty, ParticipatingParty>
      get participatingParty => $value.participatingParty.copyWith
          .$chain((v) => call(participatingParty: v));
  @override
  $R call(
          {String? id,
          ParticipatingParty? participatingParty,
          LocationMarker? locationMarker}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (participatingParty != null) #participatingParty: participatingParty,
        if (locationMarker != null) #locationMarker: locationMarker
      }));
  @override
  ParticipatingPartyAppearance $make(CopyWithData data) =>
      ParticipatingPartyAppearance(
          id: data.get(#id, or: $value.id),
          participatingParty:
              data.get(#participatingParty, or: $value.participatingParty),
          locationMarker: data.get(#locationMarker, or: $value.locationMarker));

  @override
  ParticipatingPartyAppearanceCopyWith<$R2, ParticipatingPartyAppearance, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _ParticipatingPartyAppearanceCopyWithImpl($value, $cast, t);
}
