import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/rally/location_marker.dart';
import 'package:gibz_mobileapp/models/rally/participating_party.dart';

part 'participating_party_appearance.mapper.dart';

@MappableClass()
class ParticipatingPartyAppearance extends Equatable
    with ParticipatingPartyAppearanceMappable {
  final String id;
  final ParticipatingParty participatingParty;
  final LocationMarker locationMarker;

  const ParticipatingPartyAppearance({
    required this.id,
    required this.participatingParty,
    required this.locationMarker,
  });

  @override
  List<Object?> get props => [id];
}
