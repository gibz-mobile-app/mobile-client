import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/rally/stage_activity_answer.dart';

part 'participating_party_answer.mapper.dart';

@MappableClass(discriminatorKey: "type")
abstract class ParticipatingPartyAnswer extends Equatable
    with ParticipatingPartyAnswerMappable {
  final String id;
  final bool isCorrect;

  const ParticipatingPartyAnswer({
    required this.id,
    required this.isCorrect,
  });

  @override
  List<Object?> get props => [id];
}

@MappableClass(discriminatorValue: "closedQuestionAnswer")
class ClosedQuestionAnswer extends ParticipatingPartyAnswer
    with ClosedQuestionAnswerMappable {
  final StageActivityAnswer answer;

  const ClosedQuestionAnswer({
    required super.id,
    required super.isCorrect,
    required this.answer,
  });
}

@MappableClass(discriminatorValue: "openQuestionAnswer")
class OpenQuestionAnswer extends ParticipatingPartyAnswer
    with OpenQuestionAnswerMappable {
  final String answerText;

  const OpenQuestionAnswer({
    required super.id,
    required super.isCorrect,
    required this.answerText,
  });
}

@MappableClass(discriminatorValue: "qrCodeActivityAnswer")
class QrCodeActivityAnswer extends ParticipatingPartyAnswer
    with QrCodeActivityAnswerMappable {
  final int percentage;
  final String? label;

  const QrCodeActivityAnswer({
    required super.id,
    required super.isCorrect,
    required this.percentage,
    this.label,
  });
}
