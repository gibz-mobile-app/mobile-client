import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/rally/participating_party.dart';
import 'package:gibz_mobileapp/models/rally/rally.dart';

part 'successful_rally_join_response.mapper.dart';

@MappableClass()
class SuccessfulRallyJoinResponse extends Equatable
    with SuccessfulRallyJoinResponseMappable {
  final ParticipatingParty participatingParty;
  final Rally rally;

  const SuccessfulRallyJoinResponse({
    required this.participatingParty,
    required this.rally,
  });

  @override
  List<Object?> get props => [participatingParty, rally];
}
