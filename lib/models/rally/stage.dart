import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/rally/location.dart';
import 'package:gibz_mobileapp/models/rally/stage_activity.dart';
import 'package:gibz_mobileapp/models/rally/stage_information.dart';

part 'stage.mapper.dart';

@MappableClass()
class Stage extends Equatable with StageMappable {
  final String id;
  final String title;
  final StageInformation preArrivalInformation;
  final StageInformation information;
  final List<Location> locations;
  final StageActivity stageActivity;

  const Stage({
    required this.id,
    required this.title,
    required this.preArrivalInformation,
    required this.information,
    required this.locations,
    required this.stageActivity,
  });

  @override
  List<Object?> get props => [id];
}
