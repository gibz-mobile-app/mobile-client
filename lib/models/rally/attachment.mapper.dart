// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'attachment.dart';

class AttachmentMapper extends ClassMapperBase<Attachment> {
  AttachmentMapper._();

  static AttachmentMapper? _instance;
  static AttachmentMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = AttachmentMapper._());
      VideoAttachmentMapper.ensureInitialized();
      ExternalLinkAttachmentMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'Attachment';

  static String _$id(Attachment v) => v.id;
  static const Field<Attachment, String> _f$id = Field('id', _$id);
  static String _$title(Attachment v) => v.title;
  static const Field<Attachment, String> _f$title = Field('title', _$title);
  static String _$url(Attachment v) => v.url;
  static const Field<Attachment, String> _f$url = Field('url', _$url);

  @override
  final MappableFields<Attachment> fields = const {
    #id: _f$id,
    #title: _f$title,
    #url: _f$url,
  };

  static Attachment _instantiate(DecodingData data) {
    throw MapperException.missingSubclass(
        'Attachment', 'type', '${data.value['type']}');
  }

  @override
  final Function instantiate = _instantiate;

  static Attachment fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Attachment>(map);
  }

  static Attachment fromJson(String json) {
    return ensureInitialized().decodeJson<Attachment>(json);
  }
}

mixin AttachmentMappable {
  String toJson();
  Map<String, dynamic> toMap();
  AttachmentCopyWith<Attachment, Attachment, Attachment> get copyWith;
}

abstract class AttachmentCopyWith<$R, $In extends Attachment, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? title, String? url});
  AttachmentCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class VideoAttachmentMapper extends SubClassMapperBase<VideoAttachment> {
  VideoAttachmentMapper._();

  static VideoAttachmentMapper? _instance;
  static VideoAttachmentMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = VideoAttachmentMapper._());
      AttachmentMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'VideoAttachment';

  static String _$id(VideoAttachment v) => v.id;
  static const Field<VideoAttachment, String> _f$id = Field('id', _$id);
  static String _$title(VideoAttachment v) => v.title;
  static const Field<VideoAttachment, String> _f$title =
      Field('title', _$title);
  static String _$url(VideoAttachment v) => v.url;
  static const Field<VideoAttachment, String> _f$url = Field('url', _$url);

  @override
  final MappableFields<VideoAttachment> fields = const {
    #id: _f$id,
    #title: _f$title,
    #url: _f$url,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "video";
  @override
  late final ClassMapperBase superMapper = AttachmentMapper.ensureInitialized();

  static VideoAttachment _instantiate(DecodingData data) {
    return VideoAttachment(
        id: data.dec(_f$id), title: data.dec(_f$title), url: data.dec(_f$url));
  }

  @override
  final Function instantiate = _instantiate;

  static VideoAttachment fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<VideoAttachment>(map);
  }

  static VideoAttachment fromJson(String json) {
    return ensureInitialized().decodeJson<VideoAttachment>(json);
  }
}

mixin VideoAttachmentMappable {
  String toJson() {
    return VideoAttachmentMapper.ensureInitialized()
        .encodeJson<VideoAttachment>(this as VideoAttachment);
  }

  Map<String, dynamic> toMap() {
    return VideoAttachmentMapper.ensureInitialized()
        .encodeMap<VideoAttachment>(this as VideoAttachment);
  }

  VideoAttachmentCopyWith<VideoAttachment, VideoAttachment, VideoAttachment>
      get copyWith => _VideoAttachmentCopyWithImpl(
          this as VideoAttachment, $identity, $identity);
  @override
  String toString() {
    return VideoAttachmentMapper.ensureInitialized()
        .stringifyValue(this as VideoAttachment);
  }

  @override
  bool operator ==(Object other) {
    return VideoAttachmentMapper.ensureInitialized()
        .equalsValue(this as VideoAttachment, other);
  }

  @override
  int get hashCode {
    return VideoAttachmentMapper.ensureInitialized()
        .hashValue(this as VideoAttachment);
  }
}

extension VideoAttachmentValueCopy<$R, $Out>
    on ObjectCopyWith<$R, VideoAttachment, $Out> {
  VideoAttachmentCopyWith<$R, VideoAttachment, $Out> get $asVideoAttachment =>
      $base.as((v, t, t2) => _VideoAttachmentCopyWithImpl(v, t, t2));
}

abstract class VideoAttachmentCopyWith<$R, $In extends VideoAttachment, $Out>
    implements AttachmentCopyWith<$R, $In, $Out> {
  @override
  $R call({String? id, String? title, String? url});
  VideoAttachmentCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _VideoAttachmentCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, VideoAttachment, $Out>
    implements VideoAttachmentCopyWith<$R, VideoAttachment, $Out> {
  _VideoAttachmentCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<VideoAttachment> $mapper =
      VideoAttachmentMapper.ensureInitialized();
  @override
  $R call({String? id, String? title, String? url}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (url != null) #url: url
      }));
  @override
  VideoAttachment $make(CopyWithData data) => VideoAttachment(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      url: data.get(#url, or: $value.url));

  @override
  VideoAttachmentCopyWith<$R2, VideoAttachment, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _VideoAttachmentCopyWithImpl($value, $cast, t);
}

class ExternalLinkAttachmentMapper
    extends SubClassMapperBase<ExternalLinkAttachment> {
  ExternalLinkAttachmentMapper._();

  static ExternalLinkAttachmentMapper? _instance;
  static ExternalLinkAttachmentMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ExternalLinkAttachmentMapper._());
      AttachmentMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'ExternalLinkAttachment';

  static String _$id(ExternalLinkAttachment v) => v.id;
  static const Field<ExternalLinkAttachment, String> _f$id = Field('id', _$id);
  static String _$title(ExternalLinkAttachment v) => v.title;
  static const Field<ExternalLinkAttachment, String> _f$title =
      Field('title', _$title);
  static String _$url(ExternalLinkAttachment v) => v.url;
  static const Field<ExternalLinkAttachment, String> _f$url =
      Field('url', _$url);

  @override
  final MappableFields<ExternalLinkAttachment> fields = const {
    #id: _f$id,
    #title: _f$title,
    #url: _f$url,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "link";
  @override
  late final ClassMapperBase superMapper = AttachmentMapper.ensureInitialized();

  static ExternalLinkAttachment _instantiate(DecodingData data) {
    return ExternalLinkAttachment(
        id: data.dec(_f$id), title: data.dec(_f$title), url: data.dec(_f$url));
  }

  @override
  final Function instantiate = _instantiate;

  static ExternalLinkAttachment fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ExternalLinkAttachment>(map);
  }

  static ExternalLinkAttachment fromJson(String json) {
    return ensureInitialized().decodeJson<ExternalLinkAttachment>(json);
  }
}

mixin ExternalLinkAttachmentMappable {
  String toJson() {
    return ExternalLinkAttachmentMapper.ensureInitialized()
        .encodeJson<ExternalLinkAttachment>(this as ExternalLinkAttachment);
  }

  Map<String, dynamic> toMap() {
    return ExternalLinkAttachmentMapper.ensureInitialized()
        .encodeMap<ExternalLinkAttachment>(this as ExternalLinkAttachment);
  }

  ExternalLinkAttachmentCopyWith<ExternalLinkAttachment, ExternalLinkAttachment,
          ExternalLinkAttachment>
      get copyWith => _ExternalLinkAttachmentCopyWithImpl(
          this as ExternalLinkAttachment, $identity, $identity);
  @override
  String toString() {
    return ExternalLinkAttachmentMapper.ensureInitialized()
        .stringifyValue(this as ExternalLinkAttachment);
  }

  @override
  bool operator ==(Object other) {
    return ExternalLinkAttachmentMapper.ensureInitialized()
        .equalsValue(this as ExternalLinkAttachment, other);
  }

  @override
  int get hashCode {
    return ExternalLinkAttachmentMapper.ensureInitialized()
        .hashValue(this as ExternalLinkAttachment);
  }
}

extension ExternalLinkAttachmentValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ExternalLinkAttachment, $Out> {
  ExternalLinkAttachmentCopyWith<$R, ExternalLinkAttachment, $Out>
      get $asExternalLinkAttachment =>
          $base.as((v, t, t2) => _ExternalLinkAttachmentCopyWithImpl(v, t, t2));
}

abstract class ExternalLinkAttachmentCopyWith<
    $R,
    $In extends ExternalLinkAttachment,
    $Out> implements AttachmentCopyWith<$R, $In, $Out> {
  @override
  $R call({String? id, String? title, String? url});
  ExternalLinkAttachmentCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _ExternalLinkAttachmentCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ExternalLinkAttachment, $Out>
    implements
        ExternalLinkAttachmentCopyWith<$R, ExternalLinkAttachment, $Out> {
  _ExternalLinkAttachmentCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ExternalLinkAttachment> $mapper =
      ExternalLinkAttachmentMapper.ensureInitialized();
  @override
  $R call({String? id, String? title, String? url}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (url != null) #url: url
      }));
  @override
  ExternalLinkAttachment $make(CopyWithData data) => ExternalLinkAttachment(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      url: data.get(#url, or: $value.url));

  @override
  ExternalLinkAttachmentCopyWith<$R2, ExternalLinkAttachment, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _ExternalLinkAttachmentCopyWithImpl($value, $cast, t);
}
