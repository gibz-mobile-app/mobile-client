// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'stage_activity_answer.dart';

class StageActivityAnswerMapper extends ClassMapperBase<StageActivityAnswer> {
  StageActivityAnswerMapper._();

  static StageActivityAnswerMapper? _instance;
  static StageActivityAnswerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = StageActivityAnswerMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'StageActivityAnswer';

  static String _$id(StageActivityAnswer v) => v.id;
  static const Field<StageActivityAnswer, String> _f$id = Field('id', _$id);
  static String _$answerText(StageActivityAnswer v) => v.answerText;
  static const Field<StageActivityAnswer, String> _f$answerText =
      Field('answerText', _$answerText);

  @override
  final MappableFields<StageActivityAnswer> fields = const {
    #id: _f$id,
    #answerText: _f$answerText,
  };

  static StageActivityAnswer _instantiate(DecodingData data) {
    return StageActivityAnswer(
        id: data.dec(_f$id), answerText: data.dec(_f$answerText));
  }

  @override
  final Function instantiate = _instantiate;

  static StageActivityAnswer fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<StageActivityAnswer>(map);
  }

  static StageActivityAnswer fromJson(String json) {
    return ensureInitialized().decodeJson<StageActivityAnswer>(json);
  }
}

mixin StageActivityAnswerMappable {
  String toJson() {
    return StageActivityAnswerMapper.ensureInitialized()
        .encodeJson<StageActivityAnswer>(this as StageActivityAnswer);
  }

  Map<String, dynamic> toMap() {
    return StageActivityAnswerMapper.ensureInitialized()
        .encodeMap<StageActivityAnswer>(this as StageActivityAnswer);
  }

  StageActivityAnswerCopyWith<StageActivityAnswer, StageActivityAnswer,
          StageActivityAnswer>
      get copyWith => _StageActivityAnswerCopyWithImpl(
          this as StageActivityAnswer, $identity, $identity);
  @override
  String toString() {
    return StageActivityAnswerMapper.ensureInitialized()
        .stringifyValue(this as StageActivityAnswer);
  }

  @override
  bool operator ==(Object other) {
    return StageActivityAnswerMapper.ensureInitialized()
        .equalsValue(this as StageActivityAnswer, other);
  }

  @override
  int get hashCode {
    return StageActivityAnswerMapper.ensureInitialized()
        .hashValue(this as StageActivityAnswer);
  }
}

extension StageActivityAnswerValueCopy<$R, $Out>
    on ObjectCopyWith<$R, StageActivityAnswer, $Out> {
  StageActivityAnswerCopyWith<$R, StageActivityAnswer, $Out>
      get $asStageActivityAnswer =>
          $base.as((v, t, t2) => _StageActivityAnswerCopyWithImpl(v, t, t2));
}

abstract class StageActivityAnswerCopyWith<$R, $In extends StageActivityAnswer,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? answerText});
  StageActivityAnswerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _StageActivityAnswerCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, StageActivityAnswer, $Out>
    implements StageActivityAnswerCopyWith<$R, StageActivityAnswer, $Out> {
  _StageActivityAnswerCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<StageActivityAnswer> $mapper =
      StageActivityAnswerMapper.ensureInitialized();
  @override
  $R call({String? id, String? answerText}) => $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (answerText != null) #answerText: answerText
      }));
  @override
  StageActivityAnswer $make(CopyWithData data) => StageActivityAnswer(
      id: data.get(#id, or: $value.id),
      answerText: data.get(#answerText, or: $value.answerText));

  @override
  StageActivityAnswerCopyWith<$R2, StageActivityAnswer, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _StageActivityAnswerCopyWithImpl($value, $cast, t);
}
