// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'stage_activity.dart';

class StageActivityMapper extends ClassMapperBase<StageActivity> {
  StageActivityMapper._();

  static StageActivityMapper? _instance;
  static StageActivityMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = StageActivityMapper._());
      ClosedQuestionActivityMapper.ensureInitialized();
      TextInputActivityMapper.ensureInitialized();
      QrCodeActivityMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'StageActivity';

  static String _$id(StageActivity v) => v.id;
  static const Field<StageActivity, String> _f$id = Field('id', _$id);
  static String _$title(StageActivity v) => v.title;
  static const Field<StageActivity, String> _f$title = Field('title', _$title);
  static String _$task(StageActivity v) => v.task;
  static const Field<StageActivity, String> _f$task = Field('task', _$task);
  static int _$maxPoints(StageActivity v) => v.maxPoints;
  static const Field<StageActivity, int> _f$maxPoints =
      Field('maxPoints', _$maxPoints);

  @override
  final MappableFields<StageActivity> fields = const {
    #id: _f$id,
    #title: _f$title,
    #task: _f$task,
    #maxPoints: _f$maxPoints,
  };

  static StageActivity _instantiate(DecodingData data) {
    throw MapperException.missingSubclass(
        'StageActivity', 'type', '${data.value['type']}');
  }

  @override
  final Function instantiate = _instantiate;

  static StageActivity fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<StageActivity>(map);
  }

  static StageActivity fromJson(String json) {
    return ensureInitialized().decodeJson<StageActivity>(json);
  }
}

mixin StageActivityMappable {
  String toJson();
  Map<String, dynamic> toMap();
  StageActivityCopyWith<StageActivity, StageActivity, StageActivity>
      get copyWith;
}

abstract class StageActivityCopyWith<$R, $In extends StageActivity, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, String? title, String? task, int? maxPoints});
  StageActivityCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class ClosedQuestionActivityMapper
    extends SubClassMapperBase<ClosedQuestionActivity> {
  ClosedQuestionActivityMapper._();

  static ClosedQuestionActivityMapper? _instance;
  static ClosedQuestionActivityMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ClosedQuestionActivityMapper._());
      StageActivityMapper.ensureInitialized().addSubMapper(_instance!);
      SingleChoiceActivityMapper.ensureInitialized();
      MultipleChoiceActivityMapper.ensureInitialized();
      StageActivityAnswerMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'ClosedQuestionActivity';

  static String _$id(ClosedQuestionActivity v) => v.id;
  static const Field<ClosedQuestionActivity, String> _f$id = Field('id', _$id);
  static String _$title(ClosedQuestionActivity v) => v.title;
  static const Field<ClosedQuestionActivity, String> _f$title =
      Field('title', _$title);
  static String _$task(ClosedQuestionActivity v) => v.task;
  static const Field<ClosedQuestionActivity, String> _f$task =
      Field('task', _$task);
  static int _$maxPoints(ClosedQuestionActivity v) => v.maxPoints;
  static const Field<ClosedQuestionActivity, int> _f$maxPoints =
      Field('maxPoints', _$maxPoints);
  static List<StageActivityAnswer> _$answers(ClosedQuestionActivity v) =>
      v.answers;
  static const Field<ClosedQuestionActivity, List<StageActivityAnswer>>
      _f$answers = Field('answers', _$answers);

  @override
  final MappableFields<ClosedQuestionActivity> fields = const {
    #id: _f$id,
    #title: _f$title,
    #task: _f$task,
    #maxPoints: _f$maxPoints,
    #answers: _f$answers,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = 'ClosedQuestionActivity';
  @override
  late final ClassMapperBase superMapper =
      StageActivityMapper.ensureInitialized();

  static ClosedQuestionActivity _instantiate(DecodingData data) {
    throw MapperException.missingSubclass(
        'ClosedQuestionActivity', 'type', '${data.value['type']}');
  }

  @override
  final Function instantiate = _instantiate;

  static ClosedQuestionActivity fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ClosedQuestionActivity>(map);
  }

  static ClosedQuestionActivity fromJson(String json) {
    return ensureInitialized().decodeJson<ClosedQuestionActivity>(json);
  }
}

mixin ClosedQuestionActivityMappable {
  String toJson();
  Map<String, dynamic> toMap();
  ClosedQuestionActivityCopyWith<ClosedQuestionActivity, ClosedQuestionActivity,
      ClosedQuestionActivity> get copyWith;
}

abstract class ClosedQuestionActivityCopyWith<
    $R,
    $In extends ClosedQuestionActivity,
    $Out> implements StageActivityCopyWith<$R, $In, $Out> {
  ListCopyWith<
      $R,
      StageActivityAnswer,
      StageActivityAnswerCopyWith<$R, StageActivityAnswer,
          StageActivityAnswer>> get answers;
  @override
  $R call(
      {String? id,
      String? title,
      String? task,
      int? maxPoints,
      List<StageActivityAnswer>? answers});
  ClosedQuestionActivityCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class SingleChoiceActivityMapper
    extends SubClassMapperBase<SingleChoiceActivity> {
  SingleChoiceActivityMapper._();

  static SingleChoiceActivityMapper? _instance;
  static SingleChoiceActivityMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = SingleChoiceActivityMapper._());
      ClosedQuestionActivityMapper.ensureInitialized().addSubMapper(_instance!);
      StageActivityAnswerMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'SingleChoiceActivity';

  static String _$id(SingleChoiceActivity v) => v.id;
  static const Field<SingleChoiceActivity, String> _f$id = Field('id', _$id);
  static String _$title(SingleChoiceActivity v) => v.title;
  static const Field<SingleChoiceActivity, String> _f$title =
      Field('title', _$title);
  static String _$task(SingleChoiceActivity v) => v.task;
  static const Field<SingleChoiceActivity, String> _f$task =
      Field('task', _$task);
  static int _$maxPoints(SingleChoiceActivity v) => v.maxPoints;
  static const Field<SingleChoiceActivity, int> _f$maxPoints =
      Field('maxPoints', _$maxPoints);
  static List<StageActivityAnswer> _$answers(SingleChoiceActivity v) =>
      v.answers;
  static const Field<SingleChoiceActivity, List<StageActivityAnswer>>
      _f$answers = Field('answers', _$answers);

  @override
  final MappableFields<SingleChoiceActivity> fields = const {
    #id: _f$id,
    #title: _f$title,
    #task: _f$task,
    #maxPoints: _f$maxPoints,
    #answers: _f$answers,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "singleChoice";
  @override
  late final ClassMapperBase superMapper =
      ClosedQuestionActivityMapper.ensureInitialized();

  static SingleChoiceActivity _instantiate(DecodingData data) {
    return SingleChoiceActivity(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        task: data.dec(_f$task),
        maxPoints: data.dec(_f$maxPoints),
        answers: data.dec(_f$answers));
  }

  @override
  final Function instantiate = _instantiate;

  static SingleChoiceActivity fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<SingleChoiceActivity>(map);
  }

  static SingleChoiceActivity fromJson(String json) {
    return ensureInitialized().decodeJson<SingleChoiceActivity>(json);
  }
}

mixin SingleChoiceActivityMappable {
  String toJson() {
    return SingleChoiceActivityMapper.ensureInitialized()
        .encodeJson<SingleChoiceActivity>(this as SingleChoiceActivity);
  }

  Map<String, dynamic> toMap() {
    return SingleChoiceActivityMapper.ensureInitialized()
        .encodeMap<SingleChoiceActivity>(this as SingleChoiceActivity);
  }

  SingleChoiceActivityCopyWith<SingleChoiceActivity, SingleChoiceActivity,
          SingleChoiceActivity>
      get copyWith => _SingleChoiceActivityCopyWithImpl(
          this as SingleChoiceActivity, $identity, $identity);
  @override
  String toString() {
    return SingleChoiceActivityMapper.ensureInitialized()
        .stringifyValue(this as SingleChoiceActivity);
  }

  @override
  bool operator ==(Object other) {
    return SingleChoiceActivityMapper.ensureInitialized()
        .equalsValue(this as SingleChoiceActivity, other);
  }

  @override
  int get hashCode {
    return SingleChoiceActivityMapper.ensureInitialized()
        .hashValue(this as SingleChoiceActivity);
  }
}

extension SingleChoiceActivityValueCopy<$R, $Out>
    on ObjectCopyWith<$R, SingleChoiceActivity, $Out> {
  SingleChoiceActivityCopyWith<$R, SingleChoiceActivity, $Out>
      get $asSingleChoiceActivity =>
          $base.as((v, t, t2) => _SingleChoiceActivityCopyWithImpl(v, t, t2));
}

abstract class SingleChoiceActivityCopyWith<
    $R,
    $In extends SingleChoiceActivity,
    $Out> implements ClosedQuestionActivityCopyWith<$R, $In, $Out> {
  @override
  ListCopyWith<
      $R,
      StageActivityAnswer,
      StageActivityAnswerCopyWith<$R, StageActivityAnswer,
          StageActivityAnswer>> get answers;
  @override
  $R call(
      {String? id,
      String? title,
      String? task,
      int? maxPoints,
      List<StageActivityAnswer>? answers});
  SingleChoiceActivityCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _SingleChoiceActivityCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, SingleChoiceActivity, $Out>
    implements SingleChoiceActivityCopyWith<$R, SingleChoiceActivity, $Out> {
  _SingleChoiceActivityCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<SingleChoiceActivity> $mapper =
      SingleChoiceActivityMapper.ensureInitialized();
  @override
  ListCopyWith<
      $R,
      StageActivityAnswer,
      StageActivityAnswerCopyWith<$R, StageActivityAnswer,
          StageActivityAnswer>> get answers => ListCopyWith(
      $value.answers, (v, t) => v.copyWith.$chain(t), (v) => call(answers: v));
  @override
  $R call(
          {String? id,
          String? title,
          String? task,
          int? maxPoints,
          List<StageActivityAnswer>? answers}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (task != null) #task: task,
        if (maxPoints != null) #maxPoints: maxPoints,
        if (answers != null) #answers: answers
      }));
  @override
  SingleChoiceActivity $make(CopyWithData data) => SingleChoiceActivity(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      task: data.get(#task, or: $value.task),
      maxPoints: data.get(#maxPoints, or: $value.maxPoints),
      answers: data.get(#answers, or: $value.answers));

  @override
  SingleChoiceActivityCopyWith<$R2, SingleChoiceActivity, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _SingleChoiceActivityCopyWithImpl($value, $cast, t);
}

class MultipleChoiceActivityMapper
    extends SubClassMapperBase<MultipleChoiceActivity> {
  MultipleChoiceActivityMapper._();

  static MultipleChoiceActivityMapper? _instance;
  static MultipleChoiceActivityMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = MultipleChoiceActivityMapper._());
      ClosedQuestionActivityMapper.ensureInitialized().addSubMapper(_instance!);
      StageActivityAnswerMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'MultipleChoiceActivity';

  static String _$id(MultipleChoiceActivity v) => v.id;
  static const Field<MultipleChoiceActivity, String> _f$id = Field('id', _$id);
  static String _$title(MultipleChoiceActivity v) => v.title;
  static const Field<MultipleChoiceActivity, String> _f$title =
      Field('title', _$title);
  static String _$task(MultipleChoiceActivity v) => v.task;
  static const Field<MultipleChoiceActivity, String> _f$task =
      Field('task', _$task);
  static int _$maxPoints(MultipleChoiceActivity v) => v.maxPoints;
  static const Field<MultipleChoiceActivity, int> _f$maxPoints =
      Field('maxPoints', _$maxPoints);
  static List<StageActivityAnswer> _$answers(MultipleChoiceActivity v) =>
      v.answers;
  static const Field<MultipleChoiceActivity, List<StageActivityAnswer>>
      _f$answers = Field('answers', _$answers);

  @override
  final MappableFields<MultipleChoiceActivity> fields = const {
    #id: _f$id,
    #title: _f$title,
    #task: _f$task,
    #maxPoints: _f$maxPoints,
    #answers: _f$answers,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "multipleChoice";
  @override
  late final ClassMapperBase superMapper =
      ClosedQuestionActivityMapper.ensureInitialized();

  static MultipleChoiceActivity _instantiate(DecodingData data) {
    return MultipleChoiceActivity(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        task: data.dec(_f$task),
        maxPoints: data.dec(_f$maxPoints),
        answers: data.dec(_f$answers));
  }

  @override
  final Function instantiate = _instantiate;

  static MultipleChoiceActivity fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<MultipleChoiceActivity>(map);
  }

  static MultipleChoiceActivity fromJson(String json) {
    return ensureInitialized().decodeJson<MultipleChoiceActivity>(json);
  }
}

mixin MultipleChoiceActivityMappable {
  String toJson() {
    return MultipleChoiceActivityMapper.ensureInitialized()
        .encodeJson<MultipleChoiceActivity>(this as MultipleChoiceActivity);
  }

  Map<String, dynamic> toMap() {
    return MultipleChoiceActivityMapper.ensureInitialized()
        .encodeMap<MultipleChoiceActivity>(this as MultipleChoiceActivity);
  }

  MultipleChoiceActivityCopyWith<MultipleChoiceActivity, MultipleChoiceActivity,
          MultipleChoiceActivity>
      get copyWith => _MultipleChoiceActivityCopyWithImpl(
          this as MultipleChoiceActivity, $identity, $identity);
  @override
  String toString() {
    return MultipleChoiceActivityMapper.ensureInitialized()
        .stringifyValue(this as MultipleChoiceActivity);
  }

  @override
  bool operator ==(Object other) {
    return MultipleChoiceActivityMapper.ensureInitialized()
        .equalsValue(this as MultipleChoiceActivity, other);
  }

  @override
  int get hashCode {
    return MultipleChoiceActivityMapper.ensureInitialized()
        .hashValue(this as MultipleChoiceActivity);
  }
}

extension MultipleChoiceActivityValueCopy<$R, $Out>
    on ObjectCopyWith<$R, MultipleChoiceActivity, $Out> {
  MultipleChoiceActivityCopyWith<$R, MultipleChoiceActivity, $Out>
      get $asMultipleChoiceActivity =>
          $base.as((v, t, t2) => _MultipleChoiceActivityCopyWithImpl(v, t, t2));
}

abstract class MultipleChoiceActivityCopyWith<
    $R,
    $In extends MultipleChoiceActivity,
    $Out> implements ClosedQuestionActivityCopyWith<$R, $In, $Out> {
  @override
  ListCopyWith<
      $R,
      StageActivityAnswer,
      StageActivityAnswerCopyWith<$R, StageActivityAnswer,
          StageActivityAnswer>> get answers;
  @override
  $R call(
      {String? id,
      String? title,
      String? task,
      int? maxPoints,
      List<StageActivityAnswer>? answers});
  MultipleChoiceActivityCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _MultipleChoiceActivityCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, MultipleChoiceActivity, $Out>
    implements
        MultipleChoiceActivityCopyWith<$R, MultipleChoiceActivity, $Out> {
  _MultipleChoiceActivityCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<MultipleChoiceActivity> $mapper =
      MultipleChoiceActivityMapper.ensureInitialized();
  @override
  ListCopyWith<
      $R,
      StageActivityAnswer,
      StageActivityAnswerCopyWith<$R, StageActivityAnswer,
          StageActivityAnswer>> get answers => ListCopyWith(
      $value.answers, (v, t) => v.copyWith.$chain(t), (v) => call(answers: v));
  @override
  $R call(
          {String? id,
          String? title,
          String? task,
          int? maxPoints,
          List<StageActivityAnswer>? answers}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (task != null) #task: task,
        if (maxPoints != null) #maxPoints: maxPoints,
        if (answers != null) #answers: answers
      }));
  @override
  MultipleChoiceActivity $make(CopyWithData data) => MultipleChoiceActivity(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      task: data.get(#task, or: $value.task),
      maxPoints: data.get(#maxPoints, or: $value.maxPoints),
      answers: data.get(#answers, or: $value.answers));

  @override
  MultipleChoiceActivityCopyWith<$R2, MultipleChoiceActivity, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _MultipleChoiceActivityCopyWithImpl($value, $cast, t);
}

class TextInputActivityMapper extends SubClassMapperBase<TextInputActivity> {
  TextInputActivityMapper._();

  static TextInputActivityMapper? _instance;
  static TextInputActivityMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = TextInputActivityMapper._());
      StageActivityMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'TextInputActivity';

  static String _$id(TextInputActivity v) => v.id;
  static const Field<TextInputActivity, String> _f$id = Field('id', _$id);
  static String _$title(TextInputActivity v) => v.title;
  static const Field<TextInputActivity, String> _f$title =
      Field('title', _$title);
  static String _$task(TextInputActivity v) => v.task;
  static const Field<TextInputActivity, String> _f$task = Field('task', _$task);
  static int _$maxPoints(TextInputActivity v) => v.maxPoints;
  static const Field<TextInputActivity, int> _f$maxPoints =
      Field('maxPoints', _$maxPoints);

  @override
  final MappableFields<TextInputActivity> fields = const {
    #id: _f$id,
    #title: _f$title,
    #task: _f$task,
    #maxPoints: _f$maxPoints,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "textInput";
  @override
  late final ClassMapperBase superMapper =
      StageActivityMapper.ensureInitialized();

  static TextInputActivity _instantiate(DecodingData data) {
    return TextInputActivity(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        task: data.dec(_f$task),
        maxPoints: data.dec(_f$maxPoints));
  }

  @override
  final Function instantiate = _instantiate;

  static TextInputActivity fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<TextInputActivity>(map);
  }

  static TextInputActivity fromJson(String json) {
    return ensureInitialized().decodeJson<TextInputActivity>(json);
  }
}

mixin TextInputActivityMappable {
  String toJson() {
    return TextInputActivityMapper.ensureInitialized()
        .encodeJson<TextInputActivity>(this as TextInputActivity);
  }

  Map<String, dynamic> toMap() {
    return TextInputActivityMapper.ensureInitialized()
        .encodeMap<TextInputActivity>(this as TextInputActivity);
  }

  TextInputActivityCopyWith<TextInputActivity, TextInputActivity,
          TextInputActivity>
      get copyWith => _TextInputActivityCopyWithImpl(
          this as TextInputActivity, $identity, $identity);
  @override
  String toString() {
    return TextInputActivityMapper.ensureInitialized()
        .stringifyValue(this as TextInputActivity);
  }

  @override
  bool operator ==(Object other) {
    return TextInputActivityMapper.ensureInitialized()
        .equalsValue(this as TextInputActivity, other);
  }

  @override
  int get hashCode {
    return TextInputActivityMapper.ensureInitialized()
        .hashValue(this as TextInputActivity);
  }
}

extension TextInputActivityValueCopy<$R, $Out>
    on ObjectCopyWith<$R, TextInputActivity, $Out> {
  TextInputActivityCopyWith<$R, TextInputActivity, $Out>
      get $asTextInputActivity =>
          $base.as((v, t, t2) => _TextInputActivityCopyWithImpl(v, t, t2));
}

abstract class TextInputActivityCopyWith<$R, $In extends TextInputActivity,
    $Out> implements StageActivityCopyWith<$R, $In, $Out> {
  @override
  $R call({String? id, String? title, String? task, int? maxPoints});
  TextInputActivityCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _TextInputActivityCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, TextInputActivity, $Out>
    implements TextInputActivityCopyWith<$R, TextInputActivity, $Out> {
  _TextInputActivityCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<TextInputActivity> $mapper =
      TextInputActivityMapper.ensureInitialized();
  @override
  $R call({String? id, String? title, String? task, int? maxPoints}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (task != null) #task: task,
        if (maxPoints != null) #maxPoints: maxPoints
      }));
  @override
  TextInputActivity $make(CopyWithData data) => TextInputActivity(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      task: data.get(#task, or: $value.task),
      maxPoints: data.get(#maxPoints, or: $value.maxPoints));

  @override
  TextInputActivityCopyWith<$R2, TextInputActivity, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _TextInputActivityCopyWithImpl($value, $cast, t);
}

class QrCodeActivityMapper extends SubClassMapperBase<QrCodeActivity> {
  QrCodeActivityMapper._();

  static QrCodeActivityMapper? _instance;
  static QrCodeActivityMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = QrCodeActivityMapper._());
      StageActivityMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'QrCodeActivity';

  static String _$id(QrCodeActivity v) => v.id;
  static const Field<QrCodeActivity, String> _f$id = Field('id', _$id);
  static String _$title(QrCodeActivity v) => v.title;
  static const Field<QrCodeActivity, String> _f$title = Field('title', _$title);
  static String _$task(QrCodeActivity v) => v.task;
  static const Field<QrCodeActivity, String> _f$task = Field('task', _$task);
  static int _$maxPoints(QrCodeActivity v) => v.maxPoints;
  static const Field<QrCodeActivity, int> _f$maxPoints =
      Field('maxPoints', _$maxPoints);

  @override
  final MappableFields<QrCodeActivity> fields = const {
    #id: _f$id,
    #title: _f$title,
    #task: _f$task,
    #maxPoints: _f$maxPoints,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "qrCode";
  @override
  late final ClassMapperBase superMapper =
      StageActivityMapper.ensureInitialized();

  static QrCodeActivity _instantiate(DecodingData data) {
    return QrCodeActivity(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        task: data.dec(_f$task),
        maxPoints: data.dec(_f$maxPoints));
  }

  @override
  final Function instantiate = _instantiate;

  static QrCodeActivity fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<QrCodeActivity>(map);
  }

  static QrCodeActivity fromJson(String json) {
    return ensureInitialized().decodeJson<QrCodeActivity>(json);
  }
}

mixin QrCodeActivityMappable {
  String toJson() {
    return QrCodeActivityMapper.ensureInitialized()
        .encodeJson<QrCodeActivity>(this as QrCodeActivity);
  }

  Map<String, dynamic> toMap() {
    return QrCodeActivityMapper.ensureInitialized()
        .encodeMap<QrCodeActivity>(this as QrCodeActivity);
  }

  QrCodeActivityCopyWith<QrCodeActivity, QrCodeActivity, QrCodeActivity>
      get copyWith => _QrCodeActivityCopyWithImpl(
          this as QrCodeActivity, $identity, $identity);
  @override
  String toString() {
    return QrCodeActivityMapper.ensureInitialized()
        .stringifyValue(this as QrCodeActivity);
  }

  @override
  bool operator ==(Object other) {
    return QrCodeActivityMapper.ensureInitialized()
        .equalsValue(this as QrCodeActivity, other);
  }

  @override
  int get hashCode {
    return QrCodeActivityMapper.ensureInitialized()
        .hashValue(this as QrCodeActivity);
  }
}

extension QrCodeActivityValueCopy<$R, $Out>
    on ObjectCopyWith<$R, QrCodeActivity, $Out> {
  QrCodeActivityCopyWith<$R, QrCodeActivity, $Out> get $asQrCodeActivity =>
      $base.as((v, t, t2) => _QrCodeActivityCopyWithImpl(v, t, t2));
}

abstract class QrCodeActivityCopyWith<$R, $In extends QrCodeActivity, $Out>
    implements StageActivityCopyWith<$R, $In, $Out> {
  @override
  $R call({String? id, String? title, String? task, int? maxPoints});
  QrCodeActivityCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _QrCodeActivityCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, QrCodeActivity, $Out>
    implements QrCodeActivityCopyWith<$R, QrCodeActivity, $Out> {
  _QrCodeActivityCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<QrCodeActivity> $mapper =
      QrCodeActivityMapper.ensureInitialized();
  @override
  $R call({String? id, String? title, String? task, int? maxPoints}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (task != null) #task: task,
        if (maxPoints != null) #maxPoints: maxPoints
      }));
  @override
  QrCodeActivity $make(CopyWithData data) => QrCodeActivity(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      task: data.get(#task, or: $value.task),
      maxPoints: data.get(#maxPoints, or: $value.maxPoints));

  @override
  QrCodeActivityCopyWith<$R2, QrCodeActivity, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _QrCodeActivityCopyWithImpl($value, $cast, t);
}

class QrCodeAnswerValueMapper extends ClassMapperBase<QrCodeAnswerValue> {
  QrCodeAnswerValueMapper._();

  static QrCodeAnswerValueMapper? _instance;
  static QrCodeAnswerValueMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = QrCodeAnswerValueMapper._());
    }
    return _instance!;
  }

  @override
  final String id = 'QrCodeAnswerValue';

  static int _$percentage(QrCodeAnswerValue v) => v.percentage;
  static const Field<QrCodeAnswerValue, int> _f$percentage =
      Field('percentage', _$percentage);
  static String? _$label(QrCodeAnswerValue v) => v.label;
  static const Field<QrCodeAnswerValue, String> _f$label =
      Field('label', _$label, opt: true);

  @override
  final MappableFields<QrCodeAnswerValue> fields = const {
    #percentage: _f$percentage,
    #label: _f$label,
  };

  static QrCodeAnswerValue _instantiate(DecodingData data) {
    return QrCodeAnswerValue(data.dec(_f$percentage),
        label: data.dec(_f$label));
  }

  @override
  final Function instantiate = _instantiate;

  static QrCodeAnswerValue fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<QrCodeAnswerValue>(map);
  }

  static QrCodeAnswerValue fromJson(String json) {
    return ensureInitialized().decodeJson<QrCodeAnswerValue>(json);
  }
}

mixin QrCodeAnswerValueMappable {
  String toJson() {
    return QrCodeAnswerValueMapper.ensureInitialized()
        .encodeJson<QrCodeAnswerValue>(this as QrCodeAnswerValue);
  }

  Map<String, dynamic> toMap() {
    return QrCodeAnswerValueMapper.ensureInitialized()
        .encodeMap<QrCodeAnswerValue>(this as QrCodeAnswerValue);
  }

  QrCodeAnswerValueCopyWith<QrCodeAnswerValue, QrCodeAnswerValue,
          QrCodeAnswerValue>
      get copyWith => _QrCodeAnswerValueCopyWithImpl(
          this as QrCodeAnswerValue, $identity, $identity);
  @override
  String toString() {
    return QrCodeAnswerValueMapper.ensureInitialized()
        .stringifyValue(this as QrCodeAnswerValue);
  }

  @override
  bool operator ==(Object other) {
    return QrCodeAnswerValueMapper.ensureInitialized()
        .equalsValue(this as QrCodeAnswerValue, other);
  }

  @override
  int get hashCode {
    return QrCodeAnswerValueMapper.ensureInitialized()
        .hashValue(this as QrCodeAnswerValue);
  }
}

extension QrCodeAnswerValueValueCopy<$R, $Out>
    on ObjectCopyWith<$R, QrCodeAnswerValue, $Out> {
  QrCodeAnswerValueCopyWith<$R, QrCodeAnswerValue, $Out>
      get $asQrCodeAnswerValue =>
          $base.as((v, t, t2) => _QrCodeAnswerValueCopyWithImpl(v, t, t2));
}

abstract class QrCodeAnswerValueCopyWith<$R, $In extends QrCodeAnswerValue,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  $R call({int? percentage, String? label});
  QrCodeAnswerValueCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _QrCodeAnswerValueCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, QrCodeAnswerValue, $Out>
    implements QrCodeAnswerValueCopyWith<$R, QrCodeAnswerValue, $Out> {
  _QrCodeAnswerValueCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<QrCodeAnswerValue> $mapper =
      QrCodeAnswerValueMapper.ensureInitialized();
  @override
  $R call({int? percentage, Object? label = $none}) =>
      $apply(FieldCopyWithData({
        if (percentage != null) #percentage: percentage,
        if (label != $none) #label: label
      }));
  @override
  QrCodeAnswerValue $make(CopyWithData data) =>
      QrCodeAnswerValue(data.get(#percentage, or: $value.percentage),
          label: data.get(#label, or: $value.label));

  @override
  QrCodeAnswerValueCopyWith<$R2, QrCodeAnswerValue, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _QrCodeAnswerValueCopyWithImpl($value, $cast, t);
}
