// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'stage_information.dart';

class StageInformationMapper extends ClassMapperBase<StageInformation> {
  StageInformationMapper._();

  static StageInformationMapper? _instance;
  static StageInformationMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = StageInformationMapper._());
      AttachmentMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'StageInformation';

  static String _$id(StageInformation v) => v.id;
  static const Field<StageInformation, String> _f$id = Field('id', _$id);
  static String _$title(StageInformation v) => v.title;
  static const Field<StageInformation, String> _f$title =
      Field('title', _$title);
  static String _$content(StageInformation v) => v.content;
  static const Field<StageInformation, String> _f$content =
      Field('content', _$content);
  static List<Attachment> _$attachments(StageInformation v) => v.attachments;
  static const Field<StageInformation, List<Attachment>> _f$attachments =
      Field('attachments', _$attachments);

  @override
  final MappableFields<StageInformation> fields = const {
    #id: _f$id,
    #title: _f$title,
    #content: _f$content,
    #attachments: _f$attachments,
  };

  static StageInformation _instantiate(DecodingData data) {
    return StageInformation(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        content: data.dec(_f$content),
        attachments: data.dec(_f$attachments));
  }

  @override
  final Function instantiate = _instantiate;

  static StageInformation fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<StageInformation>(map);
  }

  static StageInformation fromJson(String json) {
    return ensureInitialized().decodeJson<StageInformation>(json);
  }
}

mixin StageInformationMappable {
  String toJson() {
    return StageInformationMapper.ensureInitialized()
        .encodeJson<StageInformation>(this as StageInformation);
  }

  Map<String, dynamic> toMap() {
    return StageInformationMapper.ensureInitialized()
        .encodeMap<StageInformation>(this as StageInformation);
  }

  StageInformationCopyWith<StageInformation, StageInformation, StageInformation>
      get copyWith => _StageInformationCopyWithImpl(
          this as StageInformation, $identity, $identity);
  @override
  String toString() {
    return StageInformationMapper.ensureInitialized()
        .stringifyValue(this as StageInformation);
  }

  @override
  bool operator ==(Object other) {
    return StageInformationMapper.ensureInitialized()
        .equalsValue(this as StageInformation, other);
  }

  @override
  int get hashCode {
    return StageInformationMapper.ensureInitialized()
        .hashValue(this as StageInformation);
  }
}

extension StageInformationValueCopy<$R, $Out>
    on ObjectCopyWith<$R, StageInformation, $Out> {
  StageInformationCopyWith<$R, StageInformation, $Out>
      get $asStageInformation =>
          $base.as((v, t, t2) => _StageInformationCopyWithImpl(v, t, t2));
}

abstract class StageInformationCopyWith<$R, $In extends StageInformation, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, Attachment, ObjectCopyWith<$R, Attachment, Attachment>>
      get attachments;
  $R call(
      {String? id,
      String? title,
      String? content,
      List<Attachment>? attachments});
  StageInformationCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _StageInformationCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, StageInformation, $Out>
    implements StageInformationCopyWith<$R, StageInformation, $Out> {
  _StageInformationCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<StageInformation> $mapper =
      StageInformationMapper.ensureInitialized();
  @override
  ListCopyWith<$R, Attachment, ObjectCopyWith<$R, Attachment, Attachment>>
      get attachments => ListCopyWith(
          $value.attachments,
          (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(attachments: v));
  @override
  $R call(
          {String? id,
          String? title,
          String? content,
          List<Attachment>? attachments}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (content != null) #content: content,
        if (attachments != null) #attachments: attachments
      }));
  @override
  StageInformation $make(CopyWithData data) => StageInformation(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      content: data.get(#content, or: $value.content),
      attachments: data.get(#attachments, or: $value.attachments));

  @override
  StageInformationCopyWith<$R2, StageInformation, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _StageInformationCopyWithImpl($value, $cast, t);
}
