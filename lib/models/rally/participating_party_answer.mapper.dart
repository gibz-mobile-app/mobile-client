// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'participating_party_answer.dart';

class ParticipatingPartyAnswerMapper
    extends ClassMapperBase<ParticipatingPartyAnswer> {
  ParticipatingPartyAnswerMapper._();

  static ParticipatingPartyAnswerMapper? _instance;
  static ParticipatingPartyAnswerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals
          .use(_instance = ParticipatingPartyAnswerMapper._());
      ClosedQuestionAnswerMapper.ensureInitialized();
      OpenQuestionAnswerMapper.ensureInitialized();
      QrCodeActivityAnswerMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'ParticipatingPartyAnswer';

  static String _$id(ParticipatingPartyAnswer v) => v.id;
  static const Field<ParticipatingPartyAnswer, String> _f$id =
      Field('id', _$id);
  static bool _$isCorrect(ParticipatingPartyAnswer v) => v.isCorrect;
  static const Field<ParticipatingPartyAnswer, bool> _f$isCorrect =
      Field('isCorrect', _$isCorrect);

  @override
  final MappableFields<ParticipatingPartyAnswer> fields = const {
    #id: _f$id,
    #isCorrect: _f$isCorrect,
  };

  static ParticipatingPartyAnswer _instantiate(DecodingData data) {
    throw MapperException.missingSubclass(
        'ParticipatingPartyAnswer', 'type', '${data.value['type']}');
  }

  @override
  final Function instantiate = _instantiate;

  static ParticipatingPartyAnswer fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ParticipatingPartyAnswer>(map);
  }

  static ParticipatingPartyAnswer fromJson(String json) {
    return ensureInitialized().decodeJson<ParticipatingPartyAnswer>(json);
  }
}

mixin ParticipatingPartyAnswerMappable {
  String toJson();
  Map<String, dynamic> toMap();
  ParticipatingPartyAnswerCopyWith<ParticipatingPartyAnswer,
      ParticipatingPartyAnswer, ParticipatingPartyAnswer> get copyWith;
}

abstract class ParticipatingPartyAnswerCopyWith<
    $R,
    $In extends ParticipatingPartyAnswer,
    $Out> implements ClassCopyWith<$R, $In, $Out> {
  $R call({String? id, bool? isCorrect});
  ParticipatingPartyAnswerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class ClosedQuestionAnswerMapper
    extends SubClassMapperBase<ClosedQuestionAnswer> {
  ClosedQuestionAnswerMapper._();

  static ClosedQuestionAnswerMapper? _instance;
  static ClosedQuestionAnswerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ClosedQuestionAnswerMapper._());
      ParticipatingPartyAnswerMapper.ensureInitialized()
          .addSubMapper(_instance!);
      StageActivityAnswerMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'ClosedQuestionAnswer';

  static String _$id(ClosedQuestionAnswer v) => v.id;
  static const Field<ClosedQuestionAnswer, String> _f$id = Field('id', _$id);
  static bool _$isCorrect(ClosedQuestionAnswer v) => v.isCorrect;
  static const Field<ClosedQuestionAnswer, bool> _f$isCorrect =
      Field('isCorrect', _$isCorrect);
  static StageActivityAnswer _$answer(ClosedQuestionAnswer v) => v.answer;
  static const Field<ClosedQuestionAnswer, StageActivityAnswer> _f$answer =
      Field('answer', _$answer);

  @override
  final MappableFields<ClosedQuestionAnswer> fields = const {
    #id: _f$id,
    #isCorrect: _f$isCorrect,
    #answer: _f$answer,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "closedQuestionAnswer";
  @override
  late final ClassMapperBase superMapper =
      ParticipatingPartyAnswerMapper.ensureInitialized();

  static ClosedQuestionAnswer _instantiate(DecodingData data) {
    return ClosedQuestionAnswer(
        id: data.dec(_f$id),
        isCorrect: data.dec(_f$isCorrect),
        answer: data.dec(_f$answer));
  }

  @override
  final Function instantiate = _instantiate;

  static ClosedQuestionAnswer fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ClosedQuestionAnswer>(map);
  }

  static ClosedQuestionAnswer fromJson(String json) {
    return ensureInitialized().decodeJson<ClosedQuestionAnswer>(json);
  }
}

mixin ClosedQuestionAnswerMappable {
  String toJson() {
    return ClosedQuestionAnswerMapper.ensureInitialized()
        .encodeJson<ClosedQuestionAnswer>(this as ClosedQuestionAnswer);
  }

  Map<String, dynamic> toMap() {
    return ClosedQuestionAnswerMapper.ensureInitialized()
        .encodeMap<ClosedQuestionAnswer>(this as ClosedQuestionAnswer);
  }

  ClosedQuestionAnswerCopyWith<ClosedQuestionAnswer, ClosedQuestionAnswer,
          ClosedQuestionAnswer>
      get copyWith => _ClosedQuestionAnswerCopyWithImpl(
          this as ClosedQuestionAnswer, $identity, $identity);
  @override
  String toString() {
    return ClosedQuestionAnswerMapper.ensureInitialized()
        .stringifyValue(this as ClosedQuestionAnswer);
  }

  @override
  bool operator ==(Object other) {
    return ClosedQuestionAnswerMapper.ensureInitialized()
        .equalsValue(this as ClosedQuestionAnswer, other);
  }

  @override
  int get hashCode {
    return ClosedQuestionAnswerMapper.ensureInitialized()
        .hashValue(this as ClosedQuestionAnswer);
  }
}

extension ClosedQuestionAnswerValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ClosedQuestionAnswer, $Out> {
  ClosedQuestionAnswerCopyWith<$R, ClosedQuestionAnswer, $Out>
      get $asClosedQuestionAnswer =>
          $base.as((v, t, t2) => _ClosedQuestionAnswerCopyWithImpl(v, t, t2));
}

abstract class ClosedQuestionAnswerCopyWith<
    $R,
    $In extends ClosedQuestionAnswer,
    $Out> implements ParticipatingPartyAnswerCopyWith<$R, $In, $Out> {
  StageActivityAnswerCopyWith<$R, StageActivityAnswer, StageActivityAnswer>
      get answer;
  @override
  $R call({String? id, bool? isCorrect, StageActivityAnswer? answer});
  ClosedQuestionAnswerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _ClosedQuestionAnswerCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ClosedQuestionAnswer, $Out>
    implements ClosedQuestionAnswerCopyWith<$R, ClosedQuestionAnswer, $Out> {
  _ClosedQuestionAnswerCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ClosedQuestionAnswer> $mapper =
      ClosedQuestionAnswerMapper.ensureInitialized();
  @override
  StageActivityAnswerCopyWith<$R, StageActivityAnswer, StageActivityAnswer>
      get answer => $value.answer.copyWith.$chain((v) => call(answer: v));
  @override
  $R call({String? id, bool? isCorrect, StageActivityAnswer? answer}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (isCorrect != null) #isCorrect: isCorrect,
        if (answer != null) #answer: answer
      }));
  @override
  ClosedQuestionAnswer $make(CopyWithData data) => ClosedQuestionAnswer(
      id: data.get(#id, or: $value.id),
      isCorrect: data.get(#isCorrect, or: $value.isCorrect),
      answer: data.get(#answer, or: $value.answer));

  @override
  ClosedQuestionAnswerCopyWith<$R2, ClosedQuestionAnswer, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _ClosedQuestionAnswerCopyWithImpl($value, $cast, t);
}

class OpenQuestionAnswerMapper extends SubClassMapperBase<OpenQuestionAnswer> {
  OpenQuestionAnswerMapper._();

  static OpenQuestionAnswerMapper? _instance;
  static OpenQuestionAnswerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = OpenQuestionAnswerMapper._());
      ParticipatingPartyAnswerMapper.ensureInitialized()
          .addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'OpenQuestionAnswer';

  static String _$id(OpenQuestionAnswer v) => v.id;
  static const Field<OpenQuestionAnswer, String> _f$id = Field('id', _$id);
  static bool _$isCorrect(OpenQuestionAnswer v) => v.isCorrect;
  static const Field<OpenQuestionAnswer, bool> _f$isCorrect =
      Field('isCorrect', _$isCorrect);
  static String _$answerText(OpenQuestionAnswer v) => v.answerText;
  static const Field<OpenQuestionAnswer, String> _f$answerText =
      Field('answerText', _$answerText);

  @override
  final MappableFields<OpenQuestionAnswer> fields = const {
    #id: _f$id,
    #isCorrect: _f$isCorrect,
    #answerText: _f$answerText,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "openQuestionAnswer";
  @override
  late final ClassMapperBase superMapper =
      ParticipatingPartyAnswerMapper.ensureInitialized();

  static OpenQuestionAnswer _instantiate(DecodingData data) {
    return OpenQuestionAnswer(
        id: data.dec(_f$id),
        isCorrect: data.dec(_f$isCorrect),
        answerText: data.dec(_f$answerText));
  }

  @override
  final Function instantiate = _instantiate;

  static OpenQuestionAnswer fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<OpenQuestionAnswer>(map);
  }

  static OpenQuestionAnswer fromJson(String json) {
    return ensureInitialized().decodeJson<OpenQuestionAnswer>(json);
  }
}

mixin OpenQuestionAnswerMappable {
  String toJson() {
    return OpenQuestionAnswerMapper.ensureInitialized()
        .encodeJson<OpenQuestionAnswer>(this as OpenQuestionAnswer);
  }

  Map<String, dynamic> toMap() {
    return OpenQuestionAnswerMapper.ensureInitialized()
        .encodeMap<OpenQuestionAnswer>(this as OpenQuestionAnswer);
  }

  OpenQuestionAnswerCopyWith<OpenQuestionAnswer, OpenQuestionAnswer,
          OpenQuestionAnswer>
      get copyWith => _OpenQuestionAnswerCopyWithImpl(
          this as OpenQuestionAnswer, $identity, $identity);
  @override
  String toString() {
    return OpenQuestionAnswerMapper.ensureInitialized()
        .stringifyValue(this as OpenQuestionAnswer);
  }

  @override
  bool operator ==(Object other) {
    return OpenQuestionAnswerMapper.ensureInitialized()
        .equalsValue(this as OpenQuestionAnswer, other);
  }

  @override
  int get hashCode {
    return OpenQuestionAnswerMapper.ensureInitialized()
        .hashValue(this as OpenQuestionAnswer);
  }
}

extension OpenQuestionAnswerValueCopy<$R, $Out>
    on ObjectCopyWith<$R, OpenQuestionAnswer, $Out> {
  OpenQuestionAnswerCopyWith<$R, OpenQuestionAnswer, $Out>
      get $asOpenQuestionAnswer =>
          $base.as((v, t, t2) => _OpenQuestionAnswerCopyWithImpl(v, t, t2));
}

abstract class OpenQuestionAnswerCopyWith<$R, $In extends OpenQuestionAnswer,
    $Out> implements ParticipatingPartyAnswerCopyWith<$R, $In, $Out> {
  @override
  $R call({String? id, bool? isCorrect, String? answerText});
  OpenQuestionAnswerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _OpenQuestionAnswerCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, OpenQuestionAnswer, $Out>
    implements OpenQuestionAnswerCopyWith<$R, OpenQuestionAnswer, $Out> {
  _OpenQuestionAnswerCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<OpenQuestionAnswer> $mapper =
      OpenQuestionAnswerMapper.ensureInitialized();
  @override
  $R call({String? id, bool? isCorrect, String? answerText}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (isCorrect != null) #isCorrect: isCorrect,
        if (answerText != null) #answerText: answerText
      }));
  @override
  OpenQuestionAnswer $make(CopyWithData data) => OpenQuestionAnswer(
      id: data.get(#id, or: $value.id),
      isCorrect: data.get(#isCorrect, or: $value.isCorrect),
      answerText: data.get(#answerText, or: $value.answerText));

  @override
  OpenQuestionAnswerCopyWith<$R2, OpenQuestionAnswer, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _OpenQuestionAnswerCopyWithImpl($value, $cast, t);
}

class QrCodeActivityAnswerMapper
    extends SubClassMapperBase<QrCodeActivityAnswer> {
  QrCodeActivityAnswerMapper._();

  static QrCodeActivityAnswerMapper? _instance;
  static QrCodeActivityAnswerMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = QrCodeActivityAnswerMapper._());
      ParticipatingPartyAnswerMapper.ensureInitialized()
          .addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'QrCodeActivityAnswer';

  static String _$id(QrCodeActivityAnswer v) => v.id;
  static const Field<QrCodeActivityAnswer, String> _f$id = Field('id', _$id);
  static bool _$isCorrect(QrCodeActivityAnswer v) => v.isCorrect;
  static const Field<QrCodeActivityAnswer, bool> _f$isCorrect =
      Field('isCorrect', _$isCorrect);
  static int _$percentage(QrCodeActivityAnswer v) => v.percentage;
  static const Field<QrCodeActivityAnswer, int> _f$percentage =
      Field('percentage', _$percentage);
  static String? _$label(QrCodeActivityAnswer v) => v.label;
  static const Field<QrCodeActivityAnswer, String> _f$label =
      Field('label', _$label, opt: true);

  @override
  final MappableFields<QrCodeActivityAnswer> fields = const {
    #id: _f$id,
    #isCorrect: _f$isCorrect,
    #percentage: _f$percentage,
    #label: _f$label,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "qrCodeActivityAnswer";
  @override
  late final ClassMapperBase superMapper =
      ParticipatingPartyAnswerMapper.ensureInitialized();

  static QrCodeActivityAnswer _instantiate(DecodingData data) {
    return QrCodeActivityAnswer(
        id: data.dec(_f$id),
        isCorrect: data.dec(_f$isCorrect),
        percentage: data.dec(_f$percentage),
        label: data.dec(_f$label));
  }

  @override
  final Function instantiate = _instantiate;

  static QrCodeActivityAnswer fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<QrCodeActivityAnswer>(map);
  }

  static QrCodeActivityAnswer fromJson(String json) {
    return ensureInitialized().decodeJson<QrCodeActivityAnswer>(json);
  }
}

mixin QrCodeActivityAnswerMappable {
  String toJson() {
    return QrCodeActivityAnswerMapper.ensureInitialized()
        .encodeJson<QrCodeActivityAnswer>(this as QrCodeActivityAnswer);
  }

  Map<String, dynamic> toMap() {
    return QrCodeActivityAnswerMapper.ensureInitialized()
        .encodeMap<QrCodeActivityAnswer>(this as QrCodeActivityAnswer);
  }

  QrCodeActivityAnswerCopyWith<QrCodeActivityAnswer, QrCodeActivityAnswer,
          QrCodeActivityAnswer>
      get copyWith => _QrCodeActivityAnswerCopyWithImpl(
          this as QrCodeActivityAnswer, $identity, $identity);
  @override
  String toString() {
    return QrCodeActivityAnswerMapper.ensureInitialized()
        .stringifyValue(this as QrCodeActivityAnswer);
  }

  @override
  bool operator ==(Object other) {
    return QrCodeActivityAnswerMapper.ensureInitialized()
        .equalsValue(this as QrCodeActivityAnswer, other);
  }

  @override
  int get hashCode {
    return QrCodeActivityAnswerMapper.ensureInitialized()
        .hashValue(this as QrCodeActivityAnswer);
  }
}

extension QrCodeActivityAnswerValueCopy<$R, $Out>
    on ObjectCopyWith<$R, QrCodeActivityAnswer, $Out> {
  QrCodeActivityAnswerCopyWith<$R, QrCodeActivityAnswer, $Out>
      get $asQrCodeActivityAnswer =>
          $base.as((v, t, t2) => _QrCodeActivityAnswerCopyWithImpl(v, t, t2));
}

abstract class QrCodeActivityAnswerCopyWith<
    $R,
    $In extends QrCodeActivityAnswer,
    $Out> implements ParticipatingPartyAnswerCopyWith<$R, $In, $Out> {
  @override
  $R call({String? id, bool? isCorrect, int? percentage, String? label});
  QrCodeActivityAnswerCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(
      Then<$Out2, $R2> t);
}

class _QrCodeActivityAnswerCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, QrCodeActivityAnswer, $Out>
    implements QrCodeActivityAnswerCopyWith<$R, QrCodeActivityAnswer, $Out> {
  _QrCodeActivityAnswerCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<QrCodeActivityAnswer> $mapper =
      QrCodeActivityAnswerMapper.ensureInitialized();
  @override
  $R call(
          {String? id,
          bool? isCorrect,
          int? percentage,
          Object? label = $none}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (isCorrect != null) #isCorrect: isCorrect,
        if (percentage != null) #percentage: percentage,
        if (label != $none) #label: label
      }));
  @override
  QrCodeActivityAnswer $make(CopyWithData data) => QrCodeActivityAnswer(
      id: data.get(#id, or: $value.id),
      isCorrect: data.get(#isCorrect, or: $value.isCorrect),
      percentage: data.get(#percentage, or: $value.percentage),
      label: data.get(#label, or: $value.label));

  @override
  QrCodeActivityAnswerCopyWith<$R2, QrCodeActivityAnswer, $Out2>
      $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
          _QrCodeActivityAnswerCopyWithImpl($value, $cast, t);
}
