import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'participating_party.mapper.dart';

@MappableClass()
class ParticipatingParty extends Equatable with ParticipatingPartyMappable {
  final String id;
  final String? title;

  const ParticipatingParty({required this.id, required this.title});

  @override
  List<Object?> get props => [id];
}
