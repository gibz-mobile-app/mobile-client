import 'package:gibz_mobileapp/models/fitness_check/attempts_response.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';

import 'students_manual.dart';

void initializeMappers() {
  _initializeStudentsManualMappers();
  _initializeMealInformationMappers();
  _initializeFitnessCheckMappers();
}

void _initializeStudentsManualMappers() {
  ArticleMapper.ensureInitialized();
  AttachmentMapper.ensureInitialized();
  ExternalLinkMapper.ensureInitialized();
  PdfDocumentMapper.ensureInitialized();
  TagMapper.ensureInitialized();
  VideoMapper.ensureInitialized();
}

void _initializeMealInformationMappers() {
  DayMapper.ensureInitialized();
  MealIconMapper.ensureInitialized();
  MealMapper.ensureInitialized();
  NutritionalValueMapper.ensureInitialized();
}

void _initializeFitnessCheckMappers() {
  CoreStrengthAttemptMapper.ensureInitialized();
  MedicineBallPushAttemptMapper.ensureInitialized();
  OneLegStandAttemptMapper.ensureInitialized();
  ShuttleRunAttemptMapper.ensureInitialized();
  StandingLongJumpAttemptMapper.ensureInitialized();
  TwelveMinutesRunAttemptMapper.ensureInitialized();
  AttemptsResponseMapper.ensureInitialized();
}
