import 'package:dart_mappable/dart_mappable.dart';
import 'attachment.dart';

part 'video.mapper.dart';

@MappableClass(discriminatorValue: "video")
class Video extends Attachment with VideoMappable {
  const Video({
    required super.id,
    required super.title,
    required super.url,
  });

  static const fromMap = VideoMapper.fromMap;
  static const fromJson = VideoMapper.fromJson;
}
