// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'article.dart';

class ArticleMapper extends ClassMapperBase<Article> {
  ArticleMapper._();

  static ArticleMapper? _instance;
  static ArticleMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ArticleMapper._());
      TagMapper.ensureInitialized();
      AttachmentMapper.ensureInitialized();
    }
    return _instance!;
  }

  @override
  final String id = 'Article';

  static String _$id(Article v) => v.id;
  static const Field<Article, String> _f$id = Field('id', _$id);
  static String _$title(Article v) => v.title;
  static const Field<Article, String> _f$title = Field('title', _$title);
  static String _$content(Article v) => v.content;
  static const Field<Article, String> _f$content = Field('content', _$content);
  static int _$viewCounter(Article v) => v.viewCounter;
  static const Field<Article, int> _f$viewCounter =
      Field('viewCounter', _$viewCounter, opt: true, def: 0);
  static List<Tag> _$tags(Article v) => v.tags;
  static const Field<Article, List<Tag>> _f$tags =
      Field('tags', _$tags, opt: true, def: const []);
  static List<Attachment> _$attachments(Article v) => v.attachments;
  static const Field<Article, List<Attachment>> _f$attachments =
      Field('attachments', _$attachments, opt: true, def: const []);
  static DateTime _$lastUpdated(Article v) => v.lastUpdated;
  static const Field<Article, DateTime> _f$lastUpdated =
      Field('lastUpdated', _$lastUpdated);

  @override
  final MappableFields<Article> fields = const {
    #id: _f$id,
    #title: _f$title,
    #content: _f$content,
    #viewCounter: _f$viewCounter,
    #tags: _f$tags,
    #attachments: _f$attachments,
    #lastUpdated: _f$lastUpdated,
  };

  static Article _instantiate(DecodingData data) {
    return Article(
        id: data.dec(_f$id),
        title: data.dec(_f$title),
        content: data.dec(_f$content),
        viewCounter: data.dec(_f$viewCounter),
        tags: data.dec(_f$tags),
        attachments: data.dec(_f$attachments),
        lastUpdated: data.dec(_f$lastUpdated));
  }

  @override
  final Function instantiate = _instantiate;

  static Article fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<Article>(map);
  }

  static Article fromJson(String json) {
    return ensureInitialized().decodeJson<Article>(json);
  }
}

mixin ArticleMappable {
  String toJson() {
    return ArticleMapper.ensureInitialized()
        .encodeJson<Article>(this as Article);
  }

  Map<String, dynamic> toMap() {
    return ArticleMapper.ensureInitialized()
        .encodeMap<Article>(this as Article);
  }

  ArticleCopyWith<Article, Article, Article> get copyWith =>
      _ArticleCopyWithImpl(this as Article, $identity, $identity);
  @override
  String toString() {
    return ArticleMapper.ensureInitialized().stringifyValue(this as Article);
  }

  @override
  bool operator ==(Object other) {
    return ArticleMapper.ensureInitialized()
        .equalsValue(this as Article, other);
  }

  @override
  int get hashCode {
    return ArticleMapper.ensureInitialized().hashValue(this as Article);
  }
}

extension ArticleValueCopy<$R, $Out> on ObjectCopyWith<$R, Article, $Out> {
  ArticleCopyWith<$R, Article, $Out> get $asArticle =>
      $base.as((v, t, t2) => _ArticleCopyWithImpl(v, t, t2));
}

abstract class ArticleCopyWith<$R, $In extends Article, $Out>
    implements ClassCopyWith<$R, $In, $Out> {
  ListCopyWith<$R, Tag, TagCopyWith<$R, Tag, Tag>> get tags;
  ListCopyWith<$R, Attachment, ObjectCopyWith<$R, Attachment, Attachment>>
      get attachments;
  $R call(
      {String? id,
      String? title,
      String? content,
      int? viewCounter,
      List<Tag>? tags,
      List<Attachment>? attachments,
      DateTime? lastUpdated});
  ArticleCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _ArticleCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, Article, $Out>
    implements ArticleCopyWith<$R, Article, $Out> {
  _ArticleCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<Article> $mapper =
      ArticleMapper.ensureInitialized();
  @override
  ListCopyWith<$R, Tag, TagCopyWith<$R, Tag, Tag>> get tags => ListCopyWith(
      $value.tags, (v, t) => v.copyWith.$chain(t), (v) => call(tags: v));
  @override
  ListCopyWith<$R, Attachment, ObjectCopyWith<$R, Attachment, Attachment>>
      get attachments => ListCopyWith(
          $value.attachments,
          (v, t) => ObjectCopyWith(v, $identity, t),
          (v) => call(attachments: v));
  @override
  $R call(
          {String? id,
          String? title,
          String? content,
          int? viewCounter,
          List<Tag>? tags,
          List<Attachment>? attachments,
          DateTime? lastUpdated}) =>
      $apply(FieldCopyWithData({
        if (id != null) #id: id,
        if (title != null) #title: title,
        if (content != null) #content: content,
        if (viewCounter != null) #viewCounter: viewCounter,
        if (tags != null) #tags: tags,
        if (attachments != null) #attachments: attachments,
        if (lastUpdated != null) #lastUpdated: lastUpdated
      }));
  @override
  Article $make(CopyWithData data) => Article(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      content: data.get(#content, or: $value.content),
      viewCounter: data.get(#viewCounter, or: $value.viewCounter),
      tags: data.get(#tags, or: $value.tags),
      attachments: data.get(#attachments, or: $value.attachments),
      lastUpdated: data.get(#lastUpdated, or: $value.lastUpdated));

  @override
  ArticleCopyWith<$R2, Article, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t) =>
      _ArticleCopyWithImpl($value, $cast, t);
}
