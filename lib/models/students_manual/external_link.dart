import 'package:dart_mappable/dart_mappable.dart';
import 'attachment.dart';

part 'external_link.mapper.dart';

@MappableClass(discriminatorValue: "link")
class ExternalLink extends Attachment with ExternalLinkMappable {
  const ExternalLink({
    required super.id,
    required super.title,
    required super.url,
  });

  static const fromMap = ExternalLinkMapper.fromMap;
  static const fromJson = ExternalLinkMapper.fromJson;
}
