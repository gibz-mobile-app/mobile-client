import 'package:dart_mappable/dart_mappable.dart';
import 'package:equatable/equatable.dart';

part 'tag.mapper.dart';

@MappableClass()
class Tag extends Equatable with TagMappable {
  final String id;
  final String title;

  @override
  List<Object> get props => [id];

  const Tag({required this.id, required this.title});

  static const fromMap = TagMapper.fromMap;
  static const fromJson = TagMapper.fromJson;
}
