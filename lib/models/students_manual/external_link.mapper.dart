// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, unnecessary_cast, override_on_non_overriding_member
// ignore_for_file: strict_raw_type, inference_failure_on_untyped_parameter

part of 'external_link.dart';

class ExternalLinkMapper extends SubClassMapperBase<ExternalLink> {
  ExternalLinkMapper._();

  static ExternalLinkMapper? _instance;
  static ExternalLinkMapper ensureInitialized() {
    if (_instance == null) {
      MapperContainer.globals.use(_instance = ExternalLinkMapper._());
      AttachmentMapper.ensureInitialized().addSubMapper(_instance!);
    }
    return _instance!;
  }

  @override
  final String id = 'ExternalLink';

  static String _$id(ExternalLink v) => v.id;
  static dynamic _arg$id(f) => f<String>();
  static const Field<ExternalLink, dynamic> _f$id =
      Field('id', _$id, arg: _arg$id);
  static String _$title(ExternalLink v) => v.title;
  static dynamic _arg$title(f) => f<String>();
  static const Field<ExternalLink, dynamic> _f$title =
      Field('title', _$title, arg: _arg$title);
  static String _$url(ExternalLink v) => v.url;
  static dynamic _arg$url(f) => f<String>();
  static const Field<ExternalLink, dynamic> _f$url =
      Field('url', _$url, arg: _arg$url);

  @override
  final MappableFields<ExternalLink> fields = const {
    #id: _f$id,
    #title: _f$title,
    #url: _f$url,
  };

  @override
  final String discriminatorKey = 'type';
  @override
  final dynamic discriminatorValue = "link";
  @override
  late final ClassMapperBase superMapper = AttachmentMapper.ensureInitialized();

  static ExternalLink _instantiate(DecodingData data) {
    return ExternalLink(
        id: data.dec(_f$id), title: data.dec(_f$title), url: data.dec(_f$url));
  }

  @override
  final Function instantiate = _instantiate;

  static ExternalLink fromMap(Map<String, dynamic> map) {
    return ensureInitialized().decodeMap<ExternalLink>(map);
  }

  static ExternalLink fromJson(String json) {
    return ensureInitialized().decodeJson<ExternalLink>(json);
  }
}

mixin ExternalLinkMappable {
  String toJson() {
    return ExternalLinkMapper.ensureInitialized()
        .encodeJson<ExternalLink>(this as ExternalLink);
  }

  Map<String, dynamic> toMap() {
    return ExternalLinkMapper.ensureInitialized()
        .encodeMap<ExternalLink>(this as ExternalLink);
  }

  ExternalLinkCopyWith<ExternalLink, ExternalLink, ExternalLink> get copyWith =>
      _ExternalLinkCopyWithImpl(this as ExternalLink, $identity, $identity);
  @override
  String toString() {
    return ExternalLinkMapper.ensureInitialized()
        .stringifyValue(this as ExternalLink);
  }

  @override
  bool operator ==(Object other) {
    return ExternalLinkMapper.ensureInitialized()
        .equalsValue(this as ExternalLink, other);
  }

  @override
  int get hashCode {
    return ExternalLinkMapper.ensureInitialized()
        .hashValue(this as ExternalLink);
  }
}

extension ExternalLinkValueCopy<$R, $Out>
    on ObjectCopyWith<$R, ExternalLink, $Out> {
  ExternalLinkCopyWith<$R, ExternalLink, $Out> get $asExternalLink =>
      $base.as((v, t, t2) => _ExternalLinkCopyWithImpl(v, t, t2));
}

abstract class ExternalLinkCopyWith<$R, $In extends ExternalLink, $Out>
    implements AttachmentCopyWith<$R, $In, $Out> {
  @override
  $R call({dynamic id, dynamic title, dynamic url});
  ExternalLinkCopyWith<$R2, $In, $Out2> $chain<$R2, $Out2>(Then<$Out2, $R2> t);
}

class _ExternalLinkCopyWithImpl<$R, $Out>
    extends ClassCopyWithBase<$R, ExternalLink, $Out>
    implements ExternalLinkCopyWith<$R, ExternalLink, $Out> {
  _ExternalLinkCopyWithImpl(super.value, super.then, super.then2);

  @override
  late final ClassMapperBase<ExternalLink> $mapper =
      ExternalLinkMapper.ensureInitialized();
  @override
  $R call({Object? id = $none, Object? title = $none, Object? url = $none}) =>
      $apply(FieldCopyWithData({
        if (id != $none) #id: id,
        if (title != $none) #title: title,
        if (url != $none) #url: url
      }));
  @override
  ExternalLink $make(CopyWithData data) => ExternalLink(
      id: data.get(#id, or: $value.id),
      title: data.get(#title, or: $value.title),
      url: data.get(#url, or: $value.url));

  @override
  ExternalLinkCopyWith<$R2, ExternalLink, $Out2> $chain<$R2, $Out2>(
          Then<$Out2, $R2> t) =>
      _ExternalLinkCopyWithImpl($value, $cast, t);
}
