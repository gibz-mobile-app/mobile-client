import 'package:dart_mappable/dart_mappable.dart';
import 'attachment.dart';

part 'pdf_document.mapper.dart';

@MappableClass(discriminatorValue: "pdf")
class PdfDocument extends Attachment with PdfDocumentMappable {
  const PdfDocument({
    required super.id,
    required super.title,
    required super.url,
  });

  static const fromMap = PdfDocumentMapper.fromMap;
  static const fromJson = PdfDocumentMapper.fromJson;
}
