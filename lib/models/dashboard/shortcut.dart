class Shortcut {
  final String title;
  final String iconPath;
  final String url;

  const Shortcut(
      {required this.title, required this.iconPath, required this.url});
}
