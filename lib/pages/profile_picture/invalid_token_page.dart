import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';

class InvalidTokenPage extends StatelessWidget {
  const InvalidTokenPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PageTitle(title: 'Portraitfoto aufnehmen'),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  const Padding(
                    padding: EdgeInsets.only(bottom: 40),
                    child: Icon(
                      // Icons.link_off,
                      Icons.warning_amber,
                      size: 180,
                      color: Colors.black38,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15),
                        child: Text(
                          'Ungültiger Link',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      ),
                      BlocBuilder<ProfilePictureBloc, ProfilePictureState>(
                        builder: (context, state) {
                          String content;
                          if (state is TokenVerificationFailed) {
                            if (state.verificationResult != null) {
                              switch (state.verificationResult?.statusCode) {
                                case 11:
                                  content =
                                      'Der Link ist nicht mehr gültig. Er wurde bereits benutzt um ein Portraitfoto zu erstellen. Jeder Link kann nur ein einziges Mal verwendet werden.';
                                case 12:
                                  final DateFormat formatter =
                                      DateFormat('dd.MM.yyyy');
                                  content =
                                      'Der Link ist am ${formatter.format(state.verificationResult!.requestExpiry)} abgelaufen. Aufforderungen zum Einreichen eines Portraitfotos sind jeweils nur für eine begrenzte Dauer gültig.';
                                default:
                                  content =
                                      'Der verwendete Link zum Erstellen eines Portraitfotos ist ungültig.';
                              }
                            } else {
                              content =
                                  'Die Überprüfung des Links ist fehlgeschlagen. Möglicherweise ist der verwendete Link ungültig.';
                            }
                            return Text(
                              content,
                              style: Theme.of(context).textTheme.bodyMedium,
                            );
                          }
                          return const SizedBox.shrink();
                        },
                      )
                    ],
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: TextButton(
                  onPressed: () {
                    context.go('/');
                  },
                  child: const Text('abbrechen'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
