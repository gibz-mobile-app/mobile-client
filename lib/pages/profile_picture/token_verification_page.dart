import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class TokenVerificationPage extends StatelessWidget {
  final String token;

  const TokenVerificationPage(this.token, {super.key});

  @override
  Widget build(BuildContext context) {
    return BlocListener<ProfilePictureBloc, ProfilePictureState>(
      listener: (context, state) {
        if (state is TokenVerifiedSuccessfully) {
          context.go('/profile-picture/$token/verified');
        } else if (state is TokenVerificationFailed) {
          context.go('/profile-picture/$token/invalid');
        }
      },
      child: Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const PageTitle(title: 'Portraitfoto aufnehmen'),
                    HeaderBackButton(
                      onTap: () {
                        context.go('/');
                      },
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(bottom: 40),
                      child: Icon(
                        // Icons.link_off,
                        Icons.policy,
                        size: 180,
                        color: Colors.black38,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Text(
                            'Link überprüfen',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        Text(
                          'Der Link zum Erstellen eines Portraitfotos wird überprüft...',
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox.shrink()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
