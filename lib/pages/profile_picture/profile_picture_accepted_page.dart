import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/widgets/profile_picture/profile_picture_final_page_template.dart';

class ProfilePictureAcceptedPage extends StatelessWidget {
  const ProfilePictureAcceptedPage({required this.token, super.key});

  final String token;

  @override
  Widget build(BuildContext context) {
    return ProfilePictureFinalPageTemplate(
      color: const Color.fromRGBO(0, 161, 96, 1),
      iconName: "accepted.svg",
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Text(
            'Portraitfoto übermittelt',
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(color: Colors.white),
          ),
        ),
        Text(
          'Das Portraitfoto wurde erfolgreich übermittelt und wird nun geprüft.',
          style: Theme.of(context)
              .textTheme
              .bodyMedium!
              .copyWith(color: Colors.white),
        ),
      ],
    );
  }
}
