import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/widgets/profile_picture/profile_picture_final_page_template.dart';

class ProfilePictureRejectedPage extends StatelessWidget {
  const ProfilePictureRejectedPage({required this.token, super.key});

  final String token;

  @override
  Widget build(BuildContext context) {
    return ProfilePictureFinalPageTemplate(
      color: const Color.fromRGBO(239, 62, 66, 1),
      iconName: "rejected.svg",
      finishButtonText: 'Neues Foto aufnehmen',
      finishUrl: '/profile-picture/$token/verified/face-instructions/camera',
      showCancelButton: true,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Text(
            'Portraitfoto abgelehnt',
            style: Theme.of(context)
                .textTheme
                .headlineMedium!
                .copyWith(color: Colors.white),
          ),
        ),
        BlocBuilder<ProfilePictureBloc, ProfilePictureState>(
          builder: (context, state) {
            final message = state is ProfilePictureRejected &&
                    state.error.message.isNotEmpty
                ? state.error.message
                : 'Das Portraitfoto konnte wegen einem technischen Fehler nicht übermittelt werden.';
            if (state is ProfilePictureRejected) {}
            return Text(
              message,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(color: Colors.white),
            );
          },
        ),
      ],
    );
  }
}
