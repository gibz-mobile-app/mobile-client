import 'package:flutter/material.dart';

class TestPage extends StatelessWidget {
  const TestPage({super.key});

  static const double fontSize = 32;

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w100),
            ),
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w200),
            ),
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w300),
            ),
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w400),
            ),
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w500),
            ),
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w600),
            ),
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w700),
            ),
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w800),
            ),
            Text(
              'Bildungszentrum',
              style: TextStyle(fontSize: fontSize, fontWeight: FontWeight.w900),
            ),
          ],
        ),
      ),
    );
  }
}
