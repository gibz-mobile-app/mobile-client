import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gibz_mobileapp/state_management/auth/cubit/auth_cubit.dart';
import 'package:go_router/go_router.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        alignment: AlignmentDirectional.topCenter,
        fit: StackFit.expand,
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            height: 200,
            child: Container(color: Theme.of(context).colorScheme.primary),
          ),
          Positioned(
            top: 200,
            left: 0,
            right: 0,
            bottom: 0,
            child: BlocConsumer<AuthCubit, AuthState>(
              listenWhen: (previous, current) {
                return current is AuthFailed || current is AuthLoggedOut;
              },
              listener: (context, state) {
                if (state is AuthFailed || state is AuthLoggedOut) {
                  context.go('/login');
                }
              },
              builder: (context, state) {
                if (state is AuthSuccessful) {
                  return Padding(
                    padding: const EdgeInsets.only(top: 68),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 80),
                          child: Column(
                            children: [
                              Text(
                                '${state.user.firstName} ${state.user.lastName}',
                                style:
                                    Theme.of(context).textTheme.headlineLarge,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                  color: Theme.of(context).colorScheme.primary,
                                  borderRadius: BorderRadius.circular(20),
                                ),
                                margin: const EdgeInsets.only(top: 10),
                                padding: const EdgeInsets.symmetric(
                                  vertical: 5,
                                  horizontal: 20,
                                ),
                                child: Text(
                                  state.user.username,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500,
                                      ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ProfilePageButton(
                                title: 'GIBZ-Fitnesstest',
                                icon: SvgPicture.asset(
                                  'assets/icons/fitness_check/fitness_check_blue.svg',
                                  height: 22,
                                  colorFilter: ColorFilter.mode(
                                    Theme.of(context).colorScheme.primary,
                                    BlendMode.srcIn,
                                  ),
                                ),
                                onPress: () {
                                  context.go('/profile/fitness-check');
                                },
                              ),
                              ProfilePageButton(
                                title: 'Abmelden',
                                icon: SvgPicture.asset(
                                  'assets/icons/login/logout.svg',
                                ),
                                onPress: () {
                                  // The redirection after logout is handled by
                                  // the BlocConsumer (listener) further up in
                                  // the widget tree
                                  context.read<AuthCubit>().logout();
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                }
                return const Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(bottom: 15),
                      child: CircularProgressIndicator(),
                    ),
                    Text('Die Authentifizierung wird geprüft...'),
                  ],
                );
              },
            ),
          ),
          Positioned(
            top: 83,
            child: SizedBox(
              width: 154,
              height: 154,
              child: Container(
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
                padding: const EdgeInsets.all(5),
                child: Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.surface,
                    shape: BoxShape.circle,
                  ),
                  child: FractionallySizedBox(
                    widthFactor: 0.75,
                    heightFactor: 0.75,
                    child: SvgPicture.asset('assets/icons/menu_profile.svg'),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ProfilePageButton extends StatelessWidget {
  final String title;
  final Widget icon;
  final Function() onPress;

  const ProfilePageButton({
    required this.title,
    required this.icon,
    required this.onPress,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(50),
              ),
              margin: const EdgeInsets.only(right: 20),
              padding: const EdgeInsets.all(8),
              width: 42,
              height: 42,
              child: icon,
            ),
            Text(
              title,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          ],
        ),
      ),
    );
  }
}
