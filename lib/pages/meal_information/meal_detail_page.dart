import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/data/repositories/meal_information/meal_repository.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/meal_information/meal_detail_data.dart';
import 'package:gibz_mobileapp/widgets/meal_information/price_pill.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class MealDetailPage extends StatelessWidget {
  const MealDetailPage({required this.mealId, super.key});

  final String mealId;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: context.read<MealRepository>().getMeal(mealId),
      builder: (context, snapshot) => Scaffold(
        extendBody: false,
        body: Stack(
          children: [
            Positioned(
              top: 0,
              bottom: MediaQuery.of(context).size.height - 320,
              left: 0,
              right: 0,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                color: Theme.of(context).colorScheme.primary,
                child: SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const PageTitle(
                        title: 'Menüplan',
                        textColor: Colors.white,
                      ),
                      HeaderBackButton(
                        onTap: () => context.pop(),
                        color: Colors.white,
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Positioned(
              right: 20,
              bottom: MediaQuery.of(context).size.height - 305,
              child: SvgPicture.asset(
                  'assets/icons/meal_information/background.svg'),
            ),
            if (snapshot.hasData)
              Align(
                alignment: Alignment.topRight,
                child: SafeArea(
                  child: Padding(
                    padding: const EdgeInsets.only(
                      top: 18,
                      right: 20,
                    ),
                    child: PricePill(
                      internalPrice: snapshot.data!.priceInternal,
                      externalPrice: snapshot.data!.priceExternal,
                    ),
                  ),
                ),
              ),
            if (snapshot.hasData)
              Positioned(
                top: 300,
                bottom: 0,
                left: 0,
                right: 0,
                child: ClipRRect(
                  borderRadius: const BorderRadius.vertical(
                    top: Radius.circular(20),
                  ),
                  child: MealDetailData(meal: snapshot.data!),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
