import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/data/repositories/meal_information/meal_repository.dart';
import 'package:gibz_mobileapp/state_management/meal_information/cubit/menu_day_cubit.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/meal_information/menu_day_choser.dart';
import 'package:gibz_mobileapp/widgets/meal_information/menu_day_meal_list.dart';
import 'package:gibz_mobileapp/widgets/meal_information/no_menu_placeholder.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class MealIndexPage extends StatefulWidget {
  const MealIndexPage({super.key});

  @override
  State<MealIndexPage> createState() => _MealIndexPageState();
}

class _MealIndexPageState extends State<MealIndexPage> {
  late PageController _menuDayPageController;

  @override
  void initState() {
    final today = DateTime.now().weekday;
    _menuDayPageController = PageController(initialPage: today - 1);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MenuDayCubit(),
      child: Scaffold(
        extendBody: false,
        body: Container(
          color: const Color.fromRGBO(238, 240, 244, 1),
          child: SafeArea(
            bottom: false,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const PageTitle(title: 'Menüplan'),
                      Row(
                        children: [
                          HeaderBackButton(
                            onTap: () => context.go('/'),
                          )
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          top: 20,
                          bottom: 20,
                        ),
                        child: MenuDayChoser(
                          controller: _menuDayPageController,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: FutureBuilder(
                    future:
                        context.read<MealRepository>().getMealsForWholeWeek(),
                    builder: (context, snapshot) {
                      if (snapshot.connectionState != ConnectionState.done) {
                        return const Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: EdgeInsets.only(top: 50),
                            child: CircularProgressIndicator(),
                          ),
                        );
                      }
                      if (!snapshot.hasData) {
                        return const NoMenuPlaceholder();
                      }

                      final menuDays = snapshot.data!;
                      return PageView(
                        controller: _menuDayPageController,
                        onPageChanged: (index) {
                          context.read<MenuDayCubit>().setActiveDayIndex(index);
                        },
                        children: [
                          ...menuDays.map((menuDay) {
                            if (menuDay.menus.isEmpty) {
                              return const NoMenuPlaceholder();
                            }
                            return MenuDayMealList(menuDay: menuDay);
                          })
                        ],
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
