import 'dart:math';

import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/discipline_item.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class FitnessCheckDashboardPage extends StatelessWidget {
  const FitnessCheckDashboardPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: PageTitle(
                  title: 'GIBZ-Fitnesstest',
                  additionalChild: IconButton(
                    onPressed: () => _infoDialogBuilder(context),
                    icon: const Icon(Icons.info_outline),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: HeaderBackButton(onTap: () {
                  context.pop();
                }),
              ),
              Expanded(
                child: GridView.count(
                  primary: false,
                  padding: const EdgeInsets.only(
                    top: 25,
                    bottom: 10,
                  ),
                  crossAxisSpacing: 10,
                  mainAxisSpacing: 10,
                  childAspectRatio: 1,
                  crossAxisCount: 2,
                  children: [
                    const DisciplineItem<MedicineBallPushAttempt>(
                      disciplineTitle: 'Medizinballstoss',
                      points: 1,
                      disciplineUrl: 'medicine-ball-push',
                    ),
                    DisciplineItem<StandingLongJumpAttempt>(
                      disciplineTitle: 'Standweitsprung',
                      points: Random().nextInt(26),
                      disciplineUrl: 'standing-long-jump',
                    ),
                    const DisciplineItem<CoreStrengthAttempt>(
                      disciplineTitle: 'Rumpfkrafttest',
                      points: 0,
                      disciplineUrl: 'core-strength-test',
                    ),
                    const DisciplineItem<OneLegStandAttempt>(
                      disciplineTitle: 'Einbeinstand',
                      points: 25,
                      disciplineUrl: 'one-leg-stand',
                    ),
                    DisciplineItem<ShuttleRunAttempt>(
                      disciplineTitle: 'Pendellauf',
                      points: Random().nextInt(26),
                      disciplineUrl: 'shuttle-run',
                    ),
                    DisciplineItem<TwelveMinutesRunAttempt>(
                      disciplineTitle: '12 Minuten Lauf',
                      points: Random().nextInt(26),
                      disciplineUrl: 'twelve-minutes-run',
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _infoDialogBuilder(BuildContext context) {
    return showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('GIBZ-Fitnesstest'),
          content: const SingleChildScrollView(
            child: Column(
              children: [
                Text(
                    'Auf dieser Seite können die Ergebnisse des GIBZ-Fitnesstests erfasst und eingesehen werden.'),
                SizedBox(height: 15),
                Text(
                    'Die erfassten Ergebnisse können während 5 Minuten nach der Erfassung gelöscht werden. Danach gelten die Versuche als definitiv.'),
                SizedBox(height: 15),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 15),
                      child: Icon(Icons.lightbulb_outline_rounded),
                    ),
                    Flexible(
                      child: Text(
                          'Tipp: Warten Sie die Informationen Ihrer Sportlehrperson ab, bevor Sie dieses Feature der GIBZ App aktiv nutzen.'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text('Alles klar!'),
            ),
          ],
        );
      },
    );
  }
}
