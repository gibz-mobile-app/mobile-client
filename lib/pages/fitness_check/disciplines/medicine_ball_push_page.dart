import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/pages/fitness_check/discipline_page.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_data_recorder.dart';

class MedicineBallPushPage extends StatelessWidget {
  const MedicineBallPushPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DisciplinePage<MedicineBallPushAttempt>(
      discipline: Discipline.medicineBallPush,
      disciplineTitle: 'Medizinballstoss',
      dataRecorder: FitnessCheckDataRecorder<MedicineBallPushAttempt>(
        minimumValue: 300,
        maximumValue: 900,
        interval: 100,
        ticksPerInterval: 9,
        initialValue: 450,
        stepperValue: 1,
        measurementUnitLabel: 'Meter',
        displayValueFractionDigits: 2,
        markerIcon: Padding(
          padding: const EdgeInsets.only(top: 6, left: 1),
          child: SvgPicture.asset(
            'assets/icons/fitness_check/medicine_ball.svg',
            height: 20,
            colorFilter: const ColorFilter.mode(
              Colors.white,
              BlendMode.srcIn,
            ),
          ),
        ),
        valueTransformer: (value) => value.roundToDouble(),
        labelFormatterCallback: (label) {
          int centimeters = double.parse(label).round();
          return (centimeters / 100).toStringAsFixed(2);
        },
      ),
    );
  }
}
