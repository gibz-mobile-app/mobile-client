import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/pages/fitness_check/discipline_page.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_data_recorder.dart';

class CoreStrengthPage extends StatelessWidget {
  const CoreStrengthPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DisciplinePage<CoreStrengthAttempt>(
      discipline: Discipline.coreStrength,
      disciplineTitle: 'Rumpfkrafttest',
      dataRecorder: FitnessCheckDataRecorder<CoreStrengthAttempt>(
        minimumValue: 0,
        maximumValue: 300,
        interval: 60,
        ticksPerInterval: 5,
        initialValue: 60,
        stepperValue: 1,
        measurementUnitLabel: 'Minuten',
        displayValueFractionDigits: 2,
        markerIcon: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: SvgPicture.asset(
            'assets/icons/fitness_check/abs.svg',
            height: 20,
            colorFilter: const ColorFilter.mode(
              Colors.white,
              BlendMode.srcIn,
            ),
          ),
        ),
        valueTransformer: (value) => value.roundToDouble(),
        labelFormatterCallback: (value) {
          final numericValue = double.parse(value);
          final duration = Duration(seconds: numericValue.toInt());

          String twoDigits(int n) => n.toString().padLeft(2, "0");
          String minutes = duration.inMinutes.remainder(60).abs().toString();
          String twoDigitSeconds =
              twoDigits(duration.inSeconds.remainder(60).abs());
          return "$minutes:$twoDigitSeconds";
        },
      ),
    );
  }
}
