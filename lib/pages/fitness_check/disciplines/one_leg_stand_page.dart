import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/pages/fitness_check/discipline_page.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/cubit/foot_picker_cubit.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_data_recorder.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/foot_picker.dart';

import '../../../models/fitness_check/foot_enum.dart';

class OneLegStandPage extends StatelessWidget {
  const OneLegStandPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FootPickerCubit(),
      child: DisciplinePage<OneLegStandAttempt>(
        discipline: Discipline.oneLegStand,
        disciplineTitle: 'Einbeinstand',
        dataRecorder: FitnessCheckDataRecorder<OneLegStandAttempt>(
          minimumValue: 10,
          maximumValue: 100,
          interval: 10,
          ticksPerInterval: 4,
          stepperValue: 1,
          initialValue: 25,
          measurementUnitLabel: 'Sekunden',
          displayValueFractionDigits: 0,
          markerIcon: BlocBuilder<FootPickerCubit, Foot>(
            builder: (context, state) {
              return Padding(
                padding: const EdgeInsets.only(top: 7),
                child: SvgPicture.asset(
                  'assets/icons/fitness_check/${state == Foot.left ? 'left' : 'right'}_foot.svg',
                  height: 25,
                  colorFilter: const ColorFilter.mode(
                    Colors.white,
                    BlendMode.srcIn,
                  ),
                ),
              );
            },
          ),
          valueTransformer: (value) => value.roundToDouble(),
          prependWidget: const FootPicker(),
        ),
      ),
    );
  }
}
