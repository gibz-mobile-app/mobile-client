import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/pages/fitness_check/discipline_page.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_data_recorder.dart';

class TwelveMinutesRunPage extends StatelessWidget {
  const TwelveMinutesRunPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DisciplinePage<TwelveMinutesRunAttempt>(
      discipline: Discipline.twelveMinutesRun,
      disciplineTitle: '12-Minuten Lauf',
      dataRecorder: FitnessCheckDataRecorder<TwelveMinutesRunAttempt>(
        minimumValue: 20,
        maximumValue: 50,
        interval: 5,
        ticksPerInterval: 4,
        stepperValue: 0.5,
        initialValue: 25,
        measurementUnitLabel: 'Runden',
        displayValueFractionDigits: 1,
        markerIcon: Padding(
          padding: const EdgeInsets.only(top: 6, left: 1),
          child: SvgPicture.asset(
            'assets/icons/fitness_check/twelve_minute_run.svg',
            height: 22,
            colorFilter: const ColorFilter.mode(
              Colors.white,
              BlendMode.srcIn,
            ),
          ),
        ),
        valueTransformer: (value) => (value * 2).roundToDouble() / 2,
      ),
    );
  }
}
