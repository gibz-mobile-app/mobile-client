import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/pages/fitness_check/discipline_page.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_data_recorder.dart';

class ShuttleRunPage extends StatelessWidget {
  const ShuttleRunPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DisciplinePage<ShuttleRunAttempt>(
      discipline: Discipline.shuttleRun,
      disciplineTitle: 'Pendellauf',
      dataRecorder: FitnessCheckDataRecorder<ShuttleRunAttempt>(
        minimumValue: 8.5 * 1000,
        maximumValue: 13 * 1000,
        interval: 0.5 * 1000,
        ticksPerInterval: 4,
        stepperValue: 0.01 * 1000,
        initialValue: 9.5 * 1000,
        measurementUnitLabel: 'Sekunden',
        displayValueFractionDigits: 2,
        markerIcon: Padding(
          padding: const EdgeInsets.only(top: 7, left: 2),
          child: SvgPicture.asset(
            'assets/icons/fitness_check/shuttle_run.svg',
            height: 22,
            colorFilter: const ColorFilter.mode(
              Colors.white,
              BlendMode.srcIn,
            ),
          ),
        ),
        valueTransformer: (value) => (value / 10).roundToDouble() * 10,
        labelFormatterCallback: (label) {
          int milliseconds = double.parse(label).round();
          return (milliseconds / 1000).toStringAsFixed(1);
        },
        valueFormatterCallback: (label) {
          int milliseconds = double.parse(label).round();
          return (milliseconds / 1000).toStringAsFixed(2);
        },
      ),
    );
  }
}
