import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/pages/fitness_check/discipline_page.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_data_recorder.dart';

class StandingLongJumpPage extends StatelessWidget {
  const StandingLongJumpPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DisciplinePage<StandingLongJumpAttempt>(
      discipline: Discipline.standingLongJump,
      disciplineTitle: 'Standweitsprung',
      dataRecorder: FitnessCheckDataRecorder<StandingLongJumpAttempt>(
        minimumValue: 100,
        maximumValue: 300,
        interval: 25,
        ticksPerInterval: 4,
        stepperValue: 1,
        initialValue: 200,
        measurementUnitLabel: 'Meter',
        displayValueFractionDigits: 2,
        markerIcon: Padding(
          padding: const EdgeInsets.only(top: 7, right: 3),
          child: SvgPicture.asset(
            'assets/icons/fitness_check/standing_long_jump.svg',
            height: 24,
            colorFilter: const ColorFilter.mode(
              Colors.white,
              BlendMode.srcIn,
            ),
          ),
        ),
        valueTransformer: (value) => value.roundToDouble(),
        labelFormatterCallback: (label) {
          int centimeters = double.parse(label).round();
          return (centimeters / 100).toStringAsFixed(2);
        },
      ),
    );
  }
}
