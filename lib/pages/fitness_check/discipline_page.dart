import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_platform_alert/flutter_platform_alert.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/fitness_check_bloc.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/attempts_carrousel.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_data_recorder.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class DisciplinePage<T extends Attempt> extends StatelessWidget {
  final Discipline discipline;
  final String disciplineTitle;
  final FitnessCheckDataRecorder dataRecorder;

  const DisciplinePage({
    required this.discipline,
    required this.disciplineTitle,
    required this.dataRecorder,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: BlocListener<FitnessCheckBloc<T>, FitnessCheckState>(
        listenWhen: (previous, current) =>
            current is FitnessCheckAttemptCreationFailed,
        listener: (context, state) {
          final message = (state as FitnessCheckAttemptCreationFailed).message;
          FlutterPlatformAlert.showAlert(
            windowTitle: 'Fehler',
            text: message,
            alertStyle: AlertButtonStyle.ok,
            iconStyle: IconStyle.error,
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        PageTitle(title: disciplineTitle),
                        HeaderBackButton(
                          onTap: () {
                            context.pop();
                          },
                        )
                      ],
                    ),
                    Expanded(child: dataRecorder),
                  ],
                ),
              ),
            ),
            AttemptsCarrousel<T>(),
          ],
        ),
      ),
    ));
  }
}
