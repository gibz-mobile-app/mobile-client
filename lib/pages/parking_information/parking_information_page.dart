import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:gibz_mobileapp/widgets/parking_information.dart/opening_hours_card.dart';
import 'package:gibz_mobileapp/widgets/parking_information.dart/parking_info_availability_card.dart';
import 'package:go_router/go_router.dart';

class ParkingInformationPage extends StatelessWidget {
  const ParkingInformationPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const PageTitle(title: 'Parking'),
                  HeaderBackButton(onTap: () {
                    context.go('/');
                  }),
                  const Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: ParkingInfoAvailabilityCard(),
                  ),
                  const Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: OpeningHoursCard(),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30, bottom: 30),
                child: TextButton(
                  onPressed: () {
                    // Go to article "Parkplätze" in students manual.
                    context.go(
                        '/students-manual/08da793d-1869-4889-889e-47c4ba6e9e9f');
                  },
                  child: const Text('mehr Informationen'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
