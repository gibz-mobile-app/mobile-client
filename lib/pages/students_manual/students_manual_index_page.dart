import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/data/repositories/students_manual/article_repository.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';
import 'package:gibz_mobileapp/state_management/students_manual/cubit/students_manual_article_filter_cubit.dart';
import 'package:gibz_mobileapp/widgets/no_content_hint.dart';
import 'package:gibz_mobileapp/widgets/students_manual/students_manual_index_header.dart';
import 'package:gibz_mobileapp/widgets/students_manual/students_manual_article_list.dart';

class StudentsManualIndexPage extends StatelessWidget {
  const StudentsManualIndexPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => StudentsManualArticleFilterCubit(),
      child: Scaffold(
        body: FutureBuilder(
          future: context.read<ArticleRepository>().getArticles(),
          builder: (context, snapshot) =>
              BlocBuilder<StudentsManualArticleFilterCubit, String>(
            builder: (context, filter) {
              return CustomScrollView(
                slivers: [
                  StudentsManualIndexHeader(isSearchEnabled: snapshot.hasData),
                  SliverToBoxAdapter(
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(15, 15, 15, 5),
                      child: Row(
                        children: [
                          const Icon(
                            Icons.info_outline,
                            color: Colors.black54,
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 15),
                              child: Text(
                                'Das Handbuch für Lernende informiert ausführlich zu Themen des Schulalltags mit zahlreichen weiterführenden Links zu Dokumenten, Formularen und Fachstellen innerhalb und ausserhalb der Schule.',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodySmall
                                    ?.copyWith(color: Colors.black54),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (snapshot.hasData)
                    ..._getGroupedArticleList(snapshot.data!, filter)
                        .entries
                        .map((entry) {
                      return StudentsManualArticleList(
                        indexLetter: entry.key,
                        articles: entry.value,
                      );
                    }),
                  if (snapshot.connectionState == ConnectionState.waiting)
                    const SliverFillRemaining(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                  if (snapshot.hasError ||
                      snapshot.data == null ||
                      snapshot.data!.isEmpty) ...[
                    const SliverFillRemaining(
                      child: Align(
                        alignment: Alignment.topCenter,
                        child: NoContentHint(
                          message:
                              'Der Inhalt des Handbuchs für Lernende konnte nicht geladen werden.',
                        ),
                      ),
                    ),
                  ],
                  const SliverPadding(
                    padding: EdgeInsets.only(bottom: 92),
                  ),
                ],
                physics: snapshot.connectionState == ConnectionState.done &&
                        snapshot.hasData
                    ? const AlwaysScrollableScrollPhysics()
                    : const NeverScrollableScrollPhysics(),
              );
            },
          ),
        ),
      ),
    );
  }

  _getGroupedArticleList(List<Article> articles, String filter) {
    final Map<String, List<Article>> groupedArticles = {};

    if (filter.isNotEmpty) {
      final lowerCaseFilterValue = filter.toLowerCase();
      articles = articles
          .where((article) =>
              article.title.toLowerCase().contains(lowerCaseFilterValue) ||
              article.content.toLowerCase().contains(lowerCaseFilterValue))
          .toList();
    }

    for (var article in articles) {
      final letter = article.title[0].toUpperCase();
      if (groupedArticles[letter] == null) {
        groupedArticles[letter] = <Article>[];
      }

      groupedArticles[letter]!.add(article);
    }

    return groupedArticles;
  }
}
