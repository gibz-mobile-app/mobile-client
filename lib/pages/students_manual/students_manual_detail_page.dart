import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:gibz_mobileapp/data/repositories/students_manual/article_repository.dart';
import 'package:gibz_mobileapp/utilities/default_markdown_stylesheet.dart';
import 'package:gibz_mobileapp/widgets/header_back_button.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../widgets/students_manual/students_manual_attachment.dart';

class StudentsManualDetailPage extends StatelessWidget {
  StudentsManualDetailPage({required this.articleId, super.key});

  final String articleId;
  final DateFormat dateFormat = DateFormat('dd.MM.yyyy');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: context.read<ArticleRepository>().getArticle(articleId),
        builder: (context, snapshot) {
          final title = snapshot.hasData ? snapshot.data!.title : 'Handbuch';
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 200,
                color: Theme.of(context).colorScheme.primary,
                child: SafeArea(
                  bottom: false,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        PageTitle(
                          title: title,
                          textColor: Colors.white,
                        ),
                        HeaderBackButton(
                          onTap: context.pop,
                          color: Colors.white,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 20 + 92),
                      child: Builder(
                        builder: (context) {
                          if (snapshot.hasData) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                MarkdownBody(
                                  data: snapshot.data!.content,
                                  styleSheet: defaultMarkdownStylesheet,
                                  onTapLink: (text, href, title) {
                                    if (href != null && href.isNotEmpty) {
                                      launchUrl(Uri.parse(href));
                                    }
                                  },
                                ),
                                if (snapshot.data!.attachments.isNotEmpty)
                                  Container(
                                    padding: const EdgeInsets.only(
                                      top: 20,
                                      bottom: 10,
                                    ),
                                    child: Column(
                                      children: [
                                        ...snapshot.data!.attachments.map(
                                            (attachment) =>
                                                StudentsManualAttachment(
                                                    attachment))
                                      ],
                                    ),
                                  ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 15),
                                  child: Text(
                                    "Letzte Aktualisierung: ${dateFormat.format(snapshot.data!.lastUpdated)}",
                                    style: Theme.of(context)
                                        .textTheme
                                        .labelSmall!
                                        .copyWith(color: Colors.black38),
                                  ),
                                ),
                              ],
                            );
                          } else if (snapshot.hasError) {
                            return Center(
                              child: Text(
                                'Der gewünschte Artikel ist leider nicht verfügbar.',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(fontStyle: FontStyle.italic),
                                textAlign: TextAlign.center,
                              ),
                            );
                          } else {
                            return const Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                    top: 50,
                                    bottom: 20,
                                  ),
                                  child: CircularProgressIndicator(),
                                ),
                                Text('Der Artikel wird geladen...')
                              ],
                            );
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
