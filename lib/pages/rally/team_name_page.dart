import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class TeamNamePage extends StatefulWidget {
  const TeamNamePage({super.key});

  @override
  State<TeamNamePage> createState() => _TeamNamePageState();
}

class _TeamNamePageState extends State<TeamNamePage> {
  final _teamNameController = TextEditingController();

  @override
  void dispose() {
    _teamNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BlocListener<RallyBloc, RallyState>(
        listener: (context, state) {
          if (state is PendingTeamMembersCompletion) {
            context.go('/gibzrally/members');
          }
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const PageTitle(title: 'GIBZ Rallye'),
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: Text(
                        'Team erstellen',
                        style: Theme.of(context).textTheme.headlineMedium,
                      ),
                    ),
                    Text(
                      'Wählen Sie den Namen für Ihr Team.',
                      style: Theme.of(context).textTheme.bodyMedium,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 35),
                      child: TextField(
                        controller: _teamNameController,
                        textCapitalization: TextCapitalization.words,
                        decoration: const InputDecoration(
                          hintText: 'Teamname eingeben...',
                        ),
                      ),
                    )
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30),
                      child: ValueListenableBuilder<TextEditingValue>(
                        valueListenable: _teamNameController,
                        builder: (context, value, child) => ElevatedButton(
                          onPressed: value.text.isNotEmpty
                              ? () {
                                  context.read<RallyBloc>().add(
                                        TeamNameConfirmed(
                                          teamName: _teamNameController.text,
                                        ),
                                      );
                                }
                              : null,
                          child: const Text('Teamname bestätigen'),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
