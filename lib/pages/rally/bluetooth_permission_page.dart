import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';
import 'package:permission_handler/permission_handler.dart';

class BluetoothPermissionPage extends StatelessWidget {
  const BluetoothPermissionPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RallyBloc, RallyState>(
        listener: (context, state) {
          context.go('/gibzrally');
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const PageTitle(title: 'GIBZ Rallye'),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 60),
                      child: SvgPicture.asset(
                        'assets/icons/rally/bluetooth.svg',
                        width: MediaQuery.of(context).size.shortestSide - 120,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 15),
                          child: Text(
                            'Bluetooth aktivieren',
                            style: Theme.of(context).textTheme.headlineMedium,
                          ),
                        ),
                        BlocBuilder<RallyBloc, RallyState>(
                          builder: (context, state) {
                            String text =
                                'Diese App benötigt Zugriff auf die Bluetooth-Schnittstelle Ihres Smartphones, damit die GIBZ Rallye funktionieren kann.';
                            if (state is PendingBluetoothPermissionGrant) {
                              if (state.bluetoothPermission.isGranted) {
                                if (state.bluetoothService.isDisabled) {
                                  text =
                                      'Aktivieren Sie bitte die Bluetooth-Verbindung in den Einstellungen Ihres Smartphones, damit die GIBZ Rallye funktionieren kann.';
                                } else {
                                  if (!state
                                      .bluetoothScanPermission.isGranted) {
                                    text =
                                        'Diese App muss eine Bluetooth-Verbindung zu anderen Geräten herstellen können, damit die GIBZ Rallye funktionieren kann.';
                                  }
                                }
                              }
                            }
                            return Text(
                              text,
                              style: Theme.of(context).textTheme.bodyMedium,
                            );
                          },
                        ),
                      ],
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                  child: BlocBuilder<RallyBloc, RallyState>(
                    builder: (context, state) {
                      String buttonText = 'Zugriff gewähren';
                      void Function()? onPressed;
                      if (state is PendingBluetoothPermissionGrant) {
                        if (!state.bluetoothPermission.isGranted ||
                            !state.bluetoothScanPermission.isGranted) {
                          onPressed = () {
                            context
                                .read<RallyBloc>()
                                .add(BluetoothPermissionGrantRequested());
                          };
                        } else if (!state.bluetoothService.isEnabled) {
                          buttonText = 'Bluetooth-Verbindung aktivieren';
                          onPressed = null;
                        }
                      }
                      return ElevatedButton(
                        onPressed: onPressed,
                        child: Text(buttonText),
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
