import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:go_router/go_router.dart';

class TeamMembersPage extends StatefulWidget {
  const TeamMembersPage({super.key});

  @override
  State<TeamMembersPage> createState() => _TeamMembersPageState();
}

class _TeamMembersPageState extends State<TeamMembersPage> {
  final List<String> teamMembers = [];

  bool isInputFieldEmpty = true;
  final teamMemberInputController = TextEditingController();

  @override
  void initState() {
    teamMemberInputController.addListener(() {
      setState(() {
        isInputFieldEmpty = teamMemberInputController.text.isEmpty;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    teamMemberInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: BlocListener<RallyBloc, RallyState>(
        listener: (context, state) {
          context.go('/gibzrally');
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const PageTitle(title: 'GIBZ Rallye'),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          bottom: 15,
                          top: 15,
                        ),
                        child: Text(
                          'Teammitglieder erfassen',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      ),
                      Text(
                        'Geben Sie den Vornamen und Nachnamen aller Teammitglieder ein.',
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 35, bottom: 15),
                        child: TextField(
                          controller: teamMemberInputController,
                          textCapitalization: TextCapitalization.words,
                          enabled: teamMembers.length <= 5,
                          textInputAction: TextInputAction.done,
                          onSubmitted: (_) {
                            _addTeamMember();
                          },
                          decoration: InputDecoration(
                            hintText: 'Name eingeben...',
                            suffixIcon: IconButton(
                              icon: const Icon(
                                Icons.add,
                                size: 25,
                              ),
                              onPressed:
                                  isInputFieldEmpty ? null : _addTeamMember,
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: teamMembers.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  teamMembers[index],
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyLarge!
                                      .copyWith(
                                        color: Theme.of(context)
                                            .colorScheme
                                            .primary,
                                      ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(right: 3),
                                  child: IconButton(
                                    icon: Icon(
                                      Icons.remove,
                                      color:
                                          Theme.of(context).colorScheme.primary,
                                    ),
                                    visualDensity: VisualDensity.compact,
                                    onPressed: () {
                                      if (teamMembers
                                          .contains(teamMembers[index])) {
                                        setState(() {
                                          teamMembers
                                              .remove(teamMembers[index]);
                                        });
                                      }
                                    },
                                  ),
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30),
                      child: BlocBuilder<ProgressIndicatorCubit, LoadingState>(
                        builder: (context, state) {
                          return ElevatedButton(
                            onPressed: teamMembers.isNotEmpty &&
                                    !state.contains(LoadingAspect
                                        .updatingParticipatingParty)
                                ? () {
                                    context.read<RallyBloc>().add(
                                        TeamMembersConfirmed(
                                            teamMembers: teamMembers));
                                  }
                                : null,
                            child: const Text('Teammitglieder bestätigen'),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _addTeamMember() {
    if (teamMemberInputController.text.isNotEmpty) {
      setState(() {
        teamMembers.insert(0, teamMemberInputController.text);
        teamMemberInputController.clear();
      });
    }
  }
}
