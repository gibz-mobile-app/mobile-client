import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/rally/cubit/rally_code_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/page_title.dart';
import 'package:gibz_mobileapp/widgets/rally/rally_code_display.dart';
import 'package:gibz_mobileapp/widgets/rally/rally_code_keys.dart';
import 'package:go_router/go_router.dart';

class RallyCodePage extends StatelessWidget {
  const RallyCodePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<RallyBloc, RallyState>(
        listener: (context, state) {
          if (state is PendingTeamNameCompletion) {
            context.go('/gibzrally/teamname');
          }
        },
        child: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: BlocProvider(
              create: (context) => RallyCodeCubit(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const PageTitle(title: 'GIBZ Rallye'),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: Text(
                          'Code eingeben',
                          style: Theme.of(context).textTheme.headlineMedium,
                        ),
                      ),
                      Text(
                        'Geben Sie bitte den sechsstelligen Code ein, um die GIBZ Rallye zu starten.',
                        style: Theme.of(context).textTheme.bodyMedium,
                      ),
                      const Padding(
                        padding: EdgeInsets.symmetric(vertical: 15),
                        child: RallyCodeDisplay(),
                      ),
                      BlocBuilder<RallyBloc, RallyState>(
                        builder: (context, state) {
                          if (state is RallyInitial &&
                              (state.message?.isNotEmpty ?? false)) {
                            return Padding(
                              padding: const EdgeInsets.only(bottom: 10),
                              child: Center(
                                child: Text(
                                  state.message!,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                        fontWeight: FontWeight.w500,
                                        color: Colors.red,
                                      ),
                                ),
                              ),
                            );
                          }
                          return const SizedBox.shrink();
                        },
                      ),
                      const Padding(
                        padding: EdgeInsets.only(bottom: 20),
                        child: RallyCodeKeys(),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
