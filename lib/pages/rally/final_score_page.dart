import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/rally/score/score_template.dart';
import 'package:go_router/go_router.dart';

class FinalScorePage extends StatelessWidget {
  const FinalScorePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocConsumer<RallyBloc, RallyState>(
        listener: (context, state) {
          context.go('/');
        },
        builder: (context, state) {
          if (state is! DisplayingFinalScore) {
            return const SizedBox.shrink();
          }

          return ScoreTemplate(
              cardWidget: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        'Herzliche Gratulation!',
                        style: Theme.of(context)
                            .textTheme
                            .headlineMedium!
                            .copyWith(
                                fontWeight: FontWeight.normal,
                                color: Theme.of(context).colorScheme.primary),
                      ),
                    ),
                    Text(
                      'Sie haben alle Stationen der GIBZ Rallye absolviert.',
                      style: Theme.of(context)
                          .textTheme
                          .bodyMedium!
                          .copyWith(fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              additionalStackChildren: [
                Positioned(
                  left: 80,
                  right: 80,
                  top: -65, // This relates to the ScoreCard height
                  bottom: 65, // This relates to the ScoreCard height
                  child: SvgPicture.asset('assets/icons/rally/trophy.svg'),
                )
              ],
              additionalColumnChildren: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 30,
                    right: 30,
                    top: 30,
                    bottom: 30 + 92,
                  ),
                  child: ElevatedButton(
                    onPressed: () {
                      context.read<RallyBloc>().add(FinishedRally());
                    },
                    child: const Text('Rallye beenden'),
                  ),
                ),
              ]);
        },
      ),
    );
  }
}
