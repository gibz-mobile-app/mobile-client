import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DataRecorderCubit extends Cubit<double> {
  DataRecorderCubit(super.initialValue);

  void setValue(double value) {
    _hapticFeedback();
    emit(value);
  }

  void incrementValue(
    double increment, [
    double Function(double)? valueTransformer,
  ]) {
    double newValue = state + increment;

    if (valueTransformer != null) {
      newValue = valueTransformer(newValue);
    }

    _hapticFeedback();
    emit(newValue);
  }

  void decrementValue(
    double decrement, [
    double Function(double)? valueTransformer,
  ]) {
    double newValue = state - decrement;

    if (valueTransformer != null) {
      newValue = valueTransformer(newValue);
    }

    _hapticFeedback();
    emit(newValue);
  }

  void _hapticFeedback() {
    HapticFeedback.selectionClick();
  }
}
