import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/models/fitness_check/foot_enum.dart';

class FootPickerCubit extends Cubit<Foot> {
  FootPickerCubit() : super(Foot.left);

  void setFoot(Foot foot) {
    emit(foot);
  }
}
