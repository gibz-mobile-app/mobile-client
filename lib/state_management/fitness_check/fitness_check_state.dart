part of 'fitness_check_bloc.dart';

sealed class FitnessCheckState extends Equatable {
  const FitnessCheckState();

  @override
  List<Object> get props => [];
}

final class FitnessCheckInitial extends FitnessCheckState {}

final class FitnessCheckAttemptSubmissionInProgress extends FitnessCheckState {}

final class FitnessCheckAttemptCreationFailed extends FitnessCheckState {
  final String message;

  const FitnessCheckAttemptCreationFailed({required this.message});

  @override
  List<Object> get props => [message];
}

final class FitnessCheckDisciplineDataLoading extends FitnessCheckState {}

final class FitnessCheckDisciplineDataReady extends FitnessCheckState {
  final AttemptsResponse? attemptsResponse;

  const FitnessCheckDisciplineDataReady({required this.attemptsResponse});
}

final class FitnessCheckAttemptDeletionInProgress extends FitnessCheckState {
  final Attempt attempt;

  const FitnessCheckAttemptDeletionInProgress(this.attempt);

  @override
  List<Object> get props => [attempt.id];
}

final class FitnessCheckAttemptDeletionFailed extends FitnessCheckState {
  final String message;

  const FitnessCheckAttemptDeletionFailed({required this.message});

  @override
  List<Object> get props => [message];
}
