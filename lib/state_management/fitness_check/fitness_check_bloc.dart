import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_platform_alert/flutter_platform_alert.dart';
import 'package:gibz_mobileapp/data/api/fitness_check/fitness_check_api_data_provider.dart';
import 'package:gibz_mobileapp/data/repositories/fitness_check/fitness_check_repository.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt_creation/attempt_creation_request.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt_creation/attempt_creation_request_factory.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt_measurement.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempts_response.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/foot_enum.dart';

import '../../models/fitness_check/fitness_check.dart';

part 'fitness_check_event.dart';
part 'fitness_check_state.dart';

class FitnessCheckBloc<T extends Attempt>
    extends Bloc<FitnessCheckEvent, FitnessCheckState> {
  final Discipline discipline;
  final FitnessCheckRepository repository;

  FitnessCheckBloc({
    required this.discipline,
    required this.repository,
  }) : super(FitnessCheckInitial()) {
    on<FitnessCheckEvent>((event, emit) {});
    on<FitnessCheckAttemptsLoadRequested>(_onAttemptsLoadRequested);
    on<FitnessCheckAttemptSubmissionRequested>(_onAttemptSubmissionRequested);
    on<FitnessCheckAttemptDeletionRequested>(_onAttemptDeletionRequested);

    add(FitnessCheckAttemptsLoadRequested());
  }

  Future<void> _onAttemptsLoadRequested(FitnessCheckAttemptsLoadRequested event,
      Emitter<FitnessCheckState> emit) async {
    emit(FitnessCheckDisciplineDataLoading());
    final disciplineData = await repository.getAttempts<T>();
    emit(FitnessCheckDisciplineDataReady(attemptsResponse: disciplineData));
  }

  Future<void> _onAttemptSubmissionRequested(
      FitnessCheckAttemptSubmissionRequested event,
      Emitter<FitnessCheckState> emit) async {
    emit(FitnessCheckAttemptSubmissionInProgress());

    final creationRequestPayload =
        AttemptCreationRequestFactory.createPayload<T>(
      discipline,
      event.attemptMeasurement,
    );

    try {
      await repository
          .submitAttempt<AttemptCreationRequest, T>(creationRequestPayload);
      final disciplineData =
          await repository.getAttempts<T>(forceApiRequest: true);
      emit(FitnessCheckDisciplineDataReady(attemptsResponse: disciplineData));
    } on AttemptCreationFailedException catch (exception) {
      emit(FitnessCheckAttemptCreationFailed(message: exception.message));
      final disciplineData = await repository.getAttempts<T>();
      emit(FitnessCheckDisciplineDataReady(attemptsResponse: disciplineData));
    }
  }

  Future<void> _onAttemptDeletionRequested(
      FitnessCheckAttemptDeletionRequested event,
      Emitter<FitnessCheckState> emit) async {
    final deletionDeadline =
        DateTime.now().subtract(const Duration(minutes: 5));
    if (event.attempt.momentUtc.isAfter(deletionDeadline)) {
      String legDistinction = '\n';
      if (event.attempt is OneLegStandAttempt) {
        legDistinction = (event.attempt as OneLegStandAttempt).foot == Foot.left
            ? ' mit dem linken Fuss '
            : ' mit dem rechten Fuss ';
      }
      final promptText =
          "Möchten Sie den ${event.attempt.attemptNumber}. Versuch$legDistinction(${event.attempt.displayValue}) löschen?";
      final clickedButton = await FlutterPlatformAlert.showCustomAlert(
        windowTitle: '${event.attempt.attemptNumber}. Versuch löschen',
        text: promptText,
        positiveButtonTitle: "Ja",
        negativeButtonTitle: "Nein",
        iconStyle: IconStyle.error,
      );

      if (clickedButton == CustomButton.negativeButton) {
        return;
      }
      emit(FitnessCheckAttemptDeletionInProgress(event.attempt));
      try {
        await repository.deleteAttempt<T>(event.attempt.id);
        final disciplineData =
            await repository.getAttempts<T>(forceApiRequest: true);
        emit(FitnessCheckDisciplineDataReady(attemptsResponse: disciplineData));
        return;
      } on AttemptDeletionFailedException catch (exception) {
        emit(FitnessCheckAttemptDeletionFailed(message: exception.message));
      }
    } else {
      emit(const FitnessCheckAttemptCreationFailed(
          message:
              'Das Löschen eines Versuchs ist nur innerhalb von 5 Minuten nach der Erfassung möglich.'));
    }
    final disciplineData = await repository.getAttempts<T>();
    emit(FitnessCheckDisciplineDataReady(attemptsResponse: disciplineData));
  }
}
