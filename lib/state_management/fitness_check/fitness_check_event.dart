part of 'fitness_check_bloc.dart';

sealed class FitnessCheckEvent extends Equatable {
  const FitnessCheckEvent();

  @override
  List<Object> get props => [];
}

final class FitnessCheckOverallDataRequested extends FitnessCheckEvent {}

final class FitnessCheckOverallDataLoaded extends FitnessCheckEvent {}

final class FitnessCheckDisciplineDataRequested extends FitnessCheckEvent {
  final String discipline;

  const FitnessCheckDisciplineDataRequested({required this.discipline});

  @override
  List<Object> get props => [discipline];
}

final class FitnessCheckDisciplineDataLoaded extends FitnessCheckEvent {
  final List<Attempt> disciplineData;

  const FitnessCheckDisciplineDataLoaded({required this.disciplineData});

  @override
  List<Object> get props => [disciplineData];
}

final class FitnessCheckAttemptsLoadRequested extends FitnessCheckEvent {}

final class FitnessCheckAttemptSubmissionRequested extends FitnessCheckEvent {
  final AttemptMeasurement attemptMeasurement;

  const FitnessCheckAttemptSubmissionRequested(
      {required this.attemptMeasurement});

  @override
  List<Object> get props => [attemptMeasurement];
}

final class FitnessCheckAttemptDeletionRequested extends FitnessCheckEvent {
  final Attempt attempt;

  const FitnessCheckAttemptDeletionRequested({required this.attempt});

  @override
  List<Object> get props => [attempt];
}
