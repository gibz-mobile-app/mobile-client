import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum LoadingAspect {
  joiningRally,
  updatingParticipatingParty,
  submittingStageActivityAnswer,
  loadingNextStage,
  uploadingProfilePicture,
}

class ProgressIndicatorCubit extends Cubit<LoadingState> {
  ProgressIndicatorCubit() : super(const LoadingState([]));

  void start(LoadingAspect loadingAspect) {
    emit(state.add(loadingAspect));
  }

  void finish(LoadingAspect loadingAspect) {
    emit(state.remove(loadingAspect));
  }

  @override
  void onChange(Change<LoadingState> change) {
    super.onChange(change);
  }
}

class LoadingState extends Equatable {
  final List<LoadingAspect> _loadingAspects;

  const LoadingState(this._loadingAspects);

  LoadingState add(LoadingAspect loadingAspect) {
    final newState = List<LoadingAspect>.from(_loadingAspects)
      ..add(loadingAspect);
    return copyWith(loadingAspects: () => newState);
  }

  LoadingState remove(LoadingAspect loadingAspect) {
    final newState = List<LoadingAspect>.from(_loadingAspects)
      ..remove(loadingAspect);
    return copyWith(loadingAspects: () => newState);
  }

  bool contains(LoadingAspect loadingAspect) {
    return _loadingAspects.contains(loadingAspect);
  }

  LoadingState copyWith({ValueGetter<List<LoadingAspect>>? loadingAspects}) {
    return LoadingState(
        loadingAspects != null ? loadingAspects() : _loadingAspects);
  }

  @override
  List<Object?> get props => [..._loadingAspects];
}
