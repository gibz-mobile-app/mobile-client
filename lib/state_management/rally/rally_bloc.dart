import 'dart:async';
import 'dart:io';

import 'package:dchs_flutter_beacon/dchs_flutter_beacon.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:gibz_mobileapp/data/api/rally/participating_party_api.dart';
import 'package:gibz_mobileapp/data/api/rally/rally_stage_api.dart';
import 'package:gibz_mobileapp/data/api/rally/stage_activity_result_api.dart';
import 'package:gibz_mobileapp/models/rally/dto/stage_activity_result_creation_request.dart';
import 'package:gibz_mobileapp/models/rally/stage.dart';
import 'package:gibz_mobileapp/models/rally/stage_activity_result.dart';
import 'package:gibz_mobileapp/services/rally/beacon_service.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'rally_event.dart';
part 'rally_state.dart';

class RallyBloc extends Bloc<RallyEvent, RallyState> {
  final ProgressIndicatorCubit progressIndicatorCubit;
  final BeaconService beaconService;
  final ParticipatingPartyApi participatingPartyApi;
  final RallyStageApi rallyStageApi;
  final StageActivityResultApi stageActivityResultApi;

  String? rallyId;
  String? stageId;
  String? participatingPartyId;
  String? participatingPartyTitle;
  Stage? currentStage;

  StreamSubscription? rangingStreamSubscription;

  RallyBloc({
    required this.progressIndicatorCubit,
    required this.beaconService,
    required this.participatingPartyApi,
    required this.rallyStageApi,
    required this.stageActivityResultApi,
  }) : super(const RallyInitial()) {
    if (Platform.isIOS) {
      flutterBeacon
          .setLocationAuthorizationTypeDefault(AuthorizationStatus.whenInUse);
    }
    on<ResumeRallyRequested>(_onResumeRallyRequested);
    on<RallyJoiningRequested>(_onRallyJoiningRequested);
    on<InvalidJoiningCodeSubmitted>(_onInvalidJoiningCodeSubmitted);
    on<TeamNameConfirmed>(_onTeamNameConfirmed);
    on<TeamMembersConfirmed>(_onTeamMembersConfirmed);
    on<AppSettingsPageRequested>(_onAppSettingsPageRequested);
    on<BluetoothPermissionGrantRequested>(_onBluetoothPermissionGrantRequested);
    on<BluetoothStatusChanged>(_onBluetoothStatusChanged);
    on<LocationPermissionGrantRequested>(_onLocationPermissionGrantRequested);
    on<StartRangingRequested>(_onStartRangingRequested);
    on<ReceivedBeaconRangingResult>(_onReceivedBeaconRangingResult);
    on<ArrivedAtStageLocation>(_onArrivedAtStageLocation);
    on<StageActivityRequested>(_onStageActivityRequested);
    on<StageInformationRequested>(_onStageInformationRequested);
    on<AnswerSubmissionRequested>(_onAnswerSubmissionRequested);
    on<StageActivityResultReceived>(_onStageActivityResultReceived);
    on<NextStageRequested>(_onNextStageRequested);
    on<SkippedStage>(_onSkippedStage);
    on<FinishedRally>(_onFinishedRally);
  }

  Future<FutureOr<void>> _onResumeRallyRequested(
    ResumeRallyRequested event,
    Emitter<RallyState> emit,
  ) async {
    final prefs = await SharedPreferences.getInstance();
    final rallyId = prefs.getString("rallyId");
    final participatingPartyId = prefs.getString("participatingPartyId");
    final stageId = prefs.getString("lastCompletedStageId");

    if (rallyId != null && stageId != null && participatingPartyId != null) {
      this.rallyId = rallyId;
      this.participatingPartyId = participatingPartyId;
      this.stageId = stageId;

      final nextStage = await rallyStageApi.getNext(
        rallyId,
        stageId,
        participatingPartyId,
      );

      if (nextStage != null) {
        this.stageId = nextStage.id;
        emit(PendingStageArrival(nextStage));
        await _startRanging(nextStage, emit);
      } else {
        _removeSharedPreferenceData();
        emit(DisplayingFinalScore());
      }
    }
  }

  Future<void> _onRallyJoiningRequested(
    RallyJoiningRequested event,
    Emitter<RallyState> emit,
  ) async {
    progressIndicatorCubit.start(LoadingAspect.joiningRally);

    try {
      final successfulRallyJoinResponse = await participatingPartyApi
          .createParticipatingParty(event.joiningCode);

      if (successfulRallyJoinResponse == null) {
        add(InvalidJoiningCodeSubmitted(joiningCode: event.joiningCode));
        return;
      }

      _setRallyId(successfulRallyJoinResponse.rally.id);
      _setParticipatingPartyId(
          successfulRallyJoinResponse.participatingParty.id);

      _writeSharedPreferencesString(
        "rallyId",
        rallyId ?? "",
      );
      _writeSharedPreferencesString(
        "participatingPartyId",
        participatingPartyId ?? "",
      );

      emit(PendingTeamNameCompletion());
    } finally {
      progressIndicatorCubit.finish(LoadingAspect.joiningRally);
    }
  }

  Future<void> _onInvalidJoiningCodeSubmitted(
    InvalidJoiningCodeSubmitted event,
    Emitter<RallyState> emit,
  ) async {
    emit(const RallyInitial(message: 'Der eingegebene Code ist ungültig.'));
  }

  Future<void> _onTeamNameConfirmed(
    TeamNameConfirmed event,
    Emitter<RallyState> emit,
  ) async {
    await _setParticipatingPartyTitle(event.teamName);
    emit(PendingTeamMembersCompletion());
  }

  Future<void> _onTeamMembersConfirmed(
    TeamMembersConfirmed event,
    Emitter<RallyState> emit,
  ) async {
    progressIndicatorCubit.start(LoadingAspect.updatingParticipatingParty);

    // Send participating party update to server
    await participatingPartyApi
        .updateParticipatingParty(
      id: participatingPartyId!,
      title: participatingPartyTitle!,
      names: event.teamMembers,
    )
        .then((stage) async {
      if (stage != null) {
        stageId = stage.id;
        currentStage = stage;
      } else {
        // There's no more stage available.
        _removeSharedPreferenceData();

        // TODO: Show final rally screen.
      }
    });

    progressIndicatorCubit.finish(LoadingAspect.updatingParticipatingParty);

    // Check current permissions.
    PermissionStatus bluetoothPermission = await Permission.bluetooth.status;
    PermissionStatus bluetoothScanPermission = Platform.isIOS
        ? PermissionStatus.granted
        : await Permission.bluetoothScan.status;

    // Check whether bluetooth is enabled.
    ServiceStatus bluetoothService = bluetoothPermission.isGranted
        ? await Permission.bluetooth.serviceStatus
        : ServiceStatus.notApplicable;

    // Emit next state depending on current permission and service status.
    if (bluetoothPermission.isGranted &&
        bluetoothScanPermission.isGranted &&
        bluetoothService.isEnabled) {
      // Bluetooth: READY. Check location permission...
      PermissionStatus locationPermission = await Permission.location.status;
      if (locationPermission.isGranted) {
        // All permissions OK. Start rally...
        if (currentStage == null) {
          // TODO: Load next stage...
        }
        emit(PendingStageArrival(currentStage!));
        await _startRanging(currentStage!, emit);
      } else {
        // Next: Ask for location permission...
        emit(PendingLocationPermissionGrant());
      }
      return;
    } else {
      // Next: Ask for bluetooth permissions...
      emit(PendingBluetoothPermissionGrant(
        bluetoothPermission: bluetoothPermission,
        bluetoothService: bluetoothService,
        bluetoothScanPermission: bluetoothScanPermission,
      ));
    }
  }

  void _onAppSettingsPageRequested(
    AppSettingsPageRequested event,
    Emitter<RallyState> emit,
  ) {
    openAppSettings();
  }

  Future<void> _onBluetoothPermissionGrantRequested(
    BluetoothPermissionGrantRequested event,
    Emitter<RallyState> emit,
  ) async {
    PermissionStatus bluetoothPermission = await Permission.bluetooth.status;
    if (bluetoothPermission.isDenied) {
      RallyEvent event = await Permission.bluetooth
          .request()
          .then(_getBluetoothStatusChangedEvent);
      add(event);
    } else if (bluetoothPermission.isPermanentlyDenied) {
      openAppSettings();
    } else if (bluetoothPermission.isGranted) {
      RallyEvent event =
          await _getBluetoothStatusChangedEvent(PermissionStatus.granted);
      add(event);
    }
  }

  Future<RallyEvent> _getBluetoothStatusChangedEvent(
      PermissionStatus bluetoothPermissionStatus) async {
    PermissionStatus bluetoothScanPermission = PermissionStatus.granted;
    if (!Platform.isIOS) {
      bluetoothScanPermission = await Permission.bluetoothScan.request();
    }

    ServiceStatus bluetoothService = await Permission.bluetooth.serviceStatus;

    return BluetoothStatusChanged(
      bluetoothPermission: bluetoothPermissionStatus,
      bluetoothService: bluetoothService,
      bluetoothScanPermission: bluetoothScanPermission,
    );
  }

  Future<void> _onBluetoothStatusChanged(
    BluetoothStatusChanged event,
    Emitter<RallyState> emit,
  ) async {
    if (event.bluetoothPermission.isGranted) {
      // Since bluetooth permission was granted,
      // we can now check whether bluetooth is turned on...
      ServiceStatus bluetoothService = await Permission.bluetooth.serviceStatus;

      if (bluetoothService.isDisabled) {
        emit(PendingBluetoothPermissionGrant(
          bluetoothPermission: PermissionStatus.granted,
          bluetoothService: ServiceStatus.disabled,
          bluetoothScanPermission: event.bluetoothScanPermission,
        ));

        await FlutterBluePlus.adapterState
            .where((event) => event == BluetoothAdapterState.on)
            .first;

        if (event.bluetoothPermission.isGranted &&
            event.bluetoothScanPermission.isGranted) {
          emit(PendingLocationPermissionGrant());
          return;
        }

        emit(PendingBluetoothPermissionGrant(
          bluetoothPermission: event.bluetoothPermission,
          bluetoothService: ServiceStatus.enabled,
          bluetoothScanPermission: event.bluetoothScanPermission,
        ));
        return;
      }

      // The device has Bluetooth turned on.
      // Let's now check for the bluetooth scan permission status.
      // The BluetoothScan permission is only required on android devices.
      PermissionStatus bluetoothScanPermission = Platform.isIOS
          ? PermissionStatus.granted
          : await Permission.bluetoothScan.status;
      if (bluetoothScanPermission.isGranted) {
        // Everything OK! Continue...
        emit(PendingLocationPermissionGrant());
      } else {
        // The bluetooth scan permission was not granted yet. Request it...
        await Permission.bluetoothScan.request().then((permissionStatus) {
          if (permissionStatus.isGranted) {
            // Everything OK now! Continue...
            emit(PendingLocationPermissionGrant());
          } else {
            emit(PendingBluetoothPermissionGrant(
              bluetoothPermission: PermissionStatus.granted,
              bluetoothService: ServiceStatus.enabled,
              bluetoothScanPermission: permissionStatus,
            ));
          }
        });
      }
    } else {
      emit(PendingBluetoothPermissionGrant(
        bluetoothPermission: event.bluetoothPermission,
        bluetoothService: event.bluetoothService,
        bluetoothScanPermission: event.bluetoothScanPermission,
      ));
    }
  }

  Future<void> _onLocationPermissionGrantRequested(
    LocationPermissionGrantRequested event,
    Emitter<RallyState> emit,
  ) async {
    PermissionStatus locationPermission =
        await Permission.locationWhenInUse.status;
    if (locationPermission.isDenied) {
      await Permission.locationWhenInUse.request().then((locationPermission) {
        RallyState state = locationPermission.isGranted
            ? PendingStageArrival(currentStage!)
            : PendingLocationPermissionGrant();
        emit(state);

        if (state is PendingStageArrival) {
          _startRanging(currentStage!, emit);
        }
      });
    } else if (locationPermission.isPermanentlyDenied) {
      openAppSettings();
    } else if (locationPermission.isGranted) {
      emit(PendingStageArrival(currentStage!));
      _startRanging(currentStage!, emit);
    }
  }

  FutureOr<void> _onStartRangingRequested(
    StartRangingRequested event,
    Emitter<RallyState> emit,
  ) async {
    _startRanging(event.stage, emit);
  }

  Future<void> _onReceivedBeaconRangingResult(
    ReceivedBeaconRangingResult event,
    Emitter<RallyState> emit,
  ) async {
    if (event.rangingResult.beacons.any((beacon) => [
          Proximity.far,
          Proximity.near,
          Proximity.immediate,
        ].contains(beacon.proximity))) {
      flutterBeacon.close;
      rangingStreamSubscription?.cancel();

      add(ArrivedAtStageLocation(
        stage: (state as PendingStageArrival).stage,
        locationMarkerId: event.rangingResult.region.identifier,
      ));
    }
  }

  Future<void> _onArrivedAtStageLocation(
    ArrivedAtStageLocation event,
    Emitter<RallyState> emit,
  ) async {
    if (rallyId != null && stageId != null && participatingPartyId != null) {
      await rallyStageApi.addParticipatingPartyAppearance(
        rallyId: rallyId!,
        stageId: stageId!,
        participatingPartyId: participatingPartyId!,
        locationMarkerId: event.locationMarkerId,
      );
    }
    emit(VisitingStage(event.stage));
  }

  void _onStageActivityRequested(
    StageActivityRequested event,
    Emitter<RallyState> emit,
  ) {
    emit(PendingStageActivity(event.stage));
  }

  void _onStageInformationRequested(
    StageInformationRequested event,
    Emitter<RallyState> emit,
  ) {
    emit(VisitingStage(event.stage));
  }

  Future<void> _onAnswerSubmissionRequested(
    AnswerSubmissionRequested event,
    Emitter<RallyState> emit,
  ) async {
    if (rallyId != null && stageId != null && participatingPartyId != null) {
      final StageActivityResult? stageActivityResult =
          await stageActivityResultApi.submitStageActivityResult(
        rallyId!,
        stageId!,
        StageActivityResultCreationRequestDto(
          answers: event.answers,
          participatingPartyId: participatingPartyId!,
          stageActivityId: event.stageActivityId,
        ),
      );

      if (stageActivityResult != null) {
        _setLastCompletedStageId(stageId!);

        add(StageActivityResultReceived(
          latestPoints: stageActivityResult.awardedPoints,
          totalPoints: stageActivityResult.totalPoints,
          potentialHighscore: stageActivityResult.potentialHighscore,
        ));
      }

      // TODO: Handle missing stage activity result (e.g. submission error)
    }
  }

  void _onStageActivityResultReceived(
    StageActivityResultReceived event,
    Emitter<RallyState> emit,
  ) {
    emit(DisplayingCurrentScore(
      event.latestPoints,
      event.totalPoints,
      event.potentialHighscore,
    ));
  }

  Future<void> _onNextStageRequested(
    NextStageRequested event,
    Emitter<RallyState> emit,
  ) async {
    if (rallyId != null && stageId != null && participatingPartyId != null) {
      progressIndicatorCubit.start(LoadingAspect.loadingNextStage);

      try {
        final nextStage = await rallyStageApi.getNext(
          rallyId!,
          stageId!,
          participatingPartyId!,
        );

        if (nextStage != null) {
          stageId = nextStage.id;

          emit(PendingStageArrival(nextStage));
          await _startRanging(nextStage, emit);
        } else {
          _removeSharedPreferenceData();
          emit(DisplayingFinalScore());
        }
      } finally {
        progressIndicatorCubit.finish(LoadingAspect.loadingNextStage);
      }
    }
  }

  Future<void> _onSkippedStage(
    SkippedStage event,
    Emitter<RallyState> emit,
  ) async {
    if (rallyId != null && stageId != null && participatingPartyId != null) {
      final StageActivityResult? stageActivityResult =
          await stageActivityResultApi.submitStageActivityResult(
        rallyId!,
        stageId!,
        StageActivityResultCreationRequestDto(
          answers: const [],
          participatingPartyId: participatingPartyId!,
          stageActivityId: null,
        ),
      );

      if (stageActivityResult != null) {
        _setLastCompletedStageId(stageId!);

        add(StageActivityResultReceived(
          latestPoints: stageActivityResult.awardedPoints,
          totalPoints: stageActivityResult.totalPoints,
          potentialHighscore: stageActivityResult.potentialHighscore,
        ));
      }

      // TODO: Handle missing stage activity result (e.g. submission error)
    }
  }

  void _onFinishedRally(FinishedRally event, Emitter<RallyState> emit) {
    emit(const RallyInitial());
  }

  Future<void> _startRanging(Stage stage, Emitter<RallyState> emit) async {
    beaconService.startRanging(stage).then((isRanging) {
      if (isRanging) {
        rangingStreamSubscription =
            beaconService.rangingStream.listen((RangingResult rangingResult) {
          add(ReceivedBeaconRangingResult(rangingResult));
        });
      }
    });
  }

  Future<void> _setRallyId(String rallyId) async {
    this.rallyId = rallyId;
    _writeSharedPreferencesString("rallyId", rallyId);
  }

  Future<void> _setParticipatingPartyId(String participatingPartyId) async {
    this.participatingPartyId = participatingPartyId;
    _writeSharedPreferencesString("participatingPartyId", participatingPartyId);
  }

  Future<void> _setParticipatingPartyTitle(
      String participatingPartyTitle) async {
    this.participatingPartyTitle = participatingPartyTitle;
    _writeSharedPreferencesString(
      "participatingPartyTitle",
      participatingPartyTitle,
    );
  }

  Future<void> _setLastCompletedStageId(String stageId) async {
    _writeSharedPreferencesString("lastCompletedStageId", stageId);
  }

  Future<void> _writeSharedPreferencesString(String key, String value) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(key, value);
  }

  Future<void> _removeSharedPreferenceData() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.remove('rallyId');
    await prefs.remove('participatingPartyId');
    await prefs.remove('stageId');
  }
}
