import 'package:flutter_bloc/flutter_bloc.dart';

class StageActivityCubit extends Cubit<List<String>> {
  StageActivityCubit() : super([]);

  void toggleAnswer(String answer) {
    if (state.contains(answer)) {
      removeAnswer(answer);
    } else {
      addAnswer(answer);
    }
  }

  void addAnswer(String answer) {
    if (!state.contains(answer)) {
      final answers = [...state, answer];
      emit(answers);
    }
  }

  void removeAnswer(String answer) {
    final answers = [...state];
    if (answers.contains(answer)) {
      answers.remove(answer);
    }
    emit(answers);
  }

  void setAnswers(List<String> answers) {
    emit(answers);
  }
}
