part of 'stage_video_cubit.dart';

abstract class StageVideoState extends Equatable {
  final bool showControls;

  const StageVideoState({this.showControls = true});

  factory StageVideoState.fromState(StageVideoState state,
      {bool? showControls}) {
    StageVideoState newState;
    switch (state.runtimeType) {
      case StageVideoPaused:
        newState =
            StageVideoPaused(showControls: showControls ?? state.showControls);
      case StageVideoPlaying:
        newState =
            StageVideoPlaying(showControls: showControls ?? state.showControls);
      default:
        newState =
            StageVideoInitial(showControls: showControls ?? state.showControls);
    }
    return newState;
  }

  @override
  List<Object> get props => [showControls];
}

class StageVideoInitial extends StageVideoState {
  const StageVideoInitial({super.showControls});
}

class StageVideoPlaying extends StageVideoState {
  const StageVideoPlaying({super.showControls});
}

class StageVideoPaused extends StageVideoState {
  const StageVideoPaused({super.showControls});
}
