import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RallyCodeCubit extends Cubit<String> {
  RallyCodeCubit() : super('');

  static const int offset = 48;
  static const int numOffset = 96;

  static const digits = [
    LogicalKeyboardKey.digit0,
    LogicalKeyboardKey.digit1,
    LogicalKeyboardKey.digit2,
    LogicalKeyboardKey.digit3,
    LogicalKeyboardKey.digit4,
    LogicalKeyboardKey.digit5,
    LogicalKeyboardKey.digit6,
    LogicalKeyboardKey.digit7,
    LogicalKeyboardKey.digit8,
    LogicalKeyboardKey.digit9,
  ];

  void handleKey(LogicalKeyboardKey keyboardKey) {
    if (keyboardKey == LogicalKeyboardKey.backspace) {
      _popDigit();
      return;
    }

    if (digits.contains(keyboardKey)) {
      _pushDigit(digits.indexOf(keyboardKey));
      return;
    }
  }

  void _pushDigit(int digit) {
    if (state.length <= 5) {
      emit('$state$digit');
    }
  }

  void _popDigit() {
    if (state.isEmpty) return;

    if (state.length == 1) {
      emit("");
      return;
    }

    emit(state.substring(0, state.length - 1));
  }
}
