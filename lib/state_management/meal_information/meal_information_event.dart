part of 'meal_information_bloc.dart';

abstract class MealInformationEvent extends Equatable {
  const MealInformationEvent();

  @override
  List<Object> get props => [];
}

class MealInformationRequested extends MealInformationEvent {}
