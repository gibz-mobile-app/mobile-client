part of 'meal_information_bloc.dart';

abstract class MealInformationState extends Equatable {
  const MealInformationState();

  @override
  List<Object> get props => [];
}

class MealInformationInitial extends MealInformationState {}

class LoadingMealInformation extends MealInformationState {}

class NoMealInformationAvailable extends MealInformationState {}

class MealInformationAvailable extends MealInformationState {
  final List<Day> menuDays;

  List<Meal>? get dashboardMeals {
    final today = DateTime.now();
    final menuDay =
        menuDays.firstWhereOrNull((day) => day.date.isSameDate(today));
    final meals = menuDay?.menus
      ?..sort((a, b) => -1 * a.mealRelevance.compareTo(b.mealRelevance));

    return meals?.take(2).toList();
  }

  const MealInformationAvailable(this.menuDays);

  @override
  List<Object> get props => [menuDays];
}
