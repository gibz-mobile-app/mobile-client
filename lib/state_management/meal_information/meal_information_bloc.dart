import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/data/repositories/meal_information/meal_repository.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';
import 'package:gibz_mobileapp/utilities/datetime_extension_issamedate.dart';

part 'meal_information_event.dart';
part 'meal_information_state.dart';

class MealInformationBloc
    extends Bloc<MealInformationEvent, MealInformationState> {
  final MealRepository mealRepository;

  MealInformationBloc({required this.mealRepository})
      : super(MealInformationInitial()) {
    on<MealInformationRequested>(_onMealInformationRequested);
  }

  FutureOr<void> _onMealInformationRequested(
    MealInformationRequested event,
    Emitter<MealInformationState> emit,
  ) async {
    emit(LoadingMealInformation());

    var menuDays = await mealRepository.getMealsForWholeWeek();

    if (menuDays == null || menuDays.isEmpty) {
      emit(NoMealInformationAvailable());
      return null;
    }

    emit(MealInformationAvailable(menuDays));
  }
}
