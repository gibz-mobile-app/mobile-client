import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';

sealed class StudentsManualState extends Equatable {
  @override
  List<Object?> get props => [];
}

class StudentsManualUninitialized extends StudentsManualState {}

sealed class ShowIndex extends StudentsManualState {}

class IndexLoadInProgress extends ShowIndex {}

class IndexLoadSuccess extends ShowIndex {
  final Map<String, List<Article>> groupedArticles;

  IndexLoadSuccess(this.groupedArticles);

  @override
  List<Object?> get props => [groupedArticles];
}

class IndexLoadFailed extends ShowIndex {}

sealed class ShowDetail extends StudentsManualState {
  final String articleId;
  final String articleTitle;

  ShowDetail({
    required this.articleId,
    required this.articleTitle,
  });

  @override
  List<Object?> get props => [articleId];
}

class ArticleLoadInProgress extends ShowDetail {
  ArticleLoadInProgress({
    required super.articleId,
    required super.articleTitle,
  });
}

class ArticleLoadSuccess extends ShowDetail {
  final Article article;

  ArticleLoadSuccess({
    required super.articleId,
    required super.articleTitle,
    required this.article,
  });

  @override
  List<Object?> get props => [article];
}

class ArticleLoadFailed extends ShowDetail {
  ArticleLoadFailed({
    required super.articleId,
    required super.articleTitle,
  });
}
