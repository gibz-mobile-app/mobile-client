part of 'auth_cubit.dart';

sealed class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

final class AuthInitial extends AuthState {}

final class AuthPending extends AuthState {}

final class AuthInProgress extends AuthState {}

final class AuthFailed extends AuthState {}

final class AuthSuccessful extends AuthState {
  final AuthenticatedUser user;

  const AuthSuccessful({required this.user});

  @override
  List<Object> get props => [user];
}

final class AuthLoggedOut extends AuthState {}
