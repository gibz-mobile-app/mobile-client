import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:gibz_mobileapp/models/authentication/authenticated_user.dart';
import 'package:gibz_mobileapp/utilities/oauth_client.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final OAuthClient _oAuthClient;

  bool get isAuthenticated => state is AuthSuccessful;

  bool get isNotAuthenticated => !isAuthenticated;

  bool get isAuthPending => state is AuthPending;

  AuthCubit()
      : _oAuthClient = OAuthClient(),
        super(AuthInitial());

  Future<void> init() async {
    emit(AuthPending());

    final user = await _oAuthClient.getAuthenticatedUserFromCredentials();

    if (user is AuthenticatedUser) {
      emit(AuthSuccessful(user: user));
    } else {
      emit(AuthLoggedOut());
    }
  }

  Future<void> login() async {
    emit(AuthInProgress());
    await _oAuthClient.startAuthentication();
  }

  Future<void> logout() async {
    await _oAuthClient.logout();
    emit(AuthLoggedOut());
  }

  Future<bool> handleAuthorizationResponse(
      Map<String, String> queryParameters) async {
    final authenticatedUser =
        await _oAuthClient.handleAuthorizationResponse(queryParameters);
    if (authenticatedUser != null) {
      emit(AuthSuccessful(user: authenticatedUser));
      return true;
    }

    return false;
  }
}
