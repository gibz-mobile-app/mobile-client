import 'package:flutter_bloc/flutter_bloc.dart';

enum ProfilePictureApprovalCriteria {
  facePosition,
  lighting,
  headwear,
}

class ProfilePictureApprovalCubit
    extends Cubit<Map<ProfilePictureApprovalCriteria, bool>> {
  ProfilePictureApprovalCubit()
      : super({
          ProfilePictureApprovalCriteria.facePosition: false,
          ProfilePictureApprovalCriteria.lighting: false,
          ProfilePictureApprovalCriteria.headwear: false,
        });

  void setState(ProfilePictureApprovalCriteria criteria, bool newState) {
    final nextState = Map.fromEntries(state.entries);
    nextState[criteria] = newState;

    emit(nextState);
  }
}
