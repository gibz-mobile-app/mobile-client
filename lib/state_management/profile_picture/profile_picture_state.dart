part of 'profile_picture_bloc.dart';

abstract class ProfilePictureState extends Equatable {
  const ProfilePictureState();

  @override
  List<Object> get props => [];
}

class ProfilePictureInitial extends ProfilePictureState {}

class TokenVerificationPending extends ProfilePictureState {}

class TokenVerifiedSuccessfully extends ProfilePictureState {
  final SuccessfulTokenVerificationResult verificationResult;

  const TokenVerifiedSuccessfully(this.verificationResult);

  @override
  List<Object> get props => [verificationResult];
}

class TokenVerificationFailed extends ProfilePictureState {
  final FailedTokenVerificationResult? verificationResult;

  const TokenVerificationFailed({this.verificationResult});

  @override
  List<Object> get props => [];
}

class PendingProfilePictureRequest extends ProfilePictureState {
  final String token;

  const PendingProfilePictureRequest(this.token);

  @override
  List<Object> get props => [token];
}

class PendingCameraPermissionGrant extends ProfilePictureState {}

class PendingShutter extends ProfilePictureState {
  final CameraDescription camera;

  const PendingShutter(this.camera);

  @override
  List<Object> get props => [camera];
}

class PendingPictureApproval extends ProfilePictureState {
  final XFile file;

  const PendingPictureApproval(this.file);

  @override
  List<Object> get props => [file];
}

class ProfilePictureAccepted extends ProfilePictureState {}

class ProfilePictureRejected extends ProfilePictureState {
  final ProfilePictureSubmissionError error;

  const ProfilePictureRejected(this.error);

  @override
  List<Object> get props => [error];
}
