part of 'profile_picture_bloc.dart';

abstract class ProfilePictureEvent extends Equatable {
  const ProfilePictureEvent();

  @override
  List<Object> get props => [];
}

class TokenVerificationRequested extends ProfilePictureEvent {
  final String token;

  const TokenVerificationRequested(this.token);

  @override
  List<Object> get props => [token];
}

class ProfilePictureProcessLaunched extends ProfilePictureEvent {
  final String token;

  const ProfilePictureProcessLaunched(this.token);

  @override
  List<Object> get props => [token];
}

class CameraPermissionGrantRequested extends ProfilePictureEvent {}

class FacePositionInstructionsRead extends ProfilePictureEvent {}

class PictureTaken extends ProfilePictureEvent {
  final XFile file;

  const PictureTaken(this.file);

  @override
  List<Object> get props => [file];
}

class PictureApproved extends ProfilePictureEvent {
  final String token;
  final XFile file;

  const PictureApproved({required this.token, required this.file});

  @override
  List<Object> get props => [file];
}
