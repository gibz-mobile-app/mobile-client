import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';

class ProfilePictureFinalPageTemplate extends StatelessWidget {
  const ProfilePictureFinalPageTemplate({
    required this.color,
    required this.iconName,
    required this.children,
    this.finishButtonText = 'Beenden',
    this.finishUrl = '/',
    this.showCancelButton = false,
    super.key,
  });

  final Color color;
  final String iconName;
  final List<Widget> children;
  final String finishButtonText;
  final String finishUrl;
  final bool showCancelButton;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SvgPicture.asset('assets/icons/profile_picture/$iconName'),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [...children],
                    ),
                    ElevatedButton(
                      onPressed: () {
                        context.go(finishUrl);
                      },
                      style: ButtonStyle(
                        backgroundColor: WidgetStateColor.resolveWith(
                            (states) => Colors.white),
                        foregroundColor: WidgetStateColor.resolveWith(
                          (states) => color,
                        ),
                      ),
                      child: Text(finishButtonText),
                    ),
                  ],
                ),
              ),
              if (showCancelButton)
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 0),
                  child: TextButton(
                    onPressed: () {
                      context.go('/');
                    },
                    child: const Text(
                      'abbrechen',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
