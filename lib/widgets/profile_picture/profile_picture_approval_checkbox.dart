import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/cubit/profile_picture_approval_cubit.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';

class ProfilePictureApprovalCheckbox extends StatelessWidget {
  const ProfilePictureApprovalCheckbox({
    required this.labelText,
    required this.criteria,
    super.key,
  });

  final String labelText;
  final ProfilePictureApprovalCriteria criteria;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ProfilePictureApprovalCubit,
        Map<ProfilePictureApprovalCriteria, bool>>(
      builder: (context, approvalState) {
        return BlocBuilder<ProgressIndicatorCubit, LoadingState>(
          builder: (context, loadingState) {
            return CheckboxListTile(
              value: approvalState[criteria],
              onChanged: (value) {
                if (value != null) {
                  context
                      .read<ProfilePictureApprovalCubit>()
                      .setState(criteria, value);
                }
              },
              enabled:
                  !loadingState.contains(LoadingAspect.uploadingProfilePicture),
              controlAffinity: ListTileControlAffinity.leading,
              contentPadding: const EdgeInsets.symmetric(horizontal: 0),
              dense: true,
              visualDensity: VisualDensity.compact,
              title: Text(
                labelText,
                style: Theme.of(context).textTheme.bodyMedium,
              ),
            );
          },
        );
      },
    );
  }
}
