import 'package:flutter/material.dart';

class PageTitle extends StatelessWidget {
  final String title;
  final Color? textColor;
  final Widget? additionalChild;

  const PageTitle({
    required this.title,
    this.textColor,
    this.additionalChild,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 15,
        bottom: 10,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: textColor == null
                ? Theme.of(context).textTheme.headlineLarge
                : Theme.of(context)
                    .textTheme
                    .headlineLarge!
                    .copyWith(color: textColor),
          ),
          if (additionalChild != null) additionalChild as Widget
        ],
      ),
    );
  }
}
