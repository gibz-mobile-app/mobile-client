import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/models/fitness_check/foot_enum.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/fitness_check_bloc.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_point_indicator.dart';
import 'package:intl/intl.dart';

class DisciplineAttemptCard<T extends Attempt> extends StatelessWidget {
  final T attempt;

  final DateFormat dateFormat = DateFormat('dd.MM.yyyy \'um\' HH:mm');

  DisciplineAttemptCard({required this.attempt, super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: BlocBuilder<FitnessCheckBloc<T>, FitnessCheckState>(
        builder: (context, state) {
          return Container(
              margin: const EdgeInsets.symmetric(horizontal: 5),
              padding: const EdgeInsets.only(bottom: 15),
              decoration: BoxDecoration(
                color: (state is FitnessCheckAttemptDeletionInProgress &&
                        state.attempt.id == attempt.id)
                    ? const Color.fromRGBO(0, 0, 0, 0.25)
                    : Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Stack(
                    alignment: Alignment.center,
                    children: [
                      if (attempt.momentUtc.isAfter(
                          DateTime.now().subtract(const Duration(minutes: 5))))
                        Positioned(
                          top: 0,
                          right: 0,
                          child:
                              (state is FitnessCheckAttemptDeletionInProgress &&
                                      state.attempt.id == attempt.id)
                                  ? const Padding(
                                      padding: EdgeInsets.all(17),
                                      child: SizedBox(
                                        width: 14,
                                        height: 14,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 1,
                                          color: Colors.black,
                                        ),
                                      ),
                                    )
                                  : IconButton(
                                      onPressed: () {
                                        context.read<FitnessCheckBloc<T>>().add(
                                            FitnessCheckAttemptDeletionRequested(
                                                attempt: attempt));
                                      },
                                      iconSize: 20,
                                      icon: const Icon(
                                        Icons.close_rounded,
                                      ),
                                    ),
                        ),
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: FitnessCheckPointIndicator(
                          value: attempt.points,
                          size: 70,
                          strokeWidth: 9,
                          fontSizePointsValue: 22,
                          fontSizePointsLabel: 10,
                        ),
                      ),
                    ],
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        if (T == OneLegStandAttempt)
                          DisciplineAttemptCardFootIndictor(
                            foot: Foot.left,
                            isVisible: (attempt as OneLegStandAttempt).foot ==
                                Foot.left,
                          ),
                        Text(
                          attempt.displayValue,
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium!
                              .copyWith(
                                color: Theme.of(context).colorScheme.primary,
                                fontWeight: FontWeight.bold,
                              ),
                        ),
                        if (T == OneLegStandAttempt)
                          DisciplineAttemptCardFootIndictor(
                            foot: Foot.right,
                            isVisible: (attempt as OneLegStandAttempt).foot ==
                                Foot.right,
                          ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      '${attempt.attemptNumber}. Versuch, ${dateFormat.format(attempt.momentUtc)} Uhr',
                      textAlign: TextAlign.center,
                      style: Theme.of(context).textTheme.labelSmall!.copyWith(
                            color: Theme.of(context).colorScheme.tertiary,
                            fontSize: 11,
                          ),
                    ),
                  ),
                ],
              ));
        },
      ),
    );
  }
}

class DisciplineAttemptCardFootIndictor extends StatelessWidget {
  final Foot foot;
  final bool isVisible;

  const DisciplineAttemptCardFootIndictor({
    required this.foot,
    required this.isVisible,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: SvgPicture.asset(
        'assets/icons/fitness_check/${foot == Foot.left ? 'left' : 'right'}_foot.svg',
        height: 25,
        colorFilter: ColorFilter.mode(
          isVisible
              ? Theme.of(context).colorScheme.primary
              : Colors.transparent,
          BlendMode.srcIn,
        ),
      ),
    );
  }
}
