import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/fitness_check_bloc.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/discipline_attempt_card.dart';
import 'package:page_view_dot_indicator/page_view_dot_indicator.dart';

class AttemptsCarrouselContentBox extends StatelessWidget {
  final Widget child;
  const AttemptsCarrouselContentBox({required this.child, super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 40),
      child: SizedBox(
        height: 160,
        child: Center(
          child: child,
        ),
      ),
    );
  }
}

class AttemptsCarrousel<T extends Attempt> extends StatefulWidget {
  const AttemptsCarrousel({super.key});

  @override
  State<AttemptsCarrousel<T>> createState() => _AttemptsCarrouselState<T>();
}

class _AttemptsCarrouselState<T extends Attempt>
    extends State<AttemptsCarrousel<T>> {
  int _activePage = 0;

  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(
      initialPage: _activePage,
      viewportFraction: 0.7,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<FitnessCheckBloc<T>, FitnessCheckState>(
      listenWhen: _listenOrBuildWhen,
      listener: (context, state) {
        if (state is FitnessCheckDisciplineDataReady &&
            state.attemptsResponse != null &&
            state.attemptsResponse!.attempts.length > 1) {
          _pageController.animateToPage(
            state.attemptsResponse!.attempts.length - 1,
            duration: const Duration(milliseconds: 250),
            curve: Curves.easeOut,
          );
        }
      },
      buildWhen: _listenOrBuildWhen,
      builder: (context, state) {
        if (state is FitnessCheckDisciplineDataReady) {
          if (state.attemptsResponse != null &&
              state.attemptsResponse!.attempts.isEmpty) {
            return const AttemptsCarrouselContentBox(
              child: Text(
                'Für diese Disziplin des GIBZ-Fitnesstests wurden noch keine Resultate erfasst.',
                textAlign: TextAlign.center,
              ),
            );
          }
          return Column(
            children: [
              if (state.attemptsResponse!.attempts.length > 1)
                PageViewDotIndicator(
                  currentItem:
                      _activePage > state.attemptsResponse!.attempts.length - 1
                          ? state.attemptsResponse!.attempts.length - 1
                          : _activePage,
                  count: state.attemptsResponse!.attempts.length,
                  unselectedColor: Theme.of(context).colorScheme.secondary,
                  selectedColor: Theme.of(context).colorScheme.primary,
                  size: const Size(7, 7),
                  unselectedSize: const Size(7, 7),
                  margin: const EdgeInsets.symmetric(horizontal: 4),
                  onItemClicked: (index) {
                    setState(() {
                      _pageController.animateToPage(
                        index,
                        duration: const Duration(milliseconds: 250),
                        curve: Curves.easeIn,
                      );
                    });
                  },
                ),
              const SizedBox(height: 10),
              SizedBox(
                height: 160,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: PageView(
                    controller: _pageController,
                    padEnds: true,
                    onPageChanged: (int page) {
                      setState(() {
                        _activePage = page;
                      });
                    },
                    children: [
                      ...state.attemptsResponse!.attempts.map((attempt) =>
                          DisciplineAttemptCard<T>(attempt: attempt as T)),
                    ],
                  ),
                ),
              ),
            ],
          );
        }

        return const AttemptsCarrouselContentBox(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
              Padding(
                padding: EdgeInsets.only(top: 15),
                child: Text(
                  'Resultate werden geladen...',
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  bool _listenOrBuildWhen(
      FitnessCheckState previous, FitnessCheckState current) {
    return current is FitnessCheckDisciplineDataReady ||
        current is FitnessCheckDisciplineDataLoading;
  }
}
