import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/foot_enum.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/cubit/foot_picker_cubit.dart';

class FootPickerFoot extends StatelessWidget {
  final Foot foot;

  const FootPickerFoot({
    required this.foot,
    super.key,
  });

  final double _widthActive = 60;
  final double _widthInactive = 40;

  final Duration _animationDuration = const Duration(milliseconds: 200);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        context.read<FootPickerCubit>().setFoot(foot);
      },
      child: BlocBuilder<FootPickerCubit, Foot>(
        builder: (context, state) {
          return AnimatedContainer(
            duration: _animationDuration,
            width: 40,
            height: state == foot ? _widthActive : _widthInactive,
            child: TweenAnimationBuilder<Color?>(
              tween: ColorTween(
                  begin: Colors.transparent,
                  end: state == foot
                      ? Theme.of(context).colorScheme.primary
                      : Theme.of(context).colorScheme.secondary),
              duration: _animationDuration,
              builder: (_, Color? color, __) {
                return SvgPicture.asset(
                  'assets/icons/fitness_check/${foot == Foot.left ? 'left' : 'right'}_foot.svg',
                  colorFilter: ColorFilter.mode(
                    color ?? Colors.transparent,
                    BlendMode.srcIn,
                  ),
                );
              },
            ),
          );
        },
      ),
    );
  }
}
