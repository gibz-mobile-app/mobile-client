import 'dart:math';

import 'package:flutter/material.dart';
import 'package:vector_math/vector_math.dart' as vector_math;

class FitnessCheckPointIndicator extends StatefulWidget {
  final int value;
  final double size;
  final double strokeWidth;
  final double fontSizePointsValue;
  final double fontSizePointsLabel;
  final bool isPlaceholder;

  FitnessCheckPointIndicator({
    required value,
    this.size = 90,
    this.strokeWidth = 10,
    this.fontSizePointsValue = 28,
    this.fontSizePointsLabel = 11,
    this.isPlaceholder = false,
    super.key,
  }) : value = min(value, 25);

  @override
  State<FitnessCheckPointIndicator> createState() =>
      _FitnessCheckPointIndicatorState();
}

class _FitnessCheckPointIndicatorState extends State<FitnessCheckPointIndicator>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation _animation;
  late int value;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: widget.value * 60),
    )..addListener(() {
        setState(() {});
      });
    if (widget.value > 0) _animationController.forward();

    value = widget.value;

    _animation = Tween(begin: 0.0, end: value).animate(_animationController);
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.value != value) {
      setState(() {
        value = widget.value;
        _animation =
            Tween(begin: 0.0, end: value).animate(_animationController);
        _animationController.forward();
      });
    }
    return Stack(
      alignment: Alignment.center,
      children: [
        CustomPaint(
          painter: CustomCircularProgress(
            value: _animation.value,
            strokeWidth: widget.strokeWidth,
          ),
          size: Size(widget.size, widget.size),
        ),
        if (!widget.isPlaceholder)
          FitnessCheckPointIndicatorValueLabel(
            value: widget.value,
            fontSizePointsValue: widget.fontSizePointsValue,
            fontSizePointsLabel: widget.fontSizePointsLabel,
          ),
        if (widget.isPlaceholder)
          Text(
            'keine\nResultate',
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .labelSmall!
                .copyWith(fontSize: widget.fontSizePointsLabel, height: 1.1),
          )
      ],
    );
  }
}

class FitnessCheckPointIndicatorValueLabel extends StatelessWidget {
  final int value;
  final double fontSizePointsValue;
  final double fontSizePointsLabel;

  const FitnessCheckPointIndicatorValueLabel({
    required this.value,
    required this.fontSizePointsValue,
    required this.fontSizePointsLabel,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '$value',
          style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                height: 0.85,
                fontSize: fontSizePointsValue,
              ),
        ),
        Text(
          'Punkte',
          style: Theme.of(context)
              .textTheme
              .labelSmall!
              .copyWith(fontSize: fontSizePointsLabel),
        ),
      ],
    );
  }
}

class CustomCircularProgress extends CustomPainter {
  final double value;
  final double strokeWidth;
  static const maxValue = 25;

  CustomCircularProgress({required value, required this.strokeWidth})
      : value = value / maxValue;

  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width / 2, size.height / 2);

    canvas.drawArc(
      Rect.fromCenter(
        center: center,
        width: size.width - strokeWidth,
        height: size.height - strokeWidth,
      ),
      vector_math.radians(140),
      vector_math.radians(260),
      false,
      Paint()
        ..style = PaintingStyle.stroke
        ..color = Colors.black12
        ..strokeCap = StrokeCap.round
        ..strokeWidth = strokeWidth,
    );
    canvas.saveLayer(
      Rect.fromCenter(center: center, width: 100, height: 100),
      Paint(),
    );

    Gradient gradient = SweepGradient(
      startAngle: -1.5 * pi,
      endAngle: 0.5 * pi,
      tileMode: TileMode.repeated,
      stops: const [
        0.175,
        0.3,
        0.6,
        1,
      ],
      colors: <Color>[
        Colors.red,
        Colors.yellow,
        Colors.green.shade600,
        Colors.green.shade900,
      ],
    );
    canvas.drawArc(
      Rect.fromCenter(
        center: center,
        width: size.width - strokeWidth,
        height: size.height - strokeWidth,
      ),
      vector_math.radians(140),
      vector_math.radians(260 * value),
      false,
      Paint()
        ..style = PaintingStyle.stroke
        ..strokeCap = StrokeCap.round
        ..shader = gradient
            .createShader(Rect.fromLTWH(0.0, 0.0, size.width, size.height))
        ..strokeWidth = strokeWidth,
    );
    canvas.restore();
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
