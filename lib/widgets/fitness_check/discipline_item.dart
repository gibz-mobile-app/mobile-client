import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/fitness_check_bloc.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/fitness_check_point_indicator.dart';
import 'package:go_router/go_router.dart';

class DisciplineItem<T extends Attempt> extends StatelessWidget {
  final int points;
  final String disciplineTitle;
  final String disciplineUrl;

  const DisciplineItem({
    required this.disciplineTitle,
    required this.points,
    required this.disciplineUrl,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          context.go('/profile/fitness-check/$disciplineUrl');
        },
        child: BlocBuilder<FitnessCheckBloc<T>, FitnessCheckState>(
          buildWhen: (previous, current) =>
              current is FitnessCheckDisciplineDataReady ||
              current is FitnessCheckDisciplineDataLoading,
          builder: (context, state) {
            return Container(
                padding: const EdgeInsets.only(
                  top: 20,
                  bottom: 15,
                ),
                margin: const EdgeInsets.symmetric(horizontal: 5, vertical: 5),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                  boxShadow: const [
                    BoxShadow(
                      blurRadius: 5,
                      spreadRadius: 1,
                      color: Color.fromRGBO(82, 82, 82, 0.1),
                    ),
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    if (state is FitnessCheckDisciplineDataLoading)
                      Expanded(
                        child: Center(
                          child: CircularProgressIndicator(
                            strokeWidth: 2,
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                        ),
                      ),
                    if (state is FitnessCheckDisciplineDataReady &&
                        state.attemptsResponse != null &&
                        state.attemptsResponse!.attempts.isNotEmpty)
                      FitnessCheckPointIndicator(
                        value: state.attemptsResponse!.points,
                      ),
                    if (state is FitnessCheckDisciplineDataReady &&
                        (state.attemptsResponse == null ||
                            state.attemptsResponse!.attempts.isEmpty))
                      Expanded(
                        child: Center(
                          child: Padding(
                            padding: const EdgeInsets.only(
                              bottom: 10,
                              left: 15,
                              right: 15,
                            ),
                            child: Text(
                              'Sie haben für diese Disziplin noch keine Resultate erfasst.',
                              textAlign: TextAlign.center,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodySmall!
                                  .copyWith(fontSize: 10),
                            ),
                          ),
                        ),
                      ),
                    Text(
                      disciplineTitle,
                      style: Theme.of(context).textTheme.titleSmall,
                    ),
                  ],
                ));
          },
        ));
  }
}
