import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt.dart';
import 'package:gibz_mobileapp/models/fitness_check/attempt_measurement.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/cubit/data_recorder_cubit.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/cubit/foot_picker_cubit.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/fitness_check_bloc.dart';
import 'package:gibz_mobileapp/utilities/build_context_extensions.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class FitnessCheckDataRecorder<T extends Attempt> extends StatelessWidget {
  final double minimumValue;
  final double maximumValue;
  final double interval;
  final int ticksPerInterval;
  final double stepperValue;
  final double initialValue;
  final String measurementUnitLabel;
  final int displayValueFractionDigits;
  final Widget markerIcon;
  final Widget? prependWidget;
  final String Function(String)? labelFormatterCallback;
  final String Function(String)? valueFormatterCallback;
  final double Function(double)? valueTransformer;

  const FitnessCheckDataRecorder({
    required this.minimumValue,
    required this.maximumValue,
    required this.interval,
    required this.ticksPerInterval,
    required this.stepperValue,
    required this.initialValue,
    required this.measurementUnitLabel,
    required this.displayValueFractionDigits,
    required this.markerIcon,
    this.prependWidget,
    this.labelFormatterCallback,
    this.valueFormatterCallback,
    this.valueTransformer,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DataRecorderCubit(initialValue),
      child: Builder(builder: (context) {
        return Container(
          margin: const EdgeInsets.symmetric(vertical: 15),
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              if (prependWidget != null) prependWidget!,
              Padding(
                padding: const EdgeInsets.only(top: 10),
                child: BlocBuilder<DataRecorderCubit, double>(
                  builder: (context, state) {
                    return SfLinearGauge(
                      minimum: minimumValue,
                      maximum: maximumValue,
                      interval: interval,
                      minorTicksPerInterval: ticksPerInterval,
                      animationDuration: 2000,
                      labelFormatterCallback: labelFormatterCallback,
                      axisTrackStyle: const LinearAxisTrackStyle(thickness: 2),
                      markerPointers: <LinearMarkerPointer>[
                        LinearWidgetPointer(
                          enableAnimation: false,
                          value: state,
                          position: LinearElementPosition.outside,
                          onChanged: (double value) {
                            if (valueTransformer != null) {
                              value = valueTransformer!(value);
                            }
                            context.read<DataRecorderCubit>().setValue(value);
                          },
                          child: Stack(
                            alignment: Alignment.topCenter,
                            children: [
                              SvgPicture.asset(
                                'assets/icons/fitness_check/pin.svg',
                                width: 30,
                                colorFilter: ColorFilter.mode(
                                  Theme.of(context).colorScheme.primary,
                                  BlendMode.srcIn,
                                ),
                              ),
                              markerIcon
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                ),
              ),
              BlocBuilder<DataRecorderCubit, double>(
                builder: (context, state) {
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        onPressed: state > minimumValue
                            ? () {
                                context
                                    .read<DataRecorderCubit>()
                                    .decrementValue(
                                      stepperValue,
                                      valueTransformer,
                                    );
                              }
                            : null,
                        icon: const Icon(Icons.remove_circle_outline),
                      ),
                      SizedBox(
                        width: 80,
                        child: Column(
                          children: [
                            Text(
                              valueFormatterCallback != null
                                  ? valueFormatterCallback!(state.toString())
                                  : labelFormatterCallback != null
                                      ? labelFormatterCallback!(
                                          state.toString())
                                      : state.toStringAsFixed(
                                          displayValueFractionDigits),
                              style: Theme.of(context)
                                  .textTheme
                                  .headlineLarge!
                                  .copyWith(height: 0.9),
                            ),
                            Text(
                              measurementUnitLabel,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodySmall!
                                  .copyWith(
                                    color:
                                        Theme.of(context).colorScheme.tertiary,
                                  ),
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        onPressed: state < maximumValue
                            ? () {
                                context
                                    .read<DataRecorderCubit>()
                                    .incrementValue(
                                      stepperValue,
                                      valueTransformer,
                                    );
                              }
                            : null,
                        icon: const Icon(Icons.add_circle_outline),
                      ),
                    ],
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: BlocBuilder<FitnessCheckBloc<T>, FitnessCheckState>(
                  builder: (context, state) {
                    return ElevatedButton(
                      onPressed: state
                              is FitnessCheckAttemptSubmissionInProgress
                          ? null
                          : () {
                              HapticFeedback.lightImpact();
                              final value =
                                  context.read<DataRecorderCubit>().state;
                              final foot =
                                  context.readOrNull<FootPickerCubit>()?.state;
                              final measurement = AttemptMeasurement(
                                value: value.round(),
                              );
                              if (foot != null) {
                                measurement.setSide(foot);
                              }
                              context.read<FitnessCheckBloc<T>>().add(
                                    FitnessCheckAttemptSubmissionRequested(
                                      attemptMeasurement: measurement,
                                    ),
                                  );
                            },
                      child: state is FitnessCheckAttemptSubmissionInProgress
                          ? const SizedBox(
                              height: 18,
                              width: 18,
                              child: CircularProgressIndicator(
                                strokeWidth: 2,
                              ),
                            )
                          : const Text('Versuch speichern'),
                    );
                  },
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
