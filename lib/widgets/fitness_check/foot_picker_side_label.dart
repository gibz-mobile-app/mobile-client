import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/models/fitness_check/foot_enum.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/cubit/foot_picker_cubit.dart';

class FootPickerSideLabel extends StatelessWidget {
  final Foot foot;

  const FootPickerSideLabel({required this.foot, super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FootPickerCubit, Foot>(
      builder: (context, state) {
        return TweenAnimationBuilder<Color?>(
          tween: ColorTween(
            begin: Colors.blue,
            end: state == foot
                ? Theme.of(context).colorScheme.tertiary
                : Colors.transparent,
          ),
          duration: const Duration(milliseconds: 200),
          builder: (_, Color? color, __) {
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                '${foot == Foot.left ? 'linker' : 'rechter'}\nFuss',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .labelLarge!
                    .copyWith(color: color, height: 1.1),
              ),
            );
          },
        );
      },
    );
  }
}
