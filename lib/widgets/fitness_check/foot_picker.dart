import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/models/fitness_check/foot_enum.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/foot_picker_foot.dart';
import 'package:gibz_mobileapp/widgets/fitness_check/foot_picker_side_label.dart';

class FootPicker extends StatelessWidget {
  const FootPicker({super.key});

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 60,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          FootPickerSideLabel(foot: Foot.left),
          FootPickerFoot(foot: Foot.left),
          FootPickerFoot(foot: Foot.right),
          FootPickerSideLabel(foot: Foot.right),
        ],
      ),
    );
  }
}
