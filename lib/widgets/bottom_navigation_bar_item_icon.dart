import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class BottomNavigationBarItemIcon extends StatelessWidget {
  const BottomNavigationBarItemIcon({
    required this.iconPath,
    required this.color,
    super.key,
  });

  final String iconPath;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      iconPath,
      width: 20,
      height: 20,
      colorFilter: ColorFilter.mode(
        color,
        BlendMode.srcIn,
      ),
    );
  }
}
