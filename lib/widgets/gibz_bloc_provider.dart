import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/data/api/profile_picture/profile_picture_api.dart';
import 'package:gibz_mobileapp/data/api/rally/participating_party_api.dart';
import 'package:gibz_mobileapp/data/api/rally/rally_stage_api.dart';
import 'package:gibz_mobileapp/data/api/rally/stage_activity_result_api.dart';
import 'package:gibz_mobileapp/data/repositories/fitness_check/fitness_check_repository.dart';
import 'package:gibz_mobileapp/data/repositories/meal_information/meal_repository.dart';
import 'package:gibz_mobileapp/data/repositories/parking_information/car_park_repository.dart';
import 'package:gibz_mobileapp/models/fitness_check/discipline.dart';
import 'package:gibz_mobileapp/models/fitness_check/fitness_check.dart';
import 'package:gibz_mobileapp/services/rally/beacon_service.dart';
import 'package:gibz_mobileapp/state_management/auth/cubit/auth_cubit.dart';
import 'package:gibz_mobileapp/state_management/fitness_check/fitness_check_bloc.dart';
import 'package:gibz_mobileapp/state_management/meal_information/meal_information_bloc.dart';
import 'package:gibz_mobileapp/state_management/parking_information/bloc/parking_information_bloc.dart';
import 'package:gibz_mobileapp/state_management/profile_picture/profile_picture_bloc.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';

class GibzBlocProvider extends StatelessWidget {
  final Widget child;

  const GibzBlocProvider({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => AuthCubit()..init(),
        ),
        BlocProvider(
          create: (_) => RallyBloc(
            progressIndicatorCubit: context.read<ProgressIndicatorCubit>(),
            beaconService: context.read<BeaconService>(),
            participatingPartyApi: context.read<ParticipatingPartyApi>(),
            rallyStageApi: context.read<RallyStageApi>(),
            stageActivityResultApi: context.read<StageActivityResultApi>(),
          ),
        ),
        BlocProvider(
          create: (_) => MealInformationBloc(
            mealRepository: context.read<MealRepository>(),
          )..add(MealInformationRequested()),
        ),
        BlocProvider(
          create: (_) => ParkingInformationBloc(
            carParkRepository: context.read<CarParkRepository>(),
          )..add(ParkingInformationRequested()),
        ),
        BlocProvider(
          create: (_) => ProfilePictureBloc(
            progressIndicatorCubit: context.read<ProgressIndicatorCubit>(),
            profilePictureApi: context.read<ProfilePictureApi>(),
          ),
        ),
        BlocProvider(
          create: (_) => FitnessCheckBloc<CoreStrengthAttempt>(
            discipline: Discipline.coreStrength,
            repository: context.read<FitnessCheckRepository>(),
          ),
        ),
        BlocProvider(
          create: (_) => FitnessCheckBloc<MedicineBallPushAttempt>(
            discipline: Discipline.coreStrength,
            repository: context.read<FitnessCheckRepository>(),
          ),
        ),
        BlocProvider(
          create: (_) => FitnessCheckBloc<OneLegStandAttempt>(
            discipline: Discipline.coreStrength,
            repository: context.read<FitnessCheckRepository>(),
          ),
        ),
        BlocProvider(
          create: (_) => FitnessCheckBloc<ShuttleRunAttempt>(
            discipline: Discipline.coreStrength,
            repository: context.read<FitnessCheckRepository>(),
          ),
        ),
        BlocProvider(
          create: (_) => FitnessCheckBloc<StandingLongJumpAttempt>(
            discipline: Discipline.coreStrength,
            repository: context.read<FitnessCheckRepository>(),
          ),
        ),
        BlocProvider(
          create: (_) => FitnessCheckBloc<TwelveMinutesRunAttempt>(
            discipline: Discipline.coreStrength,
            repository: context.read<FitnessCheckRepository>(),
          ),
        ),
      ],
      child: child,
    );
  }
}
