import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/meal_information/cubit/menu_day_cubit.dart';
import 'package:gibz_mobileapp/widgets/meal_information/menu_day_choser_item.dart';

class MenuDayChoser extends StatelessWidget {
  const MenuDayChoser({required this.controller, super.key});

  final PageController controller;

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();
    var monday = now.subtract(Duration(days: now.weekday - 1));
    return BlocBuilder<MenuDayCubit, int>(
      builder: (context, state) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            for (String weekDay in MenuDayCubit.weekDays)
              MenuDayChoserItem(
                weekDay: weekDay,
                calendarDay:
                    '${monday.add(Duration(days: MenuDayCubit.weekDays.indexOf(weekDay))).day}',
                isActive: state == MenuDayCubit.weekDays.indexOf(weekDay),
                controller: controller,
              ),
          ],
        );
      },
    );
  }
}
