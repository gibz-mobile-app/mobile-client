import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MealIndicatorPill extends StatelessWidget {
  const MealIndicatorPill({
    required this.title,
    required this.label,
    required this.iconName,
    super.key,
  });

  // ignore: use_key_in_widget_constructors
  const MealIndicatorPill.balance(String label)
      : this(label: label, iconName: 'balance', title: 'Ausgewogenheit');

  // ignore: use_key_in_widget_constructors
  const MealIndicatorPill.environmentalImpact(String label)
      : this(label: label, iconName: 'impact', title: 'Klimawirkung');

  final String title;
  final String label;
  final String iconName;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 15),
          child: Text(title,
              style: Theme.of(context)
                  .textTheme
                  .bodyMedium!
                  .copyWith(fontWeight: FontWeight.w600)),
        ),
        Container(
          decoration: BoxDecoration(
            color: _getColorForLabel(label),
            borderRadius: BorderRadius.circular(20),
          ),
          margin: const EdgeInsets.only(top: 5),
          padding: const EdgeInsets.symmetric(
            vertical: 3,
            horizontal: 15,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 8),
                child: SvgPicture.asset(
                    'assets/icons/meal_information/$iconName.svg'),
              ),
              Text(
                label.replaceAll(", ", ",\n"),
                style: Theme.of(context)
                    .textTheme
                    .bodyMedium!
                    .copyWith(color: Colors.white),
              )
            ],
          ),
        ),
      ],
    );
  }

  Color _getColorForLabel(String label) {
    if (iconName == 'balance') {
      if (label.isEmpty) {
        return _getColorForLevel(1);
      }
      return _getColorForLevel(label.split(',').length + 1);
    }

    const environmentalImpactLabels = ["hoch", "-", "mittel", "niedrig"];
    int index = environmentalImpactLabels.indexOf(label);
    return _getColorForLevel(index + 1);
  }

  Color _getColorForLevel(int level) {
    switch (level) {
      case 4:
        return const Color.fromRGBO(0, 162, 91, 1);
      case 3:
        return const Color.fromRGBO(255, 224, 78, 1);
      case 2:
        return const Color.fromRGBO(246, 142, 30, 1);
      case 1:
        return const Color.fromRGBO(238, 58, 65, 1);
      default:
        return const Color.fromARGB(255, 182, 184, 186);
    }
  }
}
