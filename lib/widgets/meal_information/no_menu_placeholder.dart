import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NoMenuPlaceholder extends StatelessWidget {
  const NoMenuPlaceholder({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(),
      child: Align(
        alignment: Alignment.topCenter,
        child: Padding(
          padding: const EdgeInsets.only(top: 20),
          child: SvgPicture.asset(
              'assets/icons/meal_information/no_menu_placeholder.svg'),
        ),
      ),
    );
  }
}
