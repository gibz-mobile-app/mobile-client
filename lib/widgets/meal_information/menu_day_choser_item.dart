import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/state_management/meal_information/cubit/menu_day_cubit.dart';

class MenuDayChoserItem extends StatelessWidget {
  const MenuDayChoserItem({
    required this.weekDay,
    required this.calendarDay,
    this.isActive = false,
    required this.controller,
    super.key,
  });

  final bool isActive;
  final String weekDay;
  final String calendarDay;

  final PageController controller;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        controller.animateToPage(
          MenuDayCubit.weekDays.indexOf(weekDay),
          duration: const Duration(milliseconds: 250),
          curve: Curves.easeInOut,
        );
      },
      child: Container(
        width: 36,
        height: 61,
        padding: const EdgeInsets.symmetric(vertical: 3),
        decoration: BoxDecoration(
            color: isActive
                ? Theme.of(context).colorScheme.primary
                : Colors.transparent,
            borderRadius: BorderRadius.circular(20)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 7),
              child: Text(
                weekDay,
                style: Theme.of(context).textTheme.labelMedium!.copyWith(
                      fontSize: 11,
                      fontWeight: FontWeight.w500,
                      color: isActive ? Colors.white : Colors.black,
                    ),
              ),
            ),
            Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: isActive ? Colors.white : Colors.transparent,
              ),
              child: Center(
                child: Text(
                  calendarDay,
                  style: Theme.of(context).textTheme.labelLarge!.copyWith(
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: isActive
                            ? Theme.of(context).colorScheme.primary
                            : Colors.black,
                      ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
