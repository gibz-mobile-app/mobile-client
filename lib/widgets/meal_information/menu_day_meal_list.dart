import 'dart:math';

import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/models/meal_information.dart';
import 'package:gibz_mobileapp/widgets/meal_information/menu_day_meal_list_item.dart';

class MenuDayMealList extends StatelessWidget {
  const MenuDayMealList({required this.menuDay, super.key});

  final Day menuDay;

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.vertical(top: Radius.circular(20)),
      child: Container(
        decoration: const BoxDecoration(
          color: Colors.white,
        ),
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(
              left: 30,
              right: 30,
              // TODO: Make hardcoded 92 dynamic!
              // 92 is the height of the navigation bar.
              bottom: 92,
            ),
            child: Column(
              children: [
                ...menuDay.orderedMenus
                    .getRange(0, max(0, menuDay.menus.length - 1))
                    .map((meal) => MenuDayMealListItem(meal: meal)),
                MenuDayMealListItem(
                  meal: menuDay.orderedMenus.last,
                  isLastItem: true,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
