import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gibz_mobileapp/models/rally/attachment.dart';
import 'package:gibz_mobileapp/models/rally/stage.dart';
import 'package:gibz_mobileapp/state_management/rally/cubit/stage_video_duration_cubit.dart';
import 'package:gibz_mobileapp/widgets/markdown_text.dart';
import 'package:gibz_mobileapp/widgets/rally/rally_card.dart';
import 'package:gibz_mobileapp/widgets/rally/rally_video_player.dart';
import 'package:gibz_mobileapp/widgets/rally/stage_attachment_link.dart';

class StageCard extends StatelessWidget {
  final Stage stage;

  const StageCard({required this.stage, super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => StageVideoDurationCubit(),
      child: RallyCard(
        child: Column(
          children: [
            if (_getAttachments<VideoAttachment>().isNotEmpty)
              AspectRatio(
                aspectRatio: 16 / 9,
                child: Stack(
                  children: [
                    RallyVideoPlayer(
                      title: _getVideoTitle()!,
                      videoUrl: _getVideoUrl()!,
                    ),
                  ],
                ),
              ),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(color: Colors.white),
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          stage.information.title,
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 7),
                                child: SvgPicture.asset(
                                    'assets/icons/rally/time.svg'),
                              ),
                              BlocBuilder<StageVideoDurationCubit,
                                  StageVideoDurationState>(
                                builder: (context, state) {
                                  return Text(
                                    state is KnownStageVideoDuration
                                        ? '${state.roundedVideoDuration.inMinutes + 2} Minuten'
                                        : '3 Minuten',
                                    style:
                                        Theme.of(context).textTheme.bodyMedium,
                                  );
                                },
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10),
                          child: MarkdownText(
                            data: stage.information.content,
                          ),
                        ),
                        for (ExternalLinkAttachment externalLink
                            in _getAttachments<ExternalLinkAttachment>())
                          StageAttachmentLink(
                            title: externalLink.title,
                            url: externalLink.url,
                          ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  String? _getVideoUrl() {
    final videos = _getAttachments<VideoAttachment>();
    return videos.firstOrNull?.url;
  }

  String? _getVideoTitle() {
    final videos = _getAttachments<VideoAttachment>();
    return videos.firstOrNull?.title;
  }

  List<T> _getAttachments<T>() {
    return stage.information.attachments.whereType<T>().toList();
  }
}
