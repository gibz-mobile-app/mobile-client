import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/state_management/progress_indicator_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/cubit/rally_code_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/rally/rally_code_key.dart';

class RallyCodeKeys extends StatelessWidget {
  const RallyCodeKeys({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Wrap(
        direction: Axis.horizontal,
        spacing: 10,
        runSpacing: 10,
        alignment: WrapAlignment.spaceEvenly,
        runAlignment: WrapAlignment.spaceEvenly,
        children: [
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit1,
            child: Text('1', style: _getTextStyle(context)),
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit2,
            child: Text('2', style: _getTextStyle(context)),
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit3,
            child: Text('3', style: _getTextStyle(context)),
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit4,
            child: Text('4', style: _getTextStyle(context)),
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit5,
            child: Text('5', style: _getTextStyle(context)),
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit6,
            child: Text('6', style: _getTextStyle(context)),
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit7,
            child: Text('7', style: _getTextStyle(context)),
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit8,
            child: Text('8', style: _getTextStyle(context)),
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit9,
            child: Text('9', style: _getTextStyle(context)),
          ),
          BlocBuilder<RallyCodeCubit, String>(
            builder: (context, state) {
              return RallyCodeKey(
                keyboardKey: LogicalKeyboardKey.backspace,
                isEnabled: state.isNotEmpty,
                child: Icon(
                  Icons.chevron_left,
                  color: state.isNotEmpty
                      ? Theme.of(context).colorScheme.tertiary
                      : Colors.grey.shade400,
                ),
              );
            },
          ),
          RallyCodeKey(
            keyboardKey: LogicalKeyboardKey.digit0,
            child: Text('0', style: _getTextStyle(context)),
          ),
          BlocBuilder<ProgressIndicatorCubit, LoadingState>(
            builder: (context, loadingState) {
              return BlocBuilder<RallyCodeCubit, String>(
                builder: (context, rallyCodeState) {
                  return RallyCodeKey(
                    keyboardKey: LogicalKeyboardKey.enter,
                    isEnabled:
                        !loadingState.contains(LoadingAspect.joiningRally) &&
                            rallyCodeState.length == 6,
                    onTap: () {
                      context.read<RallyBloc>().add(
                          RallyJoiningRequested(joiningCode: rallyCodeState));
                    },
                    child: Icon(
                      Icons.check,
                      color: rallyCodeState.length == 6 &&
                              !loadingState.contains(LoadingAspect.joiningRally)
                          ? Theme.of(context).colorScheme.tertiary
                          : Colors.grey.shade400,
                    ),
                  );
                },
              );
            },
          ),
        ],
      ),
    );
  }

  TextStyle _getTextStyle(BuildContext context) {
    return DefaultTextStyle.of(context).style.copyWith(
          color: Theme.of(context).colorScheme.tertiary,
          fontSize: 24,
          fontWeight: FontWeight.w500,
        );
  }
}
