part of 'stage_activity_answer_input.dart';

class QrCodeAnswerInput extends StageActivityAnswerInput {
  final QrCodeActivity stageActivity;

  const QrCodeAnswerInput(this.stageActivity, {super.key});

  @override
  State<QrCodeAnswerInput> createState() => _QrCodeAnswerInputState();
}

class _QrCodeAnswerInputState extends State<QrCodeAnswerInput>
    with WidgetsBindingObserver {
  final MobileScannerController _scannerController =
      MobileScannerController(autoStart: false);
  late StreamSubscription<Object?>? _subscription;

  @override
  void initState() {
    super.initState();

    // Start listening to lifecycle changes.
    WidgetsBinding.instance.addObserver(this);

    // Start listening to the barcode events.
    _subscription = _scannerController.barcodes.listen(_handleBarcode);

    // Finally, start the scanner itself.
    // unawaited(_scannerController.start());
  }

  @override
  void dispose() async {
    // Stop listening to lifecycle changes.
    WidgetsBinding.instance.removeObserver(this);

    // Stop listening to the barcode events.
    unawaited(_subscription?.cancel());
    _subscription = null;

    // Dispose the widget itself.
    super.dispose();

    // Finally, dispose the scanner controller.
    await _scannerController.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // If the controller is not ready, do not try to start of stop it.
    // Permission dialogs can trigger lifecycle changes before the controller is ready.
    if (!_scannerController.value.isInitialized) {
      return;
    }

    switch (state) {
      case AppLifecycleState.detached:
      case AppLifecycleState.hidden:
      case AppLifecycleState.paused:
        return;
      case AppLifecycleState.resumed:
        // Restart the scanner when the app is resumed.
        // Don't forget to resume listening to the barcode events.
        _subscription = _scannerController.barcodes.listen(_handleBarcode);
        unawaited(_scannerController.start());
      case AppLifecycleState.inactive:
        // Stop the scanner when the app is paused.
        // Also stop the barcode events subscription.
        unawaited(_subscription?.cancel());
        _subscription = null;
        unawaited(_scannerController.stop());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        height: 200,
        width: 200,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(20),
          child: MobileScanner(
            controller: _scannerController,
            fit: BoxFit.cover,
            placeholderBuilder: (context, widget) {
              return Container(
                color: Colors.grey.shade200,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    GestureDetector(
                      onTap: _startScanner,
                      child: Icon(
                        Icons.qr_code_2_sharp,
                        size: 120,
                        color: Colors.black.withAlpha(90),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: TextButton(
                        onPressed: _startScanner,
                        child: const Text('QR Code scannen'),
                      ),
                    ),
                  ],
                ),
              );
            },
            overlayBuilder: (context, constraints) {
              return Icon(
                Icons.qr_code_2_sharp,
                size: 120,
                color: Colors.black.withAlpha(80),
              );
            },
          ),
        ),
      ),
    );
  }

  void _startScanner() async {
    await _scannerController.start();
  }

  void _handleBarcode(BarcodeCapture capture) {
    if (capture.barcodes.length == 1 &&
        capture.barcodes.first.type == BarcodeType.text) {
      final barcode = capture.barcodes.first;
      var content = barcode.rawValue;
      if (content?.isNotEmpty ?? false) {
        // Fix: Due to incorrect character encoding, we need to care about "wrong" quotation marks.
        if (content!.contains("“")) {
          content = content.replaceAll("“", "\"");
        }

        final qrCodeAnswer = QrCodeAnswerValue.fromJson(content);

        HapticFeedback.vibrate();
        _scannerController.stop();

        final answers = [
          'P:${qrCodeAnswer.percentage}',
        ];

        if (qrCodeAnswer.label != null) {
          answers.add('L:${qrCodeAnswer.label}');
        }

        context
            .read<RallyBloc>()
            .add(AnswerSubmissionRequested(widget.stageActivity.id, answers));
      }
    }
  }
}
