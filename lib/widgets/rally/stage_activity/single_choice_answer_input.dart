part of 'stage_activity_answer_input.dart';

class SingleChoiceAnswerInput extends StageActivityAnswerInput {
  final SingleChoiceActivity stageActivity;
  const SingleChoiceAnswerInput(this.stageActivity, {super.key});

  @override
  State<SingleChoiceAnswerInput> createState() =>
      _SingleChoiceAnswerInputState();
}

class _SingleChoiceAnswerInputState extends State<SingleChoiceAnswerInput> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<StageActivityCubit, List<String>>(
      builder: (context, state) {
        return Material(
          color: Colors.transparent,
          child: Column(
            children: [
              for (StageActivityAnswer answerOption
                  in widget.stageActivity.answers)
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 7),
                  child: GestureDetector(
                    onTap: () {
                      context
                          .read<StageActivityCubit>()
                          .setAnswers([answerOption.id]);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: state.firstOrNull == answerOption.id
                            ? Theme.of(context).colorScheme.primary
                            : const Color.fromRGBO(207, 212, 222, 1),
                        borderRadius: BorderRadius.circular(50),
                      ),
                      child: Row(children: [
                        Radio(
                          value: answerOption.id,
                          groupValue: state.firstOrNull,
                          activeColor: Colors.white,
                          fillColor: WidgetStateColor.resolveWith(
                            (states) => Colors.white,
                          ),
                          onChanged: (String? value) {
                            if (value != null) {
                              context
                                  .read<StageActivityCubit>()
                                  .setAnswers([value]);
                            }
                          },
                        ),
                        Flexible(
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 5,
                              bottom: 5,
                              right: 20,
                            ),
                            child: Text(
                              answerOption.answerText,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyMedium!
                                  .copyWith(
                                    color: Colors.white,
                                    fontWeight: FontWeight.w600,
                                  ),
                            ),
                          ),
                        )
                      ]),
                    ),
                  ),
                ),
            ],
          ),
        );
      },
    );
  }
}
