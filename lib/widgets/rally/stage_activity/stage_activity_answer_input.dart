import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/models/rally/stage_activity.dart';
import 'package:gibz_mobileapp/models/rally/stage_activity_answer.dart';
import 'package:gibz_mobileapp/state_management/rally/cubit/stage_activity_cubit.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

part 'single_choice_answer_input.dart';
part 'multiple_choice_answer_input.dart';
part 'text_input_answer_input.dart';
part 'qr_code_answer_input.dart';

abstract class StageActivityAnswerInput extends StatefulWidget {
  const StageActivityAnswerInput({super.key});

  factory StageActivityAnswerInput.forStageActivity(
      StageActivity stageActivity) {
    switch (stageActivity.runtimeType) {
      case SingleChoiceActivity:
        return StageActivityAnswerInput.singleChoice(
            stageActivity as SingleChoiceActivity);
      case MultipleChoiceActivity:
        return StageActivityAnswerInput.multipleChoice(
            stageActivity as MultipleChoiceActivity);
      case TextInputActivity:
        return StageActivityAnswerInput.textInput(
            stageActivity as TextInputActivity);
      case QrCodeActivity:
        return StageActivityAnswerInput.qrCodeInput(
            stageActivity as QrCodeActivity);
      default:
        throw Exception();
    }
  }

  factory StageActivityAnswerInput.singleChoice(
      SingleChoiceActivity stageActivity) {
    return SingleChoiceAnswerInput(stageActivity);
  }

  factory StageActivityAnswerInput.multipleChoice(
      MultipleChoiceActivity stageActivity) {
    return MultipleChoiceAnswerInput(stageActivity);
  }

  factory StageActivityAnswerInput.textInput(TextInputActivity stageActivity) {
    return TextInputAnswerInput(stageActivity);
  }

  factory StageActivityAnswerInput.qrCodeInput(QrCodeActivity stageActivity) {
    return QrCodeAnswerInput(stageActivity);
  }
}
