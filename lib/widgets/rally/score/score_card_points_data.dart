import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/state_management/rally/rally_bloc.dart';
import 'package:gibz_mobileapp/widgets/rally/score/score_card_item.dart';

class ScoreCardPointsData extends StatelessWidget {
  const ScoreCardPointsData({required this.state, super.key});

  final DisplayingCurrentScore state;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ScoreCardItem(
              title: 'Gesamtpunkte',
              value: state.totalPointsDisplay,
              color: Colors.blue,
            ),
            ScoreCardItem(
              title: 'Richtig',
              value: state.correctDisplay,
              color: const Color.fromRGBO(0, 161, 96, 1),
            ),
          ],
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ScoreCardItem(
              title: 'Maximalpunkte',
              value: state.potentialHighscoreDisplay,
              color: Colors.black,
            ),
            ScoreCardItem(
              title: 'Verlustpunkte',
              value: state.lostPointsDisplay,
              color: const Color.fromRGBO(238, 58, 65, 1),
            ),
          ],
        ),
      ],
    );
  }
}
