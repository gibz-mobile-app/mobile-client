import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';
import 'package:url_launcher/url_launcher.dart';

class StudentsManualAttachment extends StatelessWidget {
  final Attachment attachment;

  const StudentsManualAttachment(this.attachment, {super.key});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        attachment.title,
        style: Theme.of(context).textTheme.labelLarge!.copyWith(
              color: Theme.of(context).primaryColor,
            ),
      ),
      leading: Icon(
        _getAttachmentIcon(),
        color: Theme.of(context).colorScheme.primary,
      ),
      onTap: () {
        launchUrl(Uri.parse(attachment.url));
      },
    );
  }

  IconData _getAttachmentIcon() {
    IconData icon;
    switch (attachment.runtimeType) {
      case Video _:
        icon = Icons.smart_display;
      case PdfDocument _:
        icon = Icons.description;
      default:
        icon = Icons.open_in_new;
    }

    return icon;
  }
}
