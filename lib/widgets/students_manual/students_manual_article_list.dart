import 'package:flutter/widgets.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';
import 'package:gibz_mobileapp/widgets/students_manual/students_manual_article_list_item.dart';
import 'package:gibz_mobileapp/widgets/students_manual/students_manual_list_sticky_header.dart';

class StudentsManualArticleList extends StatelessWidget {
  const StudentsManualArticleList({
    super.key,
    required this.indexLetter,
    required this.articles,
  });

  final String indexLetter;
  final List<Article> articles;

  @override
  Widget build(BuildContext context) {
    return SliverStickyHeader(
      overlapsContent: true,
      header: StudentsManualListStickyHeader(
        headerChar: indexLetter,
      ),
      sliver: SliverPadding(
        padding: const EdgeInsets.only(left: 50),
        sliver: SliverList.list(
          children: [
            ...articles.map(
                (article) => StudentsManualArticleListItem(article: article))
          ],
        ),
      ),
    );
  }
}
