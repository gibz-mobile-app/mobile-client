import 'package:flutter/material.dart';
import 'package:gibz_mobileapp/models/students_manual.dart';
import 'package:go_router/go_router.dart';

class StudentsManualArticleListItem extends StatelessWidget {
  const StudentsManualArticleListItem({super.key, required this.article});

  final Article article;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(
        article.title,
        style: DefaultTextStyle.of(context).style.copyWith(
              fontSize: 15,
              fontWeight: FontWeight.w500,
            ),
      ),
      visualDensity: VisualDensity.compact,
      trailing: const Icon(
        Icons.chevron_right,
        size: 15,
      ),
      onTap: () {
        context.go('/students-manual/${article.id}');
      },
    );
  }
}
