import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class StudentsManuaDetailHeader extends StatelessWidget {
  const StudentsManuaDetailHeader({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      toolbarHeight: 95,
      automaticallyImplyLeading: false,
      backgroundColor: Theme.of(context).colorScheme.primary,
      centerTitle: false,
      pinned: true,
      titleSpacing: 30,
      title: Text(
        title,
        maxLines: 2,
        style: DefaultTextStyle.of(context).style.copyWith(
              color: Colors.white,
              fontSize: 27,
              fontWeight: FontWeight.w600,
              height: 1.3,
            ),
      ),
      bottom: PreferredSize(
          preferredSize: const Size.fromHeight(40),
          child: GestureDetector(
            onTap: () {
              context.pop();
            },
            child: Padding(
              padding: const EdgeInsets.fromLTRB(30, 15, 30, 20),
              child: Row(
                children: [
                  const Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Text(
                      'zurück',
                      style: DefaultTextStyle.of(context).style.copyWith(
                            color: Colors.white,
                            fontSize: 16,
                          ),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
