import 'package:flutter/material.dart';

class AvailabilityIndicatorBar extends StatelessWidget {
  const AvailabilityIndicatorBar({
    required this.currentAvailability,
    required this.maximumAvailability,
    super.key,
  });

  final int currentAvailability;
  final int maximumAvailability;

  @override
  Widget build(BuildContext context) {
    final percentage = currentAvailability / maximumAvailability;
    return LayoutBuilder(
      builder: (context, constraints) {
        final maxWidth = constraints.maxWidth;
        const double bubbleWidth = 37;
        const double bubbleBorder = 4;
        return Stack(
          children: [
            Container(
              height: 35,
              decoration: BoxDecoration(
                color: const Color.fromRGBO(207, 212, 222, 1),
                borderRadius: BorderRadius.circular(20),
              ),
            ),
            Positioned(
              left: 0,
              child: Container(
                width: (bubbleWidth) + ((maxWidth - bubbleWidth) * percentage),
                height: 35,
                padding: const EdgeInsets.symmetric(horizontal: bubbleBorder),
                decoration: BoxDecoration(
                  color: Theme.of(context).colorScheme.primary,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    width: bubbleWidth - (2 * bubbleBorder),
                    height: bubbleWidth - (2 * bubbleBorder),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Center(
                      child: Text(
                        '$currentAvailability',
                        style: Theme.of(context).textTheme.labelLarge!.copyWith(
                              fontWeight: FontWeight.w700,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
