import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/data/repositories/parking_information/car_park_repository.dart';

class OpeningHoursCard extends StatelessWidget {
  const OpeningHoursCard({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: context.read<CarParkRepository>().getCurrentAvailability(),
      builder: (context, snapshot) {
        if (snapshot.hasError || !snapshot.hasData) {
          return const SizedBox.shrink();
        }

        final openingHours = snapshot.data!.formattedOpeningHours;
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          padding: const EdgeInsets.all(15),
          child: (snapshot.connectionState != ConnectionState.done)
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  children: [
                    ...openingHours.entries.map((row) => Padding(
                          padding: const EdgeInsets.symmetric(vertical: 1),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                row.key,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(
                                        color: row.value.toLowerCase() ==
                                                'geschlossen'
                                            ? Colors.red
                                            : Theme.of(context)
                                                .colorScheme
                                                .tertiary),
                              ),
                              Text(
                                row.value,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyMedium!
                                    .copyWith(
                                        color: row.value.toLowerCase() ==
                                                'geschlossen'
                                            ? Colors.red
                                            : Theme.of(context)
                                                .colorScheme
                                                .tertiary),
                              ),
                            ],
                          ),
                        )),
                  ],
                ),
        );
      },
    );
  }
}
