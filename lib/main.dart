import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gibz_mobileapp/models/initialize_mappers.dart';
import 'package:gibz_mobileapp/routing.dart';
import 'package:gibz_mobileapp/widgets/gibz_repository_provider.dart';
import 'package:google_fonts/google_fonts.dart';

import 'state_management/progress_indicator_cubit.dart';

void main() {
  initializeMappers();
  runApp(
    BlocProvider(
      create: (_) => ProgressIndicatorCubit(),
      child: const GibzRepositoryProvider(child: GibzApp()),
    ),
  );
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
}

class GibzApp extends StatelessWidget {
  const GibzApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'GIBZ App',
      routerConfig: router,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: const Color.fromRGBO(0, 119, 192, 1),
          primary: const Color.fromRGBO(0, 119, 192, 1), // #0077C0
          secondary: const Color.fromRGBO(207, 212, 222, 1), // #CFD4DE
          tertiary: const Color.fromRGBO(47, 63, 92, 1),
          surface: const Color.fromRGBO(238, 240, 244, 1),
        ),
        useMaterial3: true,
        textTheme:
            GoogleFonts.interTextTheme(Theme.of(context).textTheme).copyWith(
          headlineLarge: const TextStyle(
            fontSize: 28,
            fontWeight: FontWeight.w600, //ok
          ),
          headlineMedium: const TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.normal, //ok
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
          headlineSmall: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w500, // ok
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
          titleLarge: const TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.w600, // ok
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
          titleMedium: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w600,
          ),
          titleSmall: const TextStyle(
            fontSize: 13,
            fontWeight: FontWeight.w600, // ok
          ),
          bodyMedium: const TextStyle(
            color: Color.fromRGBO(47, 63, 92, 1),
            fontWeight: FontWeight.w400, // ok
          ),
          bodySmall: const TextStyle(
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            foregroundColor: WidgetStateProperty.resolveWith<Color?>((states) {
              if (states.contains(WidgetState.disabled)) {
                return null;
              }
              return Colors.white;
            }),
            backgroundColor: WidgetStateProperty.resolveWith<Color?>((states) {
              if (states.contains(WidgetState.disabled)) {
                return null;
              }
              return const Color.fromRGBO(0, 119, 192, 1);
            }),
            textStyle: WidgetStateProperty.all<TextStyle>(
              const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        ),
        textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
            overlayColor: WidgetStateColor.resolveWith(
              (states) => Colors.transparent,
            ),
            foregroundColor: WidgetStateColor.resolveWith((states) {
              if (states.contains(WidgetState.pressed) ||
                  states.contains(WidgetState.selected)) {
                return const Color.fromRGBO(0, 119, 192, 1).withAlpha(100);
              }
              return const Color.fromRGBO(0, 119, 192, 1);
            }),
            textStyle: WidgetStateProperty.all(
              const TextStyle(fontWeight: FontWeight.normal),
            ),
          ),
        ),
        outlinedButtonTheme: OutlinedButtonThemeData(style:
            ButtonStyle(side: WidgetStateBorderSide.resolveWith((states) {
          return const BorderSide(color: Color.fromRGBO(0, 119, 192, 1));
        }))),
        inputDecorationTheme: InputDecorationTheme(
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(300),
            borderSide: BorderSide.none,
          ),
          isDense: true,
          hintStyle: const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.normal,
            color: Color.fromRGBO(47, 63, 92, 1),
          ),
          contentPadding: const EdgeInsets.fromLTRB(20, 10, 10, 10),
          suffixIconColor: const Color.fromRGBO(131, 143, 166, 1),
        ),
      ),
    );
  }
}
