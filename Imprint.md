Die GIBZ App ist für iOS und Android in den jeweiligen Stores gratis zum Download verfügbar.

Die Inhalte und Informationen stammen aus verschiedenen, öffentlich zugänglichen Quellen. Das GIBZ als Betreiberin haftet nicht für Aktualität und Korrektheit der Daten.

## Betrieb

Gewerblich-industrielles Bildungszentrum Zug  
Baarerstrasse 100  
Postfach  
6301 Zug  
[https://www.gibz.ch](https://www.gibz.ch)  
[app@gibz.ch](app@gibz.ch)  
041 728 30 30

## Idee, Konzept, Realisierung

Peter Gisler

## UI Design

Codelance GmbH

## Entwicklung

Das Backend zur Speicherung, Aufbereitung und Auslieferung verschiedener Daten, ein Web-Frontend zur Verwaltung der Inhalte sowie die mobile Applikation für iOS und Android werden unter der Leitung von Peter Gisler mit ausgewählten Lernenden und Klassen des GIBZ im Rahmen des regulären Berufskundeunterrichts für Informatik mit Fachrichtung Applikationsentwicklung oder in spezifischen Freifächern entwickelt.

In den nachfolgenden Abschnitten sind einige Lernende des GIBZ aufgeführt, welche einen substantiellen Beitrag zur produktiv genutzten GIBZ App beigetragen haben.

### Handbuch für Lernende

Alina Meyer

### Menüplan ZFV

Annika Christen und weitere Lernende

### Parking Informationen

André Schreiber und weitere Lernende

### Portraitfoto

Alexandra Serbina, Elona Dani, David Schneider, Deniz Gözütok, Philipp Würsch, Loris Janner, Alessio Seeberger, Tim Arnold, Luis Leuppi, Riccardo Zinniker

### GIBZ Rallye
Joël Trunk und Andri Spindler (beide im Teilprojekt _Kabelsalat_)

## Datenschutz

Die GIBZ App erhebt oder speichert ausschliesslich personenbezogene Daten, welche für den Betrieb der App notwendig sind und deren Nutzung die Anwenderinnen und Anwender zuvor zugestimmt haben.

Die detaillierte [Datenschutzerklärung](http://gibz-app.ch/static/privacy-policy.html) ist online einsehbar.

In der aktuellen Version werden mit dem Einverständnis der Nutzerinnen und Nutzer folgende Daten erhoben:

### Physischer Standort des Geräts

Während der Nutzung der GIBZ Rallye wird mittels Bluetooth Beacons der physische Standort des Geräts ermittelt. Dieser Standort wird lediglich für die Basisfunktionalität der GIBZ Rallye gespeichert. Bei der Nutzung anderer Funktionen der GIBZ App werden keine Standortdaten erhoben.

### Portraitfoto

Die GIBZ App kann zur Erstellung des persönlichen Portraitfotos am GIBZ genutzt werden. Dieses Foto wird nach der Erstellung und Überprüfung durch die Nutzerin bzw. den Nutzer an den Server der GIBZ App übermittelt. Die Verarbeitung des übermittelten Fotos erfolgt gemäss den Ausführungen der [Datenschutzerklärung](http://gibz-app.ch/static/privacy-policy.html) und im Einklang mit dem Schweizer Datenschutzgesetz.
